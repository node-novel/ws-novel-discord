# README

    執行切割腳本

txt 檔案放置於 [data](../../data)

## 指令

> 初次安裝或者每次想更新模組時

```
yarn run do-init
```

> 切割 [from-6757](../../data/src/from-6757) 底下的 txt 原檔

```
npm run do-split-from-6757
```

> 切割 [from-other](../../data/src/from-other) 底下的 txt 原檔

```
npm run do-split-from-other
```

## 其他

- 切割規則放置於 [rule](rule) 資料夾底下
- 非持續性使用的規則放置於 [temp](rule/temp)  
  _例如只切一次就不會再繼續用的規則_
- 腳本規則搜尋時，會優先以與 txt 來原檔案同名的規則優先  
  _如有相同小說但不同規則請自行再檔名後方加字_
- 基礎通用型規則放置於 [base](rule/base)
- 每次執行切割腳本時，會清除目標資料夾底下的內容，請善用 git 來做比對檢查變化
- 
