/**
 * Created by user on 2019/5/2.
 */
/**
 * 在這裡設定一些 與 txt 檔案名稱不同時的規則對應表
 */
export declare const rule_match: {
    [basename: string]: string;
};
declare const _default: typeof import("./setting.conf");
export default _default;
