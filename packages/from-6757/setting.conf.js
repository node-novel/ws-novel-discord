"use strict";
/**
 * Created by user on 2019/5/2.
 */
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * 在這裡設定一些 與 txt 檔案名稱不同時的規則對應表
 */
exports.rule_match = ((data) => {
    try {
        /**
         * 如果存在 setting.conf.local 檔案則將資料合併
         */
        let local = require('./setting.conf.local');
        Object.keys(local.rule_match)
            .filter(k => [
            '__esModule',
            'default',
            'rule_match',
        ].includes(k))
            .forEach(k => {
            data[k] = local.rule_match[k];
        });
    }
    catch (e) {
    }
    return data;
})({
    '双刃的伊莉娜': 'web_xx',
    '双刃的伊莉娜v2': '章_話',
    '通过扭蛋增加同伴': '000',
    '自称贤者弟子的贤者': '話_000',
    '转生贵族的异世界冒险录': '章2_話',
    '轉生為村民的我作為最強的“武器使”每天盡情地『殺戮與掠奪』': '話_000',
    '等級1最強賢者cm': 'cm001',
});
exports.default = exports;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2V0dGluZy5jb25mLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsic2V0dGluZy5jb25mLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQTs7R0FFRzs7QUFFSDs7R0FFRztBQUNVLFFBQUEsVUFBVSxHQUFHLENBQUMsQ0FBQyxJQUUzQixFQUFFLEVBQUU7SUFHSixJQUNBO1FBQ0M7O1dBRUc7UUFDSCxJQUFJLEtBQUssR0FBRyxPQUFPLENBQUMsc0JBQXNCLENBQUMsQ0FBQztRQUU1QyxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUM7YUFDM0IsTUFBTSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7WUFDWixZQUFZO1lBQ1osU0FBUztZQUNULFlBQVk7U0FDWixDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQzthQUNiLE9BQU8sQ0FBQyxDQUFDLENBQUMsRUFBRTtZQUVaLElBQUksQ0FBQyxDQUFDLENBQUMsR0FBRyxLQUFLLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQy9CLENBQUMsQ0FBQyxDQUNGO0tBQ0Q7SUFDRCxPQUFPLENBQUMsRUFDUjtLQUVDO0lBRUQsT0FBTyxJQUFJLENBQUM7QUFDYixDQUFDLENBQUMsQ0FBQztJQUNGLFFBQVEsRUFBRSxRQUFRO0lBQ2xCLFVBQVUsRUFBRSxLQUFLO0lBQ2pCLFVBQVUsRUFBRSxLQUFLO0lBQ2pCLFdBQVcsRUFBRSxPQUFPO0lBQ3BCLGFBQWEsRUFBRSxNQUFNO0lBQ3JCLCtCQUErQixFQUFFLE9BQU87SUFDeEMsV0FBVyxFQUFFLE9BQU87Q0FDcEIsQ0FBQyxDQUFDO0FBRUgsa0JBQWUsT0FBMEMsQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogQ3JlYXRlZCBieSB1c2VyIG9uIDIwMTkvNS8yLlxuICovXG5cbi8qKlxuICog5Zyo6YCZ6KOh6Kit5a6a5LiA5LqbIOiIhyB0eHQg5qqU5qGI5ZCN56ix5LiN5ZCM5pmC55qE6KaP5YmH5bCN5oeJ6KGoXG4gKi9cbmV4cG9ydCBjb25zdCBydWxlX21hdGNoID0gKChkYXRhOiB7XG5cdFtiYXNlbmFtZTogc3RyaW5nXTogc3RyaW5nLFxufSkgPT5cbntcblxuXHR0cnlcblx0e1xuXHRcdC8qKlxuXHRcdCAqIOWmguaenOWtmOWcqCBzZXR0aW5nLmNvbmYubG9jYWwg5qqU5qGI5YmH5bCH6LOH5paZ5ZCI5L21XG5cdFx0ICovXG5cdFx0bGV0IGxvY2FsID0gcmVxdWlyZSgnLi9zZXR0aW5nLmNvbmYubG9jYWwnKTtcblxuXHRcdE9iamVjdC5rZXlzKGxvY2FsLnJ1bGVfbWF0Y2gpXG5cdFx0XHQuZmlsdGVyKGsgPT4gW1xuXHRcdFx0XHQnX19lc01vZHVsZScsXG5cdFx0XHRcdCdkZWZhdWx0Jyxcblx0XHRcdFx0J3J1bGVfbWF0Y2gnLFxuXHRcdFx0XS5pbmNsdWRlcyhrKSlcblx0XHRcdC5mb3JFYWNoKGsgPT5cblx0XHRcdHtcblx0XHRcdFx0ZGF0YVtrXSA9IGxvY2FsLnJ1bGVfbWF0Y2hba107XG5cdFx0XHR9KVxuXHRcdDtcblx0fVxuXHRjYXRjaCAoZSlcblx0e1xuXG5cdH1cblxuXHRyZXR1cm4gZGF0YTtcbn0pKHtcblx0J+WPjOWIg+eahOS8iuiOieWonCc6ICd3ZWJfeHgnLFxuXHQn5Y+M5YiD55qE5LyK6I6J5aicdjInOiAn56ugX+ipsScsXG5cdCfpgJrov4fmia3om4vlop7liqDlkIzkvLQnOiAnMDAwJyxcblx0J+iHquensOi0pOiAheW8n+WtkOeahOi0pOiAhSc6ICfoqbFfMDAwJyxcblx0J+i9rOeUn+i0teaXj+eahOW8guS4lueVjOWGkumZqeW9lSc6ICfnq6AyX+ipsScsXG5cdCfovYnnlJ/ngrrmnZHmsJHnmoTmiJHkvZzngrrmnIDlvLfnmoTigJzmrablmajkvb/igJ3mr4/lpKnnm6Hmg4XlnLDjgI7mrrrmiK7oiIfmjqDlparjgI8nOiAn6KmxXzAwMCcsXG5cdCfnrYnntJox5pyA5by36LOi6ICFY20nOiAnY20wMDEnLFxufSk7XG5cbmV4cG9ydCBkZWZhdWx0IGV4cG9ydHMgYXMgdHlwZW9mIGltcG9ydCgnLi9zZXR0aW5nLmNvbmYnKTtcbiJdfQ==