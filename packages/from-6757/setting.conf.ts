/**
 * Created by user on 2019/5/2.
 */

/**
 * 在這裡設定一些 與 txt 檔案名稱不同時的規則對應表
 */
export const rule_match = ((data: {
	[basename: string]: string,
}) =>
{

	try
	{
		/**
		 * 如果存在 setting.conf.local 檔案則將資料合併
		 */
		let local = require('./setting.conf.local');

		Object.keys(local.rule_match)
			.filter(k => [
				'__esModule',
				'default',
				'rule_match',
			].includes(k))
			.forEach(k =>
			{
				data[k] = local.rule_match[k];
			})
		;
	}
	catch (e)
	{

	}

	return data;
})({
	'双刃的伊莉娜': 'web_xx',
	'双刃的伊莉娜v2': '章_話',
	'通过扭蛋增加同伴': '000',
	'自称贤者弟子的贤者': '話_000',
	'转生贵族的异世界冒险录': '章2_話',
	'轉生為村民的我作為最強的“武器使”每天盡情地『殺戮與掠奪』': '話_000',
	'等級1最強賢者cm': 'cm001',
});

export default exports as typeof import('./setting.conf');
