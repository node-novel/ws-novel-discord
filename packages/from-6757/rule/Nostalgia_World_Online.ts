/**
 * Created by user on 2019/4/14.
 */

import { _handleOptions, makeOptions } from '@node-novel/txt-split/lib/index';
import {
	IOptions,
	IOptionsRequired,
	IOptionsRequiredUser,
	IDataVolume,
	IDataChapter,
	ISplitCBParameters,
} from '@node-novel/txt-split/lib/interface';

import { console } from '@node-novel/txt-split/lib/console';
import tplBaseOptions from './base/章_話';
import {
	baseCbParseChapterMatch001,
	baseCbParseChapterMatch003,
	baseCbParseChapterMatchMain001,
	createCbParseChapterMatch, ICreateCbParseChapterMatchOptionsSub,
} from './lib/rule';
import StrUtil = require('str-util');

let v_line = `☠☠☠♛☠☠☠☠☠☠♛☠☠☠♛☠☠☠`;
let c_line = `❄✡✥✩✧✫✪✭☆★✬✰✮✦⟡✯☸✠❇✣`;

let _space = ' 　\\t';
const c = '　';

export const tplOptions: IOptionsRequiredUser = {

	...tplBaseOptions,

	volume: {

		...tplBaseOptions.volume,

		//ignoreRe: /第[\d０-９]+章\s+[^\n]+之畫家/i,

		r: [
			`^`,
			'(',
			[
				`序章`,
				`第[一-十]+章`,
				`第[\\d０-９]+章`,
			].join('|'),
			')',
			`[${_space}\\-]*`,
			`([^\\n]*)`,
			`[${_space}]*`,
			`$`,
		],

		ignoreCb(argv)
		{

			//console.dir(argv.m_last.sub);

			if (0 && /^第\s*[\d０-９]+\s*章$/.test(argv.m_last.sub[0]))
			{
				if (!argv.m_last.sub[1])
				{
					return true;
				}
				else
				{
					//console.dir(argv.m_last.sub);
				}
			}
			else
			{
				//console.dir(argv.m_last.sub);
			}

			return false
		},

	},

	/**
	 * 這個參數是必填選項
	 */
	chapter: {

		...tplBaseOptions.chapter,

		r: [
			`^`,
			'(',
			[
				'序章',
				'(?:幕間|終章)',
				`第[\\d０-９]+(?:\.[\\d０-９]+)?(?:話)`,
				`\\d+—\\d+`
			].join('|'),
			')',
			`(?:`,
			`[${_space}\\.]+`,
			`([^\\n]*)`,
			')?',
			`[${_space}]*`,
			`$`,
		],

		cb: baseCbParseChapterMatchMain001([
			createCbParseChapterMatch({

				handleMatchSubID(argv: ICreateCbParseChapterMatchOptionsSub, _data: ISplitCBParameters)
				{
					if (/^\d+(?:[—-]\d+)$/.test(argv.id))
					{

						argv.id = String(argv.id)
							.replace(/(?<=\d+)[—-](\d+)$/, function (c, s)
							{
								return '-' + s.padStart(2, '0');
							})
						;

						argv.idn = StrUtil.toHalfNumber(argv.id.toString());
					}

					return argv;
				},

			}),
		]),

	},

};

export default tplOptions
