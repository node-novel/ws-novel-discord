"use strict";
/**
 * Created by user on 2019/4/14.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const ___1 = require("./base/\u7AE0_\u8A71");
const rule_1 = require("./lib/rule");
const StrUtil = require("str-util");
let v_line = `☠☠☠♛☠☠☠☠☠☠♛☠☠☠♛☠☠☠`;
let c_line = `❄✡✥✩✧✫✪✭☆★✬✰✮✦⟡✯☸✠❇✣`;
let _space = ' 　\\t';
const c = '　';
exports.tplOptions = {
    ...___1.default,
    volume: {
        ...___1.default.volume,
        r: [
            ``,
            '(',
            [
                `第[一-十]+章`,
                `第[\\d０-９]+章`,
            ].join('|'),
            ')',
            `[${_space}]+`,
            `([^\\n]*)`,
            `[${_space}]*`,
            ``,
        ],
    },
    /**
     * 這個參數是必填選項
     */
    chapter: {
        ...___1.default.chapter,
        r: [
            `^`,
            '(',
            [
                '序章',
                '(?:幕間|終章)',
                `第[\\d０-９]+(?:\.[\\d０-９]+)?(?:話)`,
                '(?:特典|閒話)(?=　)',
            ].join('|'),
            ')',
            `(?:`,
            `[${_space}\\.]+`,
            `([^\\n]*)`,
            ')?',
            `[${_space}]*`,
            `$`,
        ],
        cb: rule_1.baseCbParseChapterMatchMain001([
            rule_1.createCbParseChapterMatch({
                handleMatchSubID(argv, _data) {
                    if (/^\d+(?:\.\d+)?$/.test(argv.id)) {
                        argv.id = String(argv.id)
                            .replace(/^\d+/, function (s) {
                            return s.padStart(3, '0');
                        });
                        argv.idn = StrUtil.toHalfNumber(argv.id.toString());
                    }
                    return argv;
                },
            }),
        ]),
    },
};
exports.default = exports.tplOptions;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoi54aK54aK54aKYmVhci5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIueGiueGiueGimJlYXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBOztHQUVHOztBQWFILDZDQUF3QztBQUN4QyxxQ0FLb0I7QUFDcEIsb0NBQXFDO0FBRXJDLElBQUksTUFBTSxHQUFHLG9CQUFvQixDQUFDO0FBQ2xDLElBQUksTUFBTSxHQUFHLHNCQUFzQixDQUFDO0FBRXBDLElBQUksTUFBTSxHQUFHLE9BQU8sQ0FBQztBQUNyQixNQUFNLENBQUMsR0FBRyxHQUFHLENBQUM7QUFFRCxRQUFBLFVBQVUsR0FBeUI7SUFFL0MsR0FBRyxZQUFjO0lBRWpCLE1BQU0sRUFBRTtRQUVQLEdBQUcsWUFBYyxDQUFDLE1BQU07UUFFeEIsQ0FBQyxFQUFFO1lBQ0YsRUFBRTtZQUNGLEdBQUc7WUFDSDtnQkFDQyxVQUFVO2dCQUNWLGFBQWE7YUFDYixDQUFDLElBQUksQ0FBQyxHQUFHLENBQUM7WUFDWCxHQUFHO1lBQ0gsSUFBSSxNQUFNLElBQUk7WUFDZCxXQUFXO1lBQ1gsSUFBSSxNQUFNLElBQUk7WUFDZCxFQUFFO1NBQ0Y7S0FFRDtJQUVEOztPQUVHO0lBQ0gsT0FBTyxFQUFFO1FBRVIsR0FBRyxZQUFjLENBQUMsT0FBTztRQUV6QixDQUFDLEVBQUU7WUFDRixHQUFHO1lBQ0gsR0FBRztZQUNIO2dCQUNDLElBQUk7Z0JBQ0osV0FBVztnQkFDWCxpQ0FBaUM7Z0JBQ2pDLGdCQUFnQjthQUNoQixDQUFDLElBQUksQ0FBQyxHQUFHLENBQUM7WUFDWCxHQUFHO1lBQ0gsS0FBSztZQUNMLElBQUksTUFBTSxPQUFPO1lBQ2pCLFdBQVc7WUFDWCxJQUFJO1lBQ0osSUFBSSxNQUFNLElBQUk7WUFDZCxHQUFHO1NBQ0g7UUFFRCxFQUFFLEVBQUUscUNBQThCLENBQUM7WUFDbEMsZ0NBQXlCLENBQUM7Z0JBRXpCLGdCQUFnQixDQUFDLElBQTBDLEVBQUUsS0FBeUI7b0JBRXJGLElBQUksaUJBQWlCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsRUFDbkM7d0JBRUMsSUFBSSxDQUFDLEVBQUUsR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQzs2QkFDdkIsT0FBTyxDQUFDLE1BQU0sRUFBRSxVQUFVLENBQUM7NEJBRTNCLE9BQU8sQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUM7d0JBQzNCLENBQUMsQ0FBQyxDQUNGO3dCQUVELElBQUksQ0FBQyxHQUFHLEdBQUcsT0FBTyxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUM7cUJBQ3BEO29CQUVELE9BQU8sSUFBSSxDQUFDO2dCQUNiLENBQUM7YUFFRCxDQUFDO1NBQ0YsQ0FBQztLQUVGO0NBRUQsQ0FBQztBQUVGLGtCQUFlLGtCQUFVLENBQUEiLCJzb3VyY2VzQ29udGVudCI6WyIvKipcbiAqIENyZWF0ZWQgYnkgdXNlciBvbiAyMDE5LzQvMTQuXG4gKi9cblxuaW1wb3J0IHsgX2hhbmRsZU9wdGlvbnMsIG1ha2VPcHRpb25zIH0gZnJvbSAnQG5vZGUtbm92ZWwvdHh0LXNwbGl0L2xpYi9pbmRleCc7XG5pbXBvcnQge1xuXHRJT3B0aW9ucyxcblx0SU9wdGlvbnNSZXF1aXJlZCxcblx0SU9wdGlvbnNSZXF1aXJlZFVzZXIsXG5cdElEYXRhVm9sdW1lLFxuXHRJRGF0YUNoYXB0ZXIsXG5cdElTcGxpdENCUGFyYW1ldGVycyxcbn0gZnJvbSAnQG5vZGUtbm92ZWwvdHh0LXNwbGl0L2xpYi9pbnRlcmZhY2UnO1xuXG5pbXBvcnQgeyBjb25zb2xlIH0gZnJvbSAnQG5vZGUtbm92ZWwvdHh0LXNwbGl0L2xpYi9jb25zb2xlJztcbmltcG9ydCB0cGxCYXNlT3B0aW9ucyBmcm9tICcuL2Jhc2Uv56ugX+ipsSc7XG5pbXBvcnQge1xuXHRiYXNlQ2JQYXJzZUNoYXB0ZXJNYXRjaDAwMSxcblx0YmFzZUNiUGFyc2VDaGFwdGVyTWF0Y2gwMDMsXG5cdGJhc2VDYlBhcnNlQ2hhcHRlck1hdGNoTWFpbjAwMSxcblx0Y3JlYXRlQ2JQYXJzZUNoYXB0ZXJNYXRjaCwgSUNyZWF0ZUNiUGFyc2VDaGFwdGVyTWF0Y2hPcHRpb25zU3ViLFxufSBmcm9tICcuL2xpYi9ydWxlJztcbmltcG9ydCBTdHJVdGlsID0gcmVxdWlyZSgnc3RyLXV0aWwnKTtcblxubGV0IHZfbGluZSA9IGDimKDimKDimKDimZvimKDimKDimKDimKDimKDimKDimZvimKDimKDimKDimZvimKDimKDimKBgO1xubGV0IGNfbGluZSA9IGDinYTinKHinKXinKninKfinKvinKrinK3imIbimIXinKzinLDinK7inKbin6HinK/imLjinKDinYfinKNgO1xuXG5sZXQgX3NwYWNlID0gJyDjgIBcXFxcdCc7XG5jb25zdCBjID0gJ+OAgCc7XG5cbmV4cG9ydCBjb25zdCB0cGxPcHRpb25zOiBJT3B0aW9uc1JlcXVpcmVkVXNlciA9IHtcblxuXHQuLi50cGxCYXNlT3B0aW9ucyxcblxuXHR2b2x1bWU6IHtcblxuXHRcdC4uLnRwbEJhc2VPcHRpb25zLnZvbHVtZSxcblxuXHRcdHI6IFtcblx0XHRcdGBgLFxuXHRcdFx0JygnLFxuXHRcdFx0W1xuXHRcdFx0XHRg56ysW+S4gC3ljYFdK+eroGAsXG5cdFx0XHRcdGDnrKxbXFxcXGTvvJAt77yZXSvnq6BgLFxuXHRcdFx0XS5qb2luKCd8JyksXG5cdFx0XHQnKScsXG5cdFx0XHRgWyR7X3NwYWNlfV0rYCxcblx0XHRcdGAoW15cXFxcbl0qKWAsXG5cdFx0XHRgWyR7X3NwYWNlfV0qYCxcblx0XHRcdGBgLFxuXHRcdF0sXG5cblx0fSxcblxuXHQvKipcblx0ICog6YCZ5YCL5Y+D5pW45piv5b+F5aGr6YG46aCFXG5cdCAqL1xuXHRjaGFwdGVyOiB7XG5cblx0XHQuLi50cGxCYXNlT3B0aW9ucy5jaGFwdGVyLFxuXG5cdFx0cjogW1xuXHRcdFx0YF5gLFxuXHRcdFx0JygnLFxuXHRcdFx0W1xuXHRcdFx0XHQn5bqP56ugJyxcblx0XHRcdFx0Jyg/OuW5lemWk3zntYLnq6ApJyxcblx0XHRcdFx0YOesrFtcXFxcZO+8kC3vvJldKyg/OlxcLltcXFxcZO+8kC3vvJldKyk/KD866KmxKWAsXG5cdFx0XHRcdCcoPzrnibnlhbh86ZaS6KmxKSg/PeOAgCknLFxuXHRcdFx0XS5qb2luKCd8JyksXG5cdFx0XHQnKScsXG5cdFx0XHRgKD86YCxcblx0XHRcdGBbJHtfc3BhY2V9XFxcXC5dK2AsXG5cdFx0XHRgKFteXFxcXG5dKilgLFxuXHRcdFx0Jyk/Jyxcblx0XHRcdGBbJHtfc3BhY2V9XSpgLFxuXHRcdFx0YCRgLFxuXHRcdF0sXG5cblx0XHRjYjogYmFzZUNiUGFyc2VDaGFwdGVyTWF0Y2hNYWluMDAxKFtcblx0XHRcdGNyZWF0ZUNiUGFyc2VDaGFwdGVyTWF0Y2goe1xuXG5cdFx0XHRcdGhhbmRsZU1hdGNoU3ViSUQoYXJndjogSUNyZWF0ZUNiUGFyc2VDaGFwdGVyTWF0Y2hPcHRpb25zU3ViLCBfZGF0YTogSVNwbGl0Q0JQYXJhbWV0ZXJzKVxuXHRcdFx0XHR7XG5cdFx0XHRcdFx0aWYgKC9eXFxkKyg/OlxcLlxcZCspPyQvLnRlc3QoYXJndi5pZCkpXG5cdFx0XHRcdFx0e1xuXG5cdFx0XHRcdFx0XHRhcmd2LmlkID0gU3RyaW5nKGFyZ3YuaWQpXG5cdFx0XHRcdFx0XHRcdC5yZXBsYWNlKC9eXFxkKy8sIGZ1bmN0aW9uIChzKVxuXHRcdFx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHRcdFx0cmV0dXJuIHMucGFkU3RhcnQoMywgJzAnKTtcblx0XHRcdFx0XHRcdFx0fSlcblx0XHRcdFx0XHRcdDtcblxuXHRcdFx0XHRcdFx0YXJndi5pZG4gPSBTdHJVdGlsLnRvSGFsZk51bWJlcihhcmd2LmlkLnRvU3RyaW5nKCkpO1xuXHRcdFx0XHRcdH1cblxuXHRcdFx0XHRcdHJldHVybiBhcmd2O1xuXHRcdFx0XHR9LFxuXG5cdFx0XHR9KSxcblx0XHRdKSxcblxuXHR9LFxuXG59O1xuXG5leHBvcnQgZGVmYXVsdCB0cGxPcHRpb25zXG4iXX0=