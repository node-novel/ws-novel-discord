"use strict";
/**
 * Created by user on 2019/4/14.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const console_1 = require("@node-novel/txt-split/lib/console");
const base_1 = require("./lib/base");
const rule_1 = require("./lib/rule");
let v_line = `✟☣ℛ☣✟☣ℛ☣✟☣ℛ☣✟☣ℛ☣✟☣ℛ☣✟☣ℛ`;
let v_line2 = `ℛ☣✟☣ℛ☣✟☣ℛ☣✟☣ℛ☣✟☣ℛ☣✟☣ℛ☣✟`;
let c_line = `🐣🐱🐶🐱🐶🐱🐶🐱🐶🐱🐶🐱🐶🐱🐶🐱🐶🐱🐶🐱🐣`;
let c_line2 = `🐣🐱🐶🐱🐶🐱🐶🐱🐶🐱🐶🐱🐶🐱🐶🐱🐶🐱🐶🐱🐣`;
exports.tplOptions = {
    ...base_1.default,
    /**
     * 這個參數 可刪除或加上 _ 如果沒有用到的話
     */
    volume: {
        /**
         * 故意放一個無效配對 實際使用時請自行更改
         *
         * 當沒有配對到的時候 會自動產生 00000_unknow 資料夾
         */
        r: [
            `${v_line}\n`,
            '([^\\n]+)\n',
            `${v_line2}\n+`,
        ],
        disable: true,
        cb({ 
        /**
         * 於 match 列表中的 index 序列
         */
        i, 
        /**
         * 檔案序列(儲存檔案時會做為前置詞)
         */
        id, 
        /**
         * 標題名稱 預設情況下等於 match 到的標題
         */
        name, 
        /**
         * 本階段的 match 值
         */
        m, 
        /**
         * 上一次的 match 值
         *
         * 但是 實際上 這參數 才是本次 callback 真正的 match 內容
         */
        m_last, 
        /**
         * 目前已經分割的檔案列表與內容
         */
        _files, 
        /**
         * 於所有章節中的序列
         *
         * @readonly
         */
        ii, 
        /**
         * 本次 match 的 內文 start index
         * 可通過修改數值來控制內文範圍
         *
         * @example
         * idx += m_last.match.length; // 內文忽略本次 match 到的標題
         */
        idx, }) {
            /**
             * 依照你給的 regexp 內容來回傳的資料
             */
            if (m_last) {
                let { 
                /**
                 * 配對到的內容
                 */
                match, 
                /**
                 * 配對出來的陣列
                 */
                sub, } = m_last;
                0 && console_1.console.dir({
                    sub,
                    match,
                });
                /**
                 * @todo 這裡可以加上更多語法來格式化標題
                 */
                name = sub[0];
                /**
                 * 將定位點加上本次配對到的內容的長度
                 * 此步驟可以省略
                 * 但使用此步驟時可以同時在切割時對於內容作精簡
                 */
                idx += m_last.match.length;
            }
            return {
                /**
                 * 檔案序列(儲存檔案時會做為前置詞)
                 */
                id,
                /**
                 * 標題名稱 預設情況下等於 match 到的標題
                 */
                name,
                /**
                 * 本次 match 的 內文 start index
                 * 可通過修改數值來控制內文範圍
                 *
                 * @example
                 * idx += m_last.match.length; // 內文忽略本次 match 到的標題
                 */
                idx,
            };
        },
    },
    chapter: {
        ...base_1.default.chapter,
        r: [
            `${c_line}\\n`,
            '(',
            [
                '(?:幕間|終章)',
                `第[\\d０-９]+(?:\.[\\d０-９]+)?(?:話)`,
                `第[一-十]+(?:話)`,
            ].join('|'),
            ')?',
            `(?:`,
            `([^\\n]+?)`,
            ')',
            `\\n${c_line2}\\n`,
        ],
        cb: rule_1.baseCbParseChapterMatchMain001([
            rule_1.baseCbParseChapterMatch005,
        ]),
    },
};
exports.default = exports.tplOptions;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoi54Sh6Ieq6Ka66IGW5aWz55qE5pG45ZG85pG45ZG85peF56iLLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsi54Sh6Ieq6Ka66IGW5aWz55qE5pG45ZG85pG45ZG85peF56iLLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQTs7R0FFRzs7QUFLSCwrREFBNEQ7QUFDNUQscUNBQXdDO0FBQ3hDLHFDQUF3RjtBQUV4RixJQUFJLE1BQU0sR0FBRyx5QkFBeUIsQ0FBQztBQUN2QyxJQUFJLE9BQU8sR0FBRyx5QkFBeUIsQ0FBQztBQUN4QyxJQUFJLE1BQU0sR0FBRyw0Q0FBNEMsQ0FBQztBQUMxRCxJQUFJLE9BQU8sR0FBRyw0Q0FBNEMsQ0FBQztBQUU5QyxRQUFBLFVBQVUsR0FBeUI7SUFFL0MsR0FBRyxjQUFjO0lBRWpCOztPQUVHO0lBQ0gsTUFBTSxFQUFFO1FBQ1A7Ozs7V0FJRztRQUNILENBQUMsRUFBRTtZQUNGLEdBQUcsTUFBTSxJQUFJO1lBQ2IsYUFBYTtZQUNiLEdBQUcsT0FBTyxLQUFLO1NBQ2Y7UUFFRCxPQUFPLEVBQUUsSUFBSTtRQUViLEVBQUUsQ0FBQztRQUNGOztXQUVHO1FBQ0gsQ0FBQztRQUNEOztXQUVHO1FBQ0gsRUFBRTtRQUNGOztXQUVHO1FBQ0gsSUFBSTtRQUNKOztXQUVHO1FBQ0gsQ0FBQztRQUNEOzs7O1dBSUc7UUFDSCxNQUFNO1FBQ047O1dBRUc7UUFDSCxNQUFNO1FBQ047Ozs7V0FJRztRQUNILEVBQUU7UUFDRjs7Ozs7O1dBTUc7UUFDSCxHQUFHLEdBQ0g7WUFFQTs7ZUFFRztZQUNILElBQUksTUFBTSxFQUNWO2dCQUNDLElBQUk7Z0JBQ0g7O21CQUVHO2dCQUNILEtBQUs7Z0JBQ0w7O21CQUVHO2dCQUNILEdBQUcsR0FDSCxHQUFHLE1BQU0sQ0FBQztnQkFFWCxDQUFDLElBQUksaUJBQU8sQ0FBQyxHQUFHLENBQUM7b0JBQ2hCLEdBQUc7b0JBQ0gsS0FBSztpQkFDTCxDQUFDLENBQUM7Z0JBRUg7O21CQUVHO2dCQUNILElBQUksR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBRWQ7Ozs7bUJBSUc7Z0JBQ0gsR0FBRyxJQUFJLE1BQU0sQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDO2FBQzNCO1lBRUQsT0FBTztnQkFDTjs7bUJBRUc7Z0JBQ0gsRUFBRTtnQkFDRjs7bUJBRUc7Z0JBQ0gsSUFBSTtnQkFDSjs7Ozs7O21CQU1HO2dCQUNILEdBQUc7YUFDSCxDQUFBO1FBQ0YsQ0FBQztLQUNEO0lBRUQsT0FBTyxFQUFFO1FBRVIsR0FBRyxjQUFjLENBQUMsT0FBTztRQUV6QixDQUFDLEVBQUU7WUFDRixHQUFHLE1BQU0sS0FBSztZQUNkLEdBQUc7WUFDSDtnQkFDQyxXQUFXO2dCQUNYLGlDQUFpQztnQkFDakMsY0FBYzthQUNkLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQztZQUNYLElBQUk7WUFDSixLQUFLO1lBQ0wsWUFBWTtZQUNaLEdBQUc7WUFDSCxNQUFNLE9BQU8sS0FBSztTQUNsQjtRQUVELEVBQUUsRUFBRSxxQ0FBOEIsQ0FBQztZQUVsQyxpQ0FBMEI7U0FFMUIsQ0FBQztLQUVGO0NBRUQsQ0FBQztBQUVGLGtCQUFlLGtCQUFVLENBQUEiLCJzb3VyY2VzQ29udGVudCI6WyIvKipcbiAqIENyZWF0ZWQgYnkgdXNlciBvbiAyMDE5LzQvMTQuXG4gKi9cblxuaW1wb3J0IHsgX2hhbmRsZU9wdGlvbnMsIG1ha2VPcHRpb25zIH0gZnJvbSAnQG5vZGUtbm92ZWwvdHh0LXNwbGl0L2xpYi9pbmRleCc7XG5pbXBvcnQgeyBJT3B0aW9ucywgSU9wdGlvbnNSZXF1aXJlZCwgSU9wdGlvbnNSZXF1aXJlZFVzZXIsIElEYXRhVm9sdW1lLCBJRGF0YUNoYXB0ZXIgfSBmcm9tICdAbm9kZS1ub3ZlbC90eHQtc3BsaXQvbGliL2ludGVyZmFjZSc7XG5cbmltcG9ydCB7IGNvbnNvbGUgfSBmcm9tICdAbm9kZS1ub3ZlbC90eHQtc3BsaXQvbGliL2NvbnNvbGUnO1xuaW1wb3J0IHRwbEJhc2VPcHRpb25zIGZyb20gJy4vbGliL2Jhc2UnO1xuaW1wb3J0IHsgYmFzZUNiUGFyc2VDaGFwdGVyTWF0Y2gwMDUsIGJhc2VDYlBhcnNlQ2hhcHRlck1hdGNoTWFpbjAwMSB9IGZyb20gJy4vbGliL3J1bGUnO1xuXG5sZXQgdl9saW5lID0gYOKcn+KYo+KEm+KYo+Kcn+KYo+KEm+KYo+Kcn+KYo+KEm+KYo+Kcn+KYo+KEm+KYo+Kcn+KYo+KEm+KYo+Kcn+KYo+KEm2A7XG5sZXQgdl9saW5lMiA9IGDihJvimKPinJ/imKPihJvimKPinJ/imKPihJvimKPinJ/imKPihJvimKPinJ/imKPihJvimKPinJ/imKPihJvimKPinJ9gO1xubGV0IGNfbGluZSA9IGDwn5Cj8J+QsfCfkLbwn5Cx8J+QtvCfkLHwn5C28J+QsfCfkLbwn5Cx8J+QtvCfkLHwn5C28J+QsfCfkLbwn5Cx8J+QtvCfkLHwn5C28J+QsfCfkKNgO1xubGV0IGNfbGluZTIgPSBg8J+Qo/CfkLHwn5C28J+QsfCfkLbwn5Cx8J+QtvCfkLHwn5C28J+QsfCfkLbwn5Cx8J+QtvCfkLHwn5C28J+QsfCfkLbwn5Cx8J+QtvCfkLHwn5CjYDtcblxuZXhwb3J0IGNvbnN0IHRwbE9wdGlvbnM6IElPcHRpb25zUmVxdWlyZWRVc2VyID0ge1xuXG5cdC4uLnRwbEJhc2VPcHRpb25zLFxuXG5cdC8qKlxuXHQgKiDpgJnlgIvlj4Pmlbgg5Y+v5Yiq6Zmk5oiW5Yqg5LiKIF8g5aaC5p6c5rKS5pyJ55So5Yiw55qE6KmxXG5cdCAqL1xuXHR2b2x1bWU6IHtcblx0XHQvKipcblx0XHQgKiDmlYXmhI/mlL7kuIDlgIvnhKHmlYjphY3lsI0g5a+m6Zqb5L2/55So5pmC6KuL6Ieq6KGM5pu05pS5XG5cdFx0ICpcblx0XHQgKiDnlbbmspLmnInphY3lsI3liLDnmoTmmYLlgJkg5pyD6Ieq5YuV55Si55SfIDAwMDAwX3Vua25vdyDos4fmlpnlpL5cblx0XHQgKi9cblx0XHRyOiBbXG5cdFx0XHRgJHt2X2xpbmV9XFxuYCxcblx0XHRcdCcoW15cXFxcbl0rKVxcbicsXG5cdFx0XHRgJHt2X2xpbmUyfVxcbitgLFxuXHRcdF0sXG5cblx0XHRkaXNhYmxlOiB0cnVlLFxuXG5cdFx0Y2Ioe1xuXHRcdFx0LyoqXG5cdFx0XHQgKiDmlrwgbWF0Y2gg5YiX6KGo5Lit55qEIGluZGV4IOW6j+WIl1xuXHRcdFx0ICovXG5cdFx0XHRpLFxuXHRcdFx0LyoqXG5cdFx0XHQgKiDmqpTmoYjluo/liJco5YSy5a2Y5qqU5qGI5pmC5pyD5YGa54K65YmN572u6KmeKVxuXHRcdFx0ICovXG5cdFx0XHRpZCxcblx0XHRcdC8qKlxuXHRcdFx0ICog5qiZ6aGM5ZCN56ixIOmgkOioreaDheazgeS4i+etieaWvCBtYXRjaCDliLDnmoTmqJnpoYxcblx0XHRcdCAqL1xuXHRcdFx0bmFtZSxcblx0XHRcdC8qKlxuXHRcdFx0ICog5pys6ZqO5q6155qEIG1hdGNoIOWAvFxuXHRcdFx0ICovXG5cdFx0XHRtLFxuXHRcdFx0LyoqXG5cdFx0XHQgKiDkuIrkuIDmrKHnmoQgbWF0Y2gg5YC8XG5cdFx0XHQgKlxuXHRcdFx0ICog5L2G5pivIOWvpumam+S4iiDpgJnlj4Pmlbgg5omN5piv5pys5qyhIGNhbGxiYWNrIOecn+ato+eahCBtYXRjaCDlhaflrrlcblx0XHRcdCAqL1xuXHRcdFx0bV9sYXN0LFxuXHRcdFx0LyoqXG5cdFx0XHQgKiDnm67liY3lt7LntpPliIblibLnmoTmqpTmoYjliJfooajoiIflhaflrrlcblx0XHRcdCAqL1xuXHRcdFx0X2ZpbGVzLFxuXHRcdFx0LyoqXG5cdFx0XHQgKiDmlrzmiYDmnInnq6Dnr4DkuK3nmoTluo/liJdcblx0XHRcdCAqXG5cdFx0XHQgKiBAcmVhZG9ubHlcblx0XHRcdCAqL1xuXHRcdFx0aWksXG5cdFx0XHQvKipcblx0XHRcdCAqIOacrOasoSBtYXRjaCDnmoQg5YWn5paHIHN0YXJ0IGluZGV4XG5cdFx0XHQgKiDlj6/pgJrpgY7kv67mlLnmlbjlgLzkvobmjqfliLblhafmlofnr4TlnI1cblx0XHRcdCAqXG5cdFx0XHQgKiBAZXhhbXBsZVxuXHRcdFx0ICogaWR4ICs9IG1fbGFzdC5tYXRjaC5sZW5ndGg7IC8vIOWFp+aWh+W/veeVpeacrOasoSBtYXRjaCDliLDnmoTmqJnpoYxcblx0XHRcdCAqL1xuXHRcdFx0aWR4LFxuXHRcdH0pXG5cdFx0e1xuXHRcdFx0LyoqXG5cdFx0XHQgKiDkvp3nhafkvaDntabnmoQgcmVnZXhwIOWFp+WuueS+huWbnuWCs+eahOizh+aWmVxuXHRcdFx0ICovXG5cdFx0XHRpZiAobV9sYXN0KVxuXHRcdFx0e1xuXHRcdFx0XHRsZXQge1xuXHRcdFx0XHRcdC8qKlxuXHRcdFx0XHRcdCAqIOmFjeWwjeWIsOeahOWFp+WuuVxuXHRcdFx0XHRcdCAqL1xuXHRcdFx0XHRcdG1hdGNoLFxuXHRcdFx0XHRcdC8qKlxuXHRcdFx0XHRcdCAqIOmFjeWwjeWHuuS+hueahOmZo+WIl1xuXHRcdFx0XHRcdCAqL1xuXHRcdFx0XHRcdHN1Yixcblx0XHRcdFx0fSA9IG1fbGFzdDtcblxuXHRcdFx0XHQwICYmIGNvbnNvbGUuZGlyKHtcblx0XHRcdFx0XHRzdWIsXG5cdFx0XHRcdFx0bWF0Y2gsXG5cdFx0XHRcdH0pO1xuXG5cdFx0XHRcdC8qKlxuXHRcdFx0XHQgKiBAdG9kbyDpgJnoo6Hlj6/ku6XliqDkuIrmm7TlpJroqp7ms5XkvobmoLzlvI/ljJbmqJnpoYxcblx0XHRcdFx0ICovXG5cdFx0XHRcdG5hbWUgPSBzdWJbMF07XG5cblx0XHRcdFx0LyoqXG5cdFx0XHRcdCAqIOWwh+WumuS9jem7nuWKoOS4iuacrOasoemFjeWwjeWIsOeahOWFp+WuueeahOmVt+W6plxuXHRcdFx0XHQgKiDmraTmraXpqZ/lj6/ku6XnnIHnlaVcblx0XHRcdFx0ICog5L2G5L2/55So5q2k5q2l6amf5pmC5Y+v5Lul5ZCM5pmC5Zyo5YiH5Ymy5pmC5bCN5pa85YWn5a655L2c57K+57ChXG5cdFx0XHRcdCAqL1xuXHRcdFx0XHRpZHggKz0gbV9sYXN0Lm1hdGNoLmxlbmd0aDtcblx0XHRcdH1cblxuXHRcdFx0cmV0dXJuIHtcblx0XHRcdFx0LyoqXG5cdFx0XHRcdCAqIOaqlOahiOW6j+WIlyjlhLLlrZjmqpTmoYjmmYLmnIPlgZrngrrliY3nva7oqZ4pXG5cdFx0XHRcdCAqL1xuXHRcdFx0XHRpZCxcblx0XHRcdFx0LyoqXG5cdFx0XHRcdCAqIOaomemhjOWQjeeosSDpoJDoqK3mg4Xms4HkuIvnrYnmlrwgbWF0Y2gg5Yiw55qE5qiZ6aGMXG5cdFx0XHRcdCAqL1xuXHRcdFx0XHRuYW1lLFxuXHRcdFx0XHQvKipcblx0XHRcdFx0ICog5pys5qyhIG1hdGNoIOeahCDlhafmlocgc3RhcnQgaW5kZXhcblx0XHRcdFx0ICog5Y+v6YCa6YGO5L+u5pS55pW45YC85L6G5o6n5Yi25YWn5paH56+E5ZyNXG5cdFx0XHRcdCAqXG5cdFx0XHRcdCAqIEBleGFtcGxlXG5cdFx0XHRcdCAqIGlkeCArPSBtX2xhc3QubWF0Y2gubGVuZ3RoOyAvLyDlhafmloflv73nlaXmnKzmrKEgbWF0Y2gg5Yiw55qE5qiZ6aGMXG5cdFx0XHRcdCAqL1xuXHRcdFx0XHRpZHgsXG5cdFx0XHR9XG5cdFx0fSxcblx0fSxcblxuXHRjaGFwdGVyOiB7XG5cblx0XHQuLi50cGxCYXNlT3B0aW9ucy5jaGFwdGVyLFxuXG5cdFx0cjogW1xuXHRcdFx0YCR7Y19saW5lfVxcXFxuYCxcblx0XHRcdCcoJyxcblx0XHRcdFtcblx0XHRcdFx0Jyg/OuW5lemWk3zntYLnq6ApJyxcblx0XHRcdFx0YOesrFtcXFxcZO+8kC3vvJldKyg/OlxcLltcXFxcZO+8kC3vvJldKyk/KD866KmxKWAsXG5cdFx0XHRcdGDnrKxb5LiALeWNgV0rKD866KmxKWAsXG5cdFx0XHRdLmpvaW4oJ3wnKSxcblx0XHRcdCcpPycsXG5cdFx0XHRgKD86YCxcblx0XHRcdGAoW15cXFxcbl0rPylgLFxuXHRcdFx0JyknLFxuXHRcdFx0YFxcXFxuJHtjX2xpbmUyfVxcXFxuYCxcblx0XHRdLFxuXG5cdFx0Y2I6IGJhc2VDYlBhcnNlQ2hhcHRlck1hdGNoTWFpbjAwMShbXG5cblx0XHRcdGJhc2VDYlBhcnNlQ2hhcHRlck1hdGNoMDA1LFxuXG5cdFx0XSksXG5cblx0fSxcblxufTtcblxuZXhwb3J0IGRlZmF1bHQgdHBsT3B0aW9uc1xuIl19