"use strict";
/**
 * Created by user on 2019/4/14.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const ___1 = require("./base/\u7AE0_\u8A71");
const rule_1 = require("./lib/rule");
const StrUtil = require("str-util");
let v_line = `☠☠☠♛☠☠☠☠☠☠♛☠☠☠♛☠☠☠`;
let c_line = `❄✡✥✩✧✫✪✭☆★✬✰✮✦⟡✯☸✠❇✣`;
let _space = ' 　\\t';
const c = '　';
exports.tplOptions = {
    ...___1.default,
    volume: {
        ...___1.default.volume,
        //ignoreRe: /第[\d０-９]+章\s+[^\n]+之畫家/i,
        r: [
            `^`,
            '(',
            [
                `序章`,
                `第[一-十]+章`,
                `第[\\d０-９]+章`,
            ].join('|'),
            ')',
            `[${_space}\\-]*`,
            `([^\\n]*)`,
            `[${_space}]*`,
            `$`,
        ],
        ignoreCb(argv) {
            //console.dir(argv.m_last.sub);
            if (0 && /^第\s*[\d０-９]+\s*章$/.test(argv.m_last.sub[0])) {
                if (!argv.m_last.sub[1]) {
                    return true;
                }
                else {
                    //console.dir(argv.m_last.sub);
                }
            }
            else {
                //console.dir(argv.m_last.sub);
            }
            return false;
        },
    },
    /**
     * 這個參數是必填選項
     */
    chapter: {
        ...___1.default.chapter,
        r: [
            `^`,
            '(',
            [
                '序章',
                '(?:幕間|終章)',
                `第[\\d０-９]+(?:\.[\\d０-９]+)?(?:話)`,
                `\\d+—\\d+`
            ].join('|'),
            ')',
            `(?:`,
            `[${_space}\\.]+`,
            `([^\\n]*)`,
            ')?',
            `[${_space}]*`,
            `$`,
        ],
        cb: rule_1.baseCbParseChapterMatchMain001([
            rule_1.createCbParseChapterMatch({
                handleMatchSubID(argv, _data) {
                    if (/^\d+(?:[—-]\d+)$/.test(argv.id)) {
                        argv.id = String(argv.id)
                            .replace(/(?<=\d+)[—-](\d+)$/, function (c, s) {
                            return '-' + s.padStart(2, '0');
                        });
                        argv.idn = StrUtil.toHalfNumber(argv.id.toString());
                    }
                    return argv;
                },
            }),
        ]),
    },
};
exports.default = exports.tplOptions;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiTm9zdGFsZ2lhX1dvcmxkX09ubGluZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIk5vc3RhbGdpYV9Xb3JsZF9PbmxpbmUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBOztHQUVHOztBQWFILDZDQUF3QztBQUN4QyxxQ0FLb0I7QUFDcEIsb0NBQXFDO0FBRXJDLElBQUksTUFBTSxHQUFHLG9CQUFvQixDQUFDO0FBQ2xDLElBQUksTUFBTSxHQUFHLHNCQUFzQixDQUFDO0FBRXBDLElBQUksTUFBTSxHQUFHLE9BQU8sQ0FBQztBQUNyQixNQUFNLENBQUMsR0FBRyxHQUFHLENBQUM7QUFFRCxRQUFBLFVBQVUsR0FBeUI7SUFFL0MsR0FBRyxZQUFjO0lBRWpCLE1BQU0sRUFBRTtRQUVQLEdBQUcsWUFBYyxDQUFDLE1BQU07UUFFeEIsc0NBQXNDO1FBRXRDLENBQUMsRUFBRTtZQUNGLEdBQUc7WUFDSCxHQUFHO1lBQ0g7Z0JBQ0MsSUFBSTtnQkFDSixVQUFVO2dCQUNWLGFBQWE7YUFDYixDQUFDLElBQUksQ0FBQyxHQUFHLENBQUM7WUFDWCxHQUFHO1lBQ0gsSUFBSSxNQUFNLE9BQU87WUFDakIsV0FBVztZQUNYLElBQUksTUFBTSxJQUFJO1lBQ2QsR0FBRztTQUNIO1FBRUQsUUFBUSxDQUFDLElBQUk7WUFHWiwrQkFBK0I7WUFFL0IsSUFBSSxDQUFDLElBQUksb0JBQW9CLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQ3REO2dCQUNDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsRUFDdkI7b0JBQ0MsT0FBTyxJQUFJLENBQUM7aUJBQ1o7cUJBRUQ7b0JBQ0MsK0JBQStCO2lCQUMvQjthQUNEO2lCQUVEO2dCQUNDLCtCQUErQjthQUMvQjtZQUVELE9BQU8sS0FBSyxDQUFBO1FBQ2IsQ0FBQztLQUVEO0lBRUQ7O09BRUc7SUFDSCxPQUFPLEVBQUU7UUFFUixHQUFHLFlBQWMsQ0FBQyxPQUFPO1FBRXpCLENBQUMsRUFBRTtZQUNGLEdBQUc7WUFDSCxHQUFHO1lBQ0g7Z0JBQ0MsSUFBSTtnQkFDSixXQUFXO2dCQUNYLGlDQUFpQztnQkFDakMsV0FBVzthQUNYLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQztZQUNYLEdBQUc7WUFDSCxLQUFLO1lBQ0wsSUFBSSxNQUFNLE9BQU87WUFDakIsV0FBVztZQUNYLElBQUk7WUFDSixJQUFJLE1BQU0sSUFBSTtZQUNkLEdBQUc7U0FDSDtRQUVELEVBQUUsRUFBRSxxQ0FBOEIsQ0FBQztZQUNsQyxnQ0FBeUIsQ0FBQztnQkFFekIsZ0JBQWdCLENBQUMsSUFBMEMsRUFBRSxLQUF5QjtvQkFFckYsSUFBSSxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxFQUNwQzt3QkFFQyxJQUFJLENBQUMsRUFBRSxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDOzZCQUN2QixPQUFPLENBQUMsb0JBQW9CLEVBQUUsVUFBVSxDQUFDLEVBQUUsQ0FBQzs0QkFFNUMsT0FBTyxHQUFHLEdBQUcsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUM7d0JBQ2pDLENBQUMsQ0FBQyxDQUNGO3dCQUVELElBQUksQ0FBQyxHQUFHLEdBQUcsT0FBTyxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUM7cUJBQ3BEO29CQUVELE9BQU8sSUFBSSxDQUFDO2dCQUNiLENBQUM7YUFFRCxDQUFDO1NBQ0YsQ0FBQztLQUVGO0NBRUQsQ0FBQztBQUVGLGtCQUFlLGtCQUFVLENBQUEiLCJzb3VyY2VzQ29udGVudCI6WyIvKipcbiAqIENyZWF0ZWQgYnkgdXNlciBvbiAyMDE5LzQvMTQuXG4gKi9cblxuaW1wb3J0IHsgX2hhbmRsZU9wdGlvbnMsIG1ha2VPcHRpb25zIH0gZnJvbSAnQG5vZGUtbm92ZWwvdHh0LXNwbGl0L2xpYi9pbmRleCc7XG5pbXBvcnQge1xuXHRJT3B0aW9ucyxcblx0SU9wdGlvbnNSZXF1aXJlZCxcblx0SU9wdGlvbnNSZXF1aXJlZFVzZXIsXG5cdElEYXRhVm9sdW1lLFxuXHRJRGF0YUNoYXB0ZXIsXG5cdElTcGxpdENCUGFyYW1ldGVycyxcbn0gZnJvbSAnQG5vZGUtbm92ZWwvdHh0LXNwbGl0L2xpYi9pbnRlcmZhY2UnO1xuXG5pbXBvcnQgeyBjb25zb2xlIH0gZnJvbSAnQG5vZGUtbm92ZWwvdHh0LXNwbGl0L2xpYi9jb25zb2xlJztcbmltcG9ydCB0cGxCYXNlT3B0aW9ucyBmcm9tICcuL2Jhc2Uv56ugX+ipsSc7XG5pbXBvcnQge1xuXHRiYXNlQ2JQYXJzZUNoYXB0ZXJNYXRjaDAwMSxcblx0YmFzZUNiUGFyc2VDaGFwdGVyTWF0Y2gwMDMsXG5cdGJhc2VDYlBhcnNlQ2hhcHRlck1hdGNoTWFpbjAwMSxcblx0Y3JlYXRlQ2JQYXJzZUNoYXB0ZXJNYXRjaCwgSUNyZWF0ZUNiUGFyc2VDaGFwdGVyTWF0Y2hPcHRpb25zU3ViLFxufSBmcm9tICcuL2xpYi9ydWxlJztcbmltcG9ydCBTdHJVdGlsID0gcmVxdWlyZSgnc3RyLXV0aWwnKTtcblxubGV0IHZfbGluZSA9IGDimKDimKDimKDimZvimKDimKDimKDimKDimKDimKDimZvimKDimKDimKDimZvimKDimKDimKBgO1xubGV0IGNfbGluZSA9IGDinYTinKHinKXinKninKfinKvinKrinK3imIbimIXinKzinLDinK7inKbin6HinK/imLjinKDinYfinKNgO1xuXG5sZXQgX3NwYWNlID0gJyDjgIBcXFxcdCc7XG5jb25zdCBjID0gJ+OAgCc7XG5cbmV4cG9ydCBjb25zdCB0cGxPcHRpb25zOiBJT3B0aW9uc1JlcXVpcmVkVXNlciA9IHtcblxuXHQuLi50cGxCYXNlT3B0aW9ucyxcblxuXHR2b2x1bWU6IHtcblxuXHRcdC4uLnRwbEJhc2VPcHRpb25zLnZvbHVtZSxcblxuXHRcdC8vaWdub3JlUmU6IC/nrKxbXFxk77yQLe+8mV0r56ugXFxzK1teXFxuXSvkuYvnlavlrrYvaSxcblxuXHRcdHI6IFtcblx0XHRcdGBeYCxcblx0XHRcdCcoJyxcblx0XHRcdFtcblx0XHRcdFx0YOW6j+eroGAsXG5cdFx0XHRcdGDnrKxb5LiALeWNgV0r56ugYCxcblx0XHRcdFx0YOesrFtcXFxcZO+8kC3vvJldK+eroGAsXG5cdFx0XHRdLmpvaW4oJ3wnKSxcblx0XHRcdCcpJyxcblx0XHRcdGBbJHtfc3BhY2V9XFxcXC1dKmAsXG5cdFx0XHRgKFteXFxcXG5dKilgLFxuXHRcdFx0YFske19zcGFjZX1dKmAsXG5cdFx0XHRgJGAsXG5cdFx0XSxcblxuXHRcdGlnbm9yZUNiKGFyZ3YpXG5cdFx0e1xuXG5cdFx0XHQvL2NvbnNvbGUuZGlyKGFyZ3YubV9sYXN0LnN1Yik7XG5cblx0XHRcdGlmICgwICYmIC9e56ysXFxzKltcXGTvvJAt77yZXStcXHMq56ugJC8udGVzdChhcmd2Lm1fbGFzdC5zdWJbMF0pKVxuXHRcdFx0e1xuXHRcdFx0XHRpZiAoIWFyZ3YubV9sYXN0LnN1YlsxXSlcblx0XHRcdFx0e1xuXHRcdFx0XHRcdHJldHVybiB0cnVlO1xuXHRcdFx0XHR9XG5cdFx0XHRcdGVsc2Vcblx0XHRcdFx0e1xuXHRcdFx0XHRcdC8vY29uc29sZS5kaXIoYXJndi5tX2xhc3Quc3ViKTtcblx0XHRcdFx0fVxuXHRcdFx0fVxuXHRcdFx0ZWxzZVxuXHRcdFx0e1xuXHRcdFx0XHQvL2NvbnNvbGUuZGlyKGFyZ3YubV9sYXN0LnN1Yik7XG5cdFx0XHR9XG5cblx0XHRcdHJldHVybiBmYWxzZVxuXHRcdH0sXG5cblx0fSxcblxuXHQvKipcblx0ICog6YCZ5YCL5Y+D5pW45piv5b+F5aGr6YG46aCFXG5cdCAqL1xuXHRjaGFwdGVyOiB7XG5cblx0XHQuLi50cGxCYXNlT3B0aW9ucy5jaGFwdGVyLFxuXG5cdFx0cjogW1xuXHRcdFx0YF5gLFxuXHRcdFx0JygnLFxuXHRcdFx0W1xuXHRcdFx0XHQn5bqP56ugJyxcblx0XHRcdFx0Jyg/OuW5lemWk3zntYLnq6ApJyxcblx0XHRcdFx0YOesrFtcXFxcZO+8kC3vvJldKyg/OlxcLltcXFxcZO+8kC3vvJldKyk/KD866KmxKWAsXG5cdFx0XHRcdGBcXFxcZCvigJRcXFxcZCtgXG5cdFx0XHRdLmpvaW4oJ3wnKSxcblx0XHRcdCcpJyxcblx0XHRcdGAoPzpgLFxuXHRcdFx0YFske19zcGFjZX1cXFxcLl0rYCxcblx0XHRcdGAoW15cXFxcbl0qKWAsXG5cdFx0XHQnKT8nLFxuXHRcdFx0YFske19zcGFjZX1dKmAsXG5cdFx0XHRgJGAsXG5cdFx0XSxcblxuXHRcdGNiOiBiYXNlQ2JQYXJzZUNoYXB0ZXJNYXRjaE1haW4wMDEoW1xuXHRcdFx0Y3JlYXRlQ2JQYXJzZUNoYXB0ZXJNYXRjaCh7XG5cblx0XHRcdFx0aGFuZGxlTWF0Y2hTdWJJRChhcmd2OiBJQ3JlYXRlQ2JQYXJzZUNoYXB0ZXJNYXRjaE9wdGlvbnNTdWIsIF9kYXRhOiBJU3BsaXRDQlBhcmFtZXRlcnMpXG5cdFx0XHRcdHtcblx0XHRcdFx0XHRpZiAoL15cXGQrKD86W+KAlC1dXFxkKykkLy50ZXN0KGFyZ3YuaWQpKVxuXHRcdFx0XHRcdHtcblxuXHRcdFx0XHRcdFx0YXJndi5pZCA9IFN0cmluZyhhcmd2LmlkKVxuXHRcdFx0XHRcdFx0XHQucmVwbGFjZSgvKD88PVxcZCspW+KAlC1dKFxcZCspJC8sIGZ1bmN0aW9uIChjLCBzKVxuXHRcdFx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHRcdFx0cmV0dXJuICctJyArIHMucGFkU3RhcnQoMiwgJzAnKTtcblx0XHRcdFx0XHRcdFx0fSlcblx0XHRcdFx0XHRcdDtcblxuXHRcdFx0XHRcdFx0YXJndi5pZG4gPSBTdHJVdGlsLnRvSGFsZk51bWJlcihhcmd2LmlkLnRvU3RyaW5nKCkpO1xuXHRcdFx0XHRcdH1cblxuXHRcdFx0XHRcdHJldHVybiBhcmd2O1xuXHRcdFx0XHR9LFxuXG5cdFx0XHR9KSxcblx0XHRdKSxcblxuXHR9LFxuXG59O1xuXG5leHBvcnQgZGVmYXVsdCB0cGxPcHRpb25zXG4iXX0=