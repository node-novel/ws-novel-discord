"use strict";
/**
 * Created by user on 2019/4/14.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const ___1 = require("./base/\u7AE0_\u8A71");
const rule_1 = require("./lib/rule");
let v_line = `☠☠☠♛☠☠☠☠☠☠♛☠☠☠♛☠☠☠`;
let c_line = `❄✡✥✩✧✫✪✭☆★✬✰✮✦⟡✯☸✠❇✣`;
let _space = ' 　\\t';
const c = '　';
exports.tplOptions = {
    ...___1.default,
    volume: {
        ...___1.default.volume,
        r: [
            `(?<=。|\\n|^)`,
            '(',
            [
                `第[一-十]+(?:章|部)`,
                `第[\\d０-９]+(?:章|部)`,
                `外傳`,
            ].join('|'),
            ')',
            `[${_space}]*`,
            `([^\\n]*)`,
            `[${_space}]*`,
            ``,
        ],
    },
    /**
     * 這個參數是必填選項
     */
    chapter: {
        ...___1.default.chapter,
        r: [
            `^`,
            '(',
            [
                '序章',
                '(?:幕間|終章)',
                `第[\\d０-９]+(?:\.[\\d０-９]+)?(?:話)`,
                `安娜的室友【前篇】`,
                `安娜的室友【後篇】`,
                `安娜的室友【中篇】`,
            ].join('|'),
            ')',
            `(?:(?:`,
            `[${_space}\\.]+`,
            `([^\\n]*)`,
            ')?',
            `[${_space}]*)?`,
            `$`,
        ],
        cb: rule_1.baseCbParseChapterMatchMain001([
            rule_1.baseCbParseChapterMatch003,
        ]),
    },
};
exports.default = exports.tplOptions;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoi5Lul5YmR5aOr5Li655uu5qCH5YWl5a2m5L2G6a2U5rOV6YCC5oCn5Y205pyJOTk5OS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIuS7peWJkeWjq+S4uuebruagh+WFpeWtpuS9humtlOazlemAguaAp+WNtOaciTk5OTkudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBOztHQUVHOztBQWFILDZDQUF3QztBQUN4QyxxQ0FLb0I7QUFHcEIsSUFBSSxNQUFNLEdBQUcsb0JBQW9CLENBQUM7QUFDbEMsSUFBSSxNQUFNLEdBQUcsc0JBQXNCLENBQUM7QUFFcEMsSUFBSSxNQUFNLEdBQUcsT0FBTyxDQUFDO0FBQ3JCLE1BQU0sQ0FBQyxHQUFHLEdBQUcsQ0FBQztBQUVELFFBQUEsVUFBVSxHQUF5QjtJQUUvQyxHQUFHLFlBQWM7SUFFakIsTUFBTSxFQUFFO1FBRVAsR0FBRyxZQUFjLENBQUMsTUFBTTtRQUV4QixDQUFDLEVBQUU7WUFDRixjQUFjO1lBQ2QsR0FBRztZQUNIO2dCQUNDLGdCQUFnQjtnQkFDaEIsbUJBQW1CO2dCQUNuQixJQUFJO2FBQ0osQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDO1lBQ1gsR0FBRztZQUNILElBQUksTUFBTSxJQUFJO1lBQ2QsV0FBVztZQUNYLElBQUksTUFBTSxJQUFJO1lBQ2QsRUFBRTtTQUNGO0tBQ0Q7SUFFRDs7T0FFRztJQUNILE9BQU8sRUFBRTtRQUVSLEdBQUcsWUFBYyxDQUFDLE9BQU87UUFFekIsQ0FBQyxFQUFFO1lBQ0YsR0FBRztZQUNILEdBQUc7WUFDSDtnQkFDQyxJQUFJO2dCQUNKLFdBQVc7Z0JBQ1gsaUNBQWlDO2dCQUNqQyxXQUFXO2dCQUNYLFdBQVc7Z0JBQ1gsV0FBVzthQUNYLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQztZQUNYLEdBQUc7WUFDSCxRQUFRO1lBQ1IsSUFBSSxNQUFNLE9BQU87WUFDakIsV0FBVztZQUNYLElBQUk7WUFDSixJQUFJLE1BQU0sTUFBTTtZQUNoQixHQUFHO1NBQ0g7UUFFRCxFQUFFLEVBQUUscUNBQThCLENBQUM7WUFFbEMsaUNBQTBCO1NBRTFCLENBQUM7S0FFRjtDQUVELENBQUM7QUFFRixrQkFBZSxrQkFBVSxDQUFBIiwic291cmNlc0NvbnRlbnQiOlsiLyoqXG4gKiBDcmVhdGVkIGJ5IHVzZXIgb24gMjAxOS80LzE0LlxuICovXG5cbmltcG9ydCB7IF9oYW5kbGVPcHRpb25zLCBtYWtlT3B0aW9ucyB9IGZyb20gJ0Bub2RlLW5vdmVsL3R4dC1zcGxpdC9saWIvaW5kZXgnO1xuaW1wb3J0IHtcblx0SU9wdGlvbnMsXG5cdElPcHRpb25zUmVxdWlyZWQsXG5cdElPcHRpb25zUmVxdWlyZWRVc2VyLFxuXHRJRGF0YVZvbHVtZSxcblx0SURhdGFDaGFwdGVyLFxuXHRJU3BsaXRDQlBhcmFtZXRlcnMsXG59IGZyb20gJ0Bub2RlLW5vdmVsL3R4dC1zcGxpdC9saWIvaW50ZXJmYWNlJztcblxuaW1wb3J0IHsgY29uc29sZSB9IGZyb20gJ0Bub2RlLW5vdmVsL3R4dC1zcGxpdC9saWIvY29uc29sZSc7XG5pbXBvcnQgdHBsQmFzZU9wdGlvbnMgZnJvbSAnLi9iYXNlL+eroF/oqbEnO1xuaW1wb3J0IHtcblx0YmFzZUNiUGFyc2VDaGFwdGVyTWF0Y2gwMDEsXG5cdGJhc2VDYlBhcnNlQ2hhcHRlck1hdGNoMDAzLFxuXHRiYXNlQ2JQYXJzZUNoYXB0ZXJNYXRjaE1haW4wMDEsXG5cdGNyZWF0ZUNiUGFyc2VDaGFwdGVyTWF0Y2gsIElDcmVhdGVDYlBhcnNlQ2hhcHRlck1hdGNoT3B0aW9uc1N1Yixcbn0gZnJvbSAnLi9saWIvcnVsZSc7XG5pbXBvcnQgU3RyVXRpbCA9IHJlcXVpcmUoJ3N0ci11dGlsJyk7XG5cbmxldCB2X2xpbmUgPSBg4pig4pig4pig4pmb4pig4pig4pig4pig4pig4pig4pmb4pig4pig4pig4pmb4pig4pig4pigYDtcbmxldCBjX2xpbmUgPSBg4p2E4pyh4pyl4pyp4pyn4pyr4pyq4pyt4piG4piF4pys4pyw4pyu4pym4p+h4pyv4pi44pyg4p2H4pyjYDtcblxubGV0IF9zcGFjZSA9ICcg44CAXFxcXHQnO1xuY29uc3QgYyA9ICfjgIAnO1xuXG5leHBvcnQgY29uc3QgdHBsT3B0aW9uczogSU9wdGlvbnNSZXF1aXJlZFVzZXIgPSB7XG5cblx0Li4udHBsQmFzZU9wdGlvbnMsXG5cblx0dm9sdW1lOiB7XG5cblx0XHQuLi50cGxCYXNlT3B0aW9ucy52b2x1bWUsXG5cblx0XHRyOiBbXG5cdFx0XHRgKD88PeOAgnxcXFxcbnxeKWAsXG5cdFx0XHQnKCcsXG5cdFx0XHRbXG5cdFx0XHRcdGDnrKxb5LiALeWNgV0rKD8656ugfOmDqClgLFxuXHRcdFx0XHRg56ysW1xcXFxk77yQLe+8mV0rKD8656ugfOmDqClgLFxuXHRcdFx0XHRg5aSW5YKzYCxcblx0XHRcdF0uam9pbignfCcpLFxuXHRcdFx0JyknLFxuXHRcdFx0YFske19zcGFjZX1dKmAsXG5cdFx0XHRgKFteXFxcXG5dKilgLFxuXHRcdFx0YFske19zcGFjZX1dKmAsXG5cdFx0XHRgYCxcblx0XHRdLFxuXHR9LFxuXG5cdC8qKlxuXHQgKiDpgJnlgIvlj4PmlbjmmK/lv4XloavpgbjpoIVcblx0ICovXG5cdGNoYXB0ZXI6IHtcblxuXHRcdC4uLnRwbEJhc2VPcHRpb25zLmNoYXB0ZXIsXG5cblx0XHRyOiBbXG5cdFx0XHRgXmAsXG5cdFx0XHQnKCcsXG5cdFx0XHRbXG5cdFx0XHRcdCfluo/nq6AnLFxuXHRcdFx0XHQnKD865bmV6ZaTfOe1gueroCknLFxuXHRcdFx0XHRg56ysW1xcXFxk77yQLe+8mV0rKD86XFwuW1xcXFxk77yQLe+8mV0rKT8oPzroqbEpYCxcblx0XHRcdFx0YOWuieWonOeahOWupOWPi+OAkOWJjeevh+OAkWAsXG5cdFx0XHRcdGDlronlqJznmoTlrqTlj4vjgJDlvoznr4fjgJFgLFxuXHRcdFx0XHRg5a6J5aic55qE5a6k5Y+L44CQ5Lit56+H44CRYCxcblx0XHRcdF0uam9pbignfCcpLFxuXHRcdFx0JyknLFxuXHRcdFx0YCg/Oig/OmAsXG5cdFx0XHRgWyR7X3NwYWNlfVxcXFwuXStgLFxuXHRcdFx0YChbXlxcXFxuXSopYCxcblx0XHRcdCcpPycsXG5cdFx0XHRgWyR7X3NwYWNlfV0qKT9gLFxuXHRcdFx0YCRgLFxuXHRcdF0sXG5cblx0XHRjYjogYmFzZUNiUGFyc2VDaGFwdGVyTWF0Y2hNYWluMDAxKFtcblxuXHRcdFx0YmFzZUNiUGFyc2VDaGFwdGVyTWF0Y2gwMDMsXG5cblx0XHRdKSxcblxuXHR9LFxuXG59O1xuXG5leHBvcnQgZGVmYXVsdCB0cGxPcHRpb25zXG4iXX0=