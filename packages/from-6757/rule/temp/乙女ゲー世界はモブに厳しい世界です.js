"use strict";
/**
 * Created by user on 2019/4/14.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const ___1 = require("../base/\u7AE0_\u8A71");
const rule_1 = require("../lib/rule");
const StrUtil = require("str-util");
let v_line = `☠☠☠♛☠☠☠☠☠☠♛☠☠☠♛☠☠☠`;
let c_line = `❄✡✥✩✧✫✪✭☆★✬✰✮✦⟡✯☸✠❇✣`;
let _space = ' 　\\t';
const c = '　';
exports.tplOptions = {
    ...___1.default,
    volume: null,
    /**
     * 這個參數是必填選項
     */
    chapter: {
        ...___1.default.chapter,
        r: [
            `^`,
            '(',
            [
                '序|戦う理由|失落道具',
            ].join('|'),
            ')',
            `(?:`,
            `[${_space}\\.]+`,
            `([^\\n]*)`,
            ')?',
            `[${_space}]*`,
            `$`,
        ],
        cb: rule_1.baseCbParseChapterMatchMain001([
            rule_1.createCbParseChapterMatch({
                handleMatchSubID(argv, _data) {
                    if (/^\d+(?:\.\d+)?$/.test(argv.id)) {
                        argv.id = String(argv.id)
                            .replace(/^\d+/, function (s) {
                            return s.padStart(3, '0');
                        });
                        argv.idn = StrUtil.toHalfNumber(argv.id.toString());
                    }
                    return argv;
                },
            }),
        ]),
    },
};
exports.default = exports.tplOptions;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoi5LmZ5aWz44Ky44O85LiW55WM44Gv44Oi44OW44Gr5Y6z44GX44GE5LiW55WM44Gn44GZLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsi5LmZ5aWz44Ky44O85LiW55WM44Gv44Oi44OW44Gr5Y6z44GX44GE5LiW55WM44Gn44GZLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQTs7R0FFRzs7QUFhSCw4Q0FBeUM7QUFDekMsc0NBS3FCO0FBQ3JCLG9DQUFxQztBQUVyQyxJQUFJLE1BQU0sR0FBRyxvQkFBb0IsQ0FBQztBQUNsQyxJQUFJLE1BQU0sR0FBRyxzQkFBc0IsQ0FBQztBQUVwQyxJQUFJLE1BQU0sR0FBRyxPQUFPLENBQUM7QUFDckIsTUFBTSxDQUFDLEdBQUcsR0FBRyxDQUFDO0FBRUQsUUFBQSxVQUFVLEdBQXlCO0lBRS9DLEdBQUcsWUFBYztJQUVqQixNQUFNLEVBQUUsSUFBSTtJQUVaOztPQUVHO0lBQ0gsT0FBTyxFQUFFO1FBRVIsR0FBRyxZQUFjLENBQUMsT0FBTztRQUV6QixDQUFDLEVBQUU7WUFDRixHQUFHO1lBQ0gsR0FBRztZQUNIO2dCQUNDLGFBQWE7YUFDYixDQUFDLElBQUksQ0FBQyxHQUFHLENBQUM7WUFDWCxHQUFHO1lBQ0gsS0FBSztZQUNMLElBQUksTUFBTSxPQUFPO1lBQ2pCLFdBQVc7WUFDWCxJQUFJO1lBQ0osSUFBSSxNQUFNLElBQUk7WUFDZCxHQUFHO1NBQ0g7UUFFRCxFQUFFLEVBQUUscUNBQThCLENBQUM7WUFDbEMsZ0NBQXlCLENBQUM7Z0JBRXpCLGdCQUFnQixDQUFDLElBQTBDLEVBQUUsS0FBeUI7b0JBRXJGLElBQUksaUJBQWlCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsRUFDbkM7d0JBRUMsSUFBSSxDQUFDLEVBQUUsR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQzs2QkFDdkIsT0FBTyxDQUFDLE1BQU0sRUFBRSxVQUFVLENBQUM7NEJBRTNCLE9BQU8sQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUM7d0JBQzNCLENBQUMsQ0FBQyxDQUNGO3dCQUVELElBQUksQ0FBQyxHQUFHLEdBQUcsT0FBTyxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUM7cUJBQ3BEO29CQUVELE9BQU8sSUFBSSxDQUFDO2dCQUNiLENBQUM7YUFFRCxDQUFDO1NBQ0YsQ0FBQztLQUVGO0NBRUQsQ0FBQztBQUVGLGtCQUFlLGtCQUFVLENBQUEiLCJzb3VyY2VzQ29udGVudCI6WyIvKipcbiAqIENyZWF0ZWQgYnkgdXNlciBvbiAyMDE5LzQvMTQuXG4gKi9cblxuaW1wb3J0IHsgX2hhbmRsZU9wdGlvbnMsIG1ha2VPcHRpb25zIH0gZnJvbSAnQG5vZGUtbm92ZWwvdHh0LXNwbGl0L2xpYi9pbmRleCc7XG5pbXBvcnQge1xuXHRJT3B0aW9ucyxcblx0SU9wdGlvbnNSZXF1aXJlZCxcblx0SU9wdGlvbnNSZXF1aXJlZFVzZXIsXG5cdElEYXRhVm9sdW1lLFxuXHRJRGF0YUNoYXB0ZXIsXG5cdElTcGxpdENCUGFyYW1ldGVycyxcbn0gZnJvbSAnQG5vZGUtbm92ZWwvdHh0LXNwbGl0L2xpYi9pbnRlcmZhY2UnO1xuXG5pbXBvcnQgeyBjb25zb2xlIH0gZnJvbSAnQG5vZGUtbm92ZWwvdHh0LXNwbGl0L2xpYi9jb25zb2xlJztcbmltcG9ydCB0cGxCYXNlT3B0aW9ucyBmcm9tICcuLi9iYXNlL+eroF/oqbEnO1xuaW1wb3J0IHtcblx0YmFzZUNiUGFyc2VDaGFwdGVyTWF0Y2gwMDEsXG5cdGJhc2VDYlBhcnNlQ2hhcHRlck1hdGNoMDAzLFxuXHRiYXNlQ2JQYXJzZUNoYXB0ZXJNYXRjaE1haW4wMDEsXG5cdGNyZWF0ZUNiUGFyc2VDaGFwdGVyTWF0Y2gsIElDcmVhdGVDYlBhcnNlQ2hhcHRlck1hdGNoT3B0aW9uc1N1Yixcbn0gZnJvbSAnLi4vbGliL3J1bGUnO1xuaW1wb3J0IFN0clV0aWwgPSByZXF1aXJlKCdzdHItdXRpbCcpO1xuXG5sZXQgdl9saW5lID0gYOKYoOKYoOKYoOKZm+KYoOKYoOKYoOKYoOKYoOKYoOKZm+KYoOKYoOKYoOKZm+KYoOKYoOKYoGA7XG5sZXQgY19saW5lID0gYOKdhOKcoeKcpeKcqeKcp+Kcq+KcquKcreKYhuKYheKcrOKcsOKcruKcpuKfoeKcr+KYuOKcoOKdh+Kco2A7XG5cbmxldCBfc3BhY2UgPSAnIOOAgFxcXFx0JztcbmNvbnN0IGMgPSAn44CAJztcblxuZXhwb3J0IGNvbnN0IHRwbE9wdGlvbnM6IElPcHRpb25zUmVxdWlyZWRVc2VyID0ge1xuXG5cdC4uLnRwbEJhc2VPcHRpb25zLFxuXG5cdHZvbHVtZTogbnVsbCxcblxuXHQvKipcblx0ICog6YCZ5YCL5Y+D5pW45piv5b+F5aGr6YG46aCFXG5cdCAqL1xuXHRjaGFwdGVyOiB7XG5cblx0XHQuLi50cGxCYXNlT3B0aW9ucy5jaGFwdGVyLFxuXG5cdFx0cjogW1xuXHRcdFx0YF5gLFxuXHRcdFx0JygnLFxuXHRcdFx0W1xuXHRcdFx0XHQn5bqPfOaIpuOBhueQhueUsXzlpLHokL3pgZPlhbcnLFxuXHRcdFx0XS5qb2luKCd8JyksXG5cdFx0XHQnKScsXG5cdFx0XHRgKD86YCxcblx0XHRcdGBbJHtfc3BhY2V9XFxcXC5dK2AsXG5cdFx0XHRgKFteXFxcXG5dKilgLFxuXHRcdFx0Jyk/Jyxcblx0XHRcdGBbJHtfc3BhY2V9XSpgLFxuXHRcdFx0YCRgLFxuXHRcdF0sXG5cblx0XHRjYjogYmFzZUNiUGFyc2VDaGFwdGVyTWF0Y2hNYWluMDAxKFtcblx0XHRcdGNyZWF0ZUNiUGFyc2VDaGFwdGVyTWF0Y2goe1xuXG5cdFx0XHRcdGhhbmRsZU1hdGNoU3ViSUQoYXJndjogSUNyZWF0ZUNiUGFyc2VDaGFwdGVyTWF0Y2hPcHRpb25zU3ViLCBfZGF0YTogSVNwbGl0Q0JQYXJhbWV0ZXJzKVxuXHRcdFx0XHR7XG5cdFx0XHRcdFx0aWYgKC9eXFxkKyg/OlxcLlxcZCspPyQvLnRlc3QoYXJndi5pZCkpXG5cdFx0XHRcdFx0e1xuXG5cdFx0XHRcdFx0XHRhcmd2LmlkID0gU3RyaW5nKGFyZ3YuaWQpXG5cdFx0XHRcdFx0XHRcdC5yZXBsYWNlKC9eXFxkKy8sIGZ1bmN0aW9uIChzKVxuXHRcdFx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHRcdFx0cmV0dXJuIHMucGFkU3RhcnQoMywgJzAnKTtcblx0XHRcdFx0XHRcdFx0fSlcblx0XHRcdFx0XHRcdDtcblxuXHRcdFx0XHRcdFx0YXJndi5pZG4gPSBTdHJVdGlsLnRvSGFsZk51bWJlcihhcmd2LmlkLnRvU3RyaW5nKCkpO1xuXHRcdFx0XHRcdH1cblxuXHRcdFx0XHRcdHJldHVybiBhcmd2O1xuXHRcdFx0XHR9LFxuXG5cdFx0XHR9KSxcblx0XHRdKSxcblxuXHR9LFxuXG59O1xuXG5leHBvcnQgZGVmYXVsdCB0cGxPcHRpb25zXG4iXX0=