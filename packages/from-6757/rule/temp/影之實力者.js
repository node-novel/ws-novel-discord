"use strict";
/**
 * Created by user on 2019/4/14.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const ___1 = require("../base/\u7AE0_\u8A71");
const rule_1 = require("../lib/rule");
const min_1 = require("cjk-conv/lib/zh/convert/min");
let v_line = `☠☠☠♛☠☠☠☠☠☠♛☠☠☠♛☠☠☠`;
let c_line = `❄✡✥✩✧✫✪✭☆★✬✰✮✦⟡✯☸✠❇✣`;
let _space = ' 　\\t';
const c = '　';
exports.tplOptions = {
    ...___1.default,
    volume: null,
    /**
     * 這個參數是必填選項
     */
    chapter: {
        ...___1.default.chapter,
        r: [
            `^`,
            '(',
            [
                '【[\\d０-９]+】',
            ].join('|'),
            ')',
            `(?:`,
            `[${_space}]*`,
            `([^\\n]*)`,
            ')?',
            `[${_space}]*`,
            `$`,
        ],
        cb: rule_1.baseCbParseChapterMatchMain001([
            rule_1.baseCbParseChapterMatch003,
        ], {
            fnAfter(_data) {
                _data.name = min_1.cn2tw_min(_data.name);
            },
        }),
    },
};
exports.default = exports.tplOptions;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoi5b2x5LmL5a+m5Yqb6ICFLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsi5b2x5LmL5a+m5Yqb6ICFLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQTs7R0FFRzs7QUFhSCw4Q0FBeUM7QUFDekMsc0NBSXNCO0FBR3RCLHFEQUFxRztBQUdyRyxJQUFJLE1BQU0sR0FBRyxvQkFBb0IsQ0FBQztBQUNsQyxJQUFJLE1BQU0sR0FBRyxzQkFBc0IsQ0FBQztBQUVwQyxJQUFJLE1BQU0sR0FBRyxPQUFPLENBQUM7QUFDckIsTUFBTSxDQUFDLEdBQUcsR0FBRyxDQUFDO0FBRUQsUUFBQSxVQUFVLEdBQXlCO0lBRS9DLEdBQUcsWUFBYztJQUVqQixNQUFNLEVBQUUsSUFBSTtJQUVaOztPQUVHO0lBQ0gsT0FBTyxFQUFFO1FBRVIsR0FBRyxZQUFjLENBQUMsT0FBTztRQUV6QixDQUFDLEVBQUU7WUFDRixHQUFHO1lBQ0gsR0FBRztZQUNIO2dCQUNDLGFBQWE7YUFDYixDQUFDLElBQUksQ0FBQyxHQUFHLENBQUM7WUFDWCxHQUFHO1lBQ0gsS0FBSztZQUNMLElBQUksTUFBTSxJQUFJO1lBQ2QsV0FBVztZQUNYLElBQUk7WUFDSixJQUFJLE1BQU0sSUFBSTtZQUNkLEdBQUc7U0FDSDtRQUVELEVBQUUsRUFBRSxxQ0FBOEIsQ0FBQztZQUNsQyxpQ0FBMEI7U0FDMUIsRUFBRTtZQUNGLE9BQU8sQ0FBQyxLQUF5QjtnQkFFaEMsS0FBSyxDQUFDLElBQUksR0FBRyxlQUFTLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ3BDLENBQUM7U0FDRCxDQUFDO0tBRUY7Q0FFRCxDQUFDO0FBRUYsa0JBQWUsa0JBQVUsQ0FBQSIsInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogQ3JlYXRlZCBieSB1c2VyIG9uIDIwMTkvNC8xNC5cbiAqL1xuXG5pbXBvcnQgeyBfaGFuZGxlT3B0aW9ucywgbWFrZU9wdGlvbnMgfSBmcm9tICdAbm9kZS1ub3ZlbC90eHQtc3BsaXQvbGliL2luZGV4JztcbmltcG9ydCB7XG5cdElPcHRpb25zLFxuXHRJT3B0aW9uc1JlcXVpcmVkLFxuXHRJT3B0aW9uc1JlcXVpcmVkVXNlcixcblx0SURhdGFWb2x1bWUsXG5cdElEYXRhQ2hhcHRlcixcblx0SVNwbGl0Q0JQYXJhbWV0ZXJzLFxufSBmcm9tICdAbm9kZS1ub3ZlbC90eHQtc3BsaXQvbGliL2ludGVyZmFjZSc7XG5cbmltcG9ydCB7IGNvbnNvbGUgfSBmcm9tICdAbm9kZS1ub3ZlbC90eHQtc3BsaXQvbGliL2NvbnNvbGUnO1xuaW1wb3J0IHRwbEJhc2VPcHRpb25zIGZyb20gJy4uL2Jhc2Uv56ugX+ipsSc7XG5pbXBvcnQge1xuXHRiYXNlQ2JQYXJzZUNoYXB0ZXJNYXRjaDAwMSxcblx0YmFzZUNiUGFyc2VDaGFwdGVyTWF0Y2gwMDMsXG5cdGJhc2VDYlBhcnNlQ2hhcHRlck1hdGNoTWFpbjAwMSxcblx0fSBmcm9tICcuLi9saWIvcnVsZSc7XG5pbXBvcnQgbm92ZWxUZXh0IGZyb20gJ25vdmVsLXRleHQnO1xuaW1wb3J0IFN0clV0aWwgPSByZXF1aXJlKCdzdHItdXRpbCcpO1xuaW1wb3J0IHsgdHcyY25fbWluLCBjbjJ0d19taW4sIHRhYmxlQ24yVHdEZWJ1ZywgdGFibGVUdzJDbkRlYnVnIH0gZnJvbSAnY2prLWNvbnYvbGliL3poL2NvbnZlcnQvbWluJztcbmltcG9ydCB7IHRyaW1EZXNjIH0gZnJvbSAnLi4vbGliL2luZGV4JztcblxubGV0IHZfbGluZSA9IGDimKDimKDimKDimZvimKDimKDimKDimKDimKDimKDimZvimKDimKDimKDimZvimKDimKDimKBgO1xubGV0IGNfbGluZSA9IGDinYTinKHinKXinKninKfinKvinKrinK3imIbimIXinKzinLDinK7inKbin6HinK/imLjinKDinYfinKNgO1xuXG5sZXQgX3NwYWNlID0gJyDjgIBcXFxcdCc7XG5jb25zdCBjID0gJ+OAgCc7XG5cbmV4cG9ydCBjb25zdCB0cGxPcHRpb25zOiBJT3B0aW9uc1JlcXVpcmVkVXNlciA9IHtcblxuXHQuLi50cGxCYXNlT3B0aW9ucyxcblxuXHR2b2x1bWU6IG51bGwsXG5cblx0LyoqXG5cdCAqIOmAmeWAi+WPg+aVuOaYr+W/heWhq+mBuOmghVxuXHQgKi9cblx0Y2hhcHRlcjoge1xuXG5cdFx0Li4udHBsQmFzZU9wdGlvbnMuY2hhcHRlcixcblxuXHRcdHI6IFtcblx0XHRcdGBeYCxcblx0XHRcdCcoJyxcblx0XHRcdFtcblx0XHRcdFx0J+OAkFtcXFxcZO+8kC3vvJldK+OAkScsXG5cdFx0XHRdLmpvaW4oJ3wnKSxcblx0XHRcdCcpJyxcblx0XHRcdGAoPzpgLFxuXHRcdFx0YFske19zcGFjZX1dKmAsXG5cdFx0XHRgKFteXFxcXG5dKilgLFxuXHRcdFx0Jyk/Jyxcblx0XHRcdGBbJHtfc3BhY2V9XSpgLFxuXHRcdFx0YCRgLFxuXHRcdF0sXG5cblx0XHRjYjogYmFzZUNiUGFyc2VDaGFwdGVyTWF0Y2hNYWluMDAxKFtcblx0XHRcdGJhc2VDYlBhcnNlQ2hhcHRlck1hdGNoMDAzLFxuXHRcdF0sIHtcblx0XHRcdGZuQWZ0ZXIoX2RhdGE6IElTcGxpdENCUGFyYW1ldGVycylcblx0XHRcdHtcblx0XHRcdFx0X2RhdGEubmFtZSA9IGNuMnR3X21pbihfZGF0YS5uYW1lKTtcblx0XHRcdH0sXG5cdFx0fSksXG5cblx0fSxcblxufTtcblxuZXhwb3J0IGRlZmF1bHQgdHBsT3B0aW9uc1xuIl19