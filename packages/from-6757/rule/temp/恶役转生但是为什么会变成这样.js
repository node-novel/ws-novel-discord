"use strict";
/**
 * Created by user on 2019/4/14.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const __000_1 = require("../base/\u8A71_000");
const rule_1 = require("../lib/rule");
const StrUtil = require("str-util");
const min_1 = require("cjk-conv/lib/zh/convert/min");
const index_1 = require("../lib/index");
let v_line = `☠☠☠♛☠☠☠☠☠☠♛☠☠☠♛☠☠☠`;
let c_line = `❄✡✥✩✧✫✪✭☆★✬✰✮✦⟡✯☸✠❇✣`;
let _space = ' 　\\t';
const c = '　';
exports.tplOptions = {
    ...__000_1.default,
    volume: {
        ...__000_1.default.volume,
        disable: false,
        r: [
            `^`,
            '(',
            [
                '第二部',
            ].join('|'),
            ')',
            `(?:`,
            //`[${_space}]+`,
            //`([^\\n]*)`,
            ')?',
        ],
    },
    /**
     * 這個參數是必填選項
     */
    chapter: {
        ...__000_1.default.chapter,
        ignoreRe: [
            `^[1-3]\\.(?!\\d)`,
            `^\\d+话完`
        ].join('|'),
        r: [
            `^`,
            `[${_space}]*`,
            '(',
            [
                '[\\d０-９]+(?:[\\.][\\d０-９]+)?',
                '16(?=使唤孩子)',
                '第四话',
            ].join('|'),
            ')',
            `(?:`,
            `[${_space}]*`,
            `([^\\n]*)`,
            ')?',
            `[${_space}]*`,
            `$`,
        ],
        cb: rule_1.baseCbParseChapterMatchMain001([
            (_data) => {
                let { 
                /**
                 * 配對到的內容
                 */
                match, 
                /**
                 * 配對出來的陣列
                 */
                sub, } = _data.m_last;
                let [ido, desc = ''] = sub;
                if (/^ss/i.test(ido)) {
                    let id = StrUtil.zh2num(ido)
                        .toString()
                        .replace(/^\D+/, '')
                        .replace(/\D+$/, '')
                        .trim();
                    let idn;
                    let ids = ido;
                    if (/^\d+(?:\.\d+)?$/.test(id)) {
                        id = String(id).padStart(3, '0');
                        idn = StrUtil.toFullNumber(id.toString());
                    }
                    if (idn) {
                        ids = `SS${idn}`;
                    }
                    else {
                        ids = ido;
                    }
                    desc = index_1.trimDesc(desc);
                    ids = StrUtil.toHalfNumber(ids);
                    desc = StrUtil.toFullNumber(desc);
                    let name = [
                        ids,
                        desc,
                    ].filter(v => v !== '').join(c);
                    _data.name = name;
                    _data.idx += _data.m_last.match.length;
                    return true;
                }
            },
            rule_1.baseCbParseChapterMatch003,
        ], {
            fnAfter(_data) {
                _data.name = min_1.cn2tw_min(_data.name);
            },
        }),
    },
};
exports.default = exports.tplOptions;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoi5oG25b256L2s55Sf5L2G5piv5Li65LuA5LmI5Lya5Y+Y5oiQ6L+Z5qC3LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsi5oG25b256L2s55Sf5L2G5piv5Li65LuA5LmI5Lya5Y+Y5oiQ6L+Z5qC3LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQTs7R0FFRzs7QUFhSCw4Q0FBMkM7QUFDM0Msc0NBSXNCO0FBRXRCLG9DQUFxQztBQUNyQyxxREFBcUc7QUFDckcsd0NBQXdDO0FBRXhDLElBQUksTUFBTSxHQUFHLG9CQUFvQixDQUFDO0FBQ2xDLElBQUksTUFBTSxHQUFHLHNCQUFzQixDQUFDO0FBRXBDLElBQUksTUFBTSxHQUFHLE9BQU8sQ0FBQztBQUNyQixNQUFNLENBQUMsR0FBRyxHQUFHLENBQUM7QUFFRCxRQUFBLFVBQVUsR0FBeUI7SUFFL0MsR0FBRyxlQUFjO0lBRWpCLE1BQU0sRUFBRTtRQUNQLEdBQUcsZUFBYyxDQUFDLE1BQU07UUFFeEIsT0FBTyxFQUFFLEtBQUs7UUFFZCxDQUFDLEVBQUU7WUFDRixHQUFHO1lBQ0gsR0FBRztZQUNIO2dCQUNDLEtBQUs7YUFDTCxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUM7WUFDWCxHQUFHO1lBQ0gsS0FBSztZQUNMLGlCQUFpQjtZQUNqQixjQUFjO1lBQ2QsSUFBSTtTQUdKO0tBRUQ7SUFFRDs7T0FFRztJQUNILE9BQU8sRUFBRTtRQUVSLEdBQUcsZUFBYyxDQUFDLE9BQU87UUFFekIsUUFBUSxFQUFFO1lBQ1Qsa0JBQWtCO1lBQ2xCLFNBQVM7U0FDVCxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUM7UUFFWCxDQUFDLEVBQUU7WUFDRixHQUFHO1lBQ0gsSUFBSSxNQUFNLElBQUk7WUFDZCxHQUFHO1lBQ0g7Z0JBQ0MsOEJBQThCO2dCQUM5QixZQUFZO2dCQUNaLEtBQUs7YUFDTCxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUM7WUFDWCxHQUFHO1lBQ0gsS0FBSztZQUNMLElBQUksTUFBTSxJQUFJO1lBQ2QsV0FBVztZQUNYLElBQUk7WUFDSixJQUFJLE1BQU0sSUFBSTtZQUNkLEdBQUc7U0FDSDtRQUVELEVBQUUsRUFBRSxxQ0FBOEIsQ0FBQztZQUNsQyxDQUFDLEtBQXlCLEVBQUUsRUFBRTtnQkFHN0IsSUFBSTtnQkFDSDs7bUJBRUc7Z0JBQ0gsS0FBSztnQkFDTDs7bUJBRUc7Z0JBQ0gsR0FBRyxHQUNILEdBQUcsS0FBSyxDQUFDLE1BQU0sQ0FBQztnQkFFakIsSUFBSSxDQUFDLEdBQUcsRUFBRSxJQUFJLEdBQUcsRUFBRSxDQUFDLEdBQUcsR0FBRyxDQUFDO2dCQUUzQixJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQ3BCO29CQUNDLElBQUksRUFBRSxHQUFHLE9BQU8sQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDO3lCQUMxQixRQUFRLEVBQUU7eUJBQ1YsT0FBTyxDQUFDLE1BQU0sRUFBRSxFQUFFLENBQUM7eUJBQ25CLE9BQU8sQ0FBQyxNQUFNLEVBQUUsRUFBRSxDQUFDO3lCQUNuQixJQUFJLEVBQUUsQ0FDUDtvQkFFRCxJQUFJLEdBQVcsQ0FBQztvQkFDaEIsSUFBSSxHQUFHLEdBQUcsR0FBRyxDQUFDO29CQUVkLElBQUksaUJBQWlCLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxFQUM5Qjt3QkFDQyxFQUFFLEdBQUcsTUFBTSxDQUFDLEVBQUUsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUM7d0JBRWpDLEdBQUcsR0FBRyxPQUFPLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDO3FCQUMxQztvQkFFRCxJQUFJLEdBQUcsRUFDUDt3QkFDQyxHQUFHLEdBQUcsS0FBSyxHQUFHLEVBQUUsQ0FBQztxQkFDakI7eUJBRUQ7d0JBQ0MsR0FBRyxHQUFHLEdBQUcsQ0FBQztxQkFDVjtvQkFFRCxJQUFJLEdBQUcsZ0JBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQztvQkFFdEIsR0FBRyxHQUFHLE9BQU8sQ0FBQyxZQUFZLENBQUMsR0FBRyxDQUFDLENBQUM7b0JBQ2hDLElBQUksR0FBRyxPQUFPLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDO29CQUVsQyxJQUFJLElBQUksR0FBK0I7d0JBQ3RDLEdBQUc7d0JBQ0gsSUFBSTtxQkFDSixDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBRWhDLEtBQUssQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDO29CQUNsQixLQUFLLENBQUMsR0FBRyxJQUFJLEtBQUssQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQztvQkFFdkMsT0FBTyxJQUFJLENBQUM7aUJBQ1o7WUFFRixDQUFDO1lBQ0QsaUNBQTBCO1NBQzFCLEVBQUU7WUFDRixPQUFPLENBQUMsS0FBeUI7Z0JBRWhDLEtBQUssQ0FBQyxJQUFJLEdBQUcsZUFBUyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUNwQyxDQUFDO1NBQ0QsQ0FBQztLQUVGO0NBRUQsQ0FBQztBQUVGLGtCQUFlLGtCQUFVLENBQUEiLCJzb3VyY2VzQ29udGVudCI6WyIvKipcbiAqIENyZWF0ZWQgYnkgdXNlciBvbiAyMDE5LzQvMTQuXG4gKi9cblxuaW1wb3J0IHsgX2hhbmRsZU9wdGlvbnMsIG1ha2VPcHRpb25zIH0gZnJvbSAnQG5vZGUtbm92ZWwvdHh0LXNwbGl0L2xpYi9pbmRleCc7XG5pbXBvcnQge1xuXHRJT3B0aW9ucyxcblx0SU9wdGlvbnNSZXF1aXJlZCxcblx0SU9wdGlvbnNSZXF1aXJlZFVzZXIsXG5cdElEYXRhVm9sdW1lLFxuXHRJRGF0YUNoYXB0ZXIsXG5cdElTcGxpdENCUGFyYW1ldGVycyxcbn0gZnJvbSAnQG5vZGUtbm92ZWwvdHh0LXNwbGl0L2xpYi9pbnRlcmZhY2UnO1xuXG5pbXBvcnQgeyBjb25zb2xlIH0gZnJvbSAnQG5vZGUtbm92ZWwvdHh0LXNwbGl0L2xpYi9jb25zb2xlJztcbmltcG9ydCB0cGxCYXNlT3B0aW9ucyBmcm9tICcuLi9iYXNlL+ipsV8wMDAnO1xuaW1wb3J0IHtcblx0YmFzZUNiUGFyc2VDaGFwdGVyTWF0Y2gwMDEsXG5cdGJhc2VDYlBhcnNlQ2hhcHRlck1hdGNoMDAzLFxuXHRiYXNlQ2JQYXJzZUNoYXB0ZXJNYXRjaE1haW4wMDEsXG5cdH0gZnJvbSAnLi4vbGliL3J1bGUnO1xuaW1wb3J0IG5vdmVsVGV4dCBmcm9tICdub3ZlbC10ZXh0JztcbmltcG9ydCBTdHJVdGlsID0gcmVxdWlyZSgnc3RyLXV0aWwnKTtcbmltcG9ydCB7IHR3MmNuX21pbiwgY24ydHdfbWluLCB0YWJsZUNuMlR3RGVidWcsIHRhYmxlVHcyQ25EZWJ1ZyB9IGZyb20gJ2Nqay1jb252L2xpYi96aC9jb252ZXJ0L21pbic7XG5pbXBvcnQgeyB0cmltRGVzYyB9IGZyb20gJy4uL2xpYi9pbmRleCc7XG5cbmxldCB2X2xpbmUgPSBg4pig4pig4pig4pmb4pig4pig4pig4pig4pig4pig4pmb4pig4pig4pig4pmb4pig4pig4pigYDtcbmxldCBjX2xpbmUgPSBg4p2E4pyh4pyl4pyp4pyn4pyr4pyq4pyt4piG4piF4pys4pyw4pyu4pym4p+h4pyv4pi44pyg4p2H4pyjYDtcblxubGV0IF9zcGFjZSA9ICcg44CAXFxcXHQnO1xuY29uc3QgYyA9ICfjgIAnO1xuXG5leHBvcnQgY29uc3QgdHBsT3B0aW9uczogSU9wdGlvbnNSZXF1aXJlZFVzZXIgPSB7XG5cblx0Li4udHBsQmFzZU9wdGlvbnMsXG5cblx0dm9sdW1lOiB7XG5cdFx0Li4udHBsQmFzZU9wdGlvbnMudm9sdW1lLFxuXG5cdFx0ZGlzYWJsZTogZmFsc2UsXG5cblx0XHRyOiBbXG5cdFx0XHRgXmAsXG5cdFx0XHQnKCcsXG5cdFx0XHRbXG5cdFx0XHRcdCfnrKzkuozpg6gnLFxuXHRcdFx0XS5qb2luKCd8JyksXG5cdFx0XHQnKScsXG5cdFx0XHRgKD86YCxcblx0XHRcdC8vYFske19zcGFjZX1dK2AsXG5cdFx0XHQvL2AoW15cXFxcbl0qKWAsXG5cdFx0XHQnKT8nLFxuXHRcdFx0Ly9gWyR7X3NwYWNlfV0qYCxcblx0XHRcdC8vYCRgLFxuXHRcdF0sXG5cblx0fSxcblxuXHQvKipcblx0ICog6YCZ5YCL5Y+D5pW45piv5b+F5aGr6YG46aCFXG5cdCAqL1xuXHRjaGFwdGVyOiB7XG5cblx0XHQuLi50cGxCYXNlT3B0aW9ucy5jaGFwdGVyLFxuXG5cdFx0aWdub3JlUmU6IFtcblx0XHRcdGBeWzEtM11cXFxcLig/IVxcXFxkKWAsXG5cdFx0XHRgXlxcXFxkK+ivneWujGBcblx0XHRdLmpvaW4oJ3wnKSxcblxuXHRcdHI6IFtcblx0XHRcdGBeYCxcblx0XHRcdGBbJHtfc3BhY2V9XSpgLFxuXHRcdFx0JygnLFxuXHRcdFx0W1xuXHRcdFx0XHQnW1xcXFxk77yQLe+8mV0rKD86W1xcXFwuXVtcXFxcZO+8kC3vvJldKyk/Jyxcblx0XHRcdFx0JzE2KD895L2/5ZSk5a2p5a2QKScsXG5cdFx0XHRcdCfnrKzlm5vor50nLFxuXHRcdFx0XS5qb2luKCd8JyksXG5cdFx0XHQnKScsXG5cdFx0XHRgKD86YCxcblx0XHRcdGBbJHtfc3BhY2V9XSpgLFxuXHRcdFx0YChbXlxcXFxuXSopYCxcblx0XHRcdCcpPycsXG5cdFx0XHRgWyR7X3NwYWNlfV0qYCxcblx0XHRcdGAkYCxcblx0XHRdLFxuXG5cdFx0Y2I6IGJhc2VDYlBhcnNlQ2hhcHRlck1hdGNoTWFpbjAwMShbXG5cdFx0XHQoX2RhdGE6IElTcGxpdENCUGFyYW1ldGVycykgPT5cblx0XHRcdHtcblxuXHRcdFx0XHRsZXQge1xuXHRcdFx0XHRcdC8qKlxuXHRcdFx0XHRcdCAqIOmFjeWwjeWIsOeahOWFp+WuuVxuXHRcdFx0XHRcdCAqL1xuXHRcdFx0XHRcdG1hdGNoLFxuXHRcdFx0XHRcdC8qKlxuXHRcdFx0XHRcdCAqIOmFjeWwjeWHuuS+hueahOmZo+WIl1xuXHRcdFx0XHRcdCAqL1xuXHRcdFx0XHRcdHN1Yixcblx0XHRcdFx0fSA9IF9kYXRhLm1fbGFzdDtcblxuXHRcdFx0XHRsZXQgW2lkbywgZGVzYyA9ICcnXSA9IHN1YjtcblxuXHRcdFx0XHRpZiAoL15zcy9pLnRlc3QoaWRvKSlcblx0XHRcdFx0e1xuXHRcdFx0XHRcdGxldCBpZCA9IFN0clV0aWwuemgybnVtKGlkbylcblx0XHRcdFx0XHRcdC50b1N0cmluZygpXG5cdFx0XHRcdFx0XHQucmVwbGFjZSgvXlxcRCsvLCAnJylcblx0XHRcdFx0XHRcdC5yZXBsYWNlKC9cXEQrJC8sICcnKVxuXHRcdFx0XHRcdFx0LnRyaW0oKVxuXHRcdFx0XHRcdDtcblxuXHRcdFx0XHRcdGxldCBpZG46IHN0cmluZztcblx0XHRcdFx0XHRsZXQgaWRzID0gaWRvO1xuXG5cdFx0XHRcdFx0aWYgKC9eXFxkKyg/OlxcLlxcZCspPyQvLnRlc3QoaWQpKVxuXHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdGlkID0gU3RyaW5nKGlkKS5wYWRTdGFydCgzLCAnMCcpO1xuXG5cdFx0XHRcdFx0XHRpZG4gPSBTdHJVdGlsLnRvRnVsbE51bWJlcihpZC50b1N0cmluZygpKTtcblx0XHRcdFx0XHR9XG5cblx0XHRcdFx0XHRpZiAoaWRuKVxuXHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdGlkcyA9IGBTUyR7aWRufWA7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHRcdGVsc2Vcblx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHRpZHMgPSBpZG87XG5cdFx0XHRcdFx0fVxuXG5cdFx0XHRcdFx0ZGVzYyA9IHRyaW1EZXNjKGRlc2MpO1xuXG5cdFx0XHRcdFx0aWRzID0gU3RyVXRpbC50b0hhbGZOdW1iZXIoaWRzKTtcblx0XHRcdFx0XHRkZXNjID0gU3RyVXRpbC50b0Z1bGxOdW1iZXIoZGVzYyk7XG5cblx0XHRcdFx0XHRsZXQgbmFtZTogSVNwbGl0Q0JQYXJhbWV0ZXJzW1wibmFtZVwiXSA9IFtcblx0XHRcdFx0XHRcdGlkcyxcblx0XHRcdFx0XHRcdGRlc2MsXG5cdFx0XHRcdFx0XS5maWx0ZXIodiA9PiB2ICE9PSAnJykuam9pbihjKTtcblxuXHRcdFx0XHRcdF9kYXRhLm5hbWUgPSBuYW1lO1xuXHRcdFx0XHRcdF9kYXRhLmlkeCArPSBfZGF0YS5tX2xhc3QubWF0Y2gubGVuZ3RoO1xuXG5cdFx0XHRcdFx0cmV0dXJuIHRydWU7XG5cdFx0XHRcdH1cblxuXHRcdFx0fSxcblx0XHRcdGJhc2VDYlBhcnNlQ2hhcHRlck1hdGNoMDAzLFxuXHRcdF0sIHtcblx0XHRcdGZuQWZ0ZXIoX2RhdGE6IElTcGxpdENCUGFyYW1ldGVycylcblx0XHRcdHtcblx0XHRcdFx0X2RhdGEubmFtZSA9IGNuMnR3X21pbihfZGF0YS5uYW1lKTtcblx0XHRcdH0sXG5cdFx0fSksXG5cblx0fSxcblxufTtcblxuZXhwb3J0IGRlZmF1bHQgdHBsT3B0aW9uc1xuIl19