/**
 * Created by user on 2019/4/14.
 */

import { _handleOptions, makeOptions } from '@node-novel/txt-split/lib/index';
import {
	IOptions,
	IOptionsRequired,
	IOptionsRequiredUser,
	IDataVolume,
	IDataChapter,
	ISplitCBParameters,
} from '@node-novel/txt-split/lib/interface';

import { console } from '@node-novel/txt-split/lib/console';
import tplBaseOptions from '../base/章_話';
import {
	baseCbParseChapterMatch001,
	baseCbParseChapterMatch003,
	baseCbParseChapterMatchMain001,
	} from '../lib/rule';
import novelText from 'novel-text';
import StrUtil = require('str-util');
import { tw2cn_min, cn2tw_min, tableCn2TwDebug, tableTw2CnDebug } from 'cjk-conv/lib/zh/convert/min';
import { trimDesc } from '../lib/index';

let v_line = `☠☠☠♛☠☠☠☠☠☠♛☠☠☠♛☠☠☠`;
let c_line = `❄✡✥✩✧✫✪✭☆★✬✰✮✦⟡✯☸✠❇✣`;

let _space = ' 　\\t';
const c = '　';

export const tplOptions: IOptionsRequiredUser = {

	...tplBaseOptions,

	volume: null,

	/**
	 * 這個參數是必填選項
	 */
	chapter: {

		...tplBaseOptions.chapter,

		r: [
			`^`,
			'(',
			[
				'【[\\d０-９]+】',
			].join('|'),
			')',
			`(?:`,
			`[${_space}]*`,
			`([^\\n]*)`,
			')?',
			`[${_space}]*`,
			`$`,
		],

		cb: baseCbParseChapterMatchMain001([
			baseCbParseChapterMatch003,
		], {
			fnAfter(_data: ISplitCBParameters)
			{
				_data.name = cn2tw_min(_data.name);
			},
		}),

	},

};

export default tplOptions
