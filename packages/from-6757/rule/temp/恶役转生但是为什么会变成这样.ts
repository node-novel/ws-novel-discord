/**
 * Created by user on 2019/4/14.
 */

import { _handleOptions, makeOptions } from '@node-novel/txt-split/lib/index';
import {
	IOptions,
	IOptionsRequired,
	IOptionsRequiredUser,
	IDataVolume,
	IDataChapter,
	ISplitCBParameters,
} from '@node-novel/txt-split/lib/interface';

import { console } from '@node-novel/txt-split/lib/console';
import tplBaseOptions from '../base/話_000';
import {
	baseCbParseChapterMatch001,
	baseCbParseChapterMatch003,
	baseCbParseChapterMatchMain001,
	} from '../lib/rule';
import novelText from 'novel-text';
import StrUtil = require('str-util');
import { tw2cn_min, cn2tw_min, tableCn2TwDebug, tableTw2CnDebug } from 'cjk-conv/lib/zh/convert/min';
import { trimDesc } from '../lib/index';

let v_line = `☠☠☠♛☠☠☠☠☠☠♛☠☠☠♛☠☠☠`;
let c_line = `❄✡✥✩✧✫✪✭☆★✬✰✮✦⟡✯☸✠❇✣`;

let _space = ' 　\\t';
const c = '　';

export const tplOptions: IOptionsRequiredUser = {

	...tplBaseOptions,

	volume: {
		...tplBaseOptions.volume,

		disable: false,

		r: [
			`^`,
			'(',
			[
				'第二部',
			].join('|'),
			')',
			`(?:`,
			//`[${_space}]+`,
			//`([^\\n]*)`,
			')?',
			//`[${_space}]*`,
			//`$`,
		],

	},

	/**
	 * 這個參數是必填選項
	 */
	chapter: {

		...tplBaseOptions.chapter,

		ignoreRe: [
			`^[1-3]\\.(?!\\d)`,
			`^\\d+话完`
		].join('|'),

		r: [
			`^`,
			`[${_space}]*`,
			'(',
			[
				'[\\d０-９]+(?:[\\.][\\d０-９]+)?',
				'16(?=使唤孩子)',
				'第四话',
			].join('|'),
			')',
			`(?:`,
			`[${_space}]*`,
			`([^\\n]*)`,
			')?',
			`[${_space}]*`,
			`$`,
		],

		cb: baseCbParseChapterMatchMain001([
			(_data: ISplitCBParameters) =>
			{

				let {
					/**
					 * 配對到的內容
					 */
					match,
					/**
					 * 配對出來的陣列
					 */
					sub,
				} = _data.m_last;

				let [ido, desc = ''] = sub;

				if (/^ss/i.test(ido))
				{
					let id = StrUtil.zh2num(ido)
						.toString()
						.replace(/^\D+/, '')
						.replace(/\D+$/, '')
						.trim()
					;

					let idn: string;
					let ids = ido;

					if (/^\d+(?:\.\d+)?$/.test(id))
					{
						id = String(id).padStart(3, '0');

						idn = StrUtil.toFullNumber(id.toString());
					}

					if (idn)
					{
						ids = `SS${idn}`;
					}
					else
					{
						ids = ido;
					}

					desc = trimDesc(desc);

					ids = StrUtil.toHalfNumber(ids);
					desc = StrUtil.toFullNumber(desc);

					let name: ISplitCBParameters["name"] = [
						ids,
						desc,
					].filter(v => v !== '').join(c);

					_data.name = name;
					_data.idx += _data.m_last.match.length;

					return true;
				}

			},
			baseCbParseChapterMatch003,
		], {
			fnAfter(_data: ISplitCBParameters)
			{
				_data.name = cn2tw_min(_data.name);
			},
		}),

	},

};

export default tplOptions
