"use strict";
/**
 * Created by user on 2019/4/14.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const ___1 = require("../base/\u7AE0_\u8A71");
const rule_1 = require("../lib/rule");
const StrUtil = require("str-util");
const min_1 = require("cjk-conv/lib/zh/convert/min");
const index_1 = require("../lib/index");
let v_line = `☠☠☠♛☠☠☠☠☠☠♛☠☠☠♛☠☠☠`;
let c_line = `❄✡✥✩✧✫✪✭☆★✬✰✮✦⟡✯☸✠❇✣`;
let _space = ' 　\\t';
const c = '　';
exports.tplOptions = {
    ...___1.default,
    volume: null,
    /**
     * 這個參數是必填選項
     */
    chapter: {
        ...___1.default.chapter,
        r: [
            `^`,
            '(',
            [
                '＜序＞',
                `(?:SS)?第[\\d０-９]+(?:\.[\\d０-９]+)?(?:話)`,
            ].join('|'),
            ')',
            `(?:`,
            `[${_space}]+`,
            `([^\\n]*)`,
            ')?',
            `[${_space}]*`,
            `$`,
        ],
        cb: rule_1.baseCbParseChapterMatchMain001([
            (_data) => {
                let { 
                /**
                 * 配對到的內容
                 */
                match, 
                /**
                 * 配對出來的陣列
                 */
                sub, } = _data.m_last;
                let [ido, desc = ''] = sub;
                if (/^ss/i.test(ido)) {
                    let id = StrUtil.zh2num(ido)
                        .toString()
                        .replace(/^\D+/, '')
                        .replace(/\D+$/, '')
                        .trim();
                    let idn;
                    let ids = ido;
                    if (/^\d+(?:\.\d+)?$/.test(id)) {
                        id = String(id).padStart(3, '0');
                        idn = StrUtil.toFullNumber(id.toString());
                    }
                    if (idn) {
                        ids = `SS${idn}`;
                    }
                    else {
                        ids = ido;
                    }
                    desc = index_1.trimDesc(desc);
                    ids = StrUtil.toHalfNumber(ids);
                    desc = StrUtil.toFullNumber(desc);
                    let name = [
                        ids,
                        desc,
                    ].filter(v => v !== '').join(c);
                    _data.name = name;
                    _data.idx += _data.m_last.match.length;
                    return true;
                }
            },
            rule_1.baseCbParseChapterMatch003,
        ], {
            fnAfter(_data) {
                _data.name = min_1.cn2tw_min(_data.name);
            },
        }),
    },
};
exports.default = exports.tplOptions;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoi6Zeq5Lqu55qE6a2U5YOP5pW05ZCILmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsi6Zeq5Lqu55qE6a2U5YOP5pW05ZCILnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQTs7R0FFRzs7QUFhSCw4Q0FBeUM7QUFDekMsc0NBSXNCO0FBRXRCLG9DQUFxQztBQUNyQyxxREFBcUc7QUFDckcsd0NBQXdDO0FBRXhDLElBQUksTUFBTSxHQUFHLG9CQUFvQixDQUFDO0FBQ2xDLElBQUksTUFBTSxHQUFHLHNCQUFzQixDQUFDO0FBRXBDLElBQUksTUFBTSxHQUFHLE9BQU8sQ0FBQztBQUNyQixNQUFNLENBQUMsR0FBRyxHQUFHLENBQUM7QUFFRCxRQUFBLFVBQVUsR0FBeUI7SUFFL0MsR0FBRyxZQUFjO0lBRWpCLE1BQU0sRUFBRSxJQUFJO0lBRVo7O09BRUc7SUFDSCxPQUFPLEVBQUU7UUFFUixHQUFHLFlBQWMsQ0FBQyxPQUFPO1FBRXpCLENBQUMsRUFBRTtZQUNGLEdBQUc7WUFDSCxHQUFHO1lBQ0g7Z0JBQ0MsS0FBSztnQkFDTCx3Q0FBd0M7YUFDeEMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDO1lBQ1gsR0FBRztZQUNILEtBQUs7WUFDTCxJQUFJLE1BQU0sSUFBSTtZQUNkLFdBQVc7WUFDWCxJQUFJO1lBQ0osSUFBSSxNQUFNLElBQUk7WUFDZCxHQUFHO1NBQ0g7UUFFRCxFQUFFLEVBQUUscUNBQThCLENBQUM7WUFDbEMsQ0FBQyxLQUF5QixFQUFFLEVBQUU7Z0JBRzdCLElBQUk7Z0JBQ0g7O21CQUVHO2dCQUNILEtBQUs7Z0JBQ0w7O21CQUVHO2dCQUNILEdBQUcsR0FDSCxHQUFHLEtBQUssQ0FBQyxNQUFNLENBQUM7Z0JBRWpCLElBQUksQ0FBQyxHQUFHLEVBQUUsSUFBSSxHQUFHLEVBQUUsQ0FBQyxHQUFHLEdBQUcsQ0FBQztnQkFFM0IsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUNwQjtvQkFDQyxJQUFJLEVBQUUsR0FBRyxPQUFPLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQzt5QkFDMUIsUUFBUSxFQUFFO3lCQUNWLE9BQU8sQ0FBQyxNQUFNLEVBQUUsRUFBRSxDQUFDO3lCQUNuQixPQUFPLENBQUMsTUFBTSxFQUFFLEVBQUUsQ0FBQzt5QkFDbkIsSUFBSSxFQUFFLENBQ1A7b0JBRUQsSUFBSSxHQUFXLENBQUM7b0JBQ2hCLElBQUksR0FBRyxHQUFHLEdBQUcsQ0FBQztvQkFFZCxJQUFJLGlCQUFpQixDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsRUFDOUI7d0JBQ0MsRUFBRSxHQUFHLE1BQU0sQ0FBQyxFQUFFLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO3dCQUVqQyxHQUFHLEdBQUcsT0FBTyxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQztxQkFDMUM7b0JBRUQsSUFBSSxHQUFHLEVBQ1A7d0JBQ0MsR0FBRyxHQUFHLEtBQUssR0FBRyxFQUFFLENBQUM7cUJBQ2pCO3lCQUVEO3dCQUNDLEdBQUcsR0FBRyxHQUFHLENBQUM7cUJBQ1Y7b0JBRUQsSUFBSSxHQUFHLGdCQUFRLENBQUMsSUFBSSxDQUFDLENBQUM7b0JBRXRCLEdBQUcsR0FBRyxPQUFPLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxDQUFDO29CQUNoQyxJQUFJLEdBQUcsT0FBTyxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQztvQkFFbEMsSUFBSSxJQUFJLEdBQStCO3dCQUN0QyxHQUFHO3dCQUNILElBQUk7cUJBQ0osQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUVoQyxLQUFLLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQztvQkFDbEIsS0FBSyxDQUFDLEdBQUcsSUFBSSxLQUFLLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUM7b0JBRXZDLE9BQU8sSUFBSSxDQUFDO2lCQUNaO1lBRUYsQ0FBQztZQUNELGlDQUEwQjtTQUMxQixFQUFFO1lBQ0YsT0FBTyxDQUFDLEtBQXlCO2dCQUVoQyxLQUFLLENBQUMsSUFBSSxHQUFHLGVBQVMsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDcEMsQ0FBQztTQUNELENBQUM7S0FFRjtDQUVELENBQUM7QUFFRixrQkFBZSxrQkFBVSxDQUFBIiwic291cmNlc0NvbnRlbnQiOlsiLyoqXG4gKiBDcmVhdGVkIGJ5IHVzZXIgb24gMjAxOS80LzE0LlxuICovXG5cbmltcG9ydCB7IF9oYW5kbGVPcHRpb25zLCBtYWtlT3B0aW9ucyB9IGZyb20gJ0Bub2RlLW5vdmVsL3R4dC1zcGxpdC9saWIvaW5kZXgnO1xuaW1wb3J0IHtcblx0SU9wdGlvbnMsXG5cdElPcHRpb25zUmVxdWlyZWQsXG5cdElPcHRpb25zUmVxdWlyZWRVc2VyLFxuXHRJRGF0YVZvbHVtZSxcblx0SURhdGFDaGFwdGVyLFxuXHRJU3BsaXRDQlBhcmFtZXRlcnMsXG59IGZyb20gJ0Bub2RlLW5vdmVsL3R4dC1zcGxpdC9saWIvaW50ZXJmYWNlJztcblxuaW1wb3J0IHsgY29uc29sZSB9IGZyb20gJ0Bub2RlLW5vdmVsL3R4dC1zcGxpdC9saWIvY29uc29sZSc7XG5pbXBvcnQgdHBsQmFzZU9wdGlvbnMgZnJvbSAnLi4vYmFzZS/nq6Bf6KmxJztcbmltcG9ydCB7XG5cdGJhc2VDYlBhcnNlQ2hhcHRlck1hdGNoMDAxLFxuXHRiYXNlQ2JQYXJzZUNoYXB0ZXJNYXRjaDAwMyxcblx0YmFzZUNiUGFyc2VDaGFwdGVyTWF0Y2hNYWluMDAxLFxuXHR9IGZyb20gJy4uL2xpYi9ydWxlJztcbmltcG9ydCBub3ZlbFRleHQgZnJvbSAnbm92ZWwtdGV4dCc7XG5pbXBvcnQgU3RyVXRpbCA9IHJlcXVpcmUoJ3N0ci11dGlsJyk7XG5pbXBvcnQgeyB0dzJjbl9taW4sIGNuMnR3X21pbiwgdGFibGVDbjJUd0RlYnVnLCB0YWJsZVR3MkNuRGVidWcgfSBmcm9tICdjamstY29udi9saWIvemgvY29udmVydC9taW4nO1xuaW1wb3J0IHsgdHJpbURlc2MgfSBmcm9tICcuLi9saWIvaW5kZXgnO1xuXG5sZXQgdl9saW5lID0gYOKYoOKYoOKYoOKZm+KYoOKYoOKYoOKYoOKYoOKYoOKZm+KYoOKYoOKYoOKZm+KYoOKYoOKYoGA7XG5sZXQgY19saW5lID0gYOKdhOKcoeKcpeKcqeKcp+Kcq+KcquKcreKYhuKYheKcrOKcsOKcruKcpuKfoeKcr+KYuOKcoOKdh+Kco2A7XG5cbmxldCBfc3BhY2UgPSAnIOOAgFxcXFx0JztcbmNvbnN0IGMgPSAn44CAJztcblxuZXhwb3J0IGNvbnN0IHRwbE9wdGlvbnM6IElPcHRpb25zUmVxdWlyZWRVc2VyID0ge1xuXG5cdC4uLnRwbEJhc2VPcHRpb25zLFxuXG5cdHZvbHVtZTogbnVsbCxcblxuXHQvKipcblx0ICog6YCZ5YCL5Y+D5pW45piv5b+F5aGr6YG46aCFXG5cdCAqL1xuXHRjaGFwdGVyOiB7XG5cblx0XHQuLi50cGxCYXNlT3B0aW9ucy5jaGFwdGVyLFxuXG5cdFx0cjogW1xuXHRcdFx0YF5gLFxuXHRcdFx0JygnLFxuXHRcdFx0W1xuXHRcdFx0XHQn77yc5bqP77yeJyxcblx0XHRcdFx0YCg/OlNTKT/nrKxbXFxcXGTvvJAt77yZXSsoPzpcXC5bXFxcXGTvvJAt77yZXSspPyg/OuipsSlgLFxuXHRcdFx0XS5qb2luKCd8JyksXG5cdFx0XHQnKScsXG5cdFx0XHRgKD86YCxcblx0XHRcdGBbJHtfc3BhY2V9XStgLFxuXHRcdFx0YChbXlxcXFxuXSopYCxcblx0XHRcdCcpPycsXG5cdFx0XHRgWyR7X3NwYWNlfV0qYCxcblx0XHRcdGAkYCxcblx0XHRdLFxuXG5cdFx0Y2I6IGJhc2VDYlBhcnNlQ2hhcHRlck1hdGNoTWFpbjAwMShbXG5cdFx0XHQoX2RhdGE6IElTcGxpdENCUGFyYW1ldGVycykgPT5cblx0XHRcdHtcblxuXHRcdFx0XHRsZXQge1xuXHRcdFx0XHRcdC8qKlxuXHRcdFx0XHRcdCAqIOmFjeWwjeWIsOeahOWFp+WuuVxuXHRcdFx0XHRcdCAqL1xuXHRcdFx0XHRcdG1hdGNoLFxuXHRcdFx0XHRcdC8qKlxuXHRcdFx0XHRcdCAqIOmFjeWwjeWHuuS+hueahOmZo+WIl1xuXHRcdFx0XHRcdCAqL1xuXHRcdFx0XHRcdHN1Yixcblx0XHRcdFx0fSA9IF9kYXRhLm1fbGFzdDtcblxuXHRcdFx0XHRsZXQgW2lkbywgZGVzYyA9ICcnXSA9IHN1YjtcblxuXHRcdFx0XHRpZiAoL15zcy9pLnRlc3QoaWRvKSlcblx0XHRcdFx0e1xuXHRcdFx0XHRcdGxldCBpZCA9IFN0clV0aWwuemgybnVtKGlkbylcblx0XHRcdFx0XHRcdC50b1N0cmluZygpXG5cdFx0XHRcdFx0XHQucmVwbGFjZSgvXlxcRCsvLCAnJylcblx0XHRcdFx0XHRcdC5yZXBsYWNlKC9cXEQrJC8sICcnKVxuXHRcdFx0XHRcdFx0LnRyaW0oKVxuXHRcdFx0XHRcdDtcblxuXHRcdFx0XHRcdGxldCBpZG46IHN0cmluZztcblx0XHRcdFx0XHRsZXQgaWRzID0gaWRvO1xuXG5cdFx0XHRcdFx0aWYgKC9eXFxkKyg/OlxcLlxcZCspPyQvLnRlc3QoaWQpKVxuXHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdGlkID0gU3RyaW5nKGlkKS5wYWRTdGFydCgzLCAnMCcpO1xuXG5cdFx0XHRcdFx0XHRpZG4gPSBTdHJVdGlsLnRvRnVsbE51bWJlcihpZC50b1N0cmluZygpKTtcblx0XHRcdFx0XHR9XG5cblx0XHRcdFx0XHRpZiAoaWRuKVxuXHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdGlkcyA9IGBTUyR7aWRufWA7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHRcdGVsc2Vcblx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHRpZHMgPSBpZG87XG5cdFx0XHRcdFx0fVxuXG5cdFx0XHRcdFx0ZGVzYyA9IHRyaW1EZXNjKGRlc2MpO1xuXG5cdFx0XHRcdFx0aWRzID0gU3RyVXRpbC50b0hhbGZOdW1iZXIoaWRzKTtcblx0XHRcdFx0XHRkZXNjID0gU3RyVXRpbC50b0Z1bGxOdW1iZXIoZGVzYyk7XG5cblx0XHRcdFx0XHRsZXQgbmFtZTogSVNwbGl0Q0JQYXJhbWV0ZXJzW1wibmFtZVwiXSA9IFtcblx0XHRcdFx0XHRcdGlkcyxcblx0XHRcdFx0XHRcdGRlc2MsXG5cdFx0XHRcdFx0XS5maWx0ZXIodiA9PiB2ICE9PSAnJykuam9pbihjKTtcblxuXHRcdFx0XHRcdF9kYXRhLm5hbWUgPSBuYW1lO1xuXHRcdFx0XHRcdF9kYXRhLmlkeCArPSBfZGF0YS5tX2xhc3QubWF0Y2gubGVuZ3RoO1xuXG5cdFx0XHRcdFx0cmV0dXJuIHRydWU7XG5cdFx0XHRcdH1cblxuXHRcdFx0fSxcblx0XHRcdGJhc2VDYlBhcnNlQ2hhcHRlck1hdGNoMDAzLFxuXHRcdF0sIHtcblx0XHRcdGZuQWZ0ZXIoX2RhdGE6IElTcGxpdENCUGFyYW1ldGVycylcblx0XHRcdHtcblx0XHRcdFx0X2RhdGEubmFtZSA9IGNuMnR3X21pbihfZGF0YS5uYW1lKTtcblx0XHRcdH0sXG5cdFx0fSksXG5cblx0fSxcblxufTtcblxuZXhwb3J0IGRlZmF1bHQgdHBsT3B0aW9uc1xuIl19