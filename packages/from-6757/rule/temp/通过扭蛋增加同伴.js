"use strict";
/**
 * Created by user on 2019/4/14.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const _000_1 = require("../base/000");
let v_line = `☠☠☠♛☠☠☠☠☠☠♛☠☠☠♛☠☠☠`;
let c_line = `❄✡✥✩✧✫✪✭☆★✬✰✮✦⟡✯☸✠❇✣`;
let _space = ' 　\\t';
const c = '　';
exports.tplOptions = {
    ..._000_1.default,
    volume: {
        ..._000_1.default.volume,
        disable: true,
    },
    /**
     * 這個參數是必填選項
     */
    chapter: {
        ..._000_1.default.chapter,
        r: [
            `^`,
            '(',
            [
                `\\d+`,
                `153(?=．暂缺)`
            ].join('|'),
            ')',
            `(?:`,
            `(?:[${_space}\\.]+|．(?=暂缺))`,
            `([^\\n]*|暂缺)`,
            ')?',
            `[${_space}]*`,
            `$`,
        ],
    },
};
exports.default = exports.tplOptions;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoi6YCa6L+H5omt6JuL5aKe5Yqg5ZCM5Ly0LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsi6YCa6L+H5omt6JuL5aKe5Yqg5ZCM5Ly0LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQTs7R0FFRzs7QUFhSCxzQ0FBeUM7QUFTekMsSUFBSSxNQUFNLEdBQUcsb0JBQW9CLENBQUM7QUFDbEMsSUFBSSxNQUFNLEdBQUcsc0JBQXNCLENBQUM7QUFFcEMsSUFBSSxNQUFNLEdBQUcsT0FBTyxDQUFDO0FBQ3JCLE1BQU0sQ0FBQyxHQUFHLEdBQUcsQ0FBQztBQUVELFFBQUEsVUFBVSxHQUF5QjtJQUUvQyxHQUFHLGNBQWM7SUFFakIsTUFBTSxFQUFFO1FBRVAsR0FBRyxjQUFjLENBQUMsTUFBTTtRQUV4QixPQUFPLEVBQUUsSUFBSTtLQUViO0lBRUQ7O09BRUc7SUFDSCxPQUFPLEVBQUU7UUFFUixHQUFHLGNBQWMsQ0FBQyxPQUFPO1FBRXpCLENBQUMsRUFBRTtZQUNGLEdBQUc7WUFDSCxHQUFHO1lBQ0g7Z0JBQ0MsTUFBTTtnQkFDTixZQUFZO2FBQ1osQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDO1lBQ1gsR0FBRztZQUNILEtBQUs7WUFDTCxPQUFPLE1BQU0sZ0JBQWdCO1lBQzdCLGNBQWM7WUFDZCxJQUFJO1lBQ0osSUFBSSxNQUFNLElBQUk7WUFDZCxHQUFHO1NBQ0g7S0FFRDtDQUVELENBQUM7QUFFRixrQkFBZSxrQkFBVSxDQUFBIiwic291cmNlc0NvbnRlbnQiOlsiLyoqXG4gKiBDcmVhdGVkIGJ5IHVzZXIgb24gMjAxOS80LzE0LlxuICovXG5cbmltcG9ydCB7IF9oYW5kbGVPcHRpb25zLCBtYWtlT3B0aW9ucyB9IGZyb20gJ0Bub2RlLW5vdmVsL3R4dC1zcGxpdC9saWIvaW5kZXgnO1xuaW1wb3J0IHtcblx0SU9wdGlvbnMsXG5cdElPcHRpb25zUmVxdWlyZWQsXG5cdElPcHRpb25zUmVxdWlyZWRVc2VyLFxuXHRJRGF0YVZvbHVtZSxcblx0SURhdGFDaGFwdGVyLFxuXHRJU3BsaXRDQlBhcmFtZXRlcnMsXG59IGZyb20gJ0Bub2RlLW5vdmVsL3R4dC1zcGxpdC9saWIvaW50ZXJmYWNlJztcblxuaW1wb3J0IHsgY29uc29sZSB9IGZyb20gJ0Bub2RlLW5vdmVsL3R4dC1zcGxpdC9saWIvY29uc29sZSc7XG5pbXBvcnQgdHBsQmFzZU9wdGlvbnMgZnJvbSAnLi4vYmFzZS8wMDAnO1xuaW1wb3J0IHtcblx0YmFzZUNiUGFyc2VDaGFwdGVyTWF0Y2gwMDEsXG5cdGJhc2VDYlBhcnNlQ2hhcHRlck1hdGNoMDAzLFxuXHRiYXNlQ2JQYXJzZUNoYXB0ZXJNYXRjaE1haW4wMDEsXG5cdGNyZWF0ZUNiUGFyc2VDaGFwdGVyTWF0Y2gsIElDcmVhdGVDYlBhcnNlQ2hhcHRlck1hdGNoT3B0aW9uc1N1Yixcbn0gZnJvbSAnLi4vbGliL3J1bGUnO1xuaW1wb3J0IFN0clV0aWwgPSByZXF1aXJlKCdzdHItdXRpbCcpO1xuXG5sZXQgdl9saW5lID0gYOKYoOKYoOKYoOKZm+KYoOKYoOKYoOKYoOKYoOKYoOKZm+KYoOKYoOKYoOKZm+KYoOKYoOKYoGA7XG5sZXQgY19saW5lID0gYOKdhOKcoeKcpeKcqeKcp+Kcq+KcquKcreKYhuKYheKcrOKcsOKcruKcpuKfoeKcr+KYuOKcoOKdh+Kco2A7XG5cbmxldCBfc3BhY2UgPSAnIOOAgFxcXFx0JztcbmNvbnN0IGMgPSAn44CAJztcblxuZXhwb3J0IGNvbnN0IHRwbE9wdGlvbnM6IElPcHRpb25zUmVxdWlyZWRVc2VyID0ge1xuXG5cdC4uLnRwbEJhc2VPcHRpb25zLFxuXG5cdHZvbHVtZToge1xuXG5cdFx0Li4udHBsQmFzZU9wdGlvbnMudm9sdW1lLFxuXG5cdFx0ZGlzYWJsZTogdHJ1ZSxcblxuXHR9LFxuXG5cdC8qKlxuXHQgKiDpgJnlgIvlj4PmlbjmmK/lv4XloavpgbjpoIVcblx0ICovXG5cdGNoYXB0ZXI6IHtcblxuXHRcdC4uLnRwbEJhc2VPcHRpb25zLmNoYXB0ZXIsXG5cblx0XHRyOiBbXG5cdFx0XHRgXmAsXG5cdFx0XHQnKCcsXG5cdFx0XHRbXG5cdFx0XHRcdGBcXFxcZCtgLFxuXHRcdFx0XHRgMTUzKD8977yO5pqC57y6KWBcblx0XHRcdF0uam9pbignfCcpLFxuXHRcdFx0JyknLFxuXHRcdFx0YCg/OmAsXG5cdFx0XHRgKD86WyR7X3NwYWNlfVxcXFwuXSt877yOKD895pqC57y6KSlgLFxuXHRcdFx0YChbXlxcXFxuXSp85pqC57y6KWAsXG5cdFx0XHQnKT8nLFxuXHRcdFx0YFske19zcGFjZX1dKmAsXG5cdFx0XHRgJGAsXG5cdFx0XSxcblxuXHR9LFxuXG59O1xuXG5leHBvcnQgZGVmYXVsdCB0cGxPcHRpb25zXG4iXX0=