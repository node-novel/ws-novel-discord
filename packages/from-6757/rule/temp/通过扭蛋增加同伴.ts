/**
 * Created by user on 2019/4/14.
 */

import { _handleOptions, makeOptions } from '@node-novel/txt-split/lib/index';
import {
	IOptions,
	IOptionsRequired,
	IOptionsRequiredUser,
	IDataVolume,
	IDataChapter,
	ISplitCBParameters,
} from '@node-novel/txt-split/lib/interface';

import { console } from '@node-novel/txt-split/lib/console';
import tplBaseOptions from '../base/000';
import {
	baseCbParseChapterMatch001,
	baseCbParseChapterMatch003,
	baseCbParseChapterMatchMain001,
	createCbParseChapterMatch, ICreateCbParseChapterMatchOptionsSub,
} from '../lib/rule';
import StrUtil = require('str-util');

let v_line = `☠☠☠♛☠☠☠☠☠☠♛☠☠☠♛☠☠☠`;
let c_line = `❄✡✥✩✧✫✪✭☆★✬✰✮✦⟡✯☸✠❇✣`;

let _space = ' 　\\t';
const c = '　';

export const tplOptions: IOptionsRequiredUser = {

	...tplBaseOptions,

	volume: {

		...tplBaseOptions.volume,

		disable: true,

	},

	/**
	 * 這個參數是必填選項
	 */
	chapter: {

		...tplBaseOptions.chapter,

		r: [
			`^`,
			'(',
			[
				`\\d+`,
				`153(?=．暂缺)`
			].join('|'),
			')',
			`(?:`,
			`(?:[${_space}\\.]+|．(?=暂缺))`,
			`([^\\n]*|暂缺)`,
			')?',
			`[${_space}]*`,
			`$`,
		],

	},

};

export default tplOptions
