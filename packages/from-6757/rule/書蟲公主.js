"use strict";
/**
 * Created by user on 2019/4/14.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const console_1 = require("@node-novel/txt-split/lib/console");
const base_1 = require("./lib/base");
const rule_1 = require("./lib/rule");
let v_line = `▒✬◕✬▒✬◕✬▒✬◕✬▒✬◕✬▒✬◕✬▒✬◕✬▒`;
let v_line2 = `▒✬◕✬▒✬◕✬▒✬◕✬▒✬◕✬▒✬◕✬▒✬◕✬▒`;
let c_line = `◭🍹◮✭◭🍹◮✭◭🍹◮✭◭🍹◮✭◭🍹◮✭◭🍹◮✭🍹`;
let c_line2 = ``;
exports.tplOptions = {
    ...base_1.default,
    /**
     * 這個參數 可刪除或加上 _ 如果沒有用到的話
     */
    volume: {
        /**
         * 故意放一個無效配對 實際使用時請自行更改
         *
         * 當沒有配對到的時候 會自動產生 00000_unknow 資料夾
         */
        r: [
            `${v_line}\n+`,
            '([^\\n]+)\n+',
            `${v_line2}\n+`,
        ],
        cb({ 
        /**
         * 於 match 列表中的 index 序列
         */
        i, 
        /**
         * 檔案序列(儲存檔案時會做為前置詞)
         */
        id, 
        /**
         * 標題名稱 預設情況下等於 match 到的標題
         */
        name, 
        /**
         * 本階段的 match 值
         */
        m, 
        /**
         * 上一次的 match 值
         *
         * 但是 實際上 這參數 才是本次 callback 真正的 match 內容
         */
        m_last, 
        /**
         * 目前已經分割的檔案列表與內容
         */
        _files, 
        /**
         * 於所有章節中的序列
         *
         * @readonly
         */
        ii, 
        /**
         * 本次 match 的 內文 start index
         * 可通過修改數值來控制內文範圍
         *
         * @example
         * idx += m_last.match.length; // 內文忽略本次 match 到的標題
         */
        idx, }) {
            /**
             * 依照你給的 regexp 內容來回傳的資料
             */
            if (m_last) {
                let { 
                /**
                 * 配對到的內容
                 */
                match, 
                /**
                 * 配對出來的陣列
                 */
                sub, } = m_last;
                0 && console_1.console.dir({
                    sub,
                    match,
                });
                /**
                 * @todo 這裡可以加上更多語法來格式化標題
                 */
                name = sub[0];
                /**
                 * 將定位點加上本次配對到的內容的長度
                 * 此步驟可以省略
                 * 但使用此步驟時可以同時在切割時對於內容作精簡
                 */
                idx += m_last.match.length;
            }
            return {
                /**
                 * 檔案序列(儲存檔案時會做為前置詞)
                 */
                id,
                /**
                 * 標題名稱 預設情況下等於 match 到的標題
                 */
                name,
                /**
                 * 本次 match 的 內文 start index
                 * 可通過修改數值來控制內文範圍
                 *
                 * @example
                 * idx += m_last.match.length; // 內文忽略本次 match 到的標題
                 */
                idx,
            };
        },
    },
    chapter: {
        ...base_1.default.chapter,
        r: [
            `${c_line}\\n`,
            '([^\\n]+)\n+',
            `${c_line2}\n`,
        ],
        cb: rule_1.baseCbParseChapterMatchMain001([
            rule_1.createCbParseChapterMatch({
                handleMatchSubIDString(argv, _data) {
                    return argv.ido;
                },
                handleMatchSubID(argv, _data) {
                    return argv;
                }
            }),
        ]),
    },
    saveFileBefore(txt, cn, data_vn, cache) {
        txt = txt
            .replace(/\n◭🍹◮✭◭🍹◮✭◭🍹◮✭◭🍹◮✭◭🍹◮✭◭🍹◮✭🍹\s*$/, '');
        // @ts-ignore
        return base_1.default.saveFileBefore(txt, cn, data_vn, cache);
    },
};
exports.default = exports.tplOptions;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoi5pu46J+y5YWs5Li7LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsi5pu46J+y5YWs5Li7LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQTs7R0FFRzs7QUFZSCwrREFBNEQ7QUFDNUQscUNBQXdDO0FBQ3hDLHFDQUFtSDtBQUVuSCxJQUFJLE1BQU0sR0FBRywyQkFBMkIsQ0FBQztBQUN6QyxJQUFJLE9BQU8sR0FBRywyQkFBMkIsQ0FBQztBQUMxQyxJQUFJLE1BQU0sR0FBRyxrQ0FBa0MsQ0FBQztBQUNoRCxJQUFJLE9BQU8sR0FBRyxFQUFFLENBQUM7QUFFSixRQUFBLFVBQVUsR0FBeUI7SUFFL0MsR0FBRyxjQUFjO0lBRWpCOztPQUVHO0lBQ0gsTUFBTSxFQUFFO1FBQ1A7Ozs7V0FJRztRQUNILENBQUMsRUFBRTtZQUNGLEdBQUcsTUFBTSxLQUFLO1lBQ2QsY0FBYztZQUNkLEdBQUcsT0FBTyxLQUFLO1NBQ2Y7UUFFRCxFQUFFLENBQUM7UUFDRjs7V0FFRztRQUNILENBQUM7UUFDRDs7V0FFRztRQUNILEVBQUU7UUFDRjs7V0FFRztRQUNILElBQUk7UUFDSjs7V0FFRztRQUNILENBQUM7UUFDRDs7OztXQUlHO1FBQ0gsTUFBTTtRQUNOOztXQUVHO1FBQ0gsTUFBTTtRQUNOOzs7O1dBSUc7UUFDSCxFQUFFO1FBQ0Y7Ozs7OztXQU1HO1FBQ0gsR0FBRyxHQUNIO1lBRUE7O2VBRUc7WUFDSCxJQUFJLE1BQU0sRUFDVjtnQkFDQyxJQUFJO2dCQUNIOzttQkFFRztnQkFDSCxLQUFLO2dCQUNMOzttQkFFRztnQkFDSCxHQUFHLEdBQ0gsR0FBRyxNQUFNLENBQUM7Z0JBRVgsQ0FBQyxJQUFJLGlCQUFPLENBQUMsR0FBRyxDQUFDO29CQUNoQixHQUFHO29CQUNILEtBQUs7aUJBQ0wsQ0FBQyxDQUFDO2dCQUVIOzttQkFFRztnQkFDSCxJQUFJLEdBQUcsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUVkOzs7O21CQUlHO2dCQUNILEdBQUcsSUFBSSxNQUFNLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQzthQUMzQjtZQUVELE9BQU87Z0JBQ047O21CQUVHO2dCQUNILEVBQUU7Z0JBQ0Y7O21CQUVHO2dCQUNILElBQUk7Z0JBQ0o7Ozs7OzttQkFNRztnQkFDSCxHQUFHO2FBQ0gsQ0FBQTtRQUNGLENBQUM7S0FDRDtJQUVELE9BQU8sRUFBRTtRQUVSLEdBQUcsY0FBYyxDQUFDLE9BQU87UUFFekIsQ0FBQyxFQUFFO1lBQ0YsR0FBRyxNQUFNLEtBQUs7WUFDZCxjQUFjO1lBQ2QsR0FBRyxPQUFPLElBQUk7U0FDZDtRQUVELEVBQUUsRUFBRSxxQ0FBOEIsQ0FBQztZQUVsQyxnQ0FBeUIsQ0FBQztnQkFFekIsc0JBQXNCLENBQUMsSUFBSSxFQUFFLEtBQUs7b0JBRWpDLE9BQU8sSUFBSSxDQUFDLEdBQUcsQ0FBQTtnQkFDaEIsQ0FBQztnQkFFRCxnQkFBZ0IsQ0FBQyxJQUFJLEVBQUUsS0FBSztvQkFFM0IsT0FBTyxJQUFJLENBQUE7Z0JBQ1osQ0FBQzthQUVELENBQUM7U0FFRixDQUFDO0tBRUY7SUFFRCxjQUFjLENBQUMsR0FBVyxFQUFFLEVBQVUsRUFBRSxPQUE2QixFQUFFLEtBQTJCO1FBRWpHLEdBQUcsR0FBRyxHQUFHO2FBQ1AsT0FBTyxDQUFDLHdDQUF3QyxFQUFFLEVBQUUsQ0FBQyxDQUN0RDtRQUVELGFBQWE7UUFDYixPQUFPLGNBQWMsQ0FBQyxjQUFjLENBQUMsR0FBRyxFQUFFLEVBQUUsRUFBRSxPQUFPLEVBQUUsS0FBSyxDQUFDLENBQUM7SUFDL0QsQ0FBQztDQUVELENBQUM7QUFFRixrQkFBZSxrQkFBVSxDQUFBIiwic291cmNlc0NvbnRlbnQiOlsiLyoqXG4gKiBDcmVhdGVkIGJ5IHVzZXIgb24gMjAxOS80LzE0LlxuICovXG5cbmltcG9ydCB7IF9oYW5kbGVPcHRpb25zLCBtYWtlT3B0aW9ucyB9IGZyb20gJ0Bub2RlLW5vdmVsL3R4dC1zcGxpdC9saWIvaW5kZXgnO1xuaW1wb3J0IHtcblx0SU9wdGlvbnMsXG5cdElPcHRpb25zUmVxdWlyZWQsXG5cdElPcHRpb25zUmVxdWlyZWRVc2VyLFxuXHRJRGF0YVZvbHVtZSxcblx0SURhdGFDaGFwdGVyLFxuXHRJU2F2ZUZpbGVCZWZvcmVDYWNoZSxcbn0gZnJvbSAnQG5vZGUtbm92ZWwvdHh0LXNwbGl0L2xpYi9pbnRlcmZhY2UnO1xuXG5pbXBvcnQgeyBjb25zb2xlIH0gZnJvbSAnQG5vZGUtbm92ZWwvdHh0LXNwbGl0L2xpYi9jb25zb2xlJztcbmltcG9ydCB0cGxCYXNlT3B0aW9ucyBmcm9tICcuL2xpYi9iYXNlJztcbmltcG9ydCB7IGJhc2VDYlBhcnNlQ2hhcHRlck1hdGNoMDA1LCBiYXNlQ2JQYXJzZUNoYXB0ZXJNYXRjaE1haW4wMDEsIGNyZWF0ZUNiUGFyc2VDaGFwdGVyTWF0Y2ggfSBmcm9tICcuL2xpYi9ydWxlJztcblxubGV0IHZfbGluZSA9IGDilpLinKzil5XinKzilpLinKzil5XinKzilpLinKzil5XinKzilpLinKzil5XinKzilpLinKzil5XinKzilpLinKzil5XinKzilpJgO1xubGV0IHZfbGluZTIgPSBg4paS4pys4peV4pys4paS4pys4peV4pys4paS4pys4peV4pys4paS4pys4peV4pys4paS4pys4peV4pys4paS4pys4peV4pys4paSYDtcbmxldCBjX2xpbmUgPSBg4pet8J+NueKXruKcreKXrfCfjbnil67inK3il63wn4254peu4pyt4pet8J+NueKXruKcreKXrfCfjbnil67inK3il63wn4254peu4pyt8J+NuWA7XG5sZXQgY19saW5lMiA9IGBgO1xuXG5leHBvcnQgY29uc3QgdHBsT3B0aW9uczogSU9wdGlvbnNSZXF1aXJlZFVzZXIgPSB7XG5cblx0Li4udHBsQmFzZU9wdGlvbnMsXG5cblx0LyoqXG5cdCAqIOmAmeWAi+WPg+aVuCDlj6/liKrpmaTmiJbliqDkuIogXyDlpoLmnpzmspLmnInnlKjliLDnmoToqbFcblx0ICovXG5cdHZvbHVtZToge1xuXHRcdC8qKlxuXHRcdCAqIOaVheaEj+aUvuS4gOWAi+eEoeaViOmFjeWwjSDlr6bpmpvkvb/nlKjmmYLoq4voh6rooYzmm7TmlLlcblx0XHQgKlxuXHRcdCAqIOeVtuaykuaciemFjeWwjeWIsOeahOaZguWAmSDmnIPoh6rli5XnlKLnlJ8gMDAwMDBfdW5rbm93IOizh+aWmeWkvlxuXHRcdCAqL1xuXHRcdHI6IFtcblx0XHRcdGAke3ZfbGluZX1cXG4rYCxcblx0XHRcdCcoW15cXFxcbl0rKVxcbisnLFxuXHRcdFx0YCR7dl9saW5lMn1cXG4rYCxcblx0XHRdLFxuXG5cdFx0Y2Ioe1xuXHRcdFx0LyoqXG5cdFx0XHQgKiDmlrwgbWF0Y2gg5YiX6KGo5Lit55qEIGluZGV4IOW6j+WIl1xuXHRcdFx0ICovXG5cdFx0XHRpLFxuXHRcdFx0LyoqXG5cdFx0XHQgKiDmqpTmoYjluo/liJco5YSy5a2Y5qqU5qGI5pmC5pyD5YGa54K65YmN572u6KmeKVxuXHRcdFx0ICovXG5cdFx0XHRpZCxcblx0XHRcdC8qKlxuXHRcdFx0ICog5qiZ6aGM5ZCN56ixIOmgkOioreaDheazgeS4i+etieaWvCBtYXRjaCDliLDnmoTmqJnpoYxcblx0XHRcdCAqL1xuXHRcdFx0bmFtZSxcblx0XHRcdC8qKlxuXHRcdFx0ICog5pys6ZqO5q6155qEIG1hdGNoIOWAvFxuXHRcdFx0ICovXG5cdFx0XHRtLFxuXHRcdFx0LyoqXG5cdFx0XHQgKiDkuIrkuIDmrKHnmoQgbWF0Y2gg5YC8XG5cdFx0XHQgKlxuXHRcdFx0ICog5L2G5pivIOWvpumam+S4iiDpgJnlj4Pmlbgg5omN5piv5pys5qyhIGNhbGxiYWNrIOecn+ato+eahCBtYXRjaCDlhaflrrlcblx0XHRcdCAqL1xuXHRcdFx0bV9sYXN0LFxuXHRcdFx0LyoqXG5cdFx0XHQgKiDnm67liY3lt7LntpPliIblibLnmoTmqpTmoYjliJfooajoiIflhaflrrlcblx0XHRcdCAqL1xuXHRcdFx0X2ZpbGVzLFxuXHRcdFx0LyoqXG5cdFx0XHQgKiDmlrzmiYDmnInnq6Dnr4DkuK3nmoTluo/liJdcblx0XHRcdCAqXG5cdFx0XHQgKiBAcmVhZG9ubHlcblx0XHRcdCAqL1xuXHRcdFx0aWksXG5cdFx0XHQvKipcblx0XHRcdCAqIOacrOasoSBtYXRjaCDnmoQg5YWn5paHIHN0YXJ0IGluZGV4XG5cdFx0XHQgKiDlj6/pgJrpgY7kv67mlLnmlbjlgLzkvobmjqfliLblhafmlofnr4TlnI1cblx0XHRcdCAqXG5cdFx0XHQgKiBAZXhhbXBsZVxuXHRcdFx0ICogaWR4ICs9IG1fbGFzdC5tYXRjaC5sZW5ndGg7IC8vIOWFp+aWh+W/veeVpeacrOasoSBtYXRjaCDliLDnmoTmqJnpoYxcblx0XHRcdCAqL1xuXHRcdFx0aWR4LFxuXHRcdH0pXG5cdFx0e1xuXHRcdFx0LyoqXG5cdFx0XHQgKiDkvp3nhafkvaDntabnmoQgcmVnZXhwIOWFp+WuueS+huWbnuWCs+eahOizh+aWmVxuXHRcdFx0ICovXG5cdFx0XHRpZiAobV9sYXN0KVxuXHRcdFx0e1xuXHRcdFx0XHRsZXQge1xuXHRcdFx0XHRcdC8qKlxuXHRcdFx0XHRcdCAqIOmFjeWwjeWIsOeahOWFp+WuuVxuXHRcdFx0XHRcdCAqL1xuXHRcdFx0XHRcdG1hdGNoLFxuXHRcdFx0XHRcdC8qKlxuXHRcdFx0XHRcdCAqIOmFjeWwjeWHuuS+hueahOmZo+WIl1xuXHRcdFx0XHRcdCAqL1xuXHRcdFx0XHRcdHN1Yixcblx0XHRcdFx0fSA9IG1fbGFzdDtcblxuXHRcdFx0XHQwICYmIGNvbnNvbGUuZGlyKHtcblx0XHRcdFx0XHRzdWIsXG5cdFx0XHRcdFx0bWF0Y2gsXG5cdFx0XHRcdH0pO1xuXG5cdFx0XHRcdC8qKlxuXHRcdFx0XHQgKiBAdG9kbyDpgJnoo6Hlj6/ku6XliqDkuIrmm7TlpJroqp7ms5XkvobmoLzlvI/ljJbmqJnpoYxcblx0XHRcdFx0ICovXG5cdFx0XHRcdG5hbWUgPSBzdWJbMF07XG5cblx0XHRcdFx0LyoqXG5cdFx0XHRcdCAqIOWwh+WumuS9jem7nuWKoOS4iuacrOasoemFjeWwjeWIsOeahOWFp+WuueeahOmVt+W6plxuXHRcdFx0XHQgKiDmraTmraXpqZ/lj6/ku6XnnIHnlaVcblx0XHRcdFx0ICog5L2G5L2/55So5q2k5q2l6amf5pmC5Y+v5Lul5ZCM5pmC5Zyo5YiH5Ymy5pmC5bCN5pa85YWn5a655L2c57K+57ChXG5cdFx0XHRcdCAqL1xuXHRcdFx0XHRpZHggKz0gbV9sYXN0Lm1hdGNoLmxlbmd0aDtcblx0XHRcdH1cblxuXHRcdFx0cmV0dXJuIHtcblx0XHRcdFx0LyoqXG5cdFx0XHRcdCAqIOaqlOahiOW6j+WIlyjlhLLlrZjmqpTmoYjmmYLmnIPlgZrngrrliY3nva7oqZ4pXG5cdFx0XHRcdCAqL1xuXHRcdFx0XHRpZCxcblx0XHRcdFx0LyoqXG5cdFx0XHRcdCAqIOaomemhjOWQjeeosSDpoJDoqK3mg4Xms4HkuIvnrYnmlrwgbWF0Y2gg5Yiw55qE5qiZ6aGMXG5cdFx0XHRcdCAqL1xuXHRcdFx0XHRuYW1lLFxuXHRcdFx0XHQvKipcblx0XHRcdFx0ICog5pys5qyhIG1hdGNoIOeahCDlhafmlocgc3RhcnQgaW5kZXhcblx0XHRcdFx0ICog5Y+v6YCa6YGO5L+u5pS55pW45YC85L6G5o6n5Yi25YWn5paH56+E5ZyNXG5cdFx0XHRcdCAqXG5cdFx0XHRcdCAqIEBleGFtcGxlXG5cdFx0XHRcdCAqIGlkeCArPSBtX2xhc3QubWF0Y2gubGVuZ3RoOyAvLyDlhafmloflv73nlaXmnKzmrKEgbWF0Y2gg5Yiw55qE5qiZ6aGMXG5cdFx0XHRcdCAqL1xuXHRcdFx0XHRpZHgsXG5cdFx0XHR9XG5cdFx0fSxcblx0fSxcblxuXHRjaGFwdGVyOiB7XG5cblx0XHQuLi50cGxCYXNlT3B0aW9ucy5jaGFwdGVyLFxuXG5cdFx0cjogW1xuXHRcdFx0YCR7Y19saW5lfVxcXFxuYCxcblx0XHRcdCcoW15cXFxcbl0rKVxcbisnLFxuXHRcdFx0YCR7Y19saW5lMn1cXG5gLFxuXHRcdF0sXG5cblx0XHRjYjogYmFzZUNiUGFyc2VDaGFwdGVyTWF0Y2hNYWluMDAxKFtcblxuXHRcdFx0Y3JlYXRlQ2JQYXJzZUNoYXB0ZXJNYXRjaCh7XG5cblx0XHRcdFx0aGFuZGxlTWF0Y2hTdWJJRFN0cmluZyhhcmd2LCBfZGF0YSlcblx0XHRcdFx0e1xuXHRcdFx0XHRcdHJldHVybiBhcmd2Lmlkb1xuXHRcdFx0XHR9LFxuXG5cdFx0XHRcdGhhbmRsZU1hdGNoU3ViSUQoYXJndiwgX2RhdGEpXG5cdFx0XHRcdHtcblx0XHRcdFx0XHRyZXR1cm4gYXJndlxuXHRcdFx0XHR9XG5cblx0XHRcdH0pLFxuXG5cdFx0XSksXG5cblx0fSxcblxuXHRzYXZlRmlsZUJlZm9yZSh0eHQ6IHN0cmluZywgY246IHN0cmluZywgZGF0YV92bjogSURhdGFDaGFwdGVyPHN0cmluZz4sIGNhY2hlOiBJU2F2ZUZpbGVCZWZvcmVDYWNoZSlcblx0e1xuXHRcdHR4dCA9IHR4dFxuXHRcdFx0LnJlcGxhY2UoL1xcbuKXrfCfjbnil67inK3il63wn4254peu4pyt4pet8J+NueKXruKcreKXrfCfjbnil67inK3il63wn4254peu4pyt4pet8J+NueKXruKcrfCfjblcXHMqJC8sICcnKVxuXHRcdDtcblxuXHRcdC8vIEB0cy1pZ25vcmVcblx0XHRyZXR1cm4gdHBsQmFzZU9wdGlvbnMuc2F2ZUZpbGVCZWZvcmUodHh0LCBjbiwgZGF0YV92biwgY2FjaGUpO1xuXHR9LFxuXG59O1xuXG5leHBvcnQgZGVmYXVsdCB0cGxPcHRpb25zXG4iXX0=