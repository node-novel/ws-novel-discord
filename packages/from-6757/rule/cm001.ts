/**
 * Created by user on 2019/4/14.
 */

import { _handleOptions, makeOptions } from '@node-novel/txt-split/lib/index';
import {
	IOptions,
	IOptionsRequired,
	IOptionsRequiredUser,
	IDataVolume,
	IDataChapter,
	ISplitCBParameters,
} from '@node-novel/txt-split/lib/interface';

import { console } from '@node-novel/txt-split/lib/console';
import tplBaseOptions from './base/話_000';
import {
	baseCbParseChapterMatch001,
	baseCbParseChapterMatch003,
	baseCbParseChapterMatchMain001, createCbParseChapterMatch,
} from './lib/rule';
import novelText from 'novel-text';
import StrUtil = require('str-util');
import { tw2cn_min, cn2tw_min, tableCn2TwDebug, tableTw2CnDebug } from 'cjk-conv/lib/zh/convert/min';
import { trimDesc } from './lib/index';
import { handleMatchSubIDString000 } from './lib/rule/handleMatchSubIDString';
import { tplBaseOptions as tplBaseOptionsCore } from './lib/base';

let v_line = `☠☠☠♛☠☠☠☠☠☠♛☠☠☠♛☠☠☠`;
let c_line = `❄✡✥✩✧✫✪✭☆★✬✰✮✦⟡✯☸✠❇✣`;

let _space = ' 　\\t';
const c = '　';

export const tplOptions: IOptionsRequiredUser = {

	...tplBaseOptions,

	volume: {
		...tplBaseOptions.volume,

		disable: true,

		r: [
			`^`,
			'(',
			[
				'第二部',
			].join('|'),
			')',
			`(?:`,
			//`[${_space}]+`,
			//`([^\\n]*)`,
			')?',
			//`[${_space}]*`,
			//`$`,
		],

	},

	/**
	 * 這個參數是必填選項
	 */
	chapter: {

		...tplBaseOptions.chapter,

		ignoreRe: null,

		r: [
			`^`,
			`[${_space}]*`,
			`(?:=\\s+(?:第.章|序章)\\s+)`,
			'(',
			[
				'\\d+',
			].join('|'),
			')',
			`(?:`,
			`[${_space}.]`,
			`([^\\n]*)`,
			')?',
			`[${_space}]*`,
			`=`,
			`$`,
		],

		cb: baseCbParseChapterMatchMain001([
			createCbParseChapterMatch({
				handleMatchSubIDString: handleMatchSubIDString000,
			})
		], {

		}),

	},

	saveFileBefore(txt, ...argv) {
		//@ts-ignore
		txt = tplBaseOptionsCore.saveFileBefore(txt, ...argv);

		if (typeof txt === 'string')
		{
			txt = txt
				.replace(/\s+\d+-完\s*$/, '')
				.replace(/^\s*本帖最後由\s+[^\n]+\+*編輯/, '')
			;

			// @ts-ignore
			txt = tplBaseOptionsCore.saveFileBefore(txt, ...argv);
		}

		return txt;
	}

};

export default tplOptions
