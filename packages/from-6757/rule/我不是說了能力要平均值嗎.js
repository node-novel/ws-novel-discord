"use strict";
/**
 * Created by user on 2019/4/14.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const ___1 = require("./base/\u7AE0_\u8A71");
const rule_1 = require("./lib/rule");
const StrUtil = require("str-util");
let v_line = `☠☠☠♛☠☠☠☠☠☠♛☠☠☠♛☠☠☠`;
let c_line = `❄✡✥✩✧✫✪✭☆★✬✰✮✦⟡✯☸✠❇✣`;
let _space = ' 　\\t';
const c = '　';
exports.tplOptions = {
    ...___1.default,
    volume: {
        ...___1.default.volume,
        disable: true,
    },
    /**
     * 這個參數是必填選項
     */
    chapter: {
        ...___1.default.chapter,
        r: [
            `^`,
            '(',
            [
                '序章',
                '(?:幕間|終章)',
                `第[\\d０-９]+(?:\.[\\d０-９]+)?(?:話)`,
            ].join('|'),
            ')',
            `(?:`,
            `[${_space}\\.]+`,
            `([^\\n]*)`,
            ')?',
            `[${_space}]*`,
            `$`,
        ],
        cb: rule_1.baseCbParseChapterMatchMain001([
            rule_1.createCbParseChapterMatch({
                handleMatchSubID(argv, _data) {
                    if (/^\d+(?:\.\d+)?$/.test(argv.id)) {
                        argv.id = String(argv.id)
                            .replace(/^\d+/, function (s) {
                            return s.padStart(3, '0');
                        });
                        argv.idn = StrUtil.toHalfNumber(argv.id.toString());
                    }
                    return argv;
                },
            }),
        ]),
    },
};
exports.default = exports.tplOptions;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoi5oiR5LiN5piv6Kqq5LqG6IO95Yqb6KaB5bmz5Z2H5YC85ZeOLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsi5oiR5LiN5piv6Kqq5LqG6IO95Yqb6KaB5bmz5Z2H5YC85ZeOLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQTs7R0FFRzs7QUFhSCw2Q0FBd0M7QUFDeEMscUNBS29CO0FBQ3BCLG9DQUFxQztBQUVyQyxJQUFJLE1BQU0sR0FBRyxvQkFBb0IsQ0FBQztBQUNsQyxJQUFJLE1BQU0sR0FBRyxzQkFBc0IsQ0FBQztBQUVwQyxJQUFJLE1BQU0sR0FBRyxPQUFPLENBQUM7QUFDckIsTUFBTSxDQUFDLEdBQUcsR0FBRyxDQUFDO0FBRUQsUUFBQSxVQUFVLEdBQXlCO0lBRS9DLEdBQUcsWUFBYztJQUVqQixNQUFNLEVBQUU7UUFFUCxHQUFHLFlBQWMsQ0FBQyxNQUFNO1FBRXhCLE9BQU8sRUFBRSxJQUFJO0tBRWI7SUFFRDs7T0FFRztJQUNILE9BQU8sRUFBRTtRQUVSLEdBQUcsWUFBYyxDQUFDLE9BQU87UUFFekIsQ0FBQyxFQUFFO1lBQ0YsR0FBRztZQUNILEdBQUc7WUFDSDtnQkFDQyxJQUFJO2dCQUNKLFdBQVc7Z0JBQ1gsaUNBQWlDO2FBQ2pDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQztZQUNYLEdBQUc7WUFDSCxLQUFLO1lBQ0wsSUFBSSxNQUFNLE9BQU87WUFDakIsV0FBVztZQUNYLElBQUk7WUFDSixJQUFJLE1BQU0sSUFBSTtZQUNkLEdBQUc7U0FDSDtRQUVELEVBQUUsRUFBRSxxQ0FBOEIsQ0FBQztZQUNsQyxnQ0FBeUIsQ0FBQztnQkFFekIsZ0JBQWdCLENBQUMsSUFBMEMsRUFBRSxLQUF5QjtvQkFFckYsSUFBSSxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxFQUNuQzt3QkFFQyxJQUFJLENBQUMsRUFBRSxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDOzZCQUN2QixPQUFPLENBQUMsTUFBTSxFQUFFLFVBQVUsQ0FBQzs0QkFFM0IsT0FBTyxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQzt3QkFDM0IsQ0FBQyxDQUFDLENBQ0Y7d0JBRUQsSUFBSSxDQUFDLEdBQUcsR0FBRyxPQUFPLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQztxQkFDcEQ7b0JBRUQsT0FBTyxJQUFJLENBQUM7Z0JBQ2IsQ0FBQzthQUVELENBQUM7U0FDRixDQUFDO0tBRUY7Q0FFRCxDQUFDO0FBRUYsa0JBQWUsa0JBQVUsQ0FBQSIsInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogQ3JlYXRlZCBieSB1c2VyIG9uIDIwMTkvNC8xNC5cbiAqL1xuXG5pbXBvcnQgeyBfaGFuZGxlT3B0aW9ucywgbWFrZU9wdGlvbnMgfSBmcm9tICdAbm9kZS1ub3ZlbC90eHQtc3BsaXQvbGliL2luZGV4JztcbmltcG9ydCB7XG5cdElPcHRpb25zLFxuXHRJT3B0aW9uc1JlcXVpcmVkLFxuXHRJT3B0aW9uc1JlcXVpcmVkVXNlcixcblx0SURhdGFWb2x1bWUsXG5cdElEYXRhQ2hhcHRlcixcblx0SVNwbGl0Q0JQYXJhbWV0ZXJzLFxufSBmcm9tICdAbm9kZS1ub3ZlbC90eHQtc3BsaXQvbGliL2ludGVyZmFjZSc7XG5cbmltcG9ydCB7IGNvbnNvbGUgfSBmcm9tICdAbm9kZS1ub3ZlbC90eHQtc3BsaXQvbGliL2NvbnNvbGUnO1xuaW1wb3J0IHRwbEJhc2VPcHRpb25zIGZyb20gJy4vYmFzZS/nq6Bf6KmxJztcbmltcG9ydCB7XG5cdGJhc2VDYlBhcnNlQ2hhcHRlck1hdGNoMDAxLFxuXHRiYXNlQ2JQYXJzZUNoYXB0ZXJNYXRjaDAwMyxcblx0YmFzZUNiUGFyc2VDaGFwdGVyTWF0Y2hNYWluMDAxLFxuXHRjcmVhdGVDYlBhcnNlQ2hhcHRlck1hdGNoLCBJQ3JlYXRlQ2JQYXJzZUNoYXB0ZXJNYXRjaE9wdGlvbnNTdWIsXG59IGZyb20gJy4vbGliL3J1bGUnO1xuaW1wb3J0IFN0clV0aWwgPSByZXF1aXJlKCdzdHItdXRpbCcpO1xuXG5sZXQgdl9saW5lID0gYOKYoOKYoOKYoOKZm+KYoOKYoOKYoOKYoOKYoOKYoOKZm+KYoOKYoOKYoOKZm+KYoOKYoOKYoGA7XG5sZXQgY19saW5lID0gYOKdhOKcoeKcpeKcqeKcp+Kcq+KcquKcreKYhuKYheKcrOKcsOKcruKcpuKfoeKcr+KYuOKcoOKdh+Kco2A7XG5cbmxldCBfc3BhY2UgPSAnIOOAgFxcXFx0JztcbmNvbnN0IGMgPSAn44CAJztcblxuZXhwb3J0IGNvbnN0IHRwbE9wdGlvbnM6IElPcHRpb25zUmVxdWlyZWRVc2VyID0ge1xuXG5cdC4uLnRwbEJhc2VPcHRpb25zLFxuXG5cdHZvbHVtZToge1xuXG5cdFx0Li4udHBsQmFzZU9wdGlvbnMudm9sdW1lLFxuXG5cdFx0ZGlzYWJsZTogdHJ1ZSxcblxuXHR9LFxuXG5cdC8qKlxuXHQgKiDpgJnlgIvlj4PmlbjmmK/lv4XloavpgbjpoIVcblx0ICovXG5cdGNoYXB0ZXI6IHtcblxuXHRcdC4uLnRwbEJhc2VPcHRpb25zLmNoYXB0ZXIsXG5cblx0XHRyOiBbXG5cdFx0XHRgXmAsXG5cdFx0XHQnKCcsXG5cdFx0XHRbXG5cdFx0XHRcdCfluo/nq6AnLFxuXHRcdFx0XHQnKD865bmV6ZaTfOe1gueroCknLFxuXHRcdFx0XHRg56ysW1xcXFxk77yQLe+8mV0rKD86XFwuW1xcXFxk77yQLe+8mV0rKT8oPzroqbEpYCxcblx0XHRcdF0uam9pbignfCcpLFxuXHRcdFx0JyknLFxuXHRcdFx0YCg/OmAsXG5cdFx0XHRgWyR7X3NwYWNlfVxcXFwuXStgLFxuXHRcdFx0YChbXlxcXFxuXSopYCxcblx0XHRcdCcpPycsXG5cdFx0XHRgWyR7X3NwYWNlfV0qYCxcblx0XHRcdGAkYCxcblx0XHRdLFxuXG5cdFx0Y2I6IGJhc2VDYlBhcnNlQ2hhcHRlck1hdGNoTWFpbjAwMShbXG5cdFx0XHRjcmVhdGVDYlBhcnNlQ2hhcHRlck1hdGNoKHtcblxuXHRcdFx0XHRoYW5kbGVNYXRjaFN1YklEKGFyZ3Y6IElDcmVhdGVDYlBhcnNlQ2hhcHRlck1hdGNoT3B0aW9uc1N1YiwgX2RhdGE6IElTcGxpdENCUGFyYW1ldGVycylcblx0XHRcdFx0e1xuXHRcdFx0XHRcdGlmICgvXlxcZCsoPzpcXC5cXGQrKT8kLy50ZXN0KGFyZ3YuaWQpKVxuXHRcdFx0XHRcdHtcblxuXHRcdFx0XHRcdFx0YXJndi5pZCA9IFN0cmluZyhhcmd2LmlkKVxuXHRcdFx0XHRcdFx0XHQucmVwbGFjZSgvXlxcZCsvLCBmdW5jdGlvbiAocylcblx0XHRcdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0XHRcdHJldHVybiBzLnBhZFN0YXJ0KDMsICcwJyk7XG5cdFx0XHRcdFx0XHRcdH0pXG5cdFx0XHRcdFx0XHQ7XG5cblx0XHRcdFx0XHRcdGFyZ3YuaWRuID0gU3RyVXRpbC50b0hhbGZOdW1iZXIoYXJndi5pZC50b1N0cmluZygpKTtcblx0XHRcdFx0XHR9XG5cblx0XHRcdFx0XHRyZXR1cm4gYXJndjtcblx0XHRcdFx0fSxcblxuXHRcdFx0fSksXG5cdFx0XSksXG5cblx0fSxcblxufTtcblxuZXhwb3J0IGRlZmF1bHQgdHBsT3B0aW9uc1xuIl19