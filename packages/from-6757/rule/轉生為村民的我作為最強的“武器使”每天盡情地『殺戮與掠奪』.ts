/**
 * Created by user on 2019/4/14.
 */

import { _handleOptions, makeOptions } from '@node-novel/txt-split/lib/index';
import { IOptions, IOptionsRequired, IOptionsRequiredUser, IDataVolume, IDataChapter } from '@node-novel/txt-split/lib/interface';

import { console } from '@node-novel/txt-split/lib/console';
import tplBaseOptions from './base/話_000';
import { baseCbParseChapterMatch001, baseCbParseChapterMatch003, baseCbParseChapterMatchMain001 } from './lib/rule';

let v_line = `☠☠☠♛☠☠☠☠☠☠♛☠☠☠♛☠☠☠`;
let c_line = `❄✡✥✩✧✫✪✭☆★✬✰✮✦⟡✯☸✠❇✣`;

let _space = ' 　\\t';
const c = '　';

export const tplOptions: IOptionsRequiredUser = {

	...tplBaseOptions,

	volume: {
		...tplBaseOptions.volume,

		disable: false,


	},

	/**
	 * 這個參數是必填選項
	 */
	chapter: {

		...tplBaseOptions.chapter,

	},



};

export default tplOptions
