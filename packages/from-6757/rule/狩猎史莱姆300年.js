"use strict";
/**
 * Created by user on 2019/4/14.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const console_1 = require("@node-novel/txt-split/lib/console");
const ___1 = require("./base/\u7AE0_\u8A71");
const rule_1 = require("./lib/rule");
const StrUtil = require("str-util");
let v_line = `☠☠☠♛☠☠☠☠☠☠♛☠☠☠♛☠☠☠`;
let c_line = `❄✡✥✩✧✫✪✭☆★✬✰✮✦⟡✯☸✠❇✣`;
let _space = ' 　\\t';
const c = '　';
exports.tplOptions = {
    ...___1.default,
    volume: {
        ...___1.default.volume,
        r: [
            `(?<=[。…！？]|\\n|^)`,
            '(',
            [
                `第[一-十]+章`,
                `第[\\d０-９]+章`,
                `外傳`,
                `參觀桃子節篇`,
            ].join('|'),
            ')',
            `[${_space}]+`,
            `([^\\n]*)`,
            `[${_space}]*`,
            ``,
        ],
        //ignoreRe: /第[\d０-９]+章\s+[^\n]+之畫家/i,
        ignoreCb(argv) {
            //console.dir(argv.m_last.sub);
            if (/^第\s*[\d０-９]+\s*章$/.test(argv.m_last.sub[0])) {
                if (!argv.m_last.sub[1]) {
                    console_1.console.debug(`[skip]`, argv.m_last.sub);
                    return true;
                }
                else {
                    //console.dir(argv.m_last.sub);
                }
            }
            else {
                //console.dir(argv.m_last.sub);
            }
            return false;
        },
    },
    /**
     * 這個參數是必填選項
     */
    chapter: {
        ...___1.default.chapter,
        r: [
            `^`,
            '(',
            [
                '序章',
                '(?:幕間|終章)',
                `第[\\d０-９]+(?:\.[\\d０-９]+)?(?:話)`,
            ].join('|'),
            ')',
            `(?:`,
            `[${_space}\\.]+`,
            `([^\\n]*)`,
            ')?',
            `[${_space}]*`,
            `$`,
        ],
        cb: rule_1.baseCbParseChapterMatchMain001([
            rule_1.createCbParseChapterMatch({
                handleMatchSubID(argv, _data) {
                    if (/^\d+(?:\.\d+)?$/.test(argv.id)) {
                        argv.id = String(argv.id)
                            .replace(/^\d+/, function (s) {
                            return s.padStart(3, '0');
                        });
                        argv.idn = StrUtil.toHalfNumber(argv.id.toString());
                    }
                    return argv;
                },
            }),
        ]),
    },
};
exports.default = exports.tplOptions;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoi54up54yO5Y+y6I6x5aeGMzAw5bm0LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsi54up54yO5Y+y6I6x5aeGMzAw5bm0LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQTs7R0FFRzs7QUFZSCwrREFBNEQ7QUFDNUQsNkNBQXdDO0FBQ3hDLHFDQUtvQjtBQUNwQixvQ0FBcUM7QUFFckMsSUFBSSxNQUFNLEdBQUcsb0JBQW9CLENBQUM7QUFDbEMsSUFBSSxNQUFNLEdBQUcsc0JBQXNCLENBQUM7QUFFcEMsSUFBSSxNQUFNLEdBQUcsT0FBTyxDQUFDO0FBQ3JCLE1BQU0sQ0FBQyxHQUFHLEdBQUcsQ0FBQztBQUVELFFBQUEsVUFBVSxHQUF5QjtJQUUvQyxHQUFHLFlBQWM7SUFFakIsTUFBTSxFQUFFO1FBRVAsR0FBRyxZQUFjLENBQUMsTUFBTTtRQUV4QixDQUFDLEVBQUU7WUFDRixtQkFBbUI7WUFDbkIsR0FBRztZQUNIO2dCQUNDLFVBQVU7Z0JBQ1YsYUFBYTtnQkFDYixJQUFJO2dCQUNKLFFBQVE7YUFDUixDQUFDLElBQUksQ0FBQyxHQUFHLENBQUM7WUFDWCxHQUFHO1lBQ0gsSUFBSSxNQUFNLElBQUk7WUFDZCxXQUFXO1lBQ1gsSUFBSSxNQUFNLElBQUk7WUFDZCxFQUFFO1NBQ0Y7UUFFRCxzQ0FBc0M7UUFFdEMsUUFBUSxDQUFDLElBQUk7WUFHWiwrQkFBK0I7WUFFL0IsSUFBSSxvQkFBb0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFDakQ7Z0JBQ0MsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxFQUN2QjtvQkFDQyxpQkFBTyxDQUFDLEtBQUssQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQztvQkFFekMsT0FBTyxJQUFJLENBQUM7aUJBQ1o7cUJBRUQ7b0JBQ0MsK0JBQStCO2lCQUMvQjthQUNEO2lCQUVEO2dCQUNDLCtCQUErQjthQUMvQjtZQUVELE9BQU8sS0FBSyxDQUFBO1FBQ2IsQ0FBQztLQUVEO0lBRUQ7O09BRUc7SUFDSCxPQUFPLEVBQUU7UUFFUixHQUFHLFlBQWMsQ0FBQyxPQUFPO1FBRXpCLENBQUMsRUFBRTtZQUNGLEdBQUc7WUFDSCxHQUFHO1lBQ0g7Z0JBQ0MsSUFBSTtnQkFDSixXQUFXO2dCQUNYLGlDQUFpQzthQUNqQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUM7WUFDWCxHQUFHO1lBQ0gsS0FBSztZQUNMLElBQUksTUFBTSxPQUFPO1lBQ2pCLFdBQVc7WUFDWCxJQUFJO1lBQ0osSUFBSSxNQUFNLElBQUk7WUFDZCxHQUFHO1NBQ0g7UUFFRCxFQUFFLEVBQUUscUNBQThCLENBQUM7WUFDbEMsZ0NBQXlCLENBQUM7Z0JBRXpCLGdCQUFnQixDQUFDLElBQTBDLEVBQUUsS0FBeUI7b0JBRXJGLElBQUksaUJBQWlCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsRUFDbkM7d0JBRUMsSUFBSSxDQUFDLEVBQUUsR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQzs2QkFDdkIsT0FBTyxDQUFDLE1BQU0sRUFBRSxVQUFVLENBQUM7NEJBRTNCLE9BQU8sQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUM7d0JBQzNCLENBQUMsQ0FBQyxDQUNGO3dCQUVELElBQUksQ0FBQyxHQUFHLEdBQUcsT0FBTyxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUM7cUJBQ3BEO29CQUVELE9BQU8sSUFBSSxDQUFDO2dCQUNiLENBQUM7YUFFRCxDQUFDO1NBQ0YsQ0FBQztLQUVGO0NBRUQsQ0FBQztBQUVGLGtCQUFlLGtCQUFVLENBQUEiLCJzb3VyY2VzQ29udGVudCI6WyIvKipcbiAqIENyZWF0ZWQgYnkgdXNlciBvbiAyMDE5LzQvMTQuXG4gKi9cblxuaW1wb3J0IHsgX2hhbmRsZU9wdGlvbnMsIG1ha2VPcHRpb25zIH0gZnJvbSAnQG5vZGUtbm92ZWwvdHh0LXNwbGl0L2xpYi9pbmRleCc7XG5pbXBvcnQge1xuXHRJT3B0aW9ucyxcblx0SU9wdGlvbnNSZXF1aXJlZCxcblx0SU9wdGlvbnNSZXF1aXJlZFVzZXIsXG5cdElEYXRhVm9sdW1lLFxuXHRJRGF0YUNoYXB0ZXIsXG5cdElTcGxpdENCUGFyYW1ldGVycyxcbn0gZnJvbSAnQG5vZGUtbm92ZWwvdHh0LXNwbGl0L2xpYi9pbnRlcmZhY2UnO1xuXG5pbXBvcnQgeyBjb25zb2xlIH0gZnJvbSAnQG5vZGUtbm92ZWwvdHh0LXNwbGl0L2xpYi9jb25zb2xlJztcbmltcG9ydCB0cGxCYXNlT3B0aW9ucyBmcm9tICcuL2Jhc2Uv56ugX+ipsSc7XG5pbXBvcnQge1xuXHRiYXNlQ2JQYXJzZUNoYXB0ZXJNYXRjaDAwMSxcblx0YmFzZUNiUGFyc2VDaGFwdGVyTWF0Y2gwMDMsXG5cdGJhc2VDYlBhcnNlQ2hhcHRlck1hdGNoTWFpbjAwMSxcblx0Y3JlYXRlQ2JQYXJzZUNoYXB0ZXJNYXRjaCwgSUNyZWF0ZUNiUGFyc2VDaGFwdGVyTWF0Y2hPcHRpb25zU3ViLFxufSBmcm9tICcuL2xpYi9ydWxlJztcbmltcG9ydCBTdHJVdGlsID0gcmVxdWlyZSgnc3RyLXV0aWwnKTtcblxubGV0IHZfbGluZSA9IGDimKDimKDimKDimZvimKDimKDimKDimKDimKDimKDimZvimKDimKDimKDimZvimKDimKDimKBgO1xubGV0IGNfbGluZSA9IGDinYTinKHinKXinKninKfinKvinKrinK3imIbimIXinKzinLDinK7inKbin6HinK/imLjinKDinYfinKNgO1xuXG5sZXQgX3NwYWNlID0gJyDjgIBcXFxcdCc7XG5jb25zdCBjID0gJ+OAgCc7XG5cbmV4cG9ydCBjb25zdCB0cGxPcHRpb25zOiBJT3B0aW9uc1JlcXVpcmVkVXNlciA9IHtcblxuXHQuLi50cGxCYXNlT3B0aW9ucyxcblxuXHR2b2x1bWU6IHtcblxuXHRcdC4uLnRwbEJhc2VPcHRpb25zLnZvbHVtZSxcblxuXHRcdHI6IFtcblx0XHRcdGAoPzw9W+OAguKApu+8ge+8n118XFxcXG58XilgLFxuXHRcdFx0JygnLFxuXHRcdFx0W1xuXHRcdFx0XHRg56ysW+S4gC3ljYFdK+eroGAsXG5cdFx0XHRcdGDnrKxbXFxcXGTvvJAt77yZXSvnq6BgLFxuXHRcdFx0XHRg5aSW5YKzYCxcblx0XHRcdFx0YOWPg+ingOahg+WtkOevgOevh2AsXG5cdFx0XHRdLmpvaW4oJ3wnKSxcblx0XHRcdCcpJyxcblx0XHRcdGBbJHtfc3BhY2V9XStgLFxuXHRcdFx0YChbXlxcXFxuXSopYCxcblx0XHRcdGBbJHtfc3BhY2V9XSpgLFxuXHRcdFx0YGAsXG5cdFx0XSxcblxuXHRcdC8vaWdub3JlUmU6IC/nrKxbXFxk77yQLe+8mV0r56ugXFxzK1teXFxuXSvkuYvnlavlrrYvaSxcblxuXHRcdGlnbm9yZUNiKGFyZ3YpXG5cdFx0e1xuXG5cdFx0XHQvL2NvbnNvbGUuZGlyKGFyZ3YubV9sYXN0LnN1Yik7XG5cblx0XHRcdGlmICgvXuesrFxccypbXFxk77yQLe+8mV0rXFxzKueroCQvLnRlc3QoYXJndi5tX2xhc3Quc3ViWzBdKSlcblx0XHRcdHtcblx0XHRcdFx0aWYgKCFhcmd2Lm1fbGFzdC5zdWJbMV0pXG5cdFx0XHRcdHtcblx0XHRcdFx0XHRjb25zb2xlLmRlYnVnKGBbc2tpcF1gLCBhcmd2Lm1fbGFzdC5zdWIpO1xuXG5cdFx0XHRcdFx0cmV0dXJuIHRydWU7XG5cdFx0XHRcdH1cblx0XHRcdFx0ZWxzZVxuXHRcdFx0XHR7XG5cdFx0XHRcdFx0Ly9jb25zb2xlLmRpcihhcmd2Lm1fbGFzdC5zdWIpO1xuXHRcdFx0XHR9XG5cdFx0XHR9XG5cdFx0XHRlbHNlXG5cdFx0XHR7XG5cdFx0XHRcdC8vY29uc29sZS5kaXIoYXJndi5tX2xhc3Quc3ViKTtcblx0XHRcdH1cblxuXHRcdFx0cmV0dXJuIGZhbHNlXG5cdFx0fSxcblxuXHR9LFxuXG5cdC8qKlxuXHQgKiDpgJnlgIvlj4PmlbjmmK/lv4XloavpgbjpoIVcblx0ICovXG5cdGNoYXB0ZXI6IHtcblxuXHRcdC4uLnRwbEJhc2VPcHRpb25zLmNoYXB0ZXIsXG5cblx0XHRyOiBbXG5cdFx0XHRgXmAsXG5cdFx0XHQnKCcsXG5cdFx0XHRbXG5cdFx0XHRcdCfluo/nq6AnLFxuXHRcdFx0XHQnKD865bmV6ZaTfOe1gueroCknLFxuXHRcdFx0XHRg56ysW1xcXFxk77yQLe+8mV0rKD86XFwuW1xcXFxk77yQLe+8mV0rKT8oPzroqbEpYCxcblx0XHRcdF0uam9pbignfCcpLFxuXHRcdFx0JyknLFxuXHRcdFx0YCg/OmAsXG5cdFx0XHRgWyR7X3NwYWNlfVxcXFwuXStgLFxuXHRcdFx0YChbXlxcXFxuXSopYCxcblx0XHRcdCcpPycsXG5cdFx0XHRgWyR7X3NwYWNlfV0qYCxcblx0XHRcdGAkYCxcblx0XHRdLFxuXG5cdFx0Y2I6IGJhc2VDYlBhcnNlQ2hhcHRlck1hdGNoTWFpbjAwMShbXG5cdFx0XHRjcmVhdGVDYlBhcnNlQ2hhcHRlck1hdGNoKHtcblxuXHRcdFx0XHRoYW5kbGVNYXRjaFN1YklEKGFyZ3Y6IElDcmVhdGVDYlBhcnNlQ2hhcHRlck1hdGNoT3B0aW9uc1N1YiwgX2RhdGE6IElTcGxpdENCUGFyYW1ldGVycylcblx0XHRcdFx0e1xuXHRcdFx0XHRcdGlmICgvXlxcZCsoPzpcXC5cXGQrKT8kLy50ZXN0KGFyZ3YuaWQpKVxuXHRcdFx0XHRcdHtcblxuXHRcdFx0XHRcdFx0YXJndi5pZCA9IFN0cmluZyhhcmd2LmlkKVxuXHRcdFx0XHRcdFx0XHQucmVwbGFjZSgvXlxcZCsvLCBmdW5jdGlvbiAocylcblx0XHRcdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0XHRcdHJldHVybiBzLnBhZFN0YXJ0KDMsICcwJyk7XG5cdFx0XHRcdFx0XHRcdH0pXG5cdFx0XHRcdFx0XHQ7XG5cblx0XHRcdFx0XHRcdGFyZ3YuaWRuID0gU3RyVXRpbC50b0hhbGZOdW1iZXIoYXJndi5pZC50b1N0cmluZygpKTtcblx0XHRcdFx0XHR9XG5cblx0XHRcdFx0XHRyZXR1cm4gYXJndjtcblx0XHRcdFx0fSxcblxuXHRcdFx0fSksXG5cdFx0XSksXG5cblx0fSxcblxufTtcblxuZXhwb3J0IGRlZmF1bHQgdHBsT3B0aW9uc1xuIl19