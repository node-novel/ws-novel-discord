/**
 * Created by user on 2019/4/14.
 */

import { _handleOptions, makeOptions } from '@node-novel/txt-split/lib/index';
import { IOptions, IOptionsRequired, IOptionsRequiredUser, IDataVolume, IDataChapter } from '@node-novel/txt-split/lib/interface';

import { console } from '@node-novel/txt-split/lib/console';
import tplBaseOptions from './lib/base';
import { baseCbParseChapterMatch005, baseCbParseChapterMatchMain001 } from './lib/rule';

let v_line = `✟✠✠✟✠✠✟✠✠✟✠✠✟✠✠✟✠✠✟✠✠✟`;
let v_line2 = `✟✠✠✟✠✠✟✠✠✟✠✠✟✠✠✟✠✠✟✠✠✟`;
let c_line = `🔥🎲💧🎲🔥🎲💧🎲🔥🎲💧🎲🔥🎲💧🎲🔥🎲💧`;
let c_line2 = `🔥🎲💧🎲🔥🎲💧🎲🔥🎲💧🎲🔥🎲💧🎲🔥🎲💧`;

export const tplOptions: IOptionsRequiredUser = {

	...tplBaseOptions,

	/**
	 * 這個參數 可刪除或加上 _ 如果沒有用到的話
	 */
	volume: {
		/**
		 * 故意放一個無效配對 實際使用時請自行更改
		 *
		 * 當沒有配對到的時候 會自動產生 00000_unknow 資料夾
		 */
		r: [
			`${v_line}\n`,
			'([^\\n]+)\n',
			`${v_line2}\n+`,
		],

		cb({
			/**
			 * 於 match 列表中的 index 序列
			 */
			i,
			/**
			 * 檔案序列(儲存檔案時會做為前置詞)
			 */
			id,
			/**
			 * 標題名稱 預設情況下等於 match 到的標題
			 */
			name,
			/**
			 * 本階段的 match 值
			 */
			m,
			/**
			 * 上一次的 match 值
			 *
			 * 但是 實際上 這參數 才是本次 callback 真正的 match 內容
			 */
			m_last,
			/**
			 * 目前已經分割的檔案列表與內容
			 */
			_files,
			/**
			 * 於所有章節中的序列
			 *
			 * @readonly
			 */
			ii,
			/**
			 * 本次 match 的 內文 start index
			 * 可通過修改數值來控制內文範圍
			 *
			 * @example
			 * idx += m_last.match.length; // 內文忽略本次 match 到的標題
			 */
			idx,
		})
		{
			/**
			 * 依照你給的 regexp 內容來回傳的資料
			 */
			if (m_last)
			{
				let {
					/**
					 * 配對到的內容
					 */
					match,
					/**
					 * 配對出來的陣列
					 */
					sub,
				} = m_last;

				0 && console.dir({
					sub,
					match,
				});

				/**
				 * @todo 這裡可以加上更多語法來格式化標題
				 */
				name = sub[0];

				/**
				 * 將定位點加上本次配對到的內容的長度
				 * 此步驟可以省略
				 * 但使用此步驟時可以同時在切割時對於內容作精簡
				 */
				idx += m_last.match.length;
			}

			return {
				/**
				 * 檔案序列(儲存檔案時會做為前置詞)
				 */
				id,
				/**
				 * 標題名稱 預設情況下等於 match 到的標題
				 */
				name,
				/**
				 * 本次 match 的 內文 start index
				 * 可通過修改數值來控制內文範圍
				 *
				 * @example
				 * idx += m_last.match.length; // 內文忽略本次 match 到的標題
				 */
				idx,
			}
		},
	},

	chapter: {

		...tplBaseOptions.chapter,

		r: [
			`${c_line}\\n`,
			'(',
			[
				'(?:幕間|終章)',
				`第[\\d０-９]+(?:\.[\\d０-９]+)?(?:話)`,
				`第[一-十]+(?:話)`,
			].join('|'),
			')?',
			`(?:`,
			`([^\\n]+?)`,
			')',
			`\\n${c_line2}\\n`,
		],

		cb: baseCbParseChapterMatchMain001([

			baseCbParseChapterMatch005,

		]),

	},

};

export default tplOptions
