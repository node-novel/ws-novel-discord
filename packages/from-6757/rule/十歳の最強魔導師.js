"use strict";
/**
 * Created by user on 2019/4/14.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const __000_1 = require("./base/\u8A71_000");
let v_line = `☠☠☠♛☠☠☠☠☠☠♛☠☠☠♛☠☠☠`;
let c_line = `❄✡✥✩✧✫✪✭☆★✬✰✮✦⟡✯☸✠❇✣`;
let _space = ' 　\\t';
const c = '　';
exports.tplOptions = {
    ...__000_1.default,
    volume: null,
    /**
     * 這個參數是必填選項
     */
    chapter: {
        ...__000_1.default.chapter,
        r: [
            `^`,
            //`[${_space}]*`,
            '(',
            [
                `第[\\d０-９]+(?:話)`,
            ].join('|'),
            ')',
            `[${_space}]+`,
            `([^\\n]*)`,
            `[${_space}]*`,
            `$`,
        ],
    },
};
exports.default = exports.tplOptions;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoi5Y2B5q2z44Gu5pyA5by36a2U5bCO5birLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsi5Y2B5q2z44Gu5pyA5by36a2U5bCO5birLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQTs7R0FFRzs7QUFNSCw2Q0FBMEM7QUFFMUMsSUFBSSxNQUFNLEdBQUcsb0JBQW9CLENBQUM7QUFDbEMsSUFBSSxNQUFNLEdBQUcsc0JBQXNCLENBQUM7QUFFcEMsSUFBSSxNQUFNLEdBQUcsT0FBTyxDQUFDO0FBQ3JCLE1BQU0sQ0FBQyxHQUFHLEdBQUcsQ0FBQztBQUVELFFBQUEsVUFBVSxHQUF5QjtJQUUvQyxHQUFHLGVBQWM7SUFFakIsTUFBTSxFQUFFLElBQUk7SUFFWjs7T0FFRztJQUNILE9BQU8sRUFBRTtRQUVSLEdBQUcsZUFBYyxDQUFDLE9BQU87UUFFekIsQ0FBQyxFQUFFO1lBQ0YsR0FBRztZQUNILGlCQUFpQjtZQUNqQixHQUFHO1lBQ0g7Z0JBQ0MsaUJBQWlCO2FBQ2pCLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQztZQUNYLEdBQUc7WUFDSCxJQUFJLE1BQU0sSUFBSTtZQUNkLFdBQVc7WUFDWCxJQUFJLE1BQU0sSUFBSTtZQUNkLEdBQUc7U0FDSDtLQUVEO0NBRUQsQ0FBQztBQUVGLGtCQUFlLGtCQUFVLENBQUEiLCJzb3VyY2VzQ29udGVudCI6WyIvKipcbiAqIENyZWF0ZWQgYnkgdXNlciBvbiAyMDE5LzQvMTQuXG4gKi9cblxuaW1wb3J0IHsgX2hhbmRsZU9wdGlvbnMsIG1ha2VPcHRpb25zIH0gZnJvbSAnQG5vZGUtbm92ZWwvdHh0LXNwbGl0L2xpYi9pbmRleCc7XG5pbXBvcnQgeyBJT3B0aW9ucywgSU9wdGlvbnNSZXF1aXJlZCwgSU9wdGlvbnNSZXF1aXJlZFVzZXIsIElEYXRhVm9sdW1lLCBJRGF0YUNoYXB0ZXIgfSBmcm9tICdAbm9kZS1ub3ZlbC90eHQtc3BsaXQvbGliL2ludGVyZmFjZSc7XG5cbmltcG9ydCB7IGNvbnNvbGUgfSBmcm9tICdAbm9kZS1ub3ZlbC90eHQtc3BsaXQvbGliL2NvbnNvbGUnO1xuaW1wb3J0IHRwbEJhc2VPcHRpb25zIGZyb20gJy4vYmFzZS/oqbFfMDAwJztcblxubGV0IHZfbGluZSA9IGDimKDimKDimKDimZvimKDimKDimKDimKDimKDimKDimZvimKDimKDimKDimZvimKDimKDimKBgO1xubGV0IGNfbGluZSA9IGDinYTinKHinKXinKninKfinKvinKrinK3imIbimIXinKzinLDinK7inKbin6HinK/imLjinKDinYfinKNgO1xuXG5sZXQgX3NwYWNlID0gJyDjgIBcXFxcdCc7XG5jb25zdCBjID0gJ+OAgCc7XG5cbmV4cG9ydCBjb25zdCB0cGxPcHRpb25zOiBJT3B0aW9uc1JlcXVpcmVkVXNlciA9IHtcblxuXHQuLi50cGxCYXNlT3B0aW9ucyxcblxuXHR2b2x1bWU6IG51bGwsXG5cblx0LyoqXG5cdCAqIOmAmeWAi+WPg+aVuOaYr+W/heWhq+mBuOmghVxuXHQgKi9cblx0Y2hhcHRlcjoge1xuXG5cdFx0Li4udHBsQmFzZU9wdGlvbnMuY2hhcHRlcixcblxuXHRcdHI6IFtcblx0XHRcdGBeYCxcblx0XHRcdC8vYFske19zcGFjZX1dKmAsXG5cdFx0XHQnKCcsXG5cdFx0XHRbXG5cdFx0XHRcdGDnrKxbXFxcXGTvvJAt77yZXSsoPzroqbEpYCxcblx0XHRcdF0uam9pbignfCcpLFxuXHRcdFx0JyknLFxuXHRcdFx0YFske19zcGFjZX1dK2AsXG5cdFx0XHRgKFteXFxcXG5dKilgLFxuXHRcdFx0YFske19zcGFjZX1dKmAsXG5cdFx0XHRgJGAsXG5cdFx0XSxcblxuXHR9LFxuXG59O1xuXG5leHBvcnQgZGVmYXVsdCB0cGxPcHRpb25zXG4iXX0=