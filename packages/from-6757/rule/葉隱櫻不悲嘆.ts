/**
 * Created by user on 2019/4/14.
 */

import { _handleOptions, makeOptions } from '@node-novel/txt-split/lib/index';
import {
	IOptions,
	IOptionsRequired,
	IOptionsRequiredUser,
	IDataVolume,
	IDataChapter,
	ISplitCBParameters,
} from '@node-novel/txt-split/lib/interface';

import { console } from '@node-novel/txt-split/lib/console';
import tplBaseOptions from './base/章_話';
import {
	baseCbParseChapterMatch001,
	baseCbParseChapterMatch003,
	baseCbParseChapterMatchMain001,
	createCbParseChapterMatch, ICreateCbParseChapterMatchOptionsSub,
} from './lib/rule';
import StrUtil = require('str-util');

let v_line = `▞▦▦▦▚▦▦▦▞▦▦▦▚▦▦▦▞▦▦▦▚▦▦▦▞`;
let v_line2 = v_line;
let c_line = `❤❤ᾪ❤❤ᾪ❤❤ᾪ❤❤ᾪ❤❤ᾪ❤❤ᾪ❤❤ᾪ`;
let c_line2 = c_line;

let _space = ' 　\\t';
const c = '　';

export const tplOptions: IOptionsRequiredUser = {

	...tplBaseOptions,

	volume: {

		...tplBaseOptions.volume,

		r: [
			`${v_line}\\n`,
			'(',
			[
				`第[一-十]+章`,
				`第[\\d０-９]+章`,
			].join('|'),
			')',
			`[${_space}\\-]*`,
			`([^\\n]*)`,
			`\\n${v_line2}\\n`,
		],

		cb: baseCbParseChapterMatchMain001([

			createCbParseChapterMatch({

				handleMatchSubIDString(argv, _data)
				{
					let ids: string;

					if (argv.idn)
					{
						ids = `第${argv.idn}章`;
					}
					else
					{
						ids = argv.ido;
					}

					return ids
				},

				handleMatchSubID(argv, _data)
				{
					if (/^\d+(?:\.\d+)?$/.test(argv.id))
					{
						argv.id = String(argv.id);

						argv.idn = StrUtil.toFullNumber(argv.id.toString());
					}

					return argv
				}

			}),

		]),

	},

	/**
	 * 這個參數是必填選項
	 */
	chapter: {

		...tplBaseOptions.chapter,

		r: [
			`${c_line}\\n`,
			'(',
			[
				'(?:幕間|終章)',
				`第[\\d０-９]+(?:\.[\\d０-９]+)?(?:話)`,
				`第[一-十]+(?:話)`,
			].join('|'),
			')?',
			`(?:`,
			`([^\\n]+?)`,
			')',
			`\\n${c_line2}\\n`,
		],

		cb: baseCbParseChapterMatchMain001([

			baseCbParseChapterMatch003,

		]),

	},

};

export default tplOptions
