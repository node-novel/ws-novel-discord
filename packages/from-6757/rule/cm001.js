"use strict";
/**
 * Created by user on 2019/4/14.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const __000_1 = require("./base/\u8A71_000");
const rule_1 = require("./lib/rule");
const handleMatchSubIDString_1 = require("./lib/rule/handleMatchSubIDString");
const base_1 = require("./lib/base");
let v_line = `☠☠☠♛☠☠☠☠☠☠♛☠☠☠♛☠☠☠`;
let c_line = `❄✡✥✩✧✫✪✭☆★✬✰✮✦⟡✯☸✠❇✣`;
let _space = ' 　\\t';
const c = '　';
exports.tplOptions = {
    ...__000_1.default,
    volume: {
        ...__000_1.default.volume,
        disable: true,
        r: [
            `^`,
            '(',
            [
                '第二部',
            ].join('|'),
            ')',
            `(?:`,
            //`[${_space}]+`,
            //`([^\\n]*)`,
            ')?',
        ],
    },
    /**
     * 這個參數是必填選項
     */
    chapter: {
        ...__000_1.default.chapter,
        ignoreRe: null,
        r: [
            `^`,
            `[${_space}]*`,
            `(?:=\\s+(?:第.章|序章)\\s+)`,
            '(',
            [
                '\\d+',
            ].join('|'),
            ')',
            `(?:`,
            `[${_space}.]`,
            `([^\\n]*)`,
            ')?',
            `[${_space}]*`,
            `=`,
            `$`,
        ],
        cb: rule_1.baseCbParseChapterMatchMain001([
            rule_1.createCbParseChapterMatch({
                handleMatchSubIDString: handleMatchSubIDString_1.handleMatchSubIDString000,
            })
        ], {}),
    },
    saveFileBefore(txt, ...argv) {
        //@ts-ignore
        txt = base_1.tplBaseOptions.saveFileBefore(txt, ...argv);
        if (typeof txt === 'string') {
            txt = txt
                .replace(/\s+\d+-完\s*$/, '')
                .replace(/^\s*本帖最後由\s+[^\n]+\+*編輯/, '');
            // @ts-ignore
            txt = base_1.tplBaseOptions.saveFileBefore(txt, ...argv);
        }
        return txt;
    }
};
exports.default = exports.tplOptions;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY20wMDEuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJjbTAwMS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUE7O0dBRUc7O0FBYUgsNkNBQTBDO0FBQzFDLHFDQUlvQjtBQUtwQiw4RUFBOEU7QUFDOUUscUNBQWtFO0FBRWxFLElBQUksTUFBTSxHQUFHLG9CQUFvQixDQUFDO0FBQ2xDLElBQUksTUFBTSxHQUFHLHNCQUFzQixDQUFDO0FBRXBDLElBQUksTUFBTSxHQUFHLE9BQU8sQ0FBQztBQUNyQixNQUFNLENBQUMsR0FBRyxHQUFHLENBQUM7QUFFRCxRQUFBLFVBQVUsR0FBeUI7SUFFL0MsR0FBRyxlQUFjO0lBRWpCLE1BQU0sRUFBRTtRQUNQLEdBQUcsZUFBYyxDQUFDLE1BQU07UUFFeEIsT0FBTyxFQUFFLElBQUk7UUFFYixDQUFDLEVBQUU7WUFDRixHQUFHO1lBQ0gsR0FBRztZQUNIO2dCQUNDLEtBQUs7YUFDTCxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUM7WUFDWCxHQUFHO1lBQ0gsS0FBSztZQUNMLGlCQUFpQjtZQUNqQixjQUFjO1lBQ2QsSUFBSTtTQUdKO0tBRUQ7SUFFRDs7T0FFRztJQUNILE9BQU8sRUFBRTtRQUVSLEdBQUcsZUFBYyxDQUFDLE9BQU87UUFFekIsUUFBUSxFQUFFLElBQUk7UUFFZCxDQUFDLEVBQUU7WUFDRixHQUFHO1lBQ0gsSUFBSSxNQUFNLElBQUk7WUFDZCx5QkFBeUI7WUFDekIsR0FBRztZQUNIO2dCQUNDLE1BQU07YUFDTixDQUFDLElBQUksQ0FBQyxHQUFHLENBQUM7WUFDWCxHQUFHO1lBQ0gsS0FBSztZQUNMLElBQUksTUFBTSxJQUFJO1lBQ2QsV0FBVztZQUNYLElBQUk7WUFDSixJQUFJLE1BQU0sSUFBSTtZQUNkLEdBQUc7WUFDSCxHQUFHO1NBQ0g7UUFFRCxFQUFFLEVBQUUscUNBQThCLENBQUM7WUFDbEMsZ0NBQXlCLENBQUM7Z0JBQ3pCLHNCQUFzQixFQUFFLGtEQUF5QjthQUNqRCxDQUFDO1NBQ0YsRUFBRSxFQUVGLENBQUM7S0FFRjtJQUVELGNBQWMsQ0FBQyxHQUFHLEVBQUUsR0FBRyxJQUFJO1FBQzFCLFlBQVk7UUFDWixHQUFHLEdBQUcscUJBQWtCLENBQUMsY0FBYyxDQUFDLEdBQUcsRUFBRSxHQUFHLElBQUksQ0FBQyxDQUFDO1FBRXRELElBQUksT0FBTyxHQUFHLEtBQUssUUFBUSxFQUMzQjtZQUNDLEdBQUcsR0FBRyxHQUFHO2lCQUNQLE9BQU8sQ0FBQyxjQUFjLEVBQUUsRUFBRSxDQUFDO2lCQUMzQixPQUFPLENBQUMseUJBQXlCLEVBQUUsRUFBRSxDQUFDLENBQ3ZDO1lBRUQsYUFBYTtZQUNiLEdBQUcsR0FBRyxxQkFBa0IsQ0FBQyxjQUFjLENBQUMsR0FBRyxFQUFFLEdBQUcsSUFBSSxDQUFDLENBQUM7U0FDdEQ7UUFFRCxPQUFPLEdBQUcsQ0FBQztJQUNaLENBQUM7Q0FFRCxDQUFDO0FBRUYsa0JBQWUsa0JBQVUsQ0FBQSIsInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogQ3JlYXRlZCBieSB1c2VyIG9uIDIwMTkvNC8xNC5cbiAqL1xuXG5pbXBvcnQgeyBfaGFuZGxlT3B0aW9ucywgbWFrZU9wdGlvbnMgfSBmcm9tICdAbm9kZS1ub3ZlbC90eHQtc3BsaXQvbGliL2luZGV4JztcbmltcG9ydCB7XG5cdElPcHRpb25zLFxuXHRJT3B0aW9uc1JlcXVpcmVkLFxuXHRJT3B0aW9uc1JlcXVpcmVkVXNlcixcblx0SURhdGFWb2x1bWUsXG5cdElEYXRhQ2hhcHRlcixcblx0SVNwbGl0Q0JQYXJhbWV0ZXJzLFxufSBmcm9tICdAbm9kZS1ub3ZlbC90eHQtc3BsaXQvbGliL2ludGVyZmFjZSc7XG5cbmltcG9ydCB7IGNvbnNvbGUgfSBmcm9tICdAbm9kZS1ub3ZlbC90eHQtc3BsaXQvbGliL2NvbnNvbGUnO1xuaW1wb3J0IHRwbEJhc2VPcHRpb25zIGZyb20gJy4vYmFzZS/oqbFfMDAwJztcbmltcG9ydCB7XG5cdGJhc2VDYlBhcnNlQ2hhcHRlck1hdGNoMDAxLFxuXHRiYXNlQ2JQYXJzZUNoYXB0ZXJNYXRjaDAwMyxcblx0YmFzZUNiUGFyc2VDaGFwdGVyTWF0Y2hNYWluMDAxLCBjcmVhdGVDYlBhcnNlQ2hhcHRlck1hdGNoLFxufSBmcm9tICcuL2xpYi9ydWxlJztcbmltcG9ydCBub3ZlbFRleHQgZnJvbSAnbm92ZWwtdGV4dCc7XG5pbXBvcnQgU3RyVXRpbCA9IHJlcXVpcmUoJ3N0ci11dGlsJyk7XG5pbXBvcnQgeyB0dzJjbl9taW4sIGNuMnR3X21pbiwgdGFibGVDbjJUd0RlYnVnLCB0YWJsZVR3MkNuRGVidWcgfSBmcm9tICdjamstY29udi9saWIvemgvY29udmVydC9taW4nO1xuaW1wb3J0IHsgdHJpbURlc2MgfSBmcm9tICcuL2xpYi9pbmRleCc7XG5pbXBvcnQgeyBoYW5kbGVNYXRjaFN1YklEU3RyaW5nMDAwIH0gZnJvbSAnLi9saWIvcnVsZS9oYW5kbGVNYXRjaFN1YklEU3RyaW5nJztcbmltcG9ydCB7IHRwbEJhc2VPcHRpb25zIGFzIHRwbEJhc2VPcHRpb25zQ29yZSB9IGZyb20gJy4vbGliL2Jhc2UnO1xuXG5sZXQgdl9saW5lID0gYOKYoOKYoOKYoOKZm+KYoOKYoOKYoOKYoOKYoOKYoOKZm+KYoOKYoOKYoOKZm+KYoOKYoOKYoGA7XG5sZXQgY19saW5lID0gYOKdhOKcoeKcpeKcqeKcp+Kcq+KcquKcreKYhuKYheKcrOKcsOKcruKcpuKfoeKcr+KYuOKcoOKdh+Kco2A7XG5cbmxldCBfc3BhY2UgPSAnIOOAgFxcXFx0JztcbmNvbnN0IGMgPSAn44CAJztcblxuZXhwb3J0IGNvbnN0IHRwbE9wdGlvbnM6IElPcHRpb25zUmVxdWlyZWRVc2VyID0ge1xuXG5cdC4uLnRwbEJhc2VPcHRpb25zLFxuXG5cdHZvbHVtZToge1xuXHRcdC4uLnRwbEJhc2VPcHRpb25zLnZvbHVtZSxcblxuXHRcdGRpc2FibGU6IHRydWUsXG5cblx0XHRyOiBbXG5cdFx0XHRgXmAsXG5cdFx0XHQnKCcsXG5cdFx0XHRbXG5cdFx0XHRcdCfnrKzkuozpg6gnLFxuXHRcdFx0XS5qb2luKCd8JyksXG5cdFx0XHQnKScsXG5cdFx0XHRgKD86YCxcblx0XHRcdC8vYFske19zcGFjZX1dK2AsXG5cdFx0XHQvL2AoW15cXFxcbl0qKWAsXG5cdFx0XHQnKT8nLFxuXHRcdFx0Ly9gWyR7X3NwYWNlfV0qYCxcblx0XHRcdC8vYCRgLFxuXHRcdF0sXG5cblx0fSxcblxuXHQvKipcblx0ICog6YCZ5YCL5Y+D5pW45piv5b+F5aGr6YG46aCFXG5cdCAqL1xuXHRjaGFwdGVyOiB7XG5cblx0XHQuLi50cGxCYXNlT3B0aW9ucy5jaGFwdGVyLFxuXG5cdFx0aWdub3JlUmU6IG51bGwsXG5cblx0XHRyOiBbXG5cdFx0XHRgXmAsXG5cdFx0XHRgWyR7X3NwYWNlfV0qYCxcblx0XHRcdGAoPzo9XFxcXHMrKD8656ysLueroHzluo/nq6ApXFxcXHMrKWAsXG5cdFx0XHQnKCcsXG5cdFx0XHRbXG5cdFx0XHRcdCdcXFxcZCsnLFxuXHRcdFx0XS5qb2luKCd8JyksXG5cdFx0XHQnKScsXG5cdFx0XHRgKD86YCxcblx0XHRcdGBbJHtfc3BhY2V9Ll1gLFxuXHRcdFx0YChbXlxcXFxuXSopYCxcblx0XHRcdCcpPycsXG5cdFx0XHRgWyR7X3NwYWNlfV0qYCxcblx0XHRcdGA9YCxcblx0XHRcdGAkYCxcblx0XHRdLFxuXG5cdFx0Y2I6IGJhc2VDYlBhcnNlQ2hhcHRlck1hdGNoTWFpbjAwMShbXG5cdFx0XHRjcmVhdGVDYlBhcnNlQ2hhcHRlck1hdGNoKHtcblx0XHRcdFx0aGFuZGxlTWF0Y2hTdWJJRFN0cmluZzogaGFuZGxlTWF0Y2hTdWJJRFN0cmluZzAwMCxcblx0XHRcdH0pXG5cdFx0XSwge1xuXG5cdFx0fSksXG5cblx0fSxcblxuXHRzYXZlRmlsZUJlZm9yZSh0eHQsIC4uLmFyZ3YpIHtcblx0XHQvL0B0cy1pZ25vcmVcblx0XHR0eHQgPSB0cGxCYXNlT3B0aW9uc0NvcmUuc2F2ZUZpbGVCZWZvcmUodHh0LCAuLi5hcmd2KTtcblxuXHRcdGlmICh0eXBlb2YgdHh0ID09PSAnc3RyaW5nJylcblx0XHR7XG5cdFx0XHR0eHQgPSB0eHRcblx0XHRcdFx0LnJlcGxhY2UoL1xccytcXGQrLeWujFxccyokLywgJycpXG5cdFx0XHRcdC5yZXBsYWNlKC9eXFxzKuacrOW4luacgOW+jOeUsVxccytbXlxcbl0rXFwrKue3qOi8ry8sICcnKVxuXHRcdFx0O1xuXG5cdFx0XHQvLyBAdHMtaWdub3JlXG5cdFx0XHR0eHQgPSB0cGxCYXNlT3B0aW9uc0NvcmUuc2F2ZUZpbGVCZWZvcmUodHh0LCAuLi5hcmd2KTtcblx0XHR9XG5cblx0XHRyZXR1cm4gdHh0O1xuXHR9XG5cbn07XG5cbmV4cG9ydCBkZWZhdWx0IHRwbE9wdGlvbnNcbiJdfQ==