"use strict";
/**
 * Created by user on 2019/4/14.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const console_1 = require("@node-novel/txt-split/lib/console");
const base_1 = require("./lib/base");
const rule_1 = require("./lib/rule");
let v_line = `✟✠✠✟✠✠✟✠✠✟✠✠✟✠✠✟✠✠✟✠✠✟`;
let v_line2 = `✟✠✠✟✠✠✟✠✠✟✠✠✟✠✠✟✠✠✟✠✠✟`;
let c_line = `🍶💙🔥💙🍶💙🔥💙🍶💙🔥💙🍶💙🔥💙🍶💙🔥💙🍶`;
let c_line2 = `🍶💙🔥💙🍶💙🔥💙🍶💙🔥💙🍶💙🔥💙🍶💙🔥💙🍶`;
let _space = ' 　\\t';
exports.tplOptions = {
    ...base_1.default,
    /**
     * 這個參數 可刪除或加上 _ 如果沒有用到的話
     */
    volume: {
        disable: true,
        /**
         * 故意放一個無效配對 實際使用時請自行更改
         *
         * 當沒有配對到的時候 會自動產生 00000_unknow 資料夾
         */
        r: [
            `${v_line}\n`,
            '([^\\n]+)\n',
            `${v_line2}\n+`,
        ],
        cb({ 
        /**
         * 於 match 列表中的 index 序列
         */
        i, 
        /**
         * 檔案序列(儲存檔案時會做為前置詞)
         */
        id, 
        /**
         * 標題名稱 預設情況下等於 match 到的標題
         */
        name, 
        /**
         * 本階段的 match 值
         */
        m, 
        /**
         * 上一次的 match 值
         *
         * 但是 實際上 這參數 才是本次 callback 真正的 match 內容
         */
        m_last, 
        /**
         * 目前已經分割的檔案列表與內容
         */
        _files, 
        /**
         * 於所有章節中的序列
         *
         * @readonly
         */
        ii, 
        /**
         * 本次 match 的 內文 start index
         * 可通過修改數值來控制內文範圍
         *
         * @example
         * idx += m_last.match.length; // 內文忽略本次 match 到的標題
         */
        idx, }) {
            /**
             * 依照你給的 regexp 內容來回傳的資料
             */
            if (m_last) {
                let { 
                /**
                 * 配對到的內容
                 */
                match, 
                /**
                 * 配對出來的陣列
                 */
                sub, } = m_last;
                0 && console_1.console.dir({
                    sub,
                    match,
                });
                /**
                 * @todo 這裡可以加上更多語法來格式化標題
                 */
                name = sub[0];
                /**
                 * 將定位點加上本次配對到的內容的長度
                 * 此步驟可以省略
                 * 但使用此步驟時可以同時在切割時對於內容作精簡
                 */
                idx += m_last.match.length;
            }
            return {
                /**
                 * 檔案序列(儲存檔案時會做為前置詞)
                 */
                id,
                /**
                 * 標題名稱 預設情況下等於 match 到的標題
                 */
                name,
                /**
                 * 本次 match 的 內文 start index
                 * 可通過修改數值來控制內文範圍
                 *
                 * @example
                 * idx += m_last.match.length; // 內文忽略本次 match 到的標題
                 */
                idx,
            };
        },
    },
    chapter: {
        ...base_1.default.chapter,
        r: [
            ``,
            `${c_line}\n`,
            '(',
            [
                `第[\\d０-９]+(?:話)`,
            ].join('|'),
            ')',
            `(?:`,
            `[${_space}\\.]+`,
            `([^\\n]*)`,
            ')?',
            `\n${c_line2}\n`,
            ``,
        ],
        cb: rule_1.baseCbParseChapterMatchMain001([
            rule_1.baseCbParseChapterMatch005,
        ]),
    },
};
exports.default = exports.tplOptions;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoi5Y2B5q2y55qE5pyA5by36a2U5bCO5birdjIuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyLljYHmrbLnmoTmnIDlvLfprZTlsI7luKt2Mi50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUE7O0dBRUc7O0FBS0gsK0RBQTREO0FBQzVELHFDQUF3QztBQUN4QyxxQ0FBd0Y7QUFFeEYsSUFBSSxNQUFNLEdBQUcsd0JBQXdCLENBQUM7QUFDdEMsSUFBSSxPQUFPLEdBQUcsd0JBQXdCLENBQUM7QUFDdkMsSUFBSSxNQUFNLEdBQUcsNENBQTRDLENBQUM7QUFDMUQsSUFBSSxPQUFPLEdBQUcsNENBQTRDLENBQUM7QUFFM0QsSUFBSSxNQUFNLEdBQUcsT0FBTyxDQUFDO0FBRVIsUUFBQSxVQUFVLEdBQXlCO0lBRS9DLEdBQUcsY0FBYztJQUVqQjs7T0FFRztJQUNILE1BQU0sRUFBRTtRQUVQLE9BQU8sRUFBRSxJQUFJO1FBRWI7Ozs7V0FJRztRQUNILENBQUMsRUFBRTtZQUNGLEdBQUcsTUFBTSxJQUFJO1lBQ2IsYUFBYTtZQUNiLEdBQUcsT0FBTyxLQUFLO1NBQ2Y7UUFFRCxFQUFFLENBQUM7UUFDRjs7V0FFRztRQUNILENBQUM7UUFDRDs7V0FFRztRQUNILEVBQUU7UUFDRjs7V0FFRztRQUNILElBQUk7UUFDSjs7V0FFRztRQUNILENBQUM7UUFDRDs7OztXQUlHO1FBQ0gsTUFBTTtRQUNOOztXQUVHO1FBQ0gsTUFBTTtRQUNOOzs7O1dBSUc7UUFDSCxFQUFFO1FBQ0Y7Ozs7OztXQU1HO1FBQ0gsR0FBRyxHQUNIO1lBRUE7O2VBRUc7WUFDSCxJQUFJLE1BQU0sRUFDVjtnQkFDQyxJQUFJO2dCQUNIOzttQkFFRztnQkFDSCxLQUFLO2dCQUNMOzttQkFFRztnQkFDSCxHQUFHLEdBQ0gsR0FBRyxNQUFNLENBQUM7Z0JBRVgsQ0FBQyxJQUFJLGlCQUFPLENBQUMsR0FBRyxDQUFDO29CQUNoQixHQUFHO29CQUNILEtBQUs7aUJBQ0wsQ0FBQyxDQUFDO2dCQUVIOzttQkFFRztnQkFDSCxJQUFJLEdBQUcsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUVkOzs7O21CQUlHO2dCQUNILEdBQUcsSUFBSSxNQUFNLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQzthQUMzQjtZQUVELE9BQU87Z0JBQ047O21CQUVHO2dCQUNILEVBQUU7Z0JBQ0Y7O21CQUVHO2dCQUNILElBQUk7Z0JBQ0o7Ozs7OzttQkFNRztnQkFDSCxHQUFHO2FBQ0gsQ0FBQTtRQUNGLENBQUM7S0FDRDtJQUVELE9BQU8sRUFBRTtRQUVSLEdBQUcsY0FBYyxDQUFDLE9BQU87UUFFekIsQ0FBQyxFQUFFO1lBQ0YsRUFBRTtZQUNGLEdBQUcsTUFBTSxJQUFJO1lBQ2IsR0FBRztZQUNIO2dCQUNDLGlCQUFpQjthQUNqQixDQUFDLElBQUksQ0FBQyxHQUFHLENBQUM7WUFDWCxHQUFHO1lBQ0gsS0FBSztZQUNMLElBQUksTUFBTSxPQUFPO1lBQ2pCLFdBQVc7WUFDWCxJQUFJO1lBQ0osS0FBSyxPQUFPLElBQUk7WUFDaEIsRUFBRTtTQUNGO1FBRUQsRUFBRSxFQUFFLHFDQUE4QixDQUFDO1lBRWxDLGlDQUEwQjtTQUUxQixDQUFDO0tBRUY7Q0FFRCxDQUFDO0FBRUYsa0JBQWUsa0JBQVUsQ0FBQSIsInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogQ3JlYXRlZCBieSB1c2VyIG9uIDIwMTkvNC8xNC5cbiAqL1xuXG5pbXBvcnQgeyBfaGFuZGxlT3B0aW9ucywgbWFrZU9wdGlvbnMgfSBmcm9tICdAbm9kZS1ub3ZlbC90eHQtc3BsaXQvbGliL2luZGV4JztcbmltcG9ydCB7IElPcHRpb25zLCBJT3B0aW9uc1JlcXVpcmVkLCBJT3B0aW9uc1JlcXVpcmVkVXNlciwgSURhdGFWb2x1bWUsIElEYXRhQ2hhcHRlciB9IGZyb20gJ0Bub2RlLW5vdmVsL3R4dC1zcGxpdC9saWIvaW50ZXJmYWNlJztcblxuaW1wb3J0IHsgY29uc29sZSB9IGZyb20gJ0Bub2RlLW5vdmVsL3R4dC1zcGxpdC9saWIvY29uc29sZSc7XG5pbXBvcnQgdHBsQmFzZU9wdGlvbnMgZnJvbSAnLi9saWIvYmFzZSc7XG5pbXBvcnQgeyBiYXNlQ2JQYXJzZUNoYXB0ZXJNYXRjaDAwNSwgYmFzZUNiUGFyc2VDaGFwdGVyTWF0Y2hNYWluMDAxIH0gZnJvbSAnLi9saWIvcnVsZSc7XG5cbmxldCB2X2xpbmUgPSBg4pyf4pyg4pyg4pyf4pyg4pyg4pyf4pyg4pyg4pyf4pyg4pyg4pyf4pyg4pyg4pyf4pyg4pyg4pyf4pyg4pyg4pyfYDtcbmxldCB2X2xpbmUyID0gYOKcn+KcoOKcoOKcn+KcoOKcoOKcn+KcoOKcoOKcn+KcoOKcoOKcn+KcoOKcoOKcn+KcoOKcoOKcn+KcoOKcoOKcn2A7XG5sZXQgY19saW5lID0gYPCfjbbwn5KZ8J+UpfCfkpnwn4228J+SmfCflKXwn5KZ8J+NtvCfkpnwn5Sl8J+SmfCfjbbwn5KZ8J+UpfCfkpnwn4228J+SmfCflKXwn5KZ8J+NtmA7XG5sZXQgY19saW5lMiA9IGDwn4228J+SmfCflKXwn5KZ8J+NtvCfkpnwn5Sl8J+SmfCfjbbwn5KZ8J+UpfCfkpnwn4228J+SmfCflKXwn5KZ8J+NtvCfkpnwn5Sl8J+SmfCfjbZgO1xuXG5sZXQgX3NwYWNlID0gJyDjgIBcXFxcdCc7XG5cbmV4cG9ydCBjb25zdCB0cGxPcHRpb25zOiBJT3B0aW9uc1JlcXVpcmVkVXNlciA9IHtcblxuXHQuLi50cGxCYXNlT3B0aW9ucyxcblxuXHQvKipcblx0ICog6YCZ5YCL5Y+D5pW4IOWPr+WIqumZpOaIluWKoOS4iiBfIOWmguaenOaykuacieeUqOWIsOeahOipsVxuXHQgKi9cblx0dm9sdW1lOiB7XG5cblx0XHRkaXNhYmxlOiB0cnVlLFxuXG5cdFx0LyoqXG5cdFx0ICog5pWF5oSP5pS+5LiA5YCL54Sh5pWI6YWN5bCNIOWvpumam+S9v+eUqOaZguiri+iHquihjOabtOaUuVxuXHRcdCAqXG5cdFx0ICog55W25rKS5pyJ6YWN5bCN5Yiw55qE5pmC5YCZIOacg+iHquWLleeUoueUnyAwMDAwMF91bmtub3cg6LOH5paZ5aS+XG5cdFx0ICovXG5cdFx0cjogW1xuXHRcdFx0YCR7dl9saW5lfVxcbmAsXG5cdFx0XHQnKFteXFxcXG5dKylcXG4nLFxuXHRcdFx0YCR7dl9saW5lMn1cXG4rYCxcblx0XHRdLFxuXG5cdFx0Y2Ioe1xuXHRcdFx0LyoqXG5cdFx0XHQgKiDmlrwgbWF0Y2gg5YiX6KGo5Lit55qEIGluZGV4IOW6j+WIl1xuXHRcdFx0ICovXG5cdFx0XHRpLFxuXHRcdFx0LyoqXG5cdFx0XHQgKiDmqpTmoYjluo/liJco5YSy5a2Y5qqU5qGI5pmC5pyD5YGa54K65YmN572u6KmeKVxuXHRcdFx0ICovXG5cdFx0XHRpZCxcblx0XHRcdC8qKlxuXHRcdFx0ICog5qiZ6aGM5ZCN56ixIOmgkOioreaDheazgeS4i+etieaWvCBtYXRjaCDliLDnmoTmqJnpoYxcblx0XHRcdCAqL1xuXHRcdFx0bmFtZSxcblx0XHRcdC8qKlxuXHRcdFx0ICog5pys6ZqO5q6155qEIG1hdGNoIOWAvFxuXHRcdFx0ICovXG5cdFx0XHRtLFxuXHRcdFx0LyoqXG5cdFx0XHQgKiDkuIrkuIDmrKHnmoQgbWF0Y2gg5YC8XG5cdFx0XHQgKlxuXHRcdFx0ICog5L2G5pivIOWvpumam+S4iiDpgJnlj4Pmlbgg5omN5piv5pys5qyhIGNhbGxiYWNrIOecn+ato+eahCBtYXRjaCDlhaflrrlcblx0XHRcdCAqL1xuXHRcdFx0bV9sYXN0LFxuXHRcdFx0LyoqXG5cdFx0XHQgKiDnm67liY3lt7LntpPliIblibLnmoTmqpTmoYjliJfooajoiIflhaflrrlcblx0XHRcdCAqL1xuXHRcdFx0X2ZpbGVzLFxuXHRcdFx0LyoqXG5cdFx0XHQgKiDmlrzmiYDmnInnq6Dnr4DkuK3nmoTluo/liJdcblx0XHRcdCAqXG5cdFx0XHQgKiBAcmVhZG9ubHlcblx0XHRcdCAqL1xuXHRcdFx0aWksXG5cdFx0XHQvKipcblx0XHRcdCAqIOacrOasoSBtYXRjaCDnmoQg5YWn5paHIHN0YXJ0IGluZGV4XG5cdFx0XHQgKiDlj6/pgJrpgY7kv67mlLnmlbjlgLzkvobmjqfliLblhafmlofnr4TlnI1cblx0XHRcdCAqXG5cdFx0XHQgKiBAZXhhbXBsZVxuXHRcdFx0ICogaWR4ICs9IG1fbGFzdC5tYXRjaC5sZW5ndGg7IC8vIOWFp+aWh+W/veeVpeacrOasoSBtYXRjaCDliLDnmoTmqJnpoYxcblx0XHRcdCAqL1xuXHRcdFx0aWR4LFxuXHRcdH0pXG5cdFx0e1xuXHRcdFx0LyoqXG5cdFx0XHQgKiDkvp3nhafkvaDntabnmoQgcmVnZXhwIOWFp+WuueS+huWbnuWCs+eahOizh+aWmVxuXHRcdFx0ICovXG5cdFx0XHRpZiAobV9sYXN0KVxuXHRcdFx0e1xuXHRcdFx0XHRsZXQge1xuXHRcdFx0XHRcdC8qKlxuXHRcdFx0XHRcdCAqIOmFjeWwjeWIsOeahOWFp+WuuVxuXHRcdFx0XHRcdCAqL1xuXHRcdFx0XHRcdG1hdGNoLFxuXHRcdFx0XHRcdC8qKlxuXHRcdFx0XHRcdCAqIOmFjeWwjeWHuuS+hueahOmZo+WIl1xuXHRcdFx0XHRcdCAqL1xuXHRcdFx0XHRcdHN1Yixcblx0XHRcdFx0fSA9IG1fbGFzdDtcblxuXHRcdFx0XHQwICYmIGNvbnNvbGUuZGlyKHtcblx0XHRcdFx0XHRzdWIsXG5cdFx0XHRcdFx0bWF0Y2gsXG5cdFx0XHRcdH0pO1xuXG5cdFx0XHRcdC8qKlxuXHRcdFx0XHQgKiBAdG9kbyDpgJnoo6Hlj6/ku6XliqDkuIrmm7TlpJroqp7ms5XkvobmoLzlvI/ljJbmqJnpoYxcblx0XHRcdFx0ICovXG5cdFx0XHRcdG5hbWUgPSBzdWJbMF07XG5cblx0XHRcdFx0LyoqXG5cdFx0XHRcdCAqIOWwh+WumuS9jem7nuWKoOS4iuacrOasoemFjeWwjeWIsOeahOWFp+WuueeahOmVt+W6plxuXHRcdFx0XHQgKiDmraTmraXpqZ/lj6/ku6XnnIHnlaVcblx0XHRcdFx0ICog5L2G5L2/55So5q2k5q2l6amf5pmC5Y+v5Lul5ZCM5pmC5Zyo5YiH5Ymy5pmC5bCN5pa85YWn5a655L2c57K+57ChXG5cdFx0XHRcdCAqL1xuXHRcdFx0XHRpZHggKz0gbV9sYXN0Lm1hdGNoLmxlbmd0aDtcblx0XHRcdH1cblxuXHRcdFx0cmV0dXJuIHtcblx0XHRcdFx0LyoqXG5cdFx0XHRcdCAqIOaqlOahiOW6j+WIlyjlhLLlrZjmqpTmoYjmmYLmnIPlgZrngrrliY3nva7oqZ4pXG5cdFx0XHRcdCAqL1xuXHRcdFx0XHRpZCxcblx0XHRcdFx0LyoqXG5cdFx0XHRcdCAqIOaomemhjOWQjeeosSDpoJDoqK3mg4Xms4HkuIvnrYnmlrwgbWF0Y2gg5Yiw55qE5qiZ6aGMXG5cdFx0XHRcdCAqL1xuXHRcdFx0XHRuYW1lLFxuXHRcdFx0XHQvKipcblx0XHRcdFx0ICog5pys5qyhIG1hdGNoIOeahCDlhafmlocgc3RhcnQgaW5kZXhcblx0XHRcdFx0ICog5Y+v6YCa6YGO5L+u5pS55pW45YC85L6G5o6n5Yi25YWn5paH56+E5ZyNXG5cdFx0XHRcdCAqXG5cdFx0XHRcdCAqIEBleGFtcGxlXG5cdFx0XHRcdCAqIGlkeCArPSBtX2xhc3QubWF0Y2gubGVuZ3RoOyAvLyDlhafmloflv73nlaXmnKzmrKEgbWF0Y2gg5Yiw55qE5qiZ6aGMXG5cdFx0XHRcdCAqL1xuXHRcdFx0XHRpZHgsXG5cdFx0XHR9XG5cdFx0fSxcblx0fSxcblxuXHRjaGFwdGVyOiB7XG5cblx0XHQuLi50cGxCYXNlT3B0aW9ucy5jaGFwdGVyLFxuXG5cdFx0cjogW1xuXHRcdFx0YGAsXG5cdFx0XHRgJHtjX2xpbmV9XFxuYCxcblx0XHRcdCcoJyxcblx0XHRcdFtcblx0XHRcdFx0YOesrFtcXFxcZO+8kC3vvJldKyg/OuipsSlgLFxuXHRcdFx0XS5qb2luKCd8JyksXG5cdFx0XHQnKScsXG5cdFx0XHRgKD86YCxcblx0XHRcdGBbJHtfc3BhY2V9XFxcXC5dK2AsXG5cdFx0XHRgKFteXFxcXG5dKilgLFxuXHRcdFx0Jyk/Jyxcblx0XHRcdGBcXG4ke2NfbGluZTJ9XFxuYCxcblx0XHRcdGBgLFxuXHRcdF0sXG5cblx0XHRjYjogYmFzZUNiUGFyc2VDaGFwdGVyTWF0Y2hNYWluMDAxKFtcblxuXHRcdFx0YmFzZUNiUGFyc2VDaGFwdGVyTWF0Y2gwMDUsXG5cblx0XHRdKSxcblxuXHR9LFxuXG59O1xuXG5leHBvcnQgZGVmYXVsdCB0cGxPcHRpb25zXG4iXX0=