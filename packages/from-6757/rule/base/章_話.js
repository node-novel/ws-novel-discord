"use strict";
/**
 * Created by user on 2019/4/14.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const base_1 = require("../lib/base");
const StrUtil = require("str-util");
const rule_1 = require("../lib/rule");
let v_line = `☠☠☠♛☠☠☠☠☠☠♛☠☠☠♛☠☠☠`;
let c_line = `❄✡✥✩✧✫✪✭☆★✬✰✮✦⟡✯☸✠❇✣`;
let _space = ' 　\\t';
const c = '　';
exports.tplOptions = {
    ...base_1.default,
    /**
     * 這個參數 可刪除或加上 _ 如果沒有用到的話
     */
    volume: {
        //ignoreRe: /第四章はある事件/,
        /**
         * 故意放一個無效配對 實際使用時請自行更改
         *
         * 當沒有配對到的時候 會自動產生 00000_unknow 資料夾
         */
        r: [
            `^`,
            '(',
            [
                `第[一-十]+章`,
                `第[\\d０-９]+章`,
            ].join('|'),
            ')',
            `[${_space}\\-]*`,
            `([^\\n]*)`,
            `[${_space}]*`,
            `$`,
        ],
        cb: rule_1.baseCbParseChapterMatchMain001([
            rule_1.createCbParseChapterMatch({
                handleMatchSubIDString(argv, _data) {
                    let ids;
                    if (argv.idn) {
                        ids = `第${argv.idn}章`;
                    }
                    else {
                        ids = argv.ido;
                    }
                    return ids;
                },
                handleMatchSubID(argv, _data) {
                    if (/^\d+(?:\.\d+)?$/.test(argv.id)) {
                        argv.id = String(argv.id);
                        argv.idn = StrUtil.toFullNumber(argv.id.toString());
                    }
                    return argv;
                }
            }),
        ]),
    },
    /**
     * 這個參數是必填選項
     */
    chapter: {
        ignoreRe: /5270|10倍|8岁|5岁！/,
        /**
         * 正常來說不需要弄得這麼複雜的樣式 只需要配對標題就好
         * 但懶惰想要在切 txt 的時候 順便縮減內文的話 就可以寫的複雜一點
         *
         * 當 r 不是 regexp 的時候 在執行時會自動將這個參數的內容轉成 regexp
         */
        r: [
            `^`,
            `[${_space}]*`,
            '(',
            [
                `第[一-十]+話`,
                `第[\\d０-９]+(?:\.[\\d０-９]+)?(?:話|集)`,
                'web[ ]*\\d+',
                '\\d+话?',
                '[０-９]+话?',
                '第二百六十话',
            ].join('|'),
            ')',
            `[${_space}\\-]*`,
            `([^\\n]*)`,
            `[${_space}]*`,
            `$`,
        ],
        cb: rule_1.baseCbParseChapterMatchMain001([
            rule_1.baseCbParseChapterMatch001,
        ]),
    },
};
exports.default = exports.tplOptions;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoi56ugX+ipsS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIueroF/oqbEudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBOztHQUVHOztBQU1ILHNDQUF5QztBQUN6QyxvQ0FBcUM7QUFFckMsc0NBQW9IO0FBRXBILElBQUksTUFBTSxHQUFHLG9CQUFvQixDQUFDO0FBQ2xDLElBQUksTUFBTSxHQUFHLHNCQUFzQixDQUFDO0FBRXBDLElBQUksTUFBTSxHQUFHLE9BQU8sQ0FBQztBQUNyQixNQUFNLENBQUMsR0FBRyxHQUFHLENBQUM7QUFFRCxRQUFBLFVBQVUsR0FBeUI7SUFFL0MsR0FBRyxjQUFjO0lBRWpCOztPQUVHO0lBQ0gsTUFBTSxFQUFFO1FBRVAsdUJBQXVCO1FBRXZCOzs7O1dBSUc7UUFDSCxDQUFDLEVBQUU7WUFDRixHQUFHO1lBQ0gsR0FBRztZQUNIO2dCQUNDLFVBQVU7Z0JBQ1YsYUFBYTthQUNiLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQztZQUNYLEdBQUc7WUFDSCxJQUFJLE1BQU0sT0FBTztZQUNqQixXQUFXO1lBQ1gsSUFBSSxNQUFNLElBQUk7WUFDZCxHQUFHO1NBQ0g7UUFFRCxFQUFFLEVBQUUscUNBQThCLENBQUM7WUFFbEMsZ0NBQXlCLENBQUM7Z0JBRXpCLHNCQUFzQixDQUFDLElBQUksRUFBRSxLQUFLO29CQUVqQyxJQUFJLEdBQVcsQ0FBQztvQkFFaEIsSUFBSSxJQUFJLENBQUMsR0FBRyxFQUNaO3dCQUNDLEdBQUcsR0FBRyxJQUFJLElBQUksQ0FBQyxHQUFHLEdBQUcsQ0FBQztxQkFDdEI7eUJBRUQ7d0JBQ0MsR0FBRyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUM7cUJBQ2Y7b0JBRUQsT0FBTyxHQUFHLENBQUE7Z0JBQ1gsQ0FBQztnQkFFRCxnQkFBZ0IsQ0FBQyxJQUFJLEVBQUUsS0FBSztvQkFFM0IsSUFBSSxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxFQUNuQzt3QkFDQyxJQUFJLENBQUMsRUFBRSxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7d0JBRTFCLElBQUksQ0FBQyxHQUFHLEdBQUcsT0FBTyxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUM7cUJBQ3BEO29CQUVELE9BQU8sSUFBSSxDQUFBO2dCQUNaLENBQUM7YUFFRCxDQUFDO1NBRUYsQ0FBQztLQUVGO0lBRUQ7O09BRUc7SUFDSCxPQUFPLEVBQUU7UUFFUixRQUFRLEVBQUUsaUJBQWlCO1FBRTNCOzs7OztXQUtHO1FBQ0gsQ0FBQyxFQUFFO1lBQ0YsR0FBRztZQUNILElBQUksTUFBTSxJQUFJO1lBQ2QsR0FBRztZQUNIO2dCQUNDLFVBQVU7Z0JBQ1YsbUNBQW1DO2dCQUNuQyxhQUFhO2dCQUNiLFFBQVE7Z0JBQ1IsVUFBVTtnQkFDVixRQUFRO2FBQ1IsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDO1lBQ1gsR0FBRztZQUNILElBQUksTUFBTSxPQUFPO1lBQ2pCLFdBQVc7WUFDWCxJQUFJLE1BQU0sSUFBSTtZQUNkLEdBQUc7U0FDSDtRQUNELEVBQUUsRUFBRSxxQ0FBOEIsQ0FBQztZQUNsQyxpQ0FBMEI7U0FDMUIsQ0FBQztLQUNGO0NBRUQsQ0FBQztBQUVGLGtCQUFlLGtCQUFVLENBQUEiLCJzb3VyY2VzQ29udGVudCI6WyIvKipcbiAqIENyZWF0ZWQgYnkgdXNlciBvbiAyMDE5LzQvMTQuXG4gKi9cblxuaW1wb3J0IHsgX2hhbmRsZU9wdGlvbnMsIG1ha2VPcHRpb25zIH0gZnJvbSAnQG5vZGUtbm92ZWwvdHh0LXNwbGl0L2xpYi9pbmRleCc7XG5pbXBvcnQgeyBJT3B0aW9ucywgSU9wdGlvbnNSZXF1aXJlZCwgSU9wdGlvbnNSZXF1aXJlZFVzZXIsIElEYXRhVm9sdW1lLCBJRGF0YUNoYXB0ZXIgfSBmcm9tICdAbm9kZS1ub3ZlbC90eHQtc3BsaXQvbGliL2ludGVyZmFjZSc7XG5cbmltcG9ydCB7IGNvbnNvbGUgfSBmcm9tICdAbm9kZS1ub3ZlbC90eHQtc3BsaXQvbGliL2NvbnNvbGUnO1xuaW1wb3J0IHRwbEJhc2VPcHRpb25zIGZyb20gJy4uL2xpYi9iYXNlJztcbmltcG9ydCBTdHJVdGlsID0gcmVxdWlyZSgnc3RyLXV0aWwnKTtcbmltcG9ydCBub3ZlbFRleHQgZnJvbSAnbm92ZWwtdGV4dCc7XG5pbXBvcnQgeyBiYXNlQ2JQYXJzZUNoYXB0ZXJNYXRjaDAwMSwgYmFzZUNiUGFyc2VDaGFwdGVyTWF0Y2hNYWluMDAxLCBjcmVhdGVDYlBhcnNlQ2hhcHRlck1hdGNoIH0gZnJvbSAnLi4vbGliL3J1bGUnO1xuXG5sZXQgdl9saW5lID0gYOKYoOKYoOKYoOKZm+KYoOKYoOKYoOKYoOKYoOKYoOKZm+KYoOKYoOKYoOKZm+KYoOKYoOKYoGA7XG5sZXQgY19saW5lID0gYOKdhOKcoeKcpeKcqeKcp+Kcq+KcquKcreKYhuKYheKcrOKcsOKcruKcpuKfoeKcr+KYuOKcoOKdh+Kco2A7XG5cbmxldCBfc3BhY2UgPSAnIOOAgFxcXFx0JztcbmNvbnN0IGMgPSAn44CAJztcblxuZXhwb3J0IGNvbnN0IHRwbE9wdGlvbnM6IElPcHRpb25zUmVxdWlyZWRVc2VyID0ge1xuXG5cdC4uLnRwbEJhc2VPcHRpb25zLFxuXG5cdC8qKlxuXHQgKiDpgJnlgIvlj4Pmlbgg5Y+v5Yiq6Zmk5oiW5Yqg5LiKIF8g5aaC5p6c5rKS5pyJ55So5Yiw55qE6KmxXG5cdCAqL1xuXHR2b2x1bWU6IHtcblxuXHRcdC8vaWdub3JlUmU6IC/nrKzlm5vnq6Djga/jgYLjgovkuovku7YvLFxuXG5cdFx0LyoqXG5cdFx0ICog5pWF5oSP5pS+5LiA5YCL54Sh5pWI6YWN5bCNIOWvpumam+S9v+eUqOaZguiri+iHquihjOabtOaUuVxuXHRcdCAqXG5cdFx0ICog55W25rKS5pyJ6YWN5bCN5Yiw55qE5pmC5YCZIOacg+iHquWLleeUoueUnyAwMDAwMF91bmtub3cg6LOH5paZ5aS+XG5cdFx0ICovXG5cdFx0cjogW1xuXHRcdFx0YF5gLFxuXHRcdFx0JygnLFxuXHRcdFx0W1xuXHRcdFx0XHRg56ysW+S4gC3ljYFdK+eroGAsXG5cdFx0XHRcdGDnrKxbXFxcXGTvvJAt77yZXSvnq6BgLFxuXHRcdFx0XS5qb2luKCd8JyksXG5cdFx0XHQnKScsXG5cdFx0XHRgWyR7X3NwYWNlfVxcXFwtXSpgLFxuXHRcdFx0YChbXlxcXFxuXSopYCxcblx0XHRcdGBbJHtfc3BhY2V9XSpgLFxuXHRcdFx0YCRgLFxuXHRcdF0sXG5cblx0XHRjYjogYmFzZUNiUGFyc2VDaGFwdGVyTWF0Y2hNYWluMDAxKFtcblxuXHRcdFx0Y3JlYXRlQ2JQYXJzZUNoYXB0ZXJNYXRjaCh7XG5cblx0XHRcdFx0aGFuZGxlTWF0Y2hTdWJJRFN0cmluZyhhcmd2LCBfZGF0YSlcblx0XHRcdFx0e1xuXHRcdFx0XHRcdGxldCBpZHM6IHN0cmluZztcblxuXHRcdFx0XHRcdGlmIChhcmd2Lmlkbilcblx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHRpZHMgPSBg56ysJHthcmd2Lmlkbn3nq6BgO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0XHRlbHNlXG5cdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0aWRzID0gYXJndi5pZG87XG5cdFx0XHRcdFx0fVxuXG5cdFx0XHRcdFx0cmV0dXJuIGlkc1xuXHRcdFx0XHR9LFxuXG5cdFx0XHRcdGhhbmRsZU1hdGNoU3ViSUQoYXJndiwgX2RhdGEpXG5cdFx0XHRcdHtcblx0XHRcdFx0XHRpZiAoL15cXGQrKD86XFwuXFxkKyk/JC8udGVzdChhcmd2LmlkKSlcblx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHRhcmd2LmlkID0gU3RyaW5nKGFyZ3YuaWQpO1xuXG5cdFx0XHRcdFx0XHRhcmd2LmlkbiA9IFN0clV0aWwudG9GdWxsTnVtYmVyKGFyZ3YuaWQudG9TdHJpbmcoKSk7XG5cdFx0XHRcdFx0fVxuXG5cdFx0XHRcdFx0cmV0dXJuIGFyZ3Zcblx0XHRcdFx0fVxuXG5cdFx0XHR9KSxcblxuXHRcdF0pLFxuXG5cdH0sXG5cblx0LyoqXG5cdCAqIOmAmeWAi+WPg+aVuOaYr+W/heWhq+mBuOmghVxuXHQgKi9cblx0Y2hhcHRlcjoge1xuXG5cdFx0aWdub3JlUmU6IC81MjcwfDEw5YCNfDjlsoF8NeWyge+8gS8sXG5cblx0XHQvKipcblx0XHQgKiDmraPluLjkvoboqqrkuI3pnIDopoHlvITlvpfpgJnpurzopIfpm5znmoTmqKPlvI8g5Y+q6ZyA6KaB6YWN5bCN5qiZ6aGM5bCx5aW9XG5cdFx0ICog5L2G5oe25oOw5oOz6KaB5Zyo5YiHIHR4dCDnmoTmmYLlgJkg6aCG5L6/57iu5rib5YWn5paH55qE6KmxIOWwseWPr+S7peWvq+eahOikh+mbnOS4gOm7nlxuXHRcdCAqXG5cdFx0ICog55W2IHIg5LiN5pivIHJlZ2V4cCDnmoTmmYLlgJkg5Zyo5Z+36KGM5pmC5pyD6Ieq5YuV5bCH6YCZ5YCL5Y+D5pW455qE5YWn5a656L2J5oiQIHJlZ2V4cFxuXHRcdCAqL1xuXHRcdHI6IFtcblx0XHRcdGBeYCxcblx0XHRcdGBbJHtfc3BhY2V9XSpgLFxuXHRcdFx0JygnLFxuXHRcdFx0W1xuXHRcdFx0XHRg56ysW+S4gC3ljYFdK+ipsWAsXG5cdFx0XHRcdGDnrKxbXFxcXGTvvJAt77yZXSsoPzpcXC5bXFxcXGTvvJAt77yZXSspPyg/OuipsXzpm4YpYCxcblx0XHRcdFx0J3dlYlsgXSpcXFxcZCsnLFxuXHRcdFx0XHQnXFxcXGQr6K+dPycsXG5cdFx0XHRcdCdb77yQLe+8mV0r6K+dPycsXG5cdFx0XHRcdCfnrKzkuoznmb7lha3ljYHor50nLFxuXHRcdFx0XS5qb2luKCd8JyksXG5cdFx0XHQnKScsXG5cdFx0XHRgWyR7X3NwYWNlfVxcXFwtXSpgLFxuXHRcdFx0YChbXlxcXFxuXSopYCxcblx0XHRcdGBbJHtfc3BhY2V9XSpgLFxuXHRcdFx0YCRgLFxuXHRcdF0sXG5cdFx0Y2I6IGJhc2VDYlBhcnNlQ2hhcHRlck1hdGNoTWFpbjAwMShbXG5cdFx0XHRiYXNlQ2JQYXJzZUNoYXB0ZXJNYXRjaDAwMSxcblx0XHRdKSxcblx0fSxcblxufTtcblxuZXhwb3J0IGRlZmF1bHQgdHBsT3B0aW9uc1xuIl19