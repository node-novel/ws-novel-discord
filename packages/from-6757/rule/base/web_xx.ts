/**
 * Created by user on 2019/4/14.
 */

import { _handleOptions, makeOptions } from '@node-novel/txt-split/lib/index';
import { IOptions, IOptionsRequired, IOptionsRequiredUser, IDataVolume, IDataChapter } from '@node-novel/txt-split/lib/interface';

import { console } from '@node-novel/txt-split/lib/console';
import tplBaseOptions from '../lib/base';

let v_line = `☠☠☠♛☠☠☠☠☠☠♛☠☠☠♛☠☠☠`;
let c_line = `❄✡✥✩✧✫✪✭☆★✬✰✮✦⟡✯☸✠❇✣`;

export const tplOptions: IOptionsRequiredUser = {

	...tplBaseOptions,

	/**
	 * 這個參數 可刪除或加上 _ 如果沒有用到的話
	 */
	_volume: {

		/**
		 * 故意放一個無效配對 實際使用時請自行更改
		 *
		 * 當沒有配對到的時候 會自動產生 00000_unknow 資料夾
		 */
		r: [
			`^`,
			'(第.章[^\\n]*)',
			`$`,
		],

		cb({
			/**
			 * 於 match 列表中的 index 序列
			 */
			i,
			/**
			 * 檔案序列(儲存檔案時會做為前置詞)
			 */
			id,
			/**
			 * 標題名稱 預設情況下等於 match 到的標題
			 */
			name,
			/**
			 * 本階段的 match 值
			 */
			m,
			/**
			 * 上一次的 match 值
			 *
			 * 但是 實際上 這參數 才是本次 callback 真正的 match 內容
			 */
			m_last,
			/**
			 * 目前已經分割的檔案列表與內容
			 */
			_files,
			/**
			 * 於所有章節中的序列
			 *
			 * @readonly
			 */
			ii,
			/**
			 * 本次 match 的 內文 start index
			 * 可通過修改數值來控制內文範圍
			 *
			 * @example
			 * idx += m_last.match.length; // 內文忽略本次 match 到的標題
			 */
			idx,
		})
		{
			/**
			 * 依照你給的 regexp 內容來回傳的資料
			 */
			if (m_last)
			{
				let {
					/**
					 * 配對到的內容
					 */
					match,
					/**
					 * 配對出來的陣列
					 */
					sub,
				} = m_last;

				0 && console.dir({
					sub,
					match,
				});

				/**
				 * @todo 這裡可以加上更多語法來格式化標題
				 */
				name = sub[0];

				/**
				 * 將定位點加上本次配對到的內容的長度
				 * 此步驟可以省略
				 * 但使用此步驟時可以同時在切割時對於內容作精簡
				 */
				idx += m_last.match.length;
			}

			return {
				/**
				 * 檔案序列(儲存檔案時會做為前置詞)
				 */
				id,
				/**
				 * 標題名稱 預設情況下等於 match 到的標題
				 */
				name,
				/**
				 * 本次 match 的 內文 start index
				 * 可通過修改數值來控制內文範圍
				 *
				 * @example
				 * idx += m_last.match.length; // 內文忽略本次 match 到的標題
				 */
				idx,
			}
		},
	},

	/**
	 * 這個參數是必填選項
	 */
	chapter: {
		//r: /^\d+\n(第\d+話) *([^\n]*)$/,

		/**
		 * 正常來說不需要弄得這麼複雜的樣式 只需要配對標題就好
		 * 但懶惰想要在切 txt 的時候 順便縮減內文的話 就可以寫的複雜一點
		 *
		 * 當 r 不是 regexp 的時候 在執行時會自動將這個參數的內容轉成 regexp
		 */
		r: [
			`^`,
			'web[ ]*(\\d+)',
			`$`,
		],
		cb({
			/**
			 * 於 match 列表中的 index 序列
			 */
			i,
			/**
			 * 檔案序列(儲存檔案時會做為前置詞)
			 */
			id,
			/**
			 * 標題名稱 預設情況下等於 match 到的標題
			 */
			name,
			/**
			 * 本階段的 match 值
			 */
			m,
			/**
			 * 上一次的 match 值
			 *
			 * 但是 實際上 這參數 才是本次 callback 真正的 match 內容
			 */
			m_last,
			/**
			 * 目前已經分割的檔案列表與內容
			 */
			_files,
			/**
			 * 於所有章節中的序列
			 *
			 * @readonly
			 */
			ii,
			/**
			 * 本次 match 的 內文 start index
			 * 可通過修改數值來控制內文範圍
			 *
			 * @example
			 * idx += m_last.match.length; // 內文忽略本次 match 到的標題
			 */
			idx,
		})
		{
			/**
			 * 依照你給的 regexp 內容來回傳的資料
			 */
			if (m_last)
			{
				let {
					/**
					 * 配對到的內容
					 */
					match,
					/**
					 * 配對出來的陣列
					 */
					sub,
				} = m_last;

				0 && console.dir({
					sub,
					match,
				});

				/**
				 * @todo 這裡可以加上更多語法來格式化標題
				 */
				name = parseInt(sub[0]).toString().padStart(3, '0');

				/**
				 * 將定位點加上本次配對到的內容的長度
				 * 此步驟可以省略
				 * 但使用此步驟時可以同時在切割時對於內容作精簡
				 */
				idx += m_last.match.length;
			}

			return {
				/**
				 * 檔案序列(儲存檔案時會做為前置詞)
				 */
				id,
				/**
				 * 標題名稱 預設情況下等於 match 到的標題
				 */
				name,
				/**
				 * 本次 match 的 內文 start index
				 * 可通過修改數值來控制內文範圍
				 *
				 * @example
				 * idx += m_last.match.length; // 內文忽略本次 match 到的標題
				 */
				idx,
			}
		},
	},

};

export default tplOptions
