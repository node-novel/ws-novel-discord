"use strict";
/**
 * Created by user on 2019/4/14.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const console_1 = require("@node-novel/txt-split/lib/console");
const base_1 = require("../lib/base");
let v_line = `☠☠☠♛☠☠☠☠☠☠♛☠☠☠♛☠☠☠`;
let c_line = `❄✡✥✩✧✫✪✭☆★✬✰✮✦⟡✯☸✠❇✣`;
exports.tplOptions = {
    ...base_1.default,
    /**
     * 這個參數 可刪除或加上 _ 如果沒有用到的話
     */
    _volume: {
        /**
         * 故意放一個無效配對 實際使用時請自行更改
         *
         * 當沒有配對到的時候 會自動產生 00000_unknow 資料夾
         */
        r: [
            `^`,
            '(第.章[^\\n]*)',
            `$`,
        ],
        cb({ 
        /**
         * 於 match 列表中的 index 序列
         */
        i, 
        /**
         * 檔案序列(儲存檔案時會做為前置詞)
         */
        id, 
        /**
         * 標題名稱 預設情況下等於 match 到的標題
         */
        name, 
        /**
         * 本階段的 match 值
         */
        m, 
        /**
         * 上一次的 match 值
         *
         * 但是 實際上 這參數 才是本次 callback 真正的 match 內容
         */
        m_last, 
        /**
         * 目前已經分割的檔案列表與內容
         */
        _files, 
        /**
         * 於所有章節中的序列
         *
         * @readonly
         */
        ii, 
        /**
         * 本次 match 的 內文 start index
         * 可通過修改數值來控制內文範圍
         *
         * @example
         * idx += m_last.match.length; // 內文忽略本次 match 到的標題
         */
        idx, }) {
            /**
             * 依照你給的 regexp 內容來回傳的資料
             */
            if (m_last) {
                let { 
                /**
                 * 配對到的內容
                 */
                match, 
                /**
                 * 配對出來的陣列
                 */
                sub, } = m_last;
                0 && console_1.console.dir({
                    sub,
                    match,
                });
                /**
                 * @todo 這裡可以加上更多語法來格式化標題
                 */
                name = sub[0];
                /**
                 * 將定位點加上本次配對到的內容的長度
                 * 此步驟可以省略
                 * 但使用此步驟時可以同時在切割時對於內容作精簡
                 */
                idx += m_last.match.length;
            }
            return {
                /**
                 * 檔案序列(儲存檔案時會做為前置詞)
                 */
                id,
                /**
                 * 標題名稱 預設情況下等於 match 到的標題
                 */
                name,
                /**
                 * 本次 match 的 內文 start index
                 * 可通過修改數值來控制內文範圍
                 *
                 * @example
                 * idx += m_last.match.length; // 內文忽略本次 match 到的標題
                 */
                idx,
            };
        },
    },
    /**
     * 這個參數是必填選項
     */
    chapter: {
        //r: /^\d+\n(第\d+話) *([^\n]*)$/,
        /**
         * 正常來說不需要弄得這麼複雜的樣式 只需要配對標題就好
         * 但懶惰想要在切 txt 的時候 順便縮減內文的話 就可以寫的複雜一點
         *
         * 當 r 不是 regexp 的時候 在執行時會自動將這個參數的內容轉成 regexp
         */
        r: [
            `^`,
            'web[ ]*(\\d+)',
            `$`,
        ],
        cb({ 
        /**
         * 於 match 列表中的 index 序列
         */
        i, 
        /**
         * 檔案序列(儲存檔案時會做為前置詞)
         */
        id, 
        /**
         * 標題名稱 預設情況下等於 match 到的標題
         */
        name, 
        /**
         * 本階段的 match 值
         */
        m, 
        /**
         * 上一次的 match 值
         *
         * 但是 實際上 這參數 才是本次 callback 真正的 match 內容
         */
        m_last, 
        /**
         * 目前已經分割的檔案列表與內容
         */
        _files, 
        /**
         * 於所有章節中的序列
         *
         * @readonly
         */
        ii, 
        /**
         * 本次 match 的 內文 start index
         * 可通過修改數值來控制內文範圍
         *
         * @example
         * idx += m_last.match.length; // 內文忽略本次 match 到的標題
         */
        idx, }) {
            /**
             * 依照你給的 regexp 內容來回傳的資料
             */
            if (m_last) {
                let { 
                /**
                 * 配對到的內容
                 */
                match, 
                /**
                 * 配對出來的陣列
                 */
                sub, } = m_last;
                0 && console_1.console.dir({
                    sub,
                    match,
                });
                /**
                 * @todo 這裡可以加上更多語法來格式化標題
                 */
                name = parseInt(sub[0]).toString().padStart(3, '0');
                /**
                 * 將定位點加上本次配對到的內容的長度
                 * 此步驟可以省略
                 * 但使用此步驟時可以同時在切割時對於內容作精簡
                 */
                idx += m_last.match.length;
            }
            return {
                /**
                 * 檔案序列(儲存檔案時會做為前置詞)
                 */
                id,
                /**
                 * 標題名稱 預設情況下等於 match 到的標題
                 */
                name,
                /**
                 * 本次 match 的 內文 start index
                 * 可通過修改數值來控制內文範圍
                 *
                 * @example
                 * idx += m_last.match.length; // 內文忽略本次 match 到的標題
                 */
                idx,
            };
        },
    },
};
exports.default = exports.tplOptions;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoid2ViX3h4LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsid2ViX3h4LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQTs7R0FFRzs7QUFLSCwrREFBNEQ7QUFDNUQsc0NBQXlDO0FBRXpDLElBQUksTUFBTSxHQUFHLG9CQUFvQixDQUFDO0FBQ2xDLElBQUksTUFBTSxHQUFHLHNCQUFzQixDQUFDO0FBRXZCLFFBQUEsVUFBVSxHQUF5QjtJQUUvQyxHQUFHLGNBQWM7SUFFakI7O09BRUc7SUFDSCxPQUFPLEVBQUU7UUFFUjs7OztXQUlHO1FBQ0gsQ0FBQyxFQUFFO1lBQ0YsR0FBRztZQUNILGNBQWM7WUFDZCxHQUFHO1NBQ0g7UUFFRCxFQUFFLENBQUM7UUFDRjs7V0FFRztRQUNILENBQUM7UUFDRDs7V0FFRztRQUNILEVBQUU7UUFDRjs7V0FFRztRQUNILElBQUk7UUFDSjs7V0FFRztRQUNILENBQUM7UUFDRDs7OztXQUlHO1FBQ0gsTUFBTTtRQUNOOztXQUVHO1FBQ0gsTUFBTTtRQUNOOzs7O1dBSUc7UUFDSCxFQUFFO1FBQ0Y7Ozs7OztXQU1HO1FBQ0gsR0FBRyxHQUNIO1lBRUE7O2VBRUc7WUFDSCxJQUFJLE1BQU0sRUFDVjtnQkFDQyxJQUFJO2dCQUNIOzttQkFFRztnQkFDSCxLQUFLO2dCQUNMOzttQkFFRztnQkFDSCxHQUFHLEdBQ0gsR0FBRyxNQUFNLENBQUM7Z0JBRVgsQ0FBQyxJQUFJLGlCQUFPLENBQUMsR0FBRyxDQUFDO29CQUNoQixHQUFHO29CQUNILEtBQUs7aUJBQ0wsQ0FBQyxDQUFDO2dCQUVIOzttQkFFRztnQkFDSCxJQUFJLEdBQUcsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUVkOzs7O21CQUlHO2dCQUNILEdBQUcsSUFBSSxNQUFNLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQzthQUMzQjtZQUVELE9BQU87Z0JBQ047O21CQUVHO2dCQUNILEVBQUU7Z0JBQ0Y7O21CQUVHO2dCQUNILElBQUk7Z0JBQ0o7Ozs7OzttQkFNRztnQkFDSCxHQUFHO2FBQ0gsQ0FBQTtRQUNGLENBQUM7S0FDRDtJQUVEOztPQUVHO0lBQ0gsT0FBTyxFQUFFO1FBQ1IsZ0NBQWdDO1FBRWhDOzs7OztXQUtHO1FBQ0gsQ0FBQyxFQUFFO1lBQ0YsR0FBRztZQUNILGVBQWU7WUFDZixHQUFHO1NBQ0g7UUFDRCxFQUFFLENBQUM7UUFDRjs7V0FFRztRQUNILENBQUM7UUFDRDs7V0FFRztRQUNILEVBQUU7UUFDRjs7V0FFRztRQUNILElBQUk7UUFDSjs7V0FFRztRQUNILENBQUM7UUFDRDs7OztXQUlHO1FBQ0gsTUFBTTtRQUNOOztXQUVHO1FBQ0gsTUFBTTtRQUNOOzs7O1dBSUc7UUFDSCxFQUFFO1FBQ0Y7Ozs7OztXQU1HO1FBQ0gsR0FBRyxHQUNIO1lBRUE7O2VBRUc7WUFDSCxJQUFJLE1BQU0sRUFDVjtnQkFDQyxJQUFJO2dCQUNIOzttQkFFRztnQkFDSCxLQUFLO2dCQUNMOzttQkFFRztnQkFDSCxHQUFHLEdBQ0gsR0FBRyxNQUFNLENBQUM7Z0JBRVgsQ0FBQyxJQUFJLGlCQUFPLENBQUMsR0FBRyxDQUFDO29CQUNoQixHQUFHO29CQUNILEtBQUs7aUJBQ0wsQ0FBQyxDQUFDO2dCQUVIOzttQkFFRztnQkFDSCxJQUFJLEdBQUcsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsRUFBRSxDQUFDLFFBQVEsQ0FBQyxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUM7Z0JBRXBEOzs7O21CQUlHO2dCQUNILEdBQUcsSUFBSSxNQUFNLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQzthQUMzQjtZQUVELE9BQU87Z0JBQ047O21CQUVHO2dCQUNILEVBQUU7Z0JBQ0Y7O21CQUVHO2dCQUNILElBQUk7Z0JBQ0o7Ozs7OzttQkFNRztnQkFDSCxHQUFHO2FBQ0gsQ0FBQTtRQUNGLENBQUM7S0FDRDtDQUVELENBQUM7QUFFRixrQkFBZSxrQkFBVSxDQUFBIiwic291cmNlc0NvbnRlbnQiOlsiLyoqXG4gKiBDcmVhdGVkIGJ5IHVzZXIgb24gMjAxOS80LzE0LlxuICovXG5cbmltcG9ydCB7IF9oYW5kbGVPcHRpb25zLCBtYWtlT3B0aW9ucyB9IGZyb20gJ0Bub2RlLW5vdmVsL3R4dC1zcGxpdC9saWIvaW5kZXgnO1xuaW1wb3J0IHsgSU9wdGlvbnMsIElPcHRpb25zUmVxdWlyZWQsIElPcHRpb25zUmVxdWlyZWRVc2VyLCBJRGF0YVZvbHVtZSwgSURhdGFDaGFwdGVyIH0gZnJvbSAnQG5vZGUtbm92ZWwvdHh0LXNwbGl0L2xpYi9pbnRlcmZhY2UnO1xuXG5pbXBvcnQgeyBjb25zb2xlIH0gZnJvbSAnQG5vZGUtbm92ZWwvdHh0LXNwbGl0L2xpYi9jb25zb2xlJztcbmltcG9ydCB0cGxCYXNlT3B0aW9ucyBmcm9tICcuLi9saWIvYmFzZSc7XG5cbmxldCB2X2xpbmUgPSBg4pig4pig4pig4pmb4pig4pig4pig4pig4pig4pig4pmb4pig4pig4pig4pmb4pig4pig4pigYDtcbmxldCBjX2xpbmUgPSBg4p2E4pyh4pyl4pyp4pyn4pyr4pyq4pyt4piG4piF4pys4pyw4pyu4pym4p+h4pyv4pi44pyg4p2H4pyjYDtcblxuZXhwb3J0IGNvbnN0IHRwbE9wdGlvbnM6IElPcHRpb25zUmVxdWlyZWRVc2VyID0ge1xuXG5cdC4uLnRwbEJhc2VPcHRpb25zLFxuXG5cdC8qKlxuXHQgKiDpgJnlgIvlj4Pmlbgg5Y+v5Yiq6Zmk5oiW5Yqg5LiKIF8g5aaC5p6c5rKS5pyJ55So5Yiw55qE6KmxXG5cdCAqL1xuXHRfdm9sdW1lOiB7XG5cblx0XHQvKipcblx0XHQgKiDmlYXmhI/mlL7kuIDlgIvnhKHmlYjphY3lsI0g5a+m6Zqb5L2/55So5pmC6KuL6Ieq6KGM5pu05pS5XG5cdFx0ICpcblx0XHQgKiDnlbbmspLmnInphY3lsI3liLDnmoTmmYLlgJkg5pyD6Ieq5YuV55Si55SfIDAwMDAwX3Vua25vdyDos4fmlpnlpL5cblx0XHQgKi9cblx0XHRyOiBbXG5cdFx0XHRgXmAsXG5cdFx0XHQnKOesrC7nq6BbXlxcXFxuXSopJyxcblx0XHRcdGAkYCxcblx0XHRdLFxuXG5cdFx0Y2Ioe1xuXHRcdFx0LyoqXG5cdFx0XHQgKiDmlrwgbWF0Y2gg5YiX6KGo5Lit55qEIGluZGV4IOW6j+WIl1xuXHRcdFx0ICovXG5cdFx0XHRpLFxuXHRcdFx0LyoqXG5cdFx0XHQgKiDmqpTmoYjluo/liJco5YSy5a2Y5qqU5qGI5pmC5pyD5YGa54K65YmN572u6KmeKVxuXHRcdFx0ICovXG5cdFx0XHRpZCxcblx0XHRcdC8qKlxuXHRcdFx0ICog5qiZ6aGM5ZCN56ixIOmgkOioreaDheazgeS4i+etieaWvCBtYXRjaCDliLDnmoTmqJnpoYxcblx0XHRcdCAqL1xuXHRcdFx0bmFtZSxcblx0XHRcdC8qKlxuXHRcdFx0ICog5pys6ZqO5q6155qEIG1hdGNoIOWAvFxuXHRcdFx0ICovXG5cdFx0XHRtLFxuXHRcdFx0LyoqXG5cdFx0XHQgKiDkuIrkuIDmrKHnmoQgbWF0Y2gg5YC8XG5cdFx0XHQgKlxuXHRcdFx0ICog5L2G5pivIOWvpumam+S4iiDpgJnlj4Pmlbgg5omN5piv5pys5qyhIGNhbGxiYWNrIOecn+ato+eahCBtYXRjaCDlhaflrrlcblx0XHRcdCAqL1xuXHRcdFx0bV9sYXN0LFxuXHRcdFx0LyoqXG5cdFx0XHQgKiDnm67liY3lt7LntpPliIblibLnmoTmqpTmoYjliJfooajoiIflhaflrrlcblx0XHRcdCAqL1xuXHRcdFx0X2ZpbGVzLFxuXHRcdFx0LyoqXG5cdFx0XHQgKiDmlrzmiYDmnInnq6Dnr4DkuK3nmoTluo/liJdcblx0XHRcdCAqXG5cdFx0XHQgKiBAcmVhZG9ubHlcblx0XHRcdCAqL1xuXHRcdFx0aWksXG5cdFx0XHQvKipcblx0XHRcdCAqIOacrOasoSBtYXRjaCDnmoQg5YWn5paHIHN0YXJ0IGluZGV4XG5cdFx0XHQgKiDlj6/pgJrpgY7kv67mlLnmlbjlgLzkvobmjqfliLblhafmlofnr4TlnI1cblx0XHRcdCAqXG5cdFx0XHQgKiBAZXhhbXBsZVxuXHRcdFx0ICogaWR4ICs9IG1fbGFzdC5tYXRjaC5sZW5ndGg7IC8vIOWFp+aWh+W/veeVpeacrOasoSBtYXRjaCDliLDnmoTmqJnpoYxcblx0XHRcdCAqL1xuXHRcdFx0aWR4LFxuXHRcdH0pXG5cdFx0e1xuXHRcdFx0LyoqXG5cdFx0XHQgKiDkvp3nhafkvaDntabnmoQgcmVnZXhwIOWFp+WuueS+huWbnuWCs+eahOizh+aWmVxuXHRcdFx0ICovXG5cdFx0XHRpZiAobV9sYXN0KVxuXHRcdFx0e1xuXHRcdFx0XHRsZXQge1xuXHRcdFx0XHRcdC8qKlxuXHRcdFx0XHRcdCAqIOmFjeWwjeWIsOeahOWFp+WuuVxuXHRcdFx0XHRcdCAqL1xuXHRcdFx0XHRcdG1hdGNoLFxuXHRcdFx0XHRcdC8qKlxuXHRcdFx0XHRcdCAqIOmFjeWwjeWHuuS+hueahOmZo+WIl1xuXHRcdFx0XHRcdCAqL1xuXHRcdFx0XHRcdHN1Yixcblx0XHRcdFx0fSA9IG1fbGFzdDtcblxuXHRcdFx0XHQwICYmIGNvbnNvbGUuZGlyKHtcblx0XHRcdFx0XHRzdWIsXG5cdFx0XHRcdFx0bWF0Y2gsXG5cdFx0XHRcdH0pO1xuXG5cdFx0XHRcdC8qKlxuXHRcdFx0XHQgKiBAdG9kbyDpgJnoo6Hlj6/ku6XliqDkuIrmm7TlpJroqp7ms5XkvobmoLzlvI/ljJbmqJnpoYxcblx0XHRcdFx0ICovXG5cdFx0XHRcdG5hbWUgPSBzdWJbMF07XG5cblx0XHRcdFx0LyoqXG5cdFx0XHRcdCAqIOWwh+WumuS9jem7nuWKoOS4iuacrOasoemFjeWwjeWIsOeahOWFp+WuueeahOmVt+W6plxuXHRcdFx0XHQgKiDmraTmraXpqZ/lj6/ku6XnnIHnlaVcblx0XHRcdFx0ICog5L2G5L2/55So5q2k5q2l6amf5pmC5Y+v5Lul5ZCM5pmC5Zyo5YiH5Ymy5pmC5bCN5pa85YWn5a655L2c57K+57ChXG5cdFx0XHRcdCAqL1xuXHRcdFx0XHRpZHggKz0gbV9sYXN0Lm1hdGNoLmxlbmd0aDtcblx0XHRcdH1cblxuXHRcdFx0cmV0dXJuIHtcblx0XHRcdFx0LyoqXG5cdFx0XHRcdCAqIOaqlOahiOW6j+WIlyjlhLLlrZjmqpTmoYjmmYLmnIPlgZrngrrliY3nva7oqZ4pXG5cdFx0XHRcdCAqL1xuXHRcdFx0XHRpZCxcblx0XHRcdFx0LyoqXG5cdFx0XHRcdCAqIOaomemhjOWQjeeosSDpoJDoqK3mg4Xms4HkuIvnrYnmlrwgbWF0Y2gg5Yiw55qE5qiZ6aGMXG5cdFx0XHRcdCAqL1xuXHRcdFx0XHRuYW1lLFxuXHRcdFx0XHQvKipcblx0XHRcdFx0ICog5pys5qyhIG1hdGNoIOeahCDlhafmlocgc3RhcnQgaW5kZXhcblx0XHRcdFx0ICog5Y+v6YCa6YGO5L+u5pS55pW45YC85L6G5o6n5Yi25YWn5paH56+E5ZyNXG5cdFx0XHRcdCAqXG5cdFx0XHRcdCAqIEBleGFtcGxlXG5cdFx0XHRcdCAqIGlkeCArPSBtX2xhc3QubWF0Y2gubGVuZ3RoOyAvLyDlhafmloflv73nlaXmnKzmrKEgbWF0Y2gg5Yiw55qE5qiZ6aGMXG5cdFx0XHRcdCAqL1xuXHRcdFx0XHRpZHgsXG5cdFx0XHR9XG5cdFx0fSxcblx0fSxcblxuXHQvKipcblx0ICog6YCZ5YCL5Y+D5pW45piv5b+F5aGr6YG46aCFXG5cdCAqL1xuXHRjaGFwdGVyOiB7XG5cdFx0Ly9yOiAvXlxcZCtcXG4o56ysXFxkK+ipsSkgKihbXlxcbl0qKSQvLFxuXG5cdFx0LyoqXG5cdFx0ICog5q2j5bi45L6G6Kqq5LiN6ZyA6KaB5byE5b6X6YCZ6bq86KSH6Zuc55qE5qij5byPIOWPqumcgOimgemFjeWwjeaomemhjOWwseWlvVxuXHRcdCAqIOS9huaHtuaDsOaDs+imgeWcqOWIhyB0eHQg55qE5pmC5YCZIOmghuS+v+e4rua4m+WFp+aWh+eahOipsSDlsLHlj6/ku6Xlr6vnmoTopIfpm5zkuIDpu55cblx0XHQgKlxuXHRcdCAqIOeVtiByIOS4jeaYryByZWdleHAg55qE5pmC5YCZIOWcqOWft+ihjOaZguacg+iHquWLleWwh+mAmeWAi+WPg+aVuOeahOWFp+Wuuei9ieaIkCByZWdleHBcblx0XHQgKi9cblx0XHRyOiBbXG5cdFx0XHRgXmAsXG5cdFx0XHQnd2ViWyBdKihcXFxcZCspJyxcblx0XHRcdGAkYCxcblx0XHRdLFxuXHRcdGNiKHtcblx0XHRcdC8qKlxuXHRcdFx0ICog5pa8IG1hdGNoIOWIl+ihqOS4reeahCBpbmRleCDluo/liJdcblx0XHRcdCAqL1xuXHRcdFx0aSxcblx0XHRcdC8qKlxuXHRcdFx0ICog5qqU5qGI5bqP5YiXKOWEsuWtmOaqlOahiOaZguacg+WBmueCuuWJjee9ruipnilcblx0XHRcdCAqL1xuXHRcdFx0aWQsXG5cdFx0XHQvKipcblx0XHRcdCAqIOaomemhjOWQjeeosSDpoJDoqK3mg4Xms4HkuIvnrYnmlrwgbWF0Y2gg5Yiw55qE5qiZ6aGMXG5cdFx0XHQgKi9cblx0XHRcdG5hbWUsXG5cdFx0XHQvKipcblx0XHRcdCAqIOacrOmajuauteeahCBtYXRjaCDlgLxcblx0XHRcdCAqL1xuXHRcdFx0bSxcblx0XHRcdC8qKlxuXHRcdFx0ICog5LiK5LiA5qyh55qEIG1hdGNoIOWAvFxuXHRcdFx0ICpcblx0XHRcdCAqIOS9huaYryDlr6bpmpvkuIog6YCZ5Y+D5pW4IOaJjeaYr+acrOasoSBjYWxsYmFjayDnnJ/mraPnmoQgbWF0Y2gg5YWn5a65XG5cdFx0XHQgKi9cblx0XHRcdG1fbGFzdCxcblx0XHRcdC8qKlxuXHRcdFx0ICog55uu5YmN5bey57aT5YiG5Ymy55qE5qqU5qGI5YiX6KGo6IiH5YWn5a65XG5cdFx0XHQgKi9cblx0XHRcdF9maWxlcyxcblx0XHRcdC8qKlxuXHRcdFx0ICog5pa85omA5pyJ56ug56+A5Lit55qE5bqP5YiXXG5cdFx0XHQgKlxuXHRcdFx0ICogQHJlYWRvbmx5XG5cdFx0XHQgKi9cblx0XHRcdGlpLFxuXHRcdFx0LyoqXG5cdFx0XHQgKiDmnKzmrKEgbWF0Y2gg55qEIOWFp+aWhyBzdGFydCBpbmRleFxuXHRcdFx0ICog5Y+v6YCa6YGO5L+u5pS55pW45YC85L6G5o6n5Yi25YWn5paH56+E5ZyNXG5cdFx0XHQgKlxuXHRcdFx0ICogQGV4YW1wbGVcblx0XHRcdCAqIGlkeCArPSBtX2xhc3QubWF0Y2gubGVuZ3RoOyAvLyDlhafmloflv73nlaXmnKzmrKEgbWF0Y2gg5Yiw55qE5qiZ6aGMXG5cdFx0XHQgKi9cblx0XHRcdGlkeCxcblx0XHR9KVxuXHRcdHtcblx0XHRcdC8qKlxuXHRcdFx0ICog5L6d54Wn5L2g57Wm55qEIHJlZ2V4cCDlhaflrrnkvoblm57lgrPnmoTos4fmlplcblx0XHRcdCAqL1xuXHRcdFx0aWYgKG1fbGFzdClcblx0XHRcdHtcblx0XHRcdFx0bGV0IHtcblx0XHRcdFx0XHQvKipcblx0XHRcdFx0XHQgKiDphY3lsI3liLDnmoTlhaflrrlcblx0XHRcdFx0XHQgKi9cblx0XHRcdFx0XHRtYXRjaCxcblx0XHRcdFx0XHQvKipcblx0XHRcdFx0XHQgKiDphY3lsI3lh7rkvobnmoTpmaPliJdcblx0XHRcdFx0XHQgKi9cblx0XHRcdFx0XHRzdWIsXG5cdFx0XHRcdH0gPSBtX2xhc3Q7XG5cblx0XHRcdFx0MCAmJiBjb25zb2xlLmRpcih7XG5cdFx0XHRcdFx0c3ViLFxuXHRcdFx0XHRcdG1hdGNoLFxuXHRcdFx0XHR9KTtcblxuXHRcdFx0XHQvKipcblx0XHRcdFx0ICogQHRvZG8g6YCZ6KOh5Y+v5Lul5Yqg5LiK5pu05aSa6Kqe5rOV5L6G5qC85byP5YyW5qiZ6aGMXG5cdFx0XHRcdCAqL1xuXHRcdFx0XHRuYW1lID0gcGFyc2VJbnQoc3ViWzBdKS50b1N0cmluZygpLnBhZFN0YXJ0KDMsICcwJyk7XG5cblx0XHRcdFx0LyoqXG5cdFx0XHRcdCAqIOWwh+WumuS9jem7nuWKoOS4iuacrOasoemFjeWwjeWIsOeahOWFp+WuueeahOmVt+W6plxuXHRcdFx0XHQgKiDmraTmraXpqZ/lj6/ku6XnnIHnlaVcblx0XHRcdFx0ICog5L2G5L2/55So5q2k5q2l6amf5pmC5Y+v5Lul5ZCM5pmC5Zyo5YiH5Ymy5pmC5bCN5pa85YWn5a655L2c57K+57ChXG5cdFx0XHRcdCAqL1xuXHRcdFx0XHRpZHggKz0gbV9sYXN0Lm1hdGNoLmxlbmd0aDtcblx0XHRcdH1cblxuXHRcdFx0cmV0dXJuIHtcblx0XHRcdFx0LyoqXG5cdFx0XHRcdCAqIOaqlOahiOW6j+WIlyjlhLLlrZjmqpTmoYjmmYLmnIPlgZrngrrliY3nva7oqZ4pXG5cdFx0XHRcdCAqL1xuXHRcdFx0XHRpZCxcblx0XHRcdFx0LyoqXG5cdFx0XHRcdCAqIOaomemhjOWQjeeosSDpoJDoqK3mg4Xms4HkuIvnrYnmlrwgbWF0Y2gg5Yiw55qE5qiZ6aGMXG5cdFx0XHRcdCAqL1xuXHRcdFx0XHRuYW1lLFxuXHRcdFx0XHQvKipcblx0XHRcdFx0ICog5pys5qyhIG1hdGNoIOeahCDlhafmlocgc3RhcnQgaW5kZXhcblx0XHRcdFx0ICog5Y+v6YCa6YGO5L+u5pS55pW45YC85L6G5o6n5Yi25YWn5paH56+E5ZyNXG5cdFx0XHRcdCAqXG5cdFx0XHRcdCAqIEBleGFtcGxlXG5cdFx0XHRcdCAqIGlkeCArPSBtX2xhc3QubWF0Y2gubGVuZ3RoOyAvLyDlhafmloflv73nlaXmnKzmrKEgbWF0Y2gg5Yiw55qE5qiZ6aGMXG5cdFx0XHRcdCAqL1xuXHRcdFx0XHRpZHgsXG5cdFx0XHR9XG5cdFx0fSxcblx0fSxcblxufTtcblxuZXhwb3J0IGRlZmF1bHQgdHBsT3B0aW9uc1xuIl19