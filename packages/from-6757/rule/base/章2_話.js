"use strict";
/**
 * Created by user on 2019/4/14.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const base_1 = require("../lib/base");
const rule_1 = require("../lib/rule");
const handleMatchSubIDString_1 = require("../lib/rule/handleMatchSubIDString");
const handleMatchSubID_1 = require("../lib/rule/handleMatchSubID");
let v_line = `☠☠☠♛☠☠☠☠☠☠♛☠☠☠♛☠☠☠`;
let c_line = `❄✡✥✩✧✫✪✭☆★✬✰✮✦⟡✯☸✠❇✣`;
let _space = ' 　\\t';
const c = '　';
exports.tplOptions = {
    ...base_1.default,
    /**
     * 這個參數 可刪除或加上 _ 如果沒有用到的話
     */
    volume: {
        //ignoreRe: /第四章はある事件/,
        /**
         * 故意放一個無效配對 實際使用時請自行更改
         *
         * 當沒有配對到的時候 會自動產生 00000_unknow 資料夾
         */
        r: [
            ``,
            '(',
            [
                `第[一-十]+章`,
                `第[\\d０-９]+章`,
            ].join('|'),
            ')',
            `[${_space}\\-]+`,
            `([^\\n]*)`,
            `[${_space}]*`,
            `$`,
        ],
        cb: rule_1.baseCbParseChapterMatchMain001([
            rule_1.createCbParseChapterMatch({
                handleMatchSubIDString: handleMatchSubIDString_1.handleMatchSubIDString101,
                handleMatchSubID: handleMatchSubID_1.handleMatchSubID002,
            }),
        ]),
    },
    /**
     * 這個參數是必填選項
     */
    chapter: {
        //ignoreRe: /5270|10倍|8岁|5岁！/,
        /**
         * 正常來說不需要弄得這麼複雜的樣式 只需要配對標題就好
         * 但懶惰想要在切 txt 的時候 順便縮減內文的話 就可以寫的複雜一點
         *
         * 當 r 不是 regexp 的時候 在執行時會自動將這個參數的內容轉成 regexp
         */
        r: [
            `^`,
            `[${_space}]*`,
            '(',
            [
                `第[一-十]+話`,
                `第[\\d０-９]+(?:\.[\\d０-９]+)?(?:話|集)`,
                'web[ ]*\\d+',
                '\\d+话?',
                '[０-９]+话?',
            ].join('|'),
            ')',
            `[${_space}\\-]*`,
            `([^\\n]*)`,
            `[${_space}]*`,
            `$`,
        ],
        cb: rule_1.baseCbParseChapterMatchMain001([
            rule_1.createCbParseChapterMatch({
                handleMatchSubID: handleMatchSubID_1.handleMatchSubID000,
                handleMatchSubIDString: handleMatchSubIDString_1.handleMatchSubIDString001,
            }),
        ]),
    },
};
exports.default = exports.tplOptions;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoi56ugMl/oqbEuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyLnq6AyX+ipsS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUE7O0dBRUc7O0FBYUgsc0NBQXlDO0FBR3pDLHNDQUtxQjtBQUNyQiwrRUFBMEc7QUFDMUcsbUVBQXdGO0FBRXhGLElBQUksTUFBTSxHQUFHLG9CQUFvQixDQUFDO0FBQ2xDLElBQUksTUFBTSxHQUFHLHNCQUFzQixDQUFDO0FBRXBDLElBQUksTUFBTSxHQUFHLE9BQU8sQ0FBQztBQUNyQixNQUFNLENBQUMsR0FBRyxHQUFHLENBQUM7QUFFRCxRQUFBLFVBQVUsR0FBeUI7SUFFL0MsR0FBRyxjQUFjO0lBRWpCOztPQUVHO0lBQ0gsTUFBTSxFQUFFO1FBRVAsdUJBQXVCO1FBRXZCOzs7O1dBSUc7UUFDSCxDQUFDLEVBQUU7WUFDRixFQUFFO1lBQ0YsR0FBRztZQUNIO2dCQUNDLFVBQVU7Z0JBQ1YsYUFBYTthQUNiLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQztZQUNYLEdBQUc7WUFDSCxJQUFJLE1BQU0sT0FBTztZQUNqQixXQUFXO1lBQ1gsSUFBSSxNQUFNLElBQUk7WUFDZCxHQUFHO1NBQ0g7UUFFRCxFQUFFLEVBQUUscUNBQThCLENBQUM7WUFFbEMsZ0NBQXlCLENBQUM7Z0JBRXpCLHNCQUFzQixFQUFFLGtEQUF5QjtnQkFFakQsZ0JBQWdCLEVBQUUsc0NBQW1CO2FBRXJDLENBQUM7U0FFRixDQUFDO0tBRUY7SUFFRDs7T0FFRztJQUNILE9BQU8sRUFBRTtRQUVSLDhCQUE4QjtRQUU5Qjs7Ozs7V0FLRztRQUNILENBQUMsRUFBRTtZQUNGLEdBQUc7WUFDSCxJQUFJLE1BQU0sSUFBSTtZQUNkLEdBQUc7WUFDSDtnQkFDQyxVQUFVO2dCQUNWLG1DQUFtQztnQkFDbkMsYUFBYTtnQkFDYixRQUFRO2dCQUNSLFVBQVU7YUFDVixDQUFDLElBQUksQ0FBQyxHQUFHLENBQUM7WUFDWCxHQUFHO1lBQ0gsSUFBSSxNQUFNLE9BQU87WUFDakIsV0FBVztZQUNYLElBQUksTUFBTSxJQUFJO1lBQ2QsR0FBRztTQUNIO1FBQ0QsRUFBRSxFQUFFLHFDQUE4QixDQUFDO1lBQ2xDLGdDQUF5QixDQUFDO2dCQUV6QixnQkFBZ0IsRUFBRSxzQ0FBbUI7Z0JBRXJDLHNCQUFzQixFQUFFLGtEQUF5QjthQUVqRCxDQUFDO1NBQ0YsQ0FBQztLQUNGO0NBRUQsQ0FBQztBQUVGLGtCQUFlLGtCQUFVLENBQUEiLCJzb3VyY2VzQ29udGVudCI6WyIvKipcbiAqIENyZWF0ZWQgYnkgdXNlciBvbiAyMDE5LzQvMTQuXG4gKi9cblxuaW1wb3J0IHsgX2hhbmRsZU9wdGlvbnMsIG1ha2VPcHRpb25zIH0gZnJvbSAnQG5vZGUtbm92ZWwvdHh0LXNwbGl0L2xpYi9pbmRleCc7XG5pbXBvcnQge1xuXHRJT3B0aW9ucyxcblx0SU9wdGlvbnNSZXF1aXJlZCxcblx0SU9wdGlvbnNSZXF1aXJlZFVzZXIsXG5cdElEYXRhVm9sdW1lLFxuXHRJRGF0YUNoYXB0ZXIsXG5cdElTcGxpdENCUGFyYW1ldGVycyxcbn0gZnJvbSAnQG5vZGUtbm92ZWwvdHh0LXNwbGl0L2xpYi9pbnRlcmZhY2UnO1xuXG5pbXBvcnQgeyBjb25zb2xlIH0gZnJvbSAnQG5vZGUtbm92ZWwvdHh0LXNwbGl0L2xpYi9jb25zb2xlJztcbmltcG9ydCB0cGxCYXNlT3B0aW9ucyBmcm9tICcuLi9saWIvYmFzZSc7XG5pbXBvcnQgU3RyVXRpbCA9IHJlcXVpcmUoJ3N0ci11dGlsJyk7XG5pbXBvcnQgbm92ZWxUZXh0IGZyb20gJ25vdmVsLXRleHQnO1xuaW1wb3J0IHtcblx0YmFzZUNiUGFyc2VDaGFwdGVyTWF0Y2gwMDEsXG5cdGJhc2VDYlBhcnNlQ2hhcHRlck1hdGNoTWFpbjAwMSxcblx0Y3JlYXRlQ2JQYXJzZUNoYXB0ZXJNYXRjaCxcblx0SUNyZWF0ZUNiUGFyc2VDaGFwdGVyTWF0Y2hPcHRpb25zU3ViLFxufSBmcm9tICcuLi9saWIvcnVsZSc7XG5pbXBvcnQgeyBoYW5kbGVNYXRjaFN1YklEU3RyaW5nMDAxLCBoYW5kbGVNYXRjaFN1YklEU3RyaW5nMTAxIH0gZnJvbSAnLi4vbGliL3J1bGUvaGFuZGxlTWF0Y2hTdWJJRFN0cmluZyc7XG5pbXBvcnQgeyBoYW5kbGVNYXRjaFN1YklEMDAwLCBoYW5kbGVNYXRjaFN1YklEMDAyIH0gZnJvbSAnLi4vbGliL3J1bGUvaGFuZGxlTWF0Y2hTdWJJRCc7XG5cbmxldCB2X2xpbmUgPSBg4pig4pig4pig4pmb4pig4pig4pig4pig4pig4pig4pmb4pig4pig4pig4pmb4pig4pig4pigYDtcbmxldCBjX2xpbmUgPSBg4p2E4pyh4pyl4pyp4pyn4pyr4pyq4pyt4piG4piF4pys4pyw4pyu4pym4p+h4pyv4pi44pyg4p2H4pyjYDtcblxubGV0IF9zcGFjZSA9ICcg44CAXFxcXHQnO1xuY29uc3QgYyA9ICfjgIAnO1xuXG5leHBvcnQgY29uc3QgdHBsT3B0aW9uczogSU9wdGlvbnNSZXF1aXJlZFVzZXIgPSB7XG5cblx0Li4udHBsQmFzZU9wdGlvbnMsXG5cblx0LyoqXG5cdCAqIOmAmeWAi+WPg+aVuCDlj6/liKrpmaTmiJbliqDkuIogXyDlpoLmnpzmspLmnInnlKjliLDnmoToqbFcblx0ICovXG5cdHZvbHVtZToge1xuXG5cdFx0Ly9pZ25vcmVSZTogL+esrOWbm+eroOOBr+OBguOCi+S6i+S7ti8sXG5cblx0XHQvKipcblx0XHQgKiDmlYXmhI/mlL7kuIDlgIvnhKHmlYjphY3lsI0g5a+m6Zqb5L2/55So5pmC6KuL6Ieq6KGM5pu05pS5XG5cdFx0ICpcblx0XHQgKiDnlbbmspLmnInphY3lsI3liLDnmoTmmYLlgJkg5pyD6Ieq5YuV55Si55SfIDAwMDAwX3Vua25vdyDos4fmlpnlpL5cblx0XHQgKi9cblx0XHRyOiBbXG5cdFx0XHRgYCxcblx0XHRcdCcoJyxcblx0XHRcdFtcblx0XHRcdFx0YOesrFvkuIAt5Y2BXSvnq6BgLFxuXHRcdFx0XHRg56ysW1xcXFxk77yQLe+8mV0r56ugYCxcblx0XHRcdF0uam9pbignfCcpLFxuXHRcdFx0JyknLFxuXHRcdFx0YFske19zcGFjZX1cXFxcLV0rYCxcblx0XHRcdGAoW15cXFxcbl0qKWAsXG5cdFx0XHRgWyR7X3NwYWNlfV0qYCxcblx0XHRcdGAkYCxcblx0XHRdLFxuXG5cdFx0Y2I6IGJhc2VDYlBhcnNlQ2hhcHRlck1hdGNoTWFpbjAwMShbXG5cblx0XHRcdGNyZWF0ZUNiUGFyc2VDaGFwdGVyTWF0Y2goe1xuXG5cdFx0XHRcdGhhbmRsZU1hdGNoU3ViSURTdHJpbmc6IGhhbmRsZU1hdGNoU3ViSURTdHJpbmcxMDEsXG5cblx0XHRcdFx0aGFuZGxlTWF0Y2hTdWJJRDogaGFuZGxlTWF0Y2hTdWJJRDAwMixcblxuXHRcdFx0fSksXG5cblx0XHRdKSxcblxuXHR9LFxuXG5cdC8qKlxuXHQgKiDpgJnlgIvlj4PmlbjmmK/lv4XloavpgbjpoIVcblx0ICovXG5cdGNoYXB0ZXI6IHtcblxuXHRcdC8vaWdub3JlUmU6IC81MjcwfDEw5YCNfDjlsoF8NeWyge+8gS8sXG5cblx0XHQvKipcblx0XHQgKiDmraPluLjkvoboqqrkuI3pnIDopoHlvITlvpfpgJnpurzopIfpm5znmoTmqKPlvI8g5Y+q6ZyA6KaB6YWN5bCN5qiZ6aGM5bCx5aW9XG5cdFx0ICog5L2G5oe25oOw5oOz6KaB5Zyo5YiHIHR4dCDnmoTmmYLlgJkg6aCG5L6/57iu5rib5YWn5paH55qE6KmxIOWwseWPr+S7peWvq+eahOikh+mbnOS4gOm7nlxuXHRcdCAqXG5cdFx0ICog55W2IHIg5LiN5pivIHJlZ2V4cCDnmoTmmYLlgJkg5Zyo5Z+36KGM5pmC5pyD6Ieq5YuV5bCH6YCZ5YCL5Y+D5pW455qE5YWn5a656L2J5oiQIHJlZ2V4cFxuXHRcdCAqL1xuXHRcdHI6IFtcblx0XHRcdGBeYCxcblx0XHRcdGBbJHtfc3BhY2V9XSpgLFxuXHRcdFx0JygnLFxuXHRcdFx0W1xuXHRcdFx0XHRg56ysW+S4gC3ljYFdK+ipsWAsXG5cdFx0XHRcdGDnrKxbXFxcXGTvvJAt77yZXSsoPzpcXC5bXFxcXGTvvJAt77yZXSspPyg/OuipsXzpm4YpYCxcblx0XHRcdFx0J3dlYlsgXSpcXFxcZCsnLFxuXHRcdFx0XHQnXFxcXGQr6K+dPycsXG5cdFx0XHRcdCdb77yQLe+8mV0r6K+dPycsXG5cdFx0XHRdLmpvaW4oJ3wnKSxcblx0XHRcdCcpJyxcblx0XHRcdGBbJHtfc3BhY2V9XFxcXC1dKmAsXG5cdFx0XHRgKFteXFxcXG5dKilgLFxuXHRcdFx0YFske19zcGFjZX1dKmAsXG5cdFx0XHRgJGAsXG5cdFx0XSxcblx0XHRjYjogYmFzZUNiUGFyc2VDaGFwdGVyTWF0Y2hNYWluMDAxKFtcblx0XHRcdGNyZWF0ZUNiUGFyc2VDaGFwdGVyTWF0Y2goe1xuXG5cdFx0XHRcdGhhbmRsZU1hdGNoU3ViSUQ6IGhhbmRsZU1hdGNoU3ViSUQwMDAsXG5cblx0XHRcdFx0aGFuZGxlTWF0Y2hTdWJJRFN0cmluZzogaGFuZGxlTWF0Y2hTdWJJRFN0cmluZzAwMSxcblxuXHRcdFx0fSksXG5cdFx0XSksXG5cdH0sXG5cbn07XG5cbmV4cG9ydCBkZWZhdWx0IHRwbE9wdGlvbnNcbiJdfQ==