/**
 * Created by user on 2019/4/14.
 */

import { _handleOptions, makeOptions } from '@node-novel/txt-split/lib/index';
import {
	IOptions,
	IOptionsRequired,
	IOptionsRequiredUser,
	IDataVolume,
	IDataChapter,
	ISplitCBParameters,
} from '@node-novel/txt-split/lib/interface';

import { console } from '@node-novel/txt-split/lib/console';
import tplBaseOptions from './章_話';
import {
	baseCbParseChapterMatch001,
	baseCbParseChapterMatch003, baseCbParseChapterMatch005,
	baseCbParseChapterMatchMain001,
	createCbParseChapterMatch, ICreateCbParseChapterMatchOptionsSub,
} from '../lib/rule';
import StrUtil = require('str-util');

let v_line = `☠☠☠♛☠☠☠☠☠☠♛☠☠☠♛☠☠☠`;
let c_line = `❄✡✥✩✧✫✪✭☆★✬✰✮✦⟡✯☸✠❇✣`;

let _space = ' 　\\t';
const c = '　';

export const tplOptions: IOptionsRequiredUser = {

	...tplBaseOptions,

	volume: {

		...tplBaseOptions.volume,

		disable: true,

		r: [
			`(?<=[。…！？]|\\n|^)`,
			'(',
			[
				`第[一-十]+章`,
				`第[\\d０-９]+章`,
			].join('|'),
			')',
			`[${_space}]+`,
			`([^\\n]*)`,
			`[${_space}]*`,
			``,
		],

		//ignoreRe: /第[\d０-９]+章\s+[^\n]+之畫家/i,

		ignoreCb(argv)
		{
			return false
		},

	},

	/**
	 * 這個參數是必填選項
	 */
	chapter: {

		...tplBaseOptions.chapter,

		r: [
			`^`,
			`[${_space}]*`,
			'(',
			[
				`第[一-十]+話`,
				`第[\\d０-９]+(?:話)`,
			].join('|'),
			')',
			`[${_space}\\-]*`,
			`([^\\n]*)`,
			`[${_space}]*`,
			`$`,
		],

		cb: baseCbParseChapterMatchMain001([

			baseCbParseChapterMatch005,

		]),

	},

};

export default tplOptions
