"use strict";
/**
 * Created by user on 2019/4/14.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const ___1 = require("./\u7AE0_\u8A71");
const rule_1 = require("../lib/rule");
const handleMatchSubID_1 = require("../lib/rule/handleMatchSubID");
let v_line = `☠☠☠♛☠☠☠☠☠☠♛☠☠☠♛☠☠☠`;
let c_line = `❄✡✥✩✧✫✪✭☆★✬✰✮✦⟡✯☸✠❇✣`;
let _space = ' 　\\t';
const c = '　';
exports.tplOptions = {
    ...___1.default,
    volume: {
        ...___1.default.volume,
        disable: true,
        r: [
            `(?<=[。…！？]|\\n|^)`,
            '(',
            [
                `第[一-十]+章`,
                `第[\\d０-９]+章`,
            ].join('|'),
            ')',
            `[${_space}]+`,
            `([^\\n]*)`,
            `[${_space}]*`,
            ``,
        ],
        //ignoreRe: /第[\d０-９]+章\s+[^\n]+之畫家/i,
        ignoreCb(argv) {
            return false;
        },
    },
    /**
     * 這個參數是必填選項
     */
    chapter: {
        ...___1.default.chapter,
        r: [
            `^`,
            '(',
            [
                `\\d+`,
            ].join('|'),
            ')',
            `(?:`,
            `[${_space}\\.]+`,
            `([^\\n]*)`,
            ')?',
            `[${_space}]*`,
            `$`,
        ],
        cb: rule_1.baseCbParseChapterMatchMain001([
            rule_1.createCbParseChapterMatch({
                handleMatchSubID: handleMatchSubID_1.handleMatchSubID000,
            }),
        ]),
    },
};
exports.default = exports.tplOptions;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiMDAwLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiMDAwLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQTs7R0FFRzs7QUFhSCx3Q0FBbUM7QUFDbkMsc0NBS3FCO0FBRXJCLG1FQUFtRTtBQUVuRSxJQUFJLE1BQU0sR0FBRyxvQkFBb0IsQ0FBQztBQUNsQyxJQUFJLE1BQU0sR0FBRyxzQkFBc0IsQ0FBQztBQUVwQyxJQUFJLE1BQU0sR0FBRyxPQUFPLENBQUM7QUFDckIsTUFBTSxDQUFDLEdBQUcsR0FBRyxDQUFDO0FBRUQsUUFBQSxVQUFVLEdBQXlCO0lBRS9DLEdBQUcsWUFBYztJQUVqQixNQUFNLEVBQUU7UUFFUCxHQUFHLFlBQWMsQ0FBQyxNQUFNO1FBRXhCLE9BQU8sRUFBRSxJQUFJO1FBRWIsQ0FBQyxFQUFFO1lBQ0YsbUJBQW1CO1lBQ25CLEdBQUc7WUFDSDtnQkFDQyxVQUFVO2dCQUNWLGFBQWE7YUFDYixDQUFDLElBQUksQ0FBQyxHQUFHLENBQUM7WUFDWCxHQUFHO1lBQ0gsSUFBSSxNQUFNLElBQUk7WUFDZCxXQUFXO1lBQ1gsSUFBSSxNQUFNLElBQUk7WUFDZCxFQUFFO1NBQ0Y7UUFFRCxzQ0FBc0M7UUFFdEMsUUFBUSxDQUFDLElBQUk7WUFFWixPQUFPLEtBQUssQ0FBQTtRQUNiLENBQUM7S0FFRDtJQUVEOztPQUVHO0lBQ0gsT0FBTyxFQUFFO1FBRVIsR0FBRyxZQUFjLENBQUMsT0FBTztRQUV6QixDQUFDLEVBQUU7WUFDRixHQUFHO1lBQ0gsR0FBRztZQUNIO2dCQUNDLE1BQU07YUFDTixDQUFDLElBQUksQ0FBQyxHQUFHLENBQUM7WUFDWCxHQUFHO1lBQ0gsS0FBSztZQUNMLElBQUksTUFBTSxPQUFPO1lBQ2pCLFdBQVc7WUFDWCxJQUFJO1lBQ0osSUFBSSxNQUFNLElBQUk7WUFDZCxHQUFHO1NBQ0g7UUFFRCxFQUFFLEVBQUUscUNBQThCLENBQUM7WUFDbEMsZ0NBQXlCLENBQUM7Z0JBRXpCLGdCQUFnQixFQUFFLHNDQUFtQjthQUVyQyxDQUFDO1NBQ0YsQ0FBQztLQUVGO0NBRUQsQ0FBQztBQUVGLGtCQUFlLGtCQUFVLENBQUEiLCJzb3VyY2VzQ29udGVudCI6WyIvKipcbiAqIENyZWF0ZWQgYnkgdXNlciBvbiAyMDE5LzQvMTQuXG4gKi9cblxuaW1wb3J0IHsgX2hhbmRsZU9wdGlvbnMsIG1ha2VPcHRpb25zIH0gZnJvbSAnQG5vZGUtbm92ZWwvdHh0LXNwbGl0L2xpYi9pbmRleCc7XG5pbXBvcnQge1xuXHRJT3B0aW9ucyxcblx0SU9wdGlvbnNSZXF1aXJlZCxcblx0SU9wdGlvbnNSZXF1aXJlZFVzZXIsXG5cdElEYXRhVm9sdW1lLFxuXHRJRGF0YUNoYXB0ZXIsXG5cdElTcGxpdENCUGFyYW1ldGVycyxcbn0gZnJvbSAnQG5vZGUtbm92ZWwvdHh0LXNwbGl0L2xpYi9pbnRlcmZhY2UnO1xuXG5pbXBvcnQgeyBjb25zb2xlIH0gZnJvbSAnQG5vZGUtbm92ZWwvdHh0LXNwbGl0L2xpYi9jb25zb2xlJztcbmltcG9ydCB0cGxCYXNlT3B0aW9ucyBmcm9tICcuL+eroF/oqbEnO1xuaW1wb3J0IHtcblx0YmFzZUNiUGFyc2VDaGFwdGVyTWF0Y2gwMDEsXG5cdGJhc2VDYlBhcnNlQ2hhcHRlck1hdGNoMDAzLFxuXHRiYXNlQ2JQYXJzZUNoYXB0ZXJNYXRjaE1haW4wMDEsXG5cdGNyZWF0ZUNiUGFyc2VDaGFwdGVyTWF0Y2gsIElDcmVhdGVDYlBhcnNlQ2hhcHRlck1hdGNoT3B0aW9uc1N1Yixcbn0gZnJvbSAnLi4vbGliL3J1bGUnO1xuaW1wb3J0IFN0clV0aWwgPSByZXF1aXJlKCdzdHItdXRpbCcpO1xuaW1wb3J0IHsgaGFuZGxlTWF0Y2hTdWJJRDAwMCB9IGZyb20gJy4uL2xpYi9ydWxlL2hhbmRsZU1hdGNoU3ViSUQnO1xuXG5sZXQgdl9saW5lID0gYOKYoOKYoOKYoOKZm+KYoOKYoOKYoOKYoOKYoOKYoOKZm+KYoOKYoOKYoOKZm+KYoOKYoOKYoGA7XG5sZXQgY19saW5lID0gYOKdhOKcoeKcpeKcqeKcp+Kcq+KcquKcreKYhuKYheKcrOKcsOKcruKcpuKfoeKcr+KYuOKcoOKdh+Kco2A7XG5cbmxldCBfc3BhY2UgPSAnIOOAgFxcXFx0JztcbmNvbnN0IGMgPSAn44CAJztcblxuZXhwb3J0IGNvbnN0IHRwbE9wdGlvbnM6IElPcHRpb25zUmVxdWlyZWRVc2VyID0ge1xuXG5cdC4uLnRwbEJhc2VPcHRpb25zLFxuXG5cdHZvbHVtZToge1xuXG5cdFx0Li4udHBsQmFzZU9wdGlvbnMudm9sdW1lLFxuXG5cdFx0ZGlzYWJsZTogdHJ1ZSxcblxuXHRcdHI6IFtcblx0XHRcdGAoPzw9W+OAguKApu+8ge+8n118XFxcXG58XilgLFxuXHRcdFx0JygnLFxuXHRcdFx0W1xuXHRcdFx0XHRg56ysW+S4gC3ljYFdK+eroGAsXG5cdFx0XHRcdGDnrKxbXFxcXGTvvJAt77yZXSvnq6BgLFxuXHRcdFx0XS5qb2luKCd8JyksXG5cdFx0XHQnKScsXG5cdFx0XHRgWyR7X3NwYWNlfV0rYCxcblx0XHRcdGAoW15cXFxcbl0qKWAsXG5cdFx0XHRgWyR7X3NwYWNlfV0qYCxcblx0XHRcdGBgLFxuXHRcdF0sXG5cblx0XHQvL2lnbm9yZVJlOiAv56ysW1xcZO+8kC3vvJldK+eroFxccytbXlxcbl0r5LmL55Wr5a62L2ksXG5cblx0XHRpZ25vcmVDYihhcmd2KVxuXHRcdHtcblx0XHRcdHJldHVybiBmYWxzZVxuXHRcdH0sXG5cblx0fSxcblxuXHQvKipcblx0ICog6YCZ5YCL5Y+D5pW45piv5b+F5aGr6YG46aCFXG5cdCAqL1xuXHRjaGFwdGVyOiB7XG5cblx0XHQuLi50cGxCYXNlT3B0aW9ucy5jaGFwdGVyLFxuXG5cdFx0cjogW1xuXHRcdFx0YF5gLFxuXHRcdFx0JygnLFxuXHRcdFx0W1xuXHRcdFx0XHRgXFxcXGQrYCxcblx0XHRcdF0uam9pbignfCcpLFxuXHRcdFx0JyknLFxuXHRcdFx0YCg/OmAsXG5cdFx0XHRgWyR7X3NwYWNlfVxcXFwuXStgLFxuXHRcdFx0YChbXlxcXFxuXSopYCxcblx0XHRcdCcpPycsXG5cdFx0XHRgWyR7X3NwYWNlfV0qYCxcblx0XHRcdGAkYCxcblx0XHRdLFxuXG5cdFx0Y2I6IGJhc2VDYlBhcnNlQ2hhcHRlck1hdGNoTWFpbjAwMShbXG5cdFx0XHRjcmVhdGVDYlBhcnNlQ2hhcHRlck1hdGNoKHtcblxuXHRcdFx0XHRoYW5kbGVNYXRjaFN1YklEOiBoYW5kbGVNYXRjaFN1YklEMDAwLFxuXG5cdFx0XHR9KSxcblx0XHRdKSxcblxuXHR9LFxuXG59O1xuXG5leHBvcnQgZGVmYXVsdCB0cGxPcHRpb25zXG4iXX0=