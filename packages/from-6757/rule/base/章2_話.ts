/**
 * Created by user on 2019/4/14.
 */

import { _handleOptions, makeOptions } from '@node-novel/txt-split/lib/index';
import {
	IOptions,
	IOptionsRequired,
	IOptionsRequiredUser,
	IDataVolume,
	IDataChapter,
	ISplitCBParameters,
} from '@node-novel/txt-split/lib/interface';

import { console } from '@node-novel/txt-split/lib/console';
import tplBaseOptions from '../lib/base';
import StrUtil = require('str-util');
import novelText from 'novel-text';
import {
	baseCbParseChapterMatch001,
	baseCbParseChapterMatchMain001,
	createCbParseChapterMatch,
	ICreateCbParseChapterMatchOptionsSub,
} from '../lib/rule';
import { handleMatchSubIDString001, handleMatchSubIDString101 } from '../lib/rule/handleMatchSubIDString';
import { handleMatchSubID000, handleMatchSubID002 } from '../lib/rule/handleMatchSubID';

let v_line = `☠☠☠♛☠☠☠☠☠☠♛☠☠☠♛☠☠☠`;
let c_line = `❄✡✥✩✧✫✪✭☆★✬✰✮✦⟡✯☸✠❇✣`;

let _space = ' 　\\t';
const c = '　';

export const tplOptions: IOptionsRequiredUser = {

	...tplBaseOptions,

	/**
	 * 這個參數 可刪除或加上 _ 如果沒有用到的話
	 */
	volume: {

		//ignoreRe: /第四章はある事件/,

		/**
		 * 故意放一個無效配對 實際使用時請自行更改
		 *
		 * 當沒有配對到的時候 會自動產生 00000_unknow 資料夾
		 */
		r: [
			``,
			'(',
			[
				`第[一-十]+章`,
				`第[\\d０-９]+章`,
			].join('|'),
			')',
			`[${_space}\\-]+`,
			`([^\\n]*)`,
			`[${_space}]*`,
			`$`,
		],

		cb: baseCbParseChapterMatchMain001([

			createCbParseChapterMatch({

				handleMatchSubIDString: handleMatchSubIDString101,

				handleMatchSubID: handleMatchSubID002,

			}),

		]),

	},

	/**
	 * 這個參數是必填選項
	 */
	chapter: {

		//ignoreRe: /5270|10倍|8岁|5岁！/,

		/**
		 * 正常來說不需要弄得這麼複雜的樣式 只需要配對標題就好
		 * 但懶惰想要在切 txt 的時候 順便縮減內文的話 就可以寫的複雜一點
		 *
		 * 當 r 不是 regexp 的時候 在執行時會自動將這個參數的內容轉成 regexp
		 */
		r: [
			`^`,
			`[${_space}]*`,
			'(',
			[
				`第[一-十]+話`,
				`第[\\d０-９]+(?:\.[\\d０-９]+)?(?:話|集)`,
				'web[ ]*\\d+',
				'\\d+话?',
				'[０-９]+话?',
			].join('|'),
			')',
			`[${_space}\\-]*`,
			`([^\\n]*)`,
			`[${_space}]*`,
			`$`,
		],
		cb: baseCbParseChapterMatchMain001([
			createCbParseChapterMatch({

				handleMatchSubID: handleMatchSubID000,

				handleMatchSubIDString: handleMatchSubIDString001,

			}),
		]),
	},

};

export default tplOptions
