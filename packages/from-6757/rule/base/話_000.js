"use strict";
/**
 * Created by user on 2019/4/14.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const ___1 = require("./\u7AE0_\u8A71");
const rule_1 = require("../lib/rule");
let v_line = `☠☠☠♛☠☠☠☠☠☠♛☠☠☠♛☠☠☠`;
let c_line = `❄✡✥✩✧✫✪✭☆★✬✰✮✦⟡✯☸✠❇✣`;
let _space = ' 　\\t';
const c = '　';
exports.tplOptions = {
    ...___1.default,
    volume: {
        ...___1.default.volume,
        disable: true,
        r: [
            `(?<=[。…！？]|\\n|^)`,
            '(',
            [
                `第[一-十]+章`,
                `第[\\d０-９]+章`,
            ].join('|'),
            ')',
            `[${_space}]+`,
            `([^\\n]*)`,
            `[${_space}]*`,
            ``,
        ],
        //ignoreRe: /第[\d０-９]+章\s+[^\n]+之畫家/i,
        ignoreCb(argv) {
            return false;
        },
    },
    /**
     * 這個參數是必填選項
     */
    chapter: {
        ...___1.default.chapter,
        r: [
            `^`,
            `[${_space}]*`,
            '(',
            [
                `第[一-十]+話`,
                `第[\\d０-９]+(?:話)`,
            ].join('|'),
            ')',
            `[${_space}\\-]*`,
            `([^\\n]*)`,
            `[${_space}]*`,
            `$`,
        ],
        cb: rule_1.baseCbParseChapterMatchMain001([
            rule_1.baseCbParseChapterMatch005,
        ]),
    },
};
exports.default = exports.tplOptions;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoi6KmxXzAwMC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIuipsV8wMDAudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBOztHQUVHOztBQWFILHdDQUFtQztBQUNuQyxzQ0FLcUI7QUFHckIsSUFBSSxNQUFNLEdBQUcsb0JBQW9CLENBQUM7QUFDbEMsSUFBSSxNQUFNLEdBQUcsc0JBQXNCLENBQUM7QUFFcEMsSUFBSSxNQUFNLEdBQUcsT0FBTyxDQUFDO0FBQ3JCLE1BQU0sQ0FBQyxHQUFHLEdBQUcsQ0FBQztBQUVELFFBQUEsVUFBVSxHQUF5QjtJQUUvQyxHQUFHLFlBQWM7SUFFakIsTUFBTSxFQUFFO1FBRVAsR0FBRyxZQUFjLENBQUMsTUFBTTtRQUV4QixPQUFPLEVBQUUsSUFBSTtRQUViLENBQUMsRUFBRTtZQUNGLG1CQUFtQjtZQUNuQixHQUFHO1lBQ0g7Z0JBQ0MsVUFBVTtnQkFDVixhQUFhO2FBQ2IsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDO1lBQ1gsR0FBRztZQUNILElBQUksTUFBTSxJQUFJO1lBQ2QsV0FBVztZQUNYLElBQUksTUFBTSxJQUFJO1lBQ2QsRUFBRTtTQUNGO1FBRUQsc0NBQXNDO1FBRXRDLFFBQVEsQ0FBQyxJQUFJO1lBRVosT0FBTyxLQUFLLENBQUE7UUFDYixDQUFDO0tBRUQ7SUFFRDs7T0FFRztJQUNILE9BQU8sRUFBRTtRQUVSLEdBQUcsWUFBYyxDQUFDLE9BQU87UUFFekIsQ0FBQyxFQUFFO1lBQ0YsR0FBRztZQUNILElBQUksTUFBTSxJQUFJO1lBQ2QsR0FBRztZQUNIO2dCQUNDLFVBQVU7Z0JBQ1YsaUJBQWlCO2FBQ2pCLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQztZQUNYLEdBQUc7WUFDSCxJQUFJLE1BQU0sT0FBTztZQUNqQixXQUFXO1lBQ1gsSUFBSSxNQUFNLElBQUk7WUFDZCxHQUFHO1NBQ0g7UUFFRCxFQUFFLEVBQUUscUNBQThCLENBQUM7WUFFbEMsaUNBQTBCO1NBRTFCLENBQUM7S0FFRjtDQUVELENBQUM7QUFFRixrQkFBZSxrQkFBVSxDQUFBIiwic291cmNlc0NvbnRlbnQiOlsiLyoqXG4gKiBDcmVhdGVkIGJ5IHVzZXIgb24gMjAxOS80LzE0LlxuICovXG5cbmltcG9ydCB7IF9oYW5kbGVPcHRpb25zLCBtYWtlT3B0aW9ucyB9IGZyb20gJ0Bub2RlLW5vdmVsL3R4dC1zcGxpdC9saWIvaW5kZXgnO1xuaW1wb3J0IHtcblx0SU9wdGlvbnMsXG5cdElPcHRpb25zUmVxdWlyZWQsXG5cdElPcHRpb25zUmVxdWlyZWRVc2VyLFxuXHRJRGF0YVZvbHVtZSxcblx0SURhdGFDaGFwdGVyLFxuXHRJU3BsaXRDQlBhcmFtZXRlcnMsXG59IGZyb20gJ0Bub2RlLW5vdmVsL3R4dC1zcGxpdC9saWIvaW50ZXJmYWNlJztcblxuaW1wb3J0IHsgY29uc29sZSB9IGZyb20gJ0Bub2RlLW5vdmVsL3R4dC1zcGxpdC9saWIvY29uc29sZSc7XG5pbXBvcnQgdHBsQmFzZU9wdGlvbnMgZnJvbSAnLi/nq6Bf6KmxJztcbmltcG9ydCB7XG5cdGJhc2VDYlBhcnNlQ2hhcHRlck1hdGNoMDAxLFxuXHRiYXNlQ2JQYXJzZUNoYXB0ZXJNYXRjaDAwMywgYmFzZUNiUGFyc2VDaGFwdGVyTWF0Y2gwMDUsXG5cdGJhc2VDYlBhcnNlQ2hhcHRlck1hdGNoTWFpbjAwMSxcblx0Y3JlYXRlQ2JQYXJzZUNoYXB0ZXJNYXRjaCwgSUNyZWF0ZUNiUGFyc2VDaGFwdGVyTWF0Y2hPcHRpb25zU3ViLFxufSBmcm9tICcuLi9saWIvcnVsZSc7XG5pbXBvcnQgU3RyVXRpbCA9IHJlcXVpcmUoJ3N0ci11dGlsJyk7XG5cbmxldCB2X2xpbmUgPSBg4pig4pig4pig4pmb4pig4pig4pig4pig4pig4pig4pmb4pig4pig4pig4pmb4pig4pig4pigYDtcbmxldCBjX2xpbmUgPSBg4p2E4pyh4pyl4pyp4pyn4pyr4pyq4pyt4piG4piF4pys4pyw4pyu4pym4p+h4pyv4pi44pyg4p2H4pyjYDtcblxubGV0IF9zcGFjZSA9ICcg44CAXFxcXHQnO1xuY29uc3QgYyA9ICfjgIAnO1xuXG5leHBvcnQgY29uc3QgdHBsT3B0aW9uczogSU9wdGlvbnNSZXF1aXJlZFVzZXIgPSB7XG5cblx0Li4udHBsQmFzZU9wdGlvbnMsXG5cblx0dm9sdW1lOiB7XG5cblx0XHQuLi50cGxCYXNlT3B0aW9ucy52b2x1bWUsXG5cblx0XHRkaXNhYmxlOiB0cnVlLFxuXG5cdFx0cjogW1xuXHRcdFx0YCg/PD1b44CC4oCm77yB77yfXXxcXFxcbnxeKWAsXG5cdFx0XHQnKCcsXG5cdFx0XHRbXG5cdFx0XHRcdGDnrKxb5LiALeWNgV0r56ugYCxcblx0XHRcdFx0YOesrFtcXFxcZO+8kC3vvJldK+eroGAsXG5cdFx0XHRdLmpvaW4oJ3wnKSxcblx0XHRcdCcpJyxcblx0XHRcdGBbJHtfc3BhY2V9XStgLFxuXHRcdFx0YChbXlxcXFxuXSopYCxcblx0XHRcdGBbJHtfc3BhY2V9XSpgLFxuXHRcdFx0YGAsXG5cdFx0XSxcblxuXHRcdC8vaWdub3JlUmU6IC/nrKxbXFxk77yQLe+8mV0r56ugXFxzK1teXFxuXSvkuYvnlavlrrYvaSxcblxuXHRcdGlnbm9yZUNiKGFyZ3YpXG5cdFx0e1xuXHRcdFx0cmV0dXJuIGZhbHNlXG5cdFx0fSxcblxuXHR9LFxuXG5cdC8qKlxuXHQgKiDpgJnlgIvlj4PmlbjmmK/lv4XloavpgbjpoIVcblx0ICovXG5cdGNoYXB0ZXI6IHtcblxuXHRcdC4uLnRwbEJhc2VPcHRpb25zLmNoYXB0ZXIsXG5cblx0XHRyOiBbXG5cdFx0XHRgXmAsXG5cdFx0XHRgWyR7X3NwYWNlfV0qYCxcblx0XHRcdCcoJyxcblx0XHRcdFtcblx0XHRcdFx0YOesrFvkuIAt5Y2BXSvoqbFgLFxuXHRcdFx0XHRg56ysW1xcXFxk77yQLe+8mV0rKD866KmxKWAsXG5cdFx0XHRdLmpvaW4oJ3wnKSxcblx0XHRcdCcpJyxcblx0XHRcdGBbJHtfc3BhY2V9XFxcXC1dKmAsXG5cdFx0XHRgKFteXFxcXG5dKilgLFxuXHRcdFx0YFske19zcGFjZX1dKmAsXG5cdFx0XHRgJGAsXG5cdFx0XSxcblxuXHRcdGNiOiBiYXNlQ2JQYXJzZUNoYXB0ZXJNYXRjaE1haW4wMDEoW1xuXG5cdFx0XHRiYXNlQ2JQYXJzZUNoYXB0ZXJNYXRjaDAwNSxcblxuXHRcdF0pLFxuXG5cdH0sXG5cbn07XG5cbmV4cG9ydCBkZWZhdWx0IHRwbE9wdGlvbnNcbiJdfQ==