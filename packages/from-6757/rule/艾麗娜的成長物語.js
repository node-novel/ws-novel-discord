"use strict";
/**
 * Created by user on 2019/4/14.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const console_1 = require("@node-novel/txt-split/lib/console");
const base_1 = require("./lib/base");
const rule_1 = require("./lib/rule");
const StrUtil = require("str-util");
let v_line = `💗⬗◈⬖💗⬗◈⬖💗⬗◈⬖💗⬗◈⬖💗⬗◈⬖💗`;
let v_line2 = `💎⬗◈⬖💎⬗◈⬖💎⬗◈⬖💎⬗◈⬖💎⬗◈⬖💎`;
let c_line = `🍒🍸💝❦💖❤💔💗💕💔❤💙💚💛💜💝💘🍸🍒`;
let c_line2 = `🍸🍒💝❦💖❤💔💗💕💔❤💙💚💛💜💝💘🍒🍸`;
exports.tplOptions = {
    ...base_1.default,
    /**
     * 這個參數 可刪除或加上 _ 如果沒有用到的話
     */
    volume: {
        /**
         * 故意放一個無效配對 實際使用時請自行更改
         *
         * 當沒有配對到的時候 會自動產生 00000_unknow 資料夾
         */
        r: [
            `${v_line}\n`,
            '([^\\n]+)\n',
            `${v_line2}\n+`,
        ],
        cb({ 
        /**
         * 於 match 列表中的 index 序列
         */
        i, 
        /**
         * 檔案序列(儲存檔案時會做為前置詞)
         */
        id, 
        /**
         * 標題名稱 預設情況下等於 match 到的標題
         */
        name, 
        /**
         * 本階段的 match 值
         */
        m, 
        /**
         * 上一次的 match 值
         *
         * 但是 實際上 這參數 才是本次 callback 真正的 match 內容
         */
        m_last, 
        /**
         * 目前已經分割的檔案列表與內容
         */
        _files, 
        /**
         * 於所有章節中的序列
         *
         * @readonly
         */
        ii, 
        /**
         * 本次 match 的 內文 start index
         * 可通過修改數值來控制內文範圍
         *
         * @example
         * idx += m_last.match.length; // 內文忽略本次 match 到的標題
         */
        idx, }) {
            /**
             * 依照你給的 regexp 內容來回傳的資料
             */
            if (m_last) {
                let { 
                /**
                 * 配對到的內容
                 */
                match, 
                /**
                 * 配對出來的陣列
                 */
                sub, } = m_last;
                0 && console_1.console.dir({
                    sub,
                    match,
                });
                /**
                 * @todo 這裡可以加上更多語法來格式化標題
                 */
                name = sub[0];
                /**
                 * 將定位點加上本次配對到的內容的長度
                 * 此步驟可以省略
                 * 但使用此步驟時可以同時在切割時對於內容作精簡
                 */
                idx += m_last.match.length;
            }
            return {
                /**
                 * 檔案序列(儲存檔案時會做為前置詞)
                 */
                id,
                /**
                 * 標題名稱 預設情況下等於 match 到的標題
                 */
                name,
                /**
                 * 本次 match 的 內文 start index
                 * 可通過修改數值來控制內文範圍
                 *
                 * @example
                 * idx += m_last.match.length; // 內文忽略本次 match 到的標題
                 */
                idx,
            };
        },
    },
    /**
     * 這個參數是必填選項
     */
    chapter: {
        ...base_1.default.chapter,
        r: [
            `${c_line}\\n`,
            '(',
            [
                '(?:幕間|終章)',
                `第[\\d０-９]+(?:\.[\\d０-９]+)?(?:話)`,
                `第[一-十]+(?:話)`,
            ].join('|'),
            ')?',
            `(?:`,
            `([^\\n]+?)`,
            ')',
            `\\n+${c_line2}\\n`,
        ],
        cb: rule_1.baseCbParseChapterMatchMain001([
            rule_1.createCbParseChapterMatch({
                handleMatchSubID(argv, _data) {
                    if (/^\d+(?:[\.]\d+)?$/.test(argv.id)) {
                        argv.id = String(argv.id);
                        argv.idn = StrUtil.toFullNumber(argv.id.toString());
                    }
                    argv.desc = argv.desc
                        .replace(/ /g, '　');
                    return argv;
                },
                handleMatchSubIDString(argv, _data) {
                    if (argv.idn) {
                        return `第${argv.idn}話`;
                    }
                    return argv.ido;
                },
            }),
        ]),
    },
};
exports.default = exports.tplOptions;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoi6Im+6bqX5aic55qE5oiQ6ZW354mp6KqeLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsi6Im+6bqX5aic55qE5oiQ6ZW354mp6KqeLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQTs7R0FFRzs7QUFZSCwrREFBNEQ7QUFDNUQscUNBQXdDO0FBQ3hDLHFDQUlzSDtBQUN0SCxvQ0FBcUM7QUFJckMsSUFBSSxNQUFNLEdBQUcsNkJBQTZCLENBQUM7QUFDM0MsSUFBSSxPQUFPLEdBQUcsNkJBQTZCLENBQUM7QUFDNUMsSUFBSSxNQUFNLEdBQUcscUNBQXFDLENBQUM7QUFDbkQsSUFBSSxPQUFPLEdBQUcscUNBQXFDLENBQUM7QUFFdkMsUUFBQSxVQUFVLEdBQXlCO0lBRS9DLEdBQUcsY0FBYztJQUVqQjs7T0FFRztJQUNILE1BQU0sRUFBRTtRQUNQOzs7O1dBSUc7UUFDSCxDQUFDLEVBQUU7WUFDRixHQUFHLE1BQU0sSUFBSTtZQUNiLGFBQWE7WUFDYixHQUFHLE9BQU8sS0FBSztTQUNmO1FBRUQsRUFBRSxDQUFDO1FBQ0Y7O1dBRUc7UUFDSCxDQUFDO1FBQ0Q7O1dBRUc7UUFDSCxFQUFFO1FBQ0Y7O1dBRUc7UUFDSCxJQUFJO1FBQ0o7O1dBRUc7UUFDSCxDQUFDO1FBQ0Q7Ozs7V0FJRztRQUNILE1BQU07UUFDTjs7V0FFRztRQUNILE1BQU07UUFDTjs7OztXQUlHO1FBQ0gsRUFBRTtRQUNGOzs7Ozs7V0FNRztRQUNILEdBQUcsR0FDSDtZQUVBOztlQUVHO1lBQ0gsSUFBSSxNQUFNLEVBQ1Y7Z0JBQ0MsSUFBSTtnQkFDSDs7bUJBRUc7Z0JBQ0gsS0FBSztnQkFDTDs7bUJBRUc7Z0JBQ0gsR0FBRyxHQUNILEdBQUcsTUFBTSxDQUFDO2dCQUVYLENBQUMsSUFBSSxpQkFBTyxDQUFDLEdBQUcsQ0FBQztvQkFDaEIsR0FBRztvQkFDSCxLQUFLO2lCQUNMLENBQUMsQ0FBQztnQkFFSDs7bUJBRUc7Z0JBQ0gsSUFBSSxHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFFZDs7OzttQkFJRztnQkFDSCxHQUFHLElBQUksTUFBTSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUM7YUFDM0I7WUFFRCxPQUFPO2dCQUNOOzttQkFFRztnQkFDSCxFQUFFO2dCQUNGOzttQkFFRztnQkFDSCxJQUFJO2dCQUNKOzs7Ozs7bUJBTUc7Z0JBQ0gsR0FBRzthQUNILENBQUE7UUFDRixDQUFDO0tBQ0Q7SUFFRDs7T0FFRztJQUNILE9BQU8sRUFBRTtRQUVSLEdBQUcsY0FBYyxDQUFDLE9BQU87UUFFekIsQ0FBQyxFQUFFO1lBQ0YsR0FBRyxNQUFNLEtBQUs7WUFDZCxHQUFHO1lBQ0g7Z0JBQ0MsV0FBVztnQkFDWCxpQ0FBaUM7Z0JBQ2pDLGNBQWM7YUFDZCxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUM7WUFDWCxJQUFJO1lBQ0osS0FBSztZQUNMLFlBQVk7WUFDWixHQUFHO1lBQ0gsT0FBTyxPQUFPLEtBQUs7U0FDbkI7UUFFRCxFQUFFLEVBQUUscUNBQThCLENBQUM7WUFFbEMsZ0NBQXlCLENBQUM7Z0JBRXpCLGdCQUFnQixDQUFDLElBQTBDLEVBQUUsS0FBeUI7b0JBRXJGLElBQUksbUJBQW1CLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsRUFDckM7d0JBQ0MsSUFBSSxDQUFDLEVBQUUsR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO3dCQUUxQixJQUFJLENBQUMsR0FBRyxHQUFHLE9BQU8sQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDO3FCQUNwRDtvQkFFRCxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxJQUFJO3lCQUNuQixPQUFPLENBQUMsSUFBSSxFQUFFLEdBQUcsQ0FBQyxDQUNuQjtvQkFFRCxPQUFPLElBQUksQ0FBQztnQkFDYixDQUFDO2dCQUVELHNCQUFzQixDQUFDLElBQTBDLEVBQUUsS0FBeUI7b0JBRTNGLElBQUksSUFBSSxDQUFDLEdBQUcsRUFDWjt3QkFDQyxPQUFPLElBQUksSUFBSSxDQUFDLEdBQUcsR0FBRyxDQUFDO3FCQUN2QjtvQkFFRCxPQUFPLElBQUksQ0FBQyxHQUFHLENBQUM7Z0JBQ2pCLENBQUM7YUFFRCxDQUFDO1NBRUYsQ0FBQztLQUVGO0NBRUQsQ0FBQztBQUVGLGtCQUFlLGtCQUFVLENBQUEiLCJzb3VyY2VzQ29udGVudCI6WyIvKipcbiAqIENyZWF0ZWQgYnkgdXNlciBvbiAyMDE5LzQvMTQuXG4gKi9cblxuaW1wb3J0IHsgX2hhbmRsZU9wdGlvbnMsIG1ha2VPcHRpb25zIH0gZnJvbSAnQG5vZGUtbm92ZWwvdHh0LXNwbGl0L2xpYi9pbmRleCc7XG5pbXBvcnQge1xuXHRJT3B0aW9ucyxcblx0SU9wdGlvbnNSZXF1aXJlZCxcblx0SU9wdGlvbnNSZXF1aXJlZFVzZXIsXG5cdElEYXRhVm9sdW1lLFxuXHRJRGF0YUNoYXB0ZXIsXG5cdElTcGxpdENCUGFyYW1ldGVycyxcbn0gZnJvbSAnQG5vZGUtbm92ZWwvdHh0LXNwbGl0L2xpYi9pbnRlcmZhY2UnO1xuXG5pbXBvcnQgeyBjb25zb2xlIH0gZnJvbSAnQG5vZGUtbm92ZWwvdHh0LXNwbGl0L2xpYi9jb25zb2xlJztcbmltcG9ydCB0cGxCYXNlT3B0aW9ucyBmcm9tICcuL2xpYi9iYXNlJztcbmltcG9ydCB7XG5cdGJhc2VDYlBhcnNlQ2hhcHRlck1hdGNoMDAxLFxuXHRiYXNlQ2JQYXJzZUNoYXB0ZXJNYXRjaDAwMixcblx0YmFzZUNiUGFyc2VDaGFwdGVyTWF0Y2gwMDMsIGJhc2VDYlBhcnNlQ2hhcHRlck1hdGNoMDA0LFxuXHRiYXNlQ2JQYXJzZUNoYXB0ZXJNYXRjaE1haW4wMDEsIGNyZWF0ZUNiUGFyc2VDaGFwdGVyTWF0Y2gsIElDcmVhdGVDYlBhcnNlQ2hhcHRlck1hdGNoT3B0aW9uc1N1YiwgfSBmcm9tICcuL2xpYi9ydWxlJztcbmltcG9ydCBTdHJVdGlsID0gcmVxdWlyZSgnc3RyLXV0aWwnKTtcbmltcG9ydCBub3ZlbFRleHQgZnJvbSAnbm92ZWwtdGV4dCc7XG5pbXBvcnQgeyB0cmltRGVzYyB9IGZyb20gJy4vbGliL2luZGV4JztcblxubGV0IHZfbGluZSA9IGDwn5KX4qyX4peI4qyW8J+Sl+Ksl+KXiOKslvCfkpfirJfil4jirJbwn5KX4qyX4peI4qyW8J+Sl+Ksl+KXiOKslvCfkpdgO1xubGV0IHZfbGluZTIgPSBg8J+SjuKsl+KXiOKslvCfko7irJfil4jirJbwn5KO4qyX4peI4qyW8J+SjuKsl+KXiOKslvCfko7irJfil4jirJbwn5KOYDtcbmxldCBjX2xpbmUgPSBg8J+NkvCfjbjwn5Kd4p2m8J+SluKdpPCfkpTwn5KX8J+SlfCfkpTinaTwn5KZ8J+SmvCfkpvwn5Kc8J+SnfCfkpjwn4248J+NkmA7XG5sZXQgY19saW5lMiA9IGDwn4248J+NkvCfkp3inabwn5KW4p2k8J+SlPCfkpfwn5KV8J+SlOKdpPCfkpnwn5Ka8J+Sm/Cfkpzwn5Kd8J+SmPCfjZLwn424YDtcblxuZXhwb3J0IGNvbnN0IHRwbE9wdGlvbnM6IElPcHRpb25zUmVxdWlyZWRVc2VyID0ge1xuXG5cdC4uLnRwbEJhc2VPcHRpb25zLFxuXG5cdC8qKlxuXHQgKiDpgJnlgIvlj4Pmlbgg5Y+v5Yiq6Zmk5oiW5Yqg5LiKIF8g5aaC5p6c5rKS5pyJ55So5Yiw55qE6KmxXG5cdCAqL1xuXHR2b2x1bWU6IHtcblx0XHQvKipcblx0XHQgKiDmlYXmhI/mlL7kuIDlgIvnhKHmlYjphY3lsI0g5a+m6Zqb5L2/55So5pmC6KuL6Ieq6KGM5pu05pS5XG5cdFx0ICpcblx0XHQgKiDnlbbmspLmnInphY3lsI3liLDnmoTmmYLlgJkg5pyD6Ieq5YuV55Si55SfIDAwMDAwX3Vua25vdyDos4fmlpnlpL5cblx0XHQgKi9cblx0XHRyOiBbXG5cdFx0XHRgJHt2X2xpbmV9XFxuYCxcblx0XHRcdCcoW15cXFxcbl0rKVxcbicsXG5cdFx0XHRgJHt2X2xpbmUyfVxcbitgLFxuXHRcdF0sXG5cblx0XHRjYih7XG5cdFx0XHQvKipcblx0XHRcdCAqIOaWvCBtYXRjaCDliJfooajkuK3nmoQgaW5kZXgg5bqP5YiXXG5cdFx0XHQgKi9cblx0XHRcdGksXG5cdFx0XHQvKipcblx0XHRcdCAqIOaqlOahiOW6j+WIlyjlhLLlrZjmqpTmoYjmmYLmnIPlgZrngrrliY3nva7oqZ4pXG5cdFx0XHQgKi9cblx0XHRcdGlkLFxuXHRcdFx0LyoqXG5cdFx0XHQgKiDmqJnpoYzlkI3nqLEg6aCQ6Kit5oOF5rOB5LiL562J5pa8IG1hdGNoIOWIsOeahOaomemhjFxuXHRcdFx0ICovXG5cdFx0XHRuYW1lLFxuXHRcdFx0LyoqXG5cdFx0XHQgKiDmnKzpmo7mrrXnmoQgbWF0Y2gg5YC8XG5cdFx0XHQgKi9cblx0XHRcdG0sXG5cdFx0XHQvKipcblx0XHRcdCAqIOS4iuS4gOasoeeahCBtYXRjaCDlgLxcblx0XHRcdCAqXG5cdFx0XHQgKiDkvYbmmK8g5a+m6Zqb5LiKIOmAmeWPg+aVuCDmiY3mmK/mnKzmrKEgY2FsbGJhY2sg55yf5q2j55qEIG1hdGNoIOWFp+WuuVxuXHRcdFx0ICovXG5cdFx0XHRtX2xhc3QsXG5cdFx0XHQvKipcblx0XHRcdCAqIOebruWJjeW3sue2k+WIhuWJsueahOaqlOahiOWIl+ihqOiIh+WFp+WuuVxuXHRcdFx0ICovXG5cdFx0XHRfZmlsZXMsXG5cdFx0XHQvKipcblx0XHRcdCAqIOaWvOaJgOacieeroOevgOS4reeahOW6j+WIl1xuXHRcdFx0ICpcblx0XHRcdCAqIEByZWFkb25seVxuXHRcdFx0ICovXG5cdFx0XHRpaSxcblx0XHRcdC8qKlxuXHRcdFx0ICog5pys5qyhIG1hdGNoIOeahCDlhafmlocgc3RhcnQgaW5kZXhcblx0XHRcdCAqIOWPr+mAmumBjuS/ruaUueaVuOWAvOS+huaOp+WItuWFp+aWh+evhOWcjVxuXHRcdFx0ICpcblx0XHRcdCAqIEBleGFtcGxlXG5cdFx0XHQgKiBpZHggKz0gbV9sYXN0Lm1hdGNoLmxlbmd0aDsgLy8g5YWn5paH5b+955Wl5pys5qyhIG1hdGNoIOWIsOeahOaomemhjFxuXHRcdFx0ICovXG5cdFx0XHRpZHgsXG5cdFx0fSlcblx0XHR7XG5cdFx0XHQvKipcblx0XHRcdCAqIOS+neeFp+S9oOe1pueahCByZWdleHAg5YWn5a655L6G5Zue5YKz55qE6LOH5paZXG5cdFx0XHQgKi9cblx0XHRcdGlmIChtX2xhc3QpXG5cdFx0XHR7XG5cdFx0XHRcdGxldCB7XG5cdFx0XHRcdFx0LyoqXG5cdFx0XHRcdFx0ICog6YWN5bCN5Yiw55qE5YWn5a65XG5cdFx0XHRcdFx0ICovXG5cdFx0XHRcdFx0bWF0Y2gsXG5cdFx0XHRcdFx0LyoqXG5cdFx0XHRcdFx0ICog6YWN5bCN5Ye65L6G55qE6Zmj5YiXXG5cdFx0XHRcdFx0ICovXG5cdFx0XHRcdFx0c3ViLFxuXHRcdFx0XHR9ID0gbV9sYXN0O1xuXG5cdFx0XHRcdDAgJiYgY29uc29sZS5kaXIoe1xuXHRcdFx0XHRcdHN1Yixcblx0XHRcdFx0XHRtYXRjaCxcblx0XHRcdFx0fSk7XG5cblx0XHRcdFx0LyoqXG5cdFx0XHRcdCAqIEB0b2RvIOmAmeijoeWPr+S7peWKoOS4iuabtOWkmuiqnuazleS+huagvOW8j+WMluaomemhjFxuXHRcdFx0XHQgKi9cblx0XHRcdFx0bmFtZSA9IHN1YlswXTtcblxuXHRcdFx0XHQvKipcblx0XHRcdFx0ICog5bCH5a6a5L2N6bue5Yqg5LiK5pys5qyh6YWN5bCN5Yiw55qE5YWn5a6555qE6ZW35bqmXG5cdFx0XHRcdCAqIOatpOatpempn+WPr+S7peecgeeVpVxuXHRcdFx0XHQgKiDkvYbkvb/nlKjmraTmraXpqZ/mmYLlj6/ku6XlkIzmmYLlnKjliIflibLmmYLlsI3mlrzlhaflrrnkvZznsr7nsKFcblx0XHRcdFx0ICovXG5cdFx0XHRcdGlkeCArPSBtX2xhc3QubWF0Y2gubGVuZ3RoO1xuXHRcdFx0fVxuXG5cdFx0XHRyZXR1cm4ge1xuXHRcdFx0XHQvKipcblx0XHRcdFx0ICog5qqU5qGI5bqP5YiXKOWEsuWtmOaqlOahiOaZguacg+WBmueCuuWJjee9ruipnilcblx0XHRcdFx0ICovXG5cdFx0XHRcdGlkLFxuXHRcdFx0XHQvKipcblx0XHRcdFx0ICog5qiZ6aGM5ZCN56ixIOmgkOioreaDheazgeS4i+etieaWvCBtYXRjaCDliLDnmoTmqJnpoYxcblx0XHRcdFx0ICovXG5cdFx0XHRcdG5hbWUsXG5cdFx0XHRcdC8qKlxuXHRcdFx0XHQgKiDmnKzmrKEgbWF0Y2gg55qEIOWFp+aWhyBzdGFydCBpbmRleFxuXHRcdFx0XHQgKiDlj6/pgJrpgY7kv67mlLnmlbjlgLzkvobmjqfliLblhafmlofnr4TlnI1cblx0XHRcdFx0ICpcblx0XHRcdFx0ICogQGV4YW1wbGVcblx0XHRcdFx0ICogaWR4ICs9IG1fbGFzdC5tYXRjaC5sZW5ndGg7IC8vIOWFp+aWh+W/veeVpeacrOasoSBtYXRjaCDliLDnmoTmqJnpoYxcblx0XHRcdFx0ICovXG5cdFx0XHRcdGlkeCxcblx0XHRcdH1cblx0XHR9LFxuXHR9LFxuXG5cdC8qKlxuXHQgKiDpgJnlgIvlj4PmlbjmmK/lv4XloavpgbjpoIVcblx0ICovXG5cdGNoYXB0ZXI6IHtcblxuXHRcdC4uLnRwbEJhc2VPcHRpb25zLmNoYXB0ZXIsXG5cblx0XHRyOiBbXG5cdFx0XHRgJHtjX2xpbmV9XFxcXG5gLFxuXHRcdFx0JygnLFxuXHRcdFx0W1xuXHRcdFx0XHQnKD865bmV6ZaTfOe1gueroCknLFxuXHRcdFx0XHRg56ysW1xcXFxk77yQLe+8mV0rKD86XFwuW1xcXFxk77yQLe+8mV0rKT8oPzroqbEpYCxcblx0XHRcdFx0YOesrFvkuIAt5Y2BXSsoPzroqbEpYCxcblx0XHRcdF0uam9pbignfCcpLFxuXHRcdFx0Jyk/Jyxcblx0XHRcdGAoPzpgLFxuXHRcdFx0YChbXlxcXFxuXSs/KWAsXG5cdFx0XHQnKScsXG5cdFx0XHRgXFxcXG4rJHtjX2xpbmUyfVxcXFxuYCxcblx0XHRdLFxuXG5cdFx0Y2I6IGJhc2VDYlBhcnNlQ2hhcHRlck1hdGNoTWFpbjAwMShbXG5cblx0XHRcdGNyZWF0ZUNiUGFyc2VDaGFwdGVyTWF0Y2goe1xuXG5cdFx0XHRcdGhhbmRsZU1hdGNoU3ViSUQoYXJndjogSUNyZWF0ZUNiUGFyc2VDaGFwdGVyTWF0Y2hPcHRpb25zU3ViLCBfZGF0YTogSVNwbGl0Q0JQYXJhbWV0ZXJzKVxuXHRcdFx0XHR7XG5cdFx0XHRcdFx0aWYgKC9eXFxkKyg/OltcXC5dXFxkKyk/JC8udGVzdChhcmd2LmlkKSlcblx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHRhcmd2LmlkID0gU3RyaW5nKGFyZ3YuaWQpO1xuXG5cdFx0XHRcdFx0XHRhcmd2LmlkbiA9IFN0clV0aWwudG9GdWxsTnVtYmVyKGFyZ3YuaWQudG9TdHJpbmcoKSk7XG5cdFx0XHRcdFx0fVxuXG5cdFx0XHRcdFx0YXJndi5kZXNjID0gYXJndi5kZXNjXG5cdFx0XHRcdFx0XHQucmVwbGFjZSgvIC9nLCAn44CAJylcblx0XHRcdFx0XHQ7XG5cblx0XHRcdFx0XHRyZXR1cm4gYXJndjtcblx0XHRcdFx0fSxcblxuXHRcdFx0XHRoYW5kbGVNYXRjaFN1YklEU3RyaW5nKGFyZ3Y6IElDcmVhdGVDYlBhcnNlQ2hhcHRlck1hdGNoT3B0aW9uc1N1YiwgX2RhdGE6IElTcGxpdENCUGFyYW1ldGVycyk6IHN0cmluZ1xuXHRcdFx0XHR7XG5cdFx0XHRcdFx0aWYgKGFyZ3YuaWRuKVxuXHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdHJldHVybiBg56ysJHthcmd2Lmlkbn3oqbFgO1xuXHRcdFx0XHRcdH1cblxuXHRcdFx0XHRcdHJldHVybiBhcmd2Lmlkbztcblx0XHRcdFx0fSxcblxuXHRcdFx0fSksXG5cblx0XHRdKSxcblxuXHR9LFxuXG59O1xuXG5leHBvcnQgZGVmYXVsdCB0cGxPcHRpb25zXG4iXX0=