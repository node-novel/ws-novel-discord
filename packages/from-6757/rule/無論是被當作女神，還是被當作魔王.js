"use strict";
/**
 * Created by user on 2019/4/14.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const ___1 = require("./base/\u7AE0_\u8A71");
const rule_1 = require("./lib/rule");
const StrUtil = require("str-util");
let v_line = `☠☠☠♛☠☠☠☠☠☠♛☠☠☠♛☠☠☠`;
let c_line = `❄✡✥✩✧✫✪✭☆★✬✰✮✦⟡✯☸✠❇✣`;
let _space = ' 　\\t';
const c = '　';
exports.tplOptions = {
    ...___1.default,
    volume: null,
    /**
     * 這個參數是必填選項
     */
    chapter: {
        ...___1.default.chapter,
        r: [
            `^`,
            '(',
            [
                '序章',
                '(?:幕間|終章)',
                `第[\\d０-９]+(?:\.[\\d０-９]+)?(?:話)`,
                `第[一-十]+話`,
            ].join('|'),
            ')',
            `(?:`,
            `[${_space}\\.]+`,
            `([^\\n]*)`,
            ')?',
            `[${_space}]*`,
            `$`,
        ],
        cb: rule_1.baseCbParseChapterMatchMain001([
            rule_1.createCbParseChapterMatch({
                handleMatchSubID(argv, _data) {
                    if (/^\d+(?:\.\d+)?$/.test(argv.id)) {
                        argv.id = String(argv.id)
                            .replace(/^\d+/, function (s) {
                            return s.padStart(3, '0');
                        });
                        argv.idn = StrUtil.toHalfNumber(argv.id.toString());
                    }
                    return argv;
                },
            }),
        ]),
    },
};
exports.default = exports.tplOptions;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoi54Sh6KuW5piv6KKr55W25L2c5aWz56We77yM6YKE5piv6KKr55W25L2c6a2U546LLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsi54Sh6KuW5piv6KKr55W25L2c5aWz56We77yM6YKE5piv6KKr55W25L2c6a2U546LLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQTs7R0FFRzs7QUFhSCw2Q0FBd0M7QUFDeEMscUNBS29CO0FBQ3BCLG9DQUFxQztBQUVyQyxJQUFJLE1BQU0sR0FBRyxvQkFBb0IsQ0FBQztBQUNsQyxJQUFJLE1BQU0sR0FBRyxzQkFBc0IsQ0FBQztBQUVwQyxJQUFJLE1BQU0sR0FBRyxPQUFPLENBQUM7QUFDckIsTUFBTSxDQUFDLEdBQUcsR0FBRyxDQUFDO0FBRUQsUUFBQSxVQUFVLEdBQXlCO0lBRS9DLEdBQUcsWUFBYztJQUVqQixNQUFNLEVBQUUsSUFBSTtJQUVaOztPQUVHO0lBQ0gsT0FBTyxFQUFFO1FBRVIsR0FBRyxZQUFjLENBQUMsT0FBTztRQUV6QixDQUFDLEVBQUU7WUFDRixHQUFHO1lBQ0gsR0FBRztZQUNIO2dCQUNDLElBQUk7Z0JBQ0osV0FBVztnQkFDWCxpQ0FBaUM7Z0JBQ2pDLFVBQVU7YUFDVixDQUFDLElBQUksQ0FBQyxHQUFHLENBQUM7WUFDWCxHQUFHO1lBQ0gsS0FBSztZQUNMLElBQUksTUFBTSxPQUFPO1lBQ2pCLFdBQVc7WUFDWCxJQUFJO1lBQ0osSUFBSSxNQUFNLElBQUk7WUFDZCxHQUFHO1NBQ0g7UUFFRCxFQUFFLEVBQUUscUNBQThCLENBQUM7WUFDbEMsZ0NBQXlCLENBQUM7Z0JBRXpCLGdCQUFnQixDQUFDLElBQTBDLEVBQUUsS0FBeUI7b0JBRXJGLElBQUksaUJBQWlCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsRUFDbkM7d0JBRUMsSUFBSSxDQUFDLEVBQUUsR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQzs2QkFDdkIsT0FBTyxDQUFDLE1BQU0sRUFBRSxVQUFVLENBQUM7NEJBRTNCLE9BQU8sQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUM7d0JBQzNCLENBQUMsQ0FBQyxDQUNGO3dCQUVELElBQUksQ0FBQyxHQUFHLEdBQUcsT0FBTyxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUM7cUJBQ3BEO29CQUVELE9BQU8sSUFBSSxDQUFDO2dCQUNiLENBQUM7YUFFRCxDQUFDO1NBQ0YsQ0FBQztLQUVGO0NBRUQsQ0FBQztBQUVGLGtCQUFlLGtCQUFVLENBQUEiLCJzb3VyY2VzQ29udGVudCI6WyIvKipcbiAqIENyZWF0ZWQgYnkgdXNlciBvbiAyMDE5LzQvMTQuXG4gKi9cblxuaW1wb3J0IHsgX2hhbmRsZU9wdGlvbnMsIG1ha2VPcHRpb25zIH0gZnJvbSAnQG5vZGUtbm92ZWwvdHh0LXNwbGl0L2xpYi9pbmRleCc7XG5pbXBvcnQge1xuXHRJT3B0aW9ucyxcblx0SU9wdGlvbnNSZXF1aXJlZCxcblx0SU9wdGlvbnNSZXF1aXJlZFVzZXIsXG5cdElEYXRhVm9sdW1lLFxuXHRJRGF0YUNoYXB0ZXIsXG5cdElTcGxpdENCUGFyYW1ldGVycyxcbn0gZnJvbSAnQG5vZGUtbm92ZWwvdHh0LXNwbGl0L2xpYi9pbnRlcmZhY2UnO1xuXG5pbXBvcnQgeyBjb25zb2xlIH0gZnJvbSAnQG5vZGUtbm92ZWwvdHh0LXNwbGl0L2xpYi9jb25zb2xlJztcbmltcG9ydCB0cGxCYXNlT3B0aW9ucyBmcm9tICcuL2Jhc2Uv56ugX+ipsSc7XG5pbXBvcnQge1xuXHRiYXNlQ2JQYXJzZUNoYXB0ZXJNYXRjaDAwMSxcblx0YmFzZUNiUGFyc2VDaGFwdGVyTWF0Y2gwMDMsXG5cdGJhc2VDYlBhcnNlQ2hhcHRlck1hdGNoTWFpbjAwMSxcblx0Y3JlYXRlQ2JQYXJzZUNoYXB0ZXJNYXRjaCwgSUNyZWF0ZUNiUGFyc2VDaGFwdGVyTWF0Y2hPcHRpb25zU3ViLFxufSBmcm9tICcuL2xpYi9ydWxlJztcbmltcG9ydCBTdHJVdGlsID0gcmVxdWlyZSgnc3RyLXV0aWwnKTtcblxubGV0IHZfbGluZSA9IGDimKDimKDimKDimZvimKDimKDimKDimKDimKDimKDimZvimKDimKDimKDimZvimKDimKDimKBgO1xubGV0IGNfbGluZSA9IGDinYTinKHinKXinKninKfinKvinKrinK3imIbimIXinKzinLDinK7inKbin6HinK/imLjinKDinYfinKNgO1xuXG5sZXQgX3NwYWNlID0gJyDjgIBcXFxcdCc7XG5jb25zdCBjID0gJ+OAgCc7XG5cbmV4cG9ydCBjb25zdCB0cGxPcHRpb25zOiBJT3B0aW9uc1JlcXVpcmVkVXNlciA9IHtcblxuXHQuLi50cGxCYXNlT3B0aW9ucyxcblxuXHR2b2x1bWU6IG51bGwsXG5cblx0LyoqXG5cdCAqIOmAmeWAi+WPg+aVuOaYr+W/heWhq+mBuOmghVxuXHQgKi9cblx0Y2hhcHRlcjoge1xuXG5cdFx0Li4udHBsQmFzZU9wdGlvbnMuY2hhcHRlcixcblxuXHRcdHI6IFtcblx0XHRcdGBeYCxcblx0XHRcdCcoJyxcblx0XHRcdFtcblx0XHRcdFx0J+W6j+eroCcsXG5cdFx0XHRcdCcoPzrluZXplpN857WC56ugKScsXG5cdFx0XHRcdGDnrKxbXFxcXGTvvJAt77yZXSsoPzpcXC5bXFxcXGTvvJAt77yZXSspPyg/OuipsSlgLFxuXHRcdFx0XHRg56ysW+S4gC3ljYFdK+ipsWAsXG5cdFx0XHRdLmpvaW4oJ3wnKSxcblx0XHRcdCcpJyxcblx0XHRcdGAoPzpgLFxuXHRcdFx0YFske19zcGFjZX1cXFxcLl0rYCxcblx0XHRcdGAoW15cXFxcbl0qKWAsXG5cdFx0XHQnKT8nLFxuXHRcdFx0YFske19zcGFjZX1dKmAsXG5cdFx0XHRgJGAsXG5cdFx0XSxcblxuXHRcdGNiOiBiYXNlQ2JQYXJzZUNoYXB0ZXJNYXRjaE1haW4wMDEoW1xuXHRcdFx0Y3JlYXRlQ2JQYXJzZUNoYXB0ZXJNYXRjaCh7XG5cblx0XHRcdFx0aGFuZGxlTWF0Y2hTdWJJRChhcmd2OiBJQ3JlYXRlQ2JQYXJzZUNoYXB0ZXJNYXRjaE9wdGlvbnNTdWIsIF9kYXRhOiBJU3BsaXRDQlBhcmFtZXRlcnMpXG5cdFx0XHRcdHtcblx0XHRcdFx0XHRpZiAoL15cXGQrKD86XFwuXFxkKyk/JC8udGVzdChhcmd2LmlkKSlcblx0XHRcdFx0XHR7XG5cblx0XHRcdFx0XHRcdGFyZ3YuaWQgPSBTdHJpbmcoYXJndi5pZClcblx0XHRcdFx0XHRcdFx0LnJlcGxhY2UoL15cXGQrLywgZnVuY3Rpb24gKHMpXG5cdFx0XHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdFx0XHRyZXR1cm4gcy5wYWRTdGFydCgzLCAnMCcpO1xuXHRcdFx0XHRcdFx0XHR9KVxuXHRcdFx0XHRcdFx0O1xuXG5cdFx0XHRcdFx0XHRhcmd2LmlkbiA9IFN0clV0aWwudG9IYWxmTnVtYmVyKGFyZ3YuaWQudG9TdHJpbmcoKSk7XG5cdFx0XHRcdFx0fVxuXG5cdFx0XHRcdFx0cmV0dXJuIGFyZ3Y7XG5cdFx0XHRcdH0sXG5cblx0XHRcdH0pLFxuXHRcdF0pLFxuXG5cdH0sXG5cbn07XG5cbmV4cG9ydCBkZWZhdWx0IHRwbE9wdGlvbnNcbiJdfQ==