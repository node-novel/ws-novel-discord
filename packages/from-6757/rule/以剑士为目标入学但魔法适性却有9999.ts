/**
 * Created by user on 2019/4/14.
 */

import { _handleOptions, makeOptions } from '@node-novel/txt-split/lib/index';
import {
	IOptions,
	IOptionsRequired,
	IOptionsRequiredUser,
	IDataVolume,
	IDataChapter,
	ISplitCBParameters,
} from '@node-novel/txt-split/lib/interface';

import { console } from '@node-novel/txt-split/lib/console';
import tplBaseOptions from './base/章_話';
import {
	baseCbParseChapterMatch001,
	baseCbParseChapterMatch003,
	baseCbParseChapterMatchMain001,
	createCbParseChapterMatch, ICreateCbParseChapterMatchOptionsSub,
} from './lib/rule';
import StrUtil = require('str-util');

let v_line = `☠☠☠♛☠☠☠☠☠☠♛☠☠☠♛☠☠☠`;
let c_line = `❄✡✥✩✧✫✪✭☆★✬✰✮✦⟡✯☸✠❇✣`;

let _space = ' 　\\t';
const c = '　';

export const tplOptions: IOptionsRequiredUser = {

	...tplBaseOptions,

	volume: {

		...tplBaseOptions.volume,

		r: [
			`(?<=。|\\n|^)`,
			'(',
			[
				`第[一-十]+(?:章|部)`,
				`第[\\d０-９]+(?:章|部)`,
				`外傳`,
			].join('|'),
			')',
			`[${_space}]*`,
			`([^\\n]*)`,
			`[${_space}]*`,
			``,
		],
	},

	/**
	 * 這個參數是必填選項
	 */
	chapter: {

		...tplBaseOptions.chapter,

		r: [
			`^`,
			'(',
			[
				'序章',
				'(?:幕間|終章)',
				`第[\\d０-９]+(?:\.[\\d０-９]+)?(?:話)`,
				`安娜的室友【前篇】`,
				`安娜的室友【後篇】`,
				`安娜的室友【中篇】`,
			].join('|'),
			')',
			`(?:(?:`,
			`[${_space}\\.]+`,
			`([^\\n]*)`,
			')?',
			`[${_space}]*)?`,
			`$`,
		],

		cb: baseCbParseChapterMatchMain001([

			baseCbParseChapterMatch003,

		]),

	},

};

export default tplOptions
