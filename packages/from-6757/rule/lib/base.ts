/**
 * Created by user on 2019/4/14.
 */

import { _handleOptions, makeOptions } from '@node-novel/txt-split/lib/index';
import { IOptions, IOptionsRequired, IOptionsRequiredUser, IDataVolume, IDataChapter } from '@node-novel/txt-split/lib/interface';

import { console } from '@node-novel/txt-split/lib/console';

export const tplBaseOptions: Partial<IOptionsRequiredUser> = {

	beforeStart(options)
	{
		0 && console.dir(options);

		[
			'volume',
			'chapter',
		].forEach(function (key)
		{
			let data = options[key];

			// @ts-ignore
			if (data && !data.disable)
			{
				console.dir(key);

				console.dir(data.r)
				data.ignoreRe && console.log(`ignoreRe`, data.ignoreRe);

				console.gray(`-`.repeat(40));
			}
		})
		;
	},

	saveFileBefore(txt: string,
		cn: string,
		data_vn: IDataChapter<string>,
		cache: { file: string; full_file: string; data: IDataVolume; options: IOptions; cn: string; vn: string },
	)
	{
		if (txt.replace(/\s+/g, '').length === 0)
		{
			console.debug(cache.vn, cn, cache.file);

			return null;
		}
		else
		{
			//console.log(Buffer.from(txt));
			//console.log(Buffer.from('\n'));
		}

		if (1)
		{
			txt = (txt + '\n')
				.replace(/\s=+\s*$/g, '')
				.replace(/^\s*\n+/g, '')
				.replace(/^\n+/g, '')

				.replace(/^-+\n+/, '')

				.replace(/\s+$/g, '\n')

				.replace(/^\s*\n+/g, '')
				.replace(/^\n+/g, '')
			;
		}

		return txt;
	},

	readFileAfter(txt: string): string | void
	{
		return txt += '\n';
	}

};

export default tplBaseOptions
