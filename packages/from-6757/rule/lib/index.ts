/**
 * Created by user on 2019/5/1.
 */

import path = require('path');
import fs = require('fs-extra');
import novelText from 'novel-text';

export const RULE_DEFAULT = `章_話`;

export const RULE_ROOT = path.join(__dirname, '..');
export const RULE_PREFIX = [
	'',
	'temp/',
	'base/',
];

export function searchRule(basename: string, options: {
	rule_root?: string,
	rule_match?: Record<string, string>,
	rule_prefix?: string[],
	returnFullPath?: boolean,
} = {}): {
	rule_name: string;
	rule_file: string;
}
{
	const { rule_root = RULE_ROOT, rule_match, returnFullPath } = options;
	const { rule_prefix = RULE_PREFIX } = options;

	let rule_name: string;
	let rule_file: string;

	for (let prefix of rule_prefix)
	{
		let file: string = path.join(rule_root, prefix, basename);

		if (fs.pathExistsSync(`${file}.ts`) || fs.pathExistsSync(`${file}.js`))
		{
			rule_name = prefix + basename;
			rule_file = file;

			break;
		}
	}

	if (rule_name == null && rule_match && basename in rule_match)
	{
		return searchRule(rule_match[basename], {
			...options,
			rule_match: null,
		});
	}

	return {
		rule_name,
		rule_file,
	}
}

export function trimDesc(desc: string)
{
	return novelText.trim(desc, {
		trim: '・；：：~～　 .,',
	});
}

export default exports as typeof import('./index');