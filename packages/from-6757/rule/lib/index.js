"use strict";
/**
 * Created by user on 2019/5/1.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const path = require("path");
const fs = require("fs-extra");
const novel_text_1 = require("novel-text");
exports.RULE_DEFAULT = `章_話`;
exports.RULE_ROOT = path.join(__dirname, '..');
exports.RULE_PREFIX = [
    '',
    'temp/',
    'base/',
];
function searchRule(basename, options = {}) {
    const { rule_root = exports.RULE_ROOT, rule_match, returnFullPath } = options;
    const { rule_prefix = exports.RULE_PREFIX } = options;
    let rule_name;
    let rule_file;
    for (let prefix of rule_prefix) {
        let file = path.join(rule_root, prefix, basename);
        if (fs.pathExistsSync(`${file}.ts`) || fs.pathExistsSync(`${file}.js`)) {
            rule_name = prefix + basename;
            rule_file = file;
            break;
        }
    }
    if (rule_name == null && rule_match && basename in rule_match) {
        return searchRule(rule_match[basename], {
            ...options,
            rule_match: null,
        });
    }
    return {
        rule_name,
        rule_file,
    };
}
exports.searchRule = searchRule;
function trimDesc(desc) {
    return novel_text_1.default.trim(desc, {
        trim: '・；：：~～　 .,',
    });
}
exports.trimDesc = trimDesc;
exports.default = exports;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUE7O0dBRUc7O0FBRUgsNkJBQThCO0FBQzlCLCtCQUFnQztBQUNoQywyQ0FBbUM7QUFFdEIsUUFBQSxZQUFZLEdBQUcsS0FBSyxDQUFDO0FBRXJCLFFBQUEsU0FBUyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxDQUFDO0FBQ3ZDLFFBQUEsV0FBVyxHQUFHO0lBQzFCLEVBQUU7SUFDRixPQUFPO0lBQ1AsT0FBTztDQUNQLENBQUM7QUFFRixTQUFnQixVQUFVLENBQUMsUUFBZ0IsRUFBRSxVQUt6QyxFQUFFO0lBS0wsTUFBTSxFQUFFLFNBQVMsR0FBRyxpQkFBUyxFQUFFLFVBQVUsRUFBRSxjQUFjLEVBQUUsR0FBRyxPQUFPLENBQUM7SUFDdEUsTUFBTSxFQUFFLFdBQVcsR0FBRyxtQkFBVyxFQUFFLEdBQUcsT0FBTyxDQUFDO0lBRTlDLElBQUksU0FBaUIsQ0FBQztJQUN0QixJQUFJLFNBQWlCLENBQUM7SUFFdEIsS0FBSyxJQUFJLE1BQU0sSUFBSSxXQUFXLEVBQzlCO1FBQ0MsSUFBSSxJQUFJLEdBQVcsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsTUFBTSxFQUFFLFFBQVEsQ0FBQyxDQUFDO1FBRTFELElBQUksRUFBRSxDQUFDLGNBQWMsQ0FBQyxHQUFHLElBQUksS0FBSyxDQUFDLElBQUksRUFBRSxDQUFDLGNBQWMsQ0FBQyxHQUFHLElBQUksS0FBSyxDQUFDLEVBQ3RFO1lBQ0MsU0FBUyxHQUFHLE1BQU0sR0FBRyxRQUFRLENBQUM7WUFDOUIsU0FBUyxHQUFHLElBQUksQ0FBQztZQUVqQixNQUFNO1NBQ047S0FDRDtJQUVELElBQUksU0FBUyxJQUFJLElBQUksSUFBSSxVQUFVLElBQUksUUFBUSxJQUFJLFVBQVUsRUFDN0Q7UUFDQyxPQUFPLFVBQVUsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDdkMsR0FBRyxPQUFPO1lBQ1YsVUFBVSxFQUFFLElBQUk7U0FDaEIsQ0FBQyxDQUFDO0tBQ0g7SUFFRCxPQUFPO1FBQ04sU0FBUztRQUNULFNBQVM7S0FDVCxDQUFBO0FBQ0YsQ0FBQztBQXpDRCxnQ0F5Q0M7QUFFRCxTQUFnQixRQUFRLENBQUMsSUFBWTtJQUVwQyxPQUFPLG9CQUFTLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRTtRQUMzQixJQUFJLEVBQUUsWUFBWTtLQUNsQixDQUFDLENBQUM7QUFDSixDQUFDO0FBTEQsNEJBS0M7QUFFRCxrQkFBZSxPQUFtQyxDQUFDIiwic291cmNlc0NvbnRlbnQiOlsiLyoqXG4gKiBDcmVhdGVkIGJ5IHVzZXIgb24gMjAxOS81LzEuXG4gKi9cblxuaW1wb3J0IHBhdGggPSByZXF1aXJlKCdwYXRoJyk7XG5pbXBvcnQgZnMgPSByZXF1aXJlKCdmcy1leHRyYScpO1xuaW1wb3J0IG5vdmVsVGV4dCBmcm9tICdub3ZlbC10ZXh0JztcblxuZXhwb3J0IGNvbnN0IFJVTEVfREVGQVVMVCA9IGDnq6Bf6KmxYDtcblxuZXhwb3J0IGNvbnN0IFJVTEVfUk9PVCA9IHBhdGguam9pbihfX2Rpcm5hbWUsICcuLicpO1xuZXhwb3J0IGNvbnN0IFJVTEVfUFJFRklYID0gW1xuXHQnJyxcblx0J3RlbXAvJyxcblx0J2Jhc2UvJyxcbl07XG5cbmV4cG9ydCBmdW5jdGlvbiBzZWFyY2hSdWxlKGJhc2VuYW1lOiBzdHJpbmcsIG9wdGlvbnM6IHtcblx0cnVsZV9yb290Pzogc3RyaW5nLFxuXHRydWxlX21hdGNoPzogUmVjb3JkPHN0cmluZywgc3RyaW5nPixcblx0cnVsZV9wcmVmaXg/OiBzdHJpbmdbXSxcblx0cmV0dXJuRnVsbFBhdGg/OiBib29sZWFuLFxufSA9IHt9KToge1xuXHRydWxlX25hbWU6IHN0cmluZztcblx0cnVsZV9maWxlOiBzdHJpbmc7XG59XG57XG5cdGNvbnN0IHsgcnVsZV9yb290ID0gUlVMRV9ST09ULCBydWxlX21hdGNoLCByZXR1cm5GdWxsUGF0aCB9ID0gb3B0aW9ucztcblx0Y29uc3QgeyBydWxlX3ByZWZpeCA9IFJVTEVfUFJFRklYIH0gPSBvcHRpb25zO1xuXG5cdGxldCBydWxlX25hbWU6IHN0cmluZztcblx0bGV0IHJ1bGVfZmlsZTogc3RyaW5nO1xuXG5cdGZvciAobGV0IHByZWZpeCBvZiBydWxlX3ByZWZpeClcblx0e1xuXHRcdGxldCBmaWxlOiBzdHJpbmcgPSBwYXRoLmpvaW4ocnVsZV9yb290LCBwcmVmaXgsIGJhc2VuYW1lKTtcblxuXHRcdGlmIChmcy5wYXRoRXhpc3RzU3luYyhgJHtmaWxlfS50c2ApIHx8IGZzLnBhdGhFeGlzdHNTeW5jKGAke2ZpbGV9LmpzYCkpXG5cdFx0e1xuXHRcdFx0cnVsZV9uYW1lID0gcHJlZml4ICsgYmFzZW5hbWU7XG5cdFx0XHRydWxlX2ZpbGUgPSBmaWxlO1xuXG5cdFx0XHRicmVhaztcblx0XHR9XG5cdH1cblxuXHRpZiAocnVsZV9uYW1lID09IG51bGwgJiYgcnVsZV9tYXRjaCAmJiBiYXNlbmFtZSBpbiBydWxlX21hdGNoKVxuXHR7XG5cdFx0cmV0dXJuIHNlYXJjaFJ1bGUocnVsZV9tYXRjaFtiYXNlbmFtZV0sIHtcblx0XHRcdC4uLm9wdGlvbnMsXG5cdFx0XHRydWxlX21hdGNoOiBudWxsLFxuXHRcdH0pO1xuXHR9XG5cblx0cmV0dXJuIHtcblx0XHRydWxlX25hbWUsXG5cdFx0cnVsZV9maWxlLFxuXHR9XG59XG5cbmV4cG9ydCBmdW5jdGlvbiB0cmltRGVzYyhkZXNjOiBzdHJpbmcpXG57XG5cdHJldHVybiBub3ZlbFRleHQudHJpbShkZXNjLCB7XG5cdFx0dHJpbTogJ+ODu++8m++8mu+8mn7vvZ7jgIAgLiwnLFxuXHR9KTtcbn1cblxuZXhwb3J0IGRlZmF1bHQgZXhwb3J0cyBhcyB0eXBlb2YgaW1wb3J0KCcuL2luZGV4Jyk7Il19