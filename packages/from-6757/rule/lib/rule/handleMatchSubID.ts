import { ISplitCBParameters } from '@node-novel/txt-split';
import { ICreateCbParseChapterMatchOptionsSub } from './index';
import { ISplitCB } from '@node-novel/txt-split/lib/interface';
import novelText from 'novel-text';
import { console } from '@node-novel/txt-split/lib/console';
import { trimDesc } from '../index';
import StrUtil = require('str-util');
import { handleMatchSubIDString001 } from './handleMatchSubIDString';

/**
 * StrUtil.toHalfNumber(000.x)
 */
export function handleMatchSubID000(argv: ICreateCbParseChapterMatchOptionsSub, _data: ISplitCBParameters)
{
	if (/^\d+(?:\.\d+)?$/.test(argv.id))
	{

		argv.id = String(argv.id)
			.replace(/^\d+/, function (s)
			{
				return s.padStart(3, '0');
			})
		;

		argv.idn = StrUtil.toHalfNumber(argv.id.toString());
	}

	return argv;
}

/**
 * StrUtil.toFullNumber(000.x)
 */
export function handleMatchSubID001(argv: ICreateCbParseChapterMatchOptionsSub, _data: ISplitCBParameters)
{
	if (/^\d+(?:\.\d+)?$/.test(argv.id))
	{
		argv.id = String(argv.id).padStart(3, '0');

		argv.idn = StrUtil.toFullNumber(argv.id.toString());
	}

	return argv;
}

/**
 * StrUtil.toFullNumber(0.x)
 */
export function handleMatchSubID002(argv: ICreateCbParseChapterMatchOptionsSub, _data: ISplitCBParameters)
{
	if (/^\d+(?:\.\d+)?$/.test(argv.id))
	{
		argv.id = String(argv.id);

		argv.idn = StrUtil.toFullNumber(argv.id.toString());
	}

	return argv;
}

/**
 * no idn
 */
export function handleMatchSubID003(argv: ICreateCbParseChapterMatchOptionsSub, _data: ISplitCBParameters)
{
	if (/^\d+(?:\.\d+)?$/.test(argv.id))
	{
		argv.id = String(argv.id);

		//argv.idn = StrUtil.toFullNumber(argv.id.toString());
	}

	return argv;
}

export default exports as typeof import('./handleMatchSubID');
