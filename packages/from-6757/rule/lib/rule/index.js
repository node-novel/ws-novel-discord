"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const console_1 = require("@node-novel/txt-split/lib/console");
const handleMatchSubIDString_1 = require("./handleMatchSubIDString");
const handleMatchSubID_1 = require("./handleMatchSubID");
const handleMatchName_1 = require("./handleMatchName");
const handleMatchSub_1 = require("./handleMatchSub");
function baseCbParseChapterMatchMain001(fn_list, options) {
    return function (_data) {
        let { 
        /**
         * 於 match 列表中的 index 序列
         */
        i, 
        /**
         * 檔案序列(儲存檔案時會做為前置詞)
         */
        id, 
        /**
         * 標題名稱 預設情況下等於 match 到的標題
         */
        name, 
        /**
         * 本階段的 match 值
         */
        m, 
        /**
         * 上一次的 match 值
         *
         * 但是 實際上 這參數 才是本次 callback 真正的 match 內容
         */
        m_last, 
        /**
         * 目前已經分割的檔案列表與內容
         */
        _files, 
        /**
         * 於所有章節中的序列
         *
         * @readonly
         */
        ii, 
        /**
         * 本次 match 的 內文 start index
         * 可通過修改數值來控制內文範圍
         *
         * @example
         * idx += m_last.match.length; // 內文忽略本次 match 到的標題
         */
        idx, } = _data;
        if (_data.m_last) {
            // @ts-ignore
            let self = this;
            let bool = fn_list.some(fn => {
                return fn.call(self, _data);
            });
            options && options.fnAfter && options.fnAfter(_data, bool);
        }
        return {
            /**
             * 檔案序列(儲存檔案時會做為前置詞)
             */
            id: _data.id,
            /**
             * 標題名稱 預設情況下等於 match 到的標題
             */
            name: _data.name,
            /**
             * 本次 match 的 內文 start index
             * 可通過修改數值來控制內文範圍
             *
             * @example
             * idx += m_last.match.length; // 內文忽略本次 match 到的標題
             */
            idx: _data.idx,
        };
    };
}
exports.baseCbParseChapterMatchMain001 = baseCbParseChapterMatchMain001;
exports.baseCbParseChapterMatch001 = createCbParseChapterMatch({
    handleMatchSubID: handleMatchSubID_1.handleMatchSubID001,
    handleMatchSubIDString: handleMatchSubIDString_1.handleMatchSubIDString001,
});
exports.baseCbParseChapterMatch002 = createCbParseChapterMatch({
    handleMatchSubIDString: handleMatchSubIDString_1.handleMatchSubIDString001,
});
exports.baseCbParseChapterMatch003 = createCbParseChapterMatch();
exports.baseCbParseChapterMatch004 = createCbParseChapterMatch({
    handleMatchSubID: handleMatchSubID_1.handleMatchSubID002,
    handleMatchSubIDString: handleMatchSubIDString_1.handleMatchSubIDString001,
});
exports.baseCbParseChapterMatch005 = createCbParseChapterMatch({
    handleMatchSubID: handleMatchSubID_1.handleMatchSubID000,
});
function createCbParseChapterMatch(createOptionsInput = {}) {
    let createOptions = {
        ...createOptionsInput,
    };
    createOptions._cache = createOptions._cache || {};
    if (createOptions.c == null) {
        createOptions.c = '　';
    }
    if (!createOptions.handleMatchSub) {
        createOptions.handleMatchSub = handleMatchSub_1.handleMatchSub001;
    }
    if (!createOptions.handleMatchSubID) {
        createOptions.handleMatchSubID = handleMatchSubID_1.handleMatchSubID000;
    }
    if (!createOptions.handleMatchSubIDString) {
        createOptions.handleMatchSubIDString = handleMatchSubIDString_1.handleMatchSubIDString000;
    }
    if (!createOptions.handleMatchName) {
        createOptions.handleMatchName = handleMatchName_1._handleMatchName(createOptions);
    }
    return function (_data) {
        if (_data.m_last) {
            let { 
            /**
             * 配對到的內容
             */
            match, 
            /**
             * 配對出來的陣列
             */
            sub, } = _data.m_last;
            createOptions.logMatch && console_1.console.dir({
                sub,
                match,
            });
            if (!sub.some(s => (typeof s === 'string' && s !== ''))) {
                throw new RangeError(`${sub}`);
            }
            let _cache = createOptions.handleMatchSub(sub, _data);
            _cache = createOptions.handleMatchSubID(_cache, _data);
            _cache.ids = createOptions.handleMatchSubIDString(_cache, _data);
            _data.name = createOptions.handleMatchName(_cache, _data);
            /**
             * 將定位點加上本次配對到的內容的長度
             * 此步驟可以省略
             * 但使用此步驟時可以同時在切割時對於內容作精簡
             */
            _data.idx += _data.m_last.match.length;
            return true;
        }
    };
}
exports.createCbParseChapterMatch = createCbParseChapterMatch;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUtBLCtEQUE0RDtBQUc1RCxxRUFBZ0c7QUFDaEcseURBQW1HO0FBQ25HLHVEQUFxRDtBQUNyRCxxREFBcUQ7QUFJckQsU0FBZ0IsOEJBQThCLENBQUMsT0FBb0QsRUFBRSxPQUVwRztJQUVBLE9BQU8sVUFBVSxLQUF5QjtRQUV6QyxJQUFJO1FBQ0g7O1dBRUc7UUFDSCxDQUFDO1FBQ0Q7O1dBRUc7UUFDSCxFQUFFO1FBQ0Y7O1dBRUc7UUFDSCxJQUFJO1FBQ0o7O1dBRUc7UUFDSCxDQUFDO1FBQ0Q7Ozs7V0FJRztRQUNILE1BQU07UUFDTjs7V0FFRztRQUNILE1BQU07UUFDTjs7OztXQUlHO1FBQ0gsRUFBRTtRQUNGOzs7Ozs7V0FNRztRQUNILEdBQUcsR0FDSCxHQUFHLEtBQUssQ0FBQztRQUVWLElBQUksS0FBSyxDQUFDLE1BQU0sRUFDaEI7WUFDQyxhQUFhO1lBQ2IsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDO1lBRWhCLElBQUksSUFBSSxHQUFHLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLEVBQUU7Z0JBRTVCLE9BQU8sRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsS0FBSyxDQUFDLENBQUM7WUFDN0IsQ0FBQyxDQUFDLENBQUM7WUFFSCxPQUFPLElBQUksT0FBTyxDQUFDLE9BQU8sSUFBSSxPQUFPLENBQUMsT0FBTyxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsQ0FBQztTQUMzRDtRQUVELE9BQU87WUFDTjs7ZUFFRztZQUNILEVBQUUsRUFBRSxLQUFLLENBQUMsRUFBRTtZQUNaOztlQUVHO1lBQ0gsSUFBSSxFQUFFLEtBQUssQ0FBQyxJQUFJO1lBQ2hCOzs7Ozs7ZUFNRztZQUNILEdBQUcsRUFBRSxLQUFLLENBQUMsR0FBRztTQUNkLENBQUM7SUFDSCxDQUFDLENBQUE7QUFDRixDQUFDO0FBakZELHdFQWlGQztBQUVZLFFBQUEsMEJBQTBCLEdBQUcseUJBQXlCLENBQUM7SUFFbkUsZ0JBQWdCLEVBQUUsc0NBQW1CO0lBRXJDLHNCQUFzQixFQUFFLGtEQUF5QjtDQUVqRCxDQUFDLENBQUM7QUFFVSxRQUFBLDBCQUEwQixHQUFHLHlCQUF5QixDQUFDO0lBRW5FLHNCQUFzQixFQUFFLGtEQUF5QjtDQUVqRCxDQUFDLENBQUM7QUFFVSxRQUFBLDBCQUEwQixHQUFHLHlCQUF5QixFQUFFLENBQUM7QUFFekQsUUFBQSwwQkFBMEIsR0FBRyx5QkFBeUIsQ0FBQztJQUVuRSxnQkFBZ0IsRUFBRSxzQ0FBbUI7SUFFckMsc0JBQXNCLEVBQUUsa0RBQXlCO0NBRWpELENBQUMsQ0FBQztBQUVVLFFBQUEsMEJBQTBCLEdBQUcseUJBQXlCLENBQUM7SUFFbkUsZ0JBQWdCLEVBQUUsc0NBQW1CO0NBRXJDLENBQUMsQ0FBQztBQWlDSCxTQUFnQix5QkFBeUIsQ0FBSyxxQkFBbUcsRUFBRTtJQUVsSixJQUFJLGFBQWEsR0FBNkM7UUFDN0QsR0FBRyxrQkFBa0I7S0FDckIsQ0FBQztJQUVGLGFBQWEsQ0FBQyxNQUFNLEdBQUcsYUFBYSxDQUFDLE1BQU0sSUFBSSxFQUFFLENBQUM7SUFFbEQsSUFBSSxhQUFhLENBQUMsQ0FBQyxJQUFJLElBQUksRUFDM0I7UUFDQyxhQUFhLENBQUMsQ0FBQyxHQUFHLEdBQUcsQ0FBQztLQUN0QjtJQUVELElBQUksQ0FBQyxhQUFhLENBQUMsY0FBYyxFQUNqQztRQUNDLGFBQWEsQ0FBQyxjQUFjLEdBQUcsa0NBQWlCLENBQUM7S0FDakQ7SUFFRCxJQUFJLENBQUMsYUFBYSxDQUFDLGdCQUFnQixFQUNuQztRQUNDLGFBQWEsQ0FBQyxnQkFBZ0IsR0FBRyxzQ0FBbUIsQ0FBQztLQUNyRDtJQUVELElBQUksQ0FBQyxhQUFhLENBQUMsc0JBQXNCLEVBQ3pDO1FBQ0MsYUFBYSxDQUFDLHNCQUFzQixHQUFHLGtEQUF5QixDQUFBO0tBQ2hFO0lBRUQsSUFBSSxDQUFDLGFBQWEsQ0FBQyxlQUFlLEVBQ2xDO1FBQ0MsYUFBYSxDQUFDLGVBQWUsR0FBRyxrQ0FBZ0IsQ0FBQyxhQUFhLENBQUMsQ0FBQztLQUNoRTtJQUVELE9BQU8sVUFBVSxLQUF5QjtRQUV6QyxJQUFJLEtBQUssQ0FBQyxNQUFNLEVBQ2hCO1lBQ0MsSUFBSTtZQUNIOztlQUVHO1lBQ0gsS0FBSztZQUNMOztlQUVHO1lBQ0gsR0FBRyxHQUNILEdBQUcsS0FBSyxDQUFDLE1BQU0sQ0FBQztZQUVqQixhQUFhLENBQUMsUUFBUSxJQUFJLGlCQUFPLENBQUMsR0FBRyxDQUFDO2dCQUNyQyxHQUFHO2dCQUNILEtBQUs7YUFDTCxDQUFDLENBQUM7WUFFSCxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsT0FBTyxDQUFDLEtBQUssUUFBUSxJQUFJLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQyxFQUN2RDtnQkFDQyxNQUFNLElBQUksVUFBVSxDQUFDLEdBQUcsR0FBRyxFQUFFLENBQUMsQ0FBQzthQUMvQjtZQUVELElBQUksTUFBTSxHQUFHLGFBQWEsQ0FBQyxjQUFjLENBQUMsR0FBRyxFQUFFLEtBQUssQ0FBQyxDQUFDO1lBRXRELE1BQU0sR0FBRyxhQUFhLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxFQUFFLEtBQUssQ0FBQyxDQUFDO1lBRXZELE1BQU0sQ0FBQyxHQUFHLEdBQUcsYUFBYSxDQUFDLHNCQUFzQixDQUFDLE1BQU0sRUFBRSxLQUFLLENBQUMsQ0FBQztZQUVqRSxLQUFLLENBQUMsSUFBSSxHQUFHLGFBQWEsQ0FBQyxlQUFlLENBQUMsTUFBTSxFQUFFLEtBQUssQ0FBQyxDQUFDO1lBRTFEOzs7O2VBSUc7WUFDSCxLQUFLLENBQUMsR0FBRyxJQUFJLEtBQUssQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQztZQUV2QyxPQUFPLElBQUksQ0FBQztTQUNaO0lBQ0YsQ0FBQyxDQUFBO0FBQ0YsQ0FBQztBQTVFRCw4REE0RUMiLCJzb3VyY2VzQ29udGVudCI6WyIvKipcbiAqIENyZWF0ZWQgYnkgdXNlciBvbiAyMDE5LzQvMjEuXG4gKi9cbmltcG9ydCB7IElTcGxpdENCLCBJU3BsaXRDQlBhcmFtZXRlcnMgfSBmcm9tICdAbm9kZS1ub3ZlbC90eHQtc3BsaXQvbGliL2ludGVyZmFjZSc7XG5pbXBvcnQgbm92ZWxUZXh0IGZyb20gJ25vdmVsLXRleHQnO1xuaW1wb3J0IHsgY29uc29sZSB9IGZyb20gJ0Bub2RlLW5vdmVsL3R4dC1zcGxpdC9saWIvY29uc29sZSc7XG5pbXBvcnQgeyB0cmltRGVzYyB9IGZyb20gJy4uL2luZGV4JztcbmltcG9ydCBTdHJVdGlsID0gcmVxdWlyZSgnc3RyLXV0aWwnKTtcbmltcG9ydCB7IGhhbmRsZU1hdGNoU3ViSURTdHJpbmcwMDEsIGhhbmRsZU1hdGNoU3ViSURTdHJpbmcwMDAgfSBmcm9tICcuL2hhbmRsZU1hdGNoU3ViSURTdHJpbmcnO1xuaW1wb3J0IHsgaGFuZGxlTWF0Y2hTdWJJRDAwMCwgaGFuZGxlTWF0Y2hTdWJJRDAwMSwgaGFuZGxlTWF0Y2hTdWJJRDAwMiB9IGZyb20gJy4vaGFuZGxlTWF0Y2hTdWJJRCc7XG5pbXBvcnQgeyBfaGFuZGxlTWF0Y2hOYW1lIH0gZnJvbSAnLi9oYW5kbGVNYXRjaE5hbWUnO1xuaW1wb3J0IHsgaGFuZGxlTWF0Y2hTdWIwMDEgfSBmcm9tICcuL2hhbmRsZU1hdGNoU3ViJztcblxuZXhwb3J0IHR5cGUgSUJhc2VDYlBhcnNlTWF0Y2ggPSAoYXJndjogSVNwbGl0Q0JQYXJhbWV0ZXJzKSA9PiBib29sZWFuO1xuXG5leHBvcnQgZnVuY3Rpb24gYmFzZUNiUGFyc2VDaGFwdGVyTWF0Y2hNYWluMDAxKGZuX2xpc3Q6IFtJQmFzZUNiUGFyc2VNYXRjaCwgLi4uSUJhc2VDYlBhcnNlTWF0Y2hbXV0sIG9wdGlvbnM/OiB7XG5cdGZuQWZ0ZXI/KF9kYXRhOiBJU3BsaXRDQlBhcmFtZXRlcnMsIGJvb2w6IGJvb2xlYW4pOiB2b2lkLFxufSk6IElTcGxpdENCXG57XG5cdHJldHVybiBmdW5jdGlvbiAoX2RhdGE6IElTcGxpdENCUGFyYW1ldGVycylcblx0e1xuXHRcdGxldCB7XG5cdFx0XHQvKipcblx0XHRcdCAqIOaWvCBtYXRjaCDliJfooajkuK3nmoQgaW5kZXgg5bqP5YiXXG5cdFx0XHQgKi9cblx0XHRcdGksXG5cdFx0XHQvKipcblx0XHRcdCAqIOaqlOahiOW6j+WIlyjlhLLlrZjmqpTmoYjmmYLmnIPlgZrngrrliY3nva7oqZ4pXG5cdFx0XHQgKi9cblx0XHRcdGlkLFxuXHRcdFx0LyoqXG5cdFx0XHQgKiDmqJnpoYzlkI3nqLEg6aCQ6Kit5oOF5rOB5LiL562J5pa8IG1hdGNoIOWIsOeahOaomemhjFxuXHRcdFx0ICovXG5cdFx0XHRuYW1lLFxuXHRcdFx0LyoqXG5cdFx0XHQgKiDmnKzpmo7mrrXnmoQgbWF0Y2gg5YC8XG5cdFx0XHQgKi9cblx0XHRcdG0sXG5cdFx0XHQvKipcblx0XHRcdCAqIOS4iuS4gOasoeeahCBtYXRjaCDlgLxcblx0XHRcdCAqXG5cdFx0XHQgKiDkvYbmmK8g5a+m6Zqb5LiKIOmAmeWPg+aVuCDmiY3mmK/mnKzmrKEgY2FsbGJhY2sg55yf5q2j55qEIG1hdGNoIOWFp+WuuVxuXHRcdFx0ICovXG5cdFx0XHRtX2xhc3QsXG5cdFx0XHQvKipcblx0XHRcdCAqIOebruWJjeW3sue2k+WIhuWJsueahOaqlOahiOWIl+ihqOiIh+WFp+WuuVxuXHRcdFx0ICovXG5cdFx0XHRfZmlsZXMsXG5cdFx0XHQvKipcblx0XHRcdCAqIOaWvOaJgOacieeroOevgOS4reeahOW6j+WIl1xuXHRcdFx0ICpcblx0XHRcdCAqIEByZWFkb25seVxuXHRcdFx0ICovXG5cdFx0XHRpaSxcblx0XHRcdC8qKlxuXHRcdFx0ICog5pys5qyhIG1hdGNoIOeahCDlhafmlocgc3RhcnQgaW5kZXhcblx0XHRcdCAqIOWPr+mAmumBjuS/ruaUueaVuOWAvOS+huaOp+WItuWFp+aWh+evhOWcjVxuXHRcdFx0ICpcblx0XHRcdCAqIEBleGFtcGxlXG5cdFx0XHQgKiBpZHggKz0gbV9sYXN0Lm1hdGNoLmxlbmd0aDsgLy8g5YWn5paH5b+955Wl5pys5qyhIG1hdGNoIOWIsOeahOaomemhjFxuXHRcdFx0ICovXG5cdFx0XHRpZHgsXG5cdFx0fSA9IF9kYXRhO1xuXG5cdFx0aWYgKF9kYXRhLm1fbGFzdClcblx0XHR7XG5cdFx0XHQvLyBAdHMtaWdub3JlXG5cdFx0XHRsZXQgc2VsZiA9IHRoaXM7XG5cblx0XHRcdGxldCBib29sID0gZm5fbGlzdC5zb21lKGZuID0+XG5cdFx0XHR7XG5cdFx0XHRcdHJldHVybiBmbi5jYWxsKHNlbGYsIF9kYXRhKTtcblx0XHRcdH0pO1xuXG5cdFx0XHRvcHRpb25zICYmIG9wdGlvbnMuZm5BZnRlciAmJiBvcHRpb25zLmZuQWZ0ZXIoX2RhdGEsIGJvb2wpO1xuXHRcdH1cblxuXHRcdHJldHVybiB7XG5cdFx0XHQvKipcblx0XHRcdCAqIOaqlOahiOW6j+WIlyjlhLLlrZjmqpTmoYjmmYLmnIPlgZrngrrliY3nva7oqZ4pXG5cdFx0XHQgKi9cblx0XHRcdGlkOiBfZGF0YS5pZCxcblx0XHRcdC8qKlxuXHRcdFx0ICog5qiZ6aGM5ZCN56ixIOmgkOioreaDheazgeS4i+etieaWvCBtYXRjaCDliLDnmoTmqJnpoYxcblx0XHRcdCAqL1xuXHRcdFx0bmFtZTogX2RhdGEubmFtZSxcblx0XHRcdC8qKlxuXHRcdFx0ICog5pys5qyhIG1hdGNoIOeahCDlhafmlocgc3RhcnQgaW5kZXhcblx0XHRcdCAqIOWPr+mAmumBjuS/ruaUueaVuOWAvOS+huaOp+WItuWFp+aWh+evhOWcjVxuXHRcdFx0ICpcblx0XHRcdCAqIEBleGFtcGxlXG5cdFx0XHQgKiBpZHggKz0gbV9sYXN0Lm1hdGNoLmxlbmd0aDsgLy8g5YWn5paH5b+955Wl5pys5qyhIG1hdGNoIOWIsOeahOaomemhjFxuXHRcdFx0ICovXG5cdFx0XHRpZHg6IF9kYXRhLmlkeCxcblx0XHR9O1xuXHR9XG59XG5cbmV4cG9ydCBjb25zdCBiYXNlQ2JQYXJzZUNoYXB0ZXJNYXRjaDAwMSA9IGNyZWF0ZUNiUGFyc2VDaGFwdGVyTWF0Y2goe1xuXG5cdGhhbmRsZU1hdGNoU3ViSUQ6IGhhbmRsZU1hdGNoU3ViSUQwMDEsXG5cblx0aGFuZGxlTWF0Y2hTdWJJRFN0cmluZzogaGFuZGxlTWF0Y2hTdWJJRFN0cmluZzAwMSxcblxufSk7XG5cbmV4cG9ydCBjb25zdCBiYXNlQ2JQYXJzZUNoYXB0ZXJNYXRjaDAwMiA9IGNyZWF0ZUNiUGFyc2VDaGFwdGVyTWF0Y2goe1xuXG5cdGhhbmRsZU1hdGNoU3ViSURTdHJpbmc6IGhhbmRsZU1hdGNoU3ViSURTdHJpbmcwMDEsXG5cbn0pO1xuXG5leHBvcnQgY29uc3QgYmFzZUNiUGFyc2VDaGFwdGVyTWF0Y2gwMDMgPSBjcmVhdGVDYlBhcnNlQ2hhcHRlck1hdGNoKCk7XG5cbmV4cG9ydCBjb25zdCBiYXNlQ2JQYXJzZUNoYXB0ZXJNYXRjaDAwNCA9IGNyZWF0ZUNiUGFyc2VDaGFwdGVyTWF0Y2goe1xuXG5cdGhhbmRsZU1hdGNoU3ViSUQ6IGhhbmRsZU1hdGNoU3ViSUQwMDIsXG5cblx0aGFuZGxlTWF0Y2hTdWJJRFN0cmluZzogaGFuZGxlTWF0Y2hTdWJJRFN0cmluZzAwMSxcblxufSk7XG5cbmV4cG9ydCBjb25zdCBiYXNlQ2JQYXJzZUNoYXB0ZXJNYXRjaDAwNSA9IGNyZWF0ZUNiUGFyc2VDaGFwdGVyTWF0Y2goe1xuXG5cdGhhbmRsZU1hdGNoU3ViSUQ6IGhhbmRsZU1hdGNoU3ViSUQwMDAsXG5cbn0pO1xuXG5leHBvcnQgdHlwZSBJU3BsaXRDQlBhcmFtZXRlcnNNTGFzdFN1YiA9IElTcGxpdENCUGFyYW1ldGVyc1tcIm1fbGFzdFwiXVtcInN1YlwiXTtcblxuZXhwb3J0IGludGVyZmFjZSBJQ3JlYXRlQ2JQYXJzZUNoYXB0ZXJNYXRjaE9wdGlvbnNcbntcblx0bG9nTWF0Y2g/OiBib29sZWFuLFxuXG5cdGhhbmRsZU1hdGNoU3ViPyhzdWI6IElTcGxpdENCUGFyYW1ldGVyc01MYXN0U3ViLCBfZGF0YTogSVNwbGl0Q0JQYXJhbWV0ZXJzKToge1xuXHRcdGlkbzogc3RyaW5nLFxuXHRcdGRlc2M6IHN0cmluZyxcblx0XHRpZDogc3RyaW5nLFxuXHRcdGlkbj86IHN0cmluZyxcblx0XHRpZHM/OiBzdHJpbmcsXG5cdH0sXG5cblx0aGFuZGxlTWF0Y2hTdWJJRD8oYXJndjogSUNyZWF0ZUNiUGFyc2VDaGFwdGVyTWF0Y2hPcHRpb25zU3ViLCBfZGF0YTogSVNwbGl0Q0JQYXJhbWV0ZXJzKTogSUNyZWF0ZUNiUGFyc2VDaGFwdGVyTWF0Y2hPcHRpb25zU3ViLFxuXG5cdGhhbmRsZU1hdGNoU3ViSURTdHJpbmc/KGFyZ3Y6IElDcmVhdGVDYlBhcnNlQ2hhcHRlck1hdGNoT3B0aW9uc1N1YiwgX2RhdGE6IElTcGxpdENCUGFyYW1ldGVycyk6IHN0cmluZyxcblxuXHRoYW5kbGVNYXRjaE5hbWU/KGFyZ3Y6IElDcmVhdGVDYlBhcnNlQ2hhcHRlck1hdGNoT3B0aW9uc1N1YiwgX2RhdGE6IElTcGxpdENCUGFyYW1ldGVycyk6IHN0cmluZyxcblxuXHRjPzogc3RyaW5nLFxufVxuXG5leHBvcnQgdHlwZSBJQ3JlYXRlQ2JQYXJzZUNoYXB0ZXJNYXRjaE9wdGlvbnNSdW50aW1lID0gSUNyZWF0ZUNiUGFyc2VDaGFwdGVyTWF0Y2hPcHRpb25zICYge1xuXHRfY2FjaGU/OiB7XG5cdFx0W2s6IHN0cmluZ106IHVua25vd247XG5cdH1cbn1cblxuZXhwb3J0IHR5cGUgSUNyZWF0ZUNiUGFyc2VDaGFwdGVyTWF0Y2hPcHRpb25zU3ViID0gUmV0dXJuVHlwZTxJQ3JlYXRlQ2JQYXJzZUNoYXB0ZXJNYXRjaE9wdGlvbnNbXCJoYW5kbGVNYXRjaFN1YlwiXT47XG5cbmV4cG9ydCBmdW5jdGlvbiBjcmVhdGVDYlBhcnNlQ2hhcHRlck1hdGNoPFQgPihjcmVhdGVPcHRpb25zSW5wdXQ6IElDcmVhdGVDYlBhcnNlQ2hhcHRlck1hdGNoT3B0aW9ucyB8IElDcmVhdGVDYlBhcnNlQ2hhcHRlck1hdGNoT3B0aW9uc1J1bnRpbWUgPSB7fSlcbntcblx0bGV0IGNyZWF0ZU9wdGlvbnM6IElDcmVhdGVDYlBhcnNlQ2hhcHRlck1hdGNoT3B0aW9uc1J1bnRpbWUgPSB7XG5cdFx0Li4uY3JlYXRlT3B0aW9uc0lucHV0LFxuXHR9O1xuXG5cdGNyZWF0ZU9wdGlvbnMuX2NhY2hlID0gY3JlYXRlT3B0aW9ucy5fY2FjaGUgfHwge307XG5cblx0aWYgKGNyZWF0ZU9wdGlvbnMuYyA9PSBudWxsKVxuXHR7XG5cdFx0Y3JlYXRlT3B0aW9ucy5jID0gJ+OAgCc7XG5cdH1cblxuXHRpZiAoIWNyZWF0ZU9wdGlvbnMuaGFuZGxlTWF0Y2hTdWIpXG5cdHtcblx0XHRjcmVhdGVPcHRpb25zLmhhbmRsZU1hdGNoU3ViID0gaGFuZGxlTWF0Y2hTdWIwMDE7XG5cdH1cblxuXHRpZiAoIWNyZWF0ZU9wdGlvbnMuaGFuZGxlTWF0Y2hTdWJJRClcblx0e1xuXHRcdGNyZWF0ZU9wdGlvbnMuaGFuZGxlTWF0Y2hTdWJJRCA9IGhhbmRsZU1hdGNoU3ViSUQwMDA7XG5cdH1cblxuXHRpZiAoIWNyZWF0ZU9wdGlvbnMuaGFuZGxlTWF0Y2hTdWJJRFN0cmluZylcblx0e1xuXHRcdGNyZWF0ZU9wdGlvbnMuaGFuZGxlTWF0Y2hTdWJJRFN0cmluZyA9IGhhbmRsZU1hdGNoU3ViSURTdHJpbmcwMDBcblx0fVxuXG5cdGlmICghY3JlYXRlT3B0aW9ucy5oYW5kbGVNYXRjaE5hbWUpXG5cdHtcblx0XHRjcmVhdGVPcHRpb25zLmhhbmRsZU1hdGNoTmFtZSA9IF9oYW5kbGVNYXRjaE5hbWUoY3JlYXRlT3B0aW9ucyk7XG5cdH1cblxuXHRyZXR1cm4gZnVuY3Rpb24gKF9kYXRhOiBJU3BsaXRDQlBhcmFtZXRlcnMpXG5cdHtcblx0XHRpZiAoX2RhdGEubV9sYXN0KVxuXHRcdHtcblx0XHRcdGxldCB7XG5cdFx0XHRcdC8qKlxuXHRcdFx0XHQgKiDphY3lsI3liLDnmoTlhaflrrlcblx0XHRcdFx0ICovXG5cdFx0XHRcdG1hdGNoLFxuXHRcdFx0XHQvKipcblx0XHRcdFx0ICog6YWN5bCN5Ye65L6G55qE6Zmj5YiXXG5cdFx0XHRcdCAqL1xuXHRcdFx0XHRzdWIsXG5cdFx0XHR9ID0gX2RhdGEubV9sYXN0O1xuXG5cdFx0XHRjcmVhdGVPcHRpb25zLmxvZ01hdGNoICYmIGNvbnNvbGUuZGlyKHtcblx0XHRcdFx0c3ViLFxuXHRcdFx0XHRtYXRjaCxcblx0XHRcdH0pO1xuXG5cdFx0XHRpZiAoIXN1Yi5zb21lKHMgPT4gKHR5cGVvZiBzID09PSAnc3RyaW5nJyAmJiBzICE9PSAnJykpKVxuXHRcdFx0e1xuXHRcdFx0XHR0aHJvdyBuZXcgUmFuZ2VFcnJvcihgJHtzdWJ9YCk7XG5cdFx0XHR9XG5cblx0XHRcdGxldCBfY2FjaGUgPSBjcmVhdGVPcHRpb25zLmhhbmRsZU1hdGNoU3ViKHN1YiwgX2RhdGEpO1xuXG5cdFx0XHRfY2FjaGUgPSBjcmVhdGVPcHRpb25zLmhhbmRsZU1hdGNoU3ViSUQoX2NhY2hlLCBfZGF0YSk7XG5cblx0XHRcdF9jYWNoZS5pZHMgPSBjcmVhdGVPcHRpb25zLmhhbmRsZU1hdGNoU3ViSURTdHJpbmcoX2NhY2hlLCBfZGF0YSk7XG5cblx0XHRcdF9kYXRhLm5hbWUgPSBjcmVhdGVPcHRpb25zLmhhbmRsZU1hdGNoTmFtZShfY2FjaGUsIF9kYXRhKTtcblxuXHRcdFx0LyoqXG5cdFx0XHQgKiDlsIflrprkvY3pu57liqDkuIrmnKzmrKHphY3lsI3liLDnmoTlhaflrrnnmoTplbfluqZcblx0XHRcdCAqIOatpOatpempn+WPr+S7peecgeeVpVxuXHRcdFx0ICog5L2G5L2/55So5q2k5q2l6amf5pmC5Y+v5Lul5ZCM5pmC5Zyo5YiH5Ymy5pmC5bCN5pa85YWn5a655L2c57K+57ChXG5cdFx0XHQgKi9cblx0XHRcdF9kYXRhLmlkeCArPSBfZGF0YS5tX2xhc3QubWF0Y2gubGVuZ3RoO1xuXG5cdFx0XHRyZXR1cm4gdHJ1ZTtcblx0XHR9XG5cdH1cbn1cbiJdfQ==