"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const StrUtil = require("str-util");
function handleMatchSub001(sub, _data) {
    let [ido = '', desc = ''] = sub;
    let idn;
    let ids = ido;
    let id = StrUtil.toHalfNumber(StrUtil.zh2num(ido).toString())
        .replace(/^[^\d０-９]+/, '')
        .replace(/[^\d０-９]+$/, '')
        .trim();
    return {
        ido,
        desc,
        id,
        idn,
        ids,
    };
}
exports.handleMatchSub001 = handleMatchSub001;
exports.default = exports;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaGFuZGxlTWF0Y2hTdWIuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJoYW5kbGVNYXRjaFN1Yi50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQU1BLG9DQUFxQztBQUVyQyxTQUFnQixpQkFBaUIsQ0FBQyxHQUErQixFQUFFLEtBQXlCO0lBRTNGLElBQUksQ0FBQyxHQUFHLEdBQUcsRUFBRSxFQUFFLElBQUksR0FBRyxFQUFFLENBQUMsR0FBRyxHQUFHLENBQUM7SUFFaEMsSUFBSSxHQUFXLENBQUM7SUFDaEIsSUFBSSxHQUFHLEdBQUcsR0FBRyxDQUFDO0lBRWQsSUFBSSxFQUFFLEdBQUcsT0FBTyxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDLFFBQVEsRUFBRSxDQUFDO1NBQzNELE9BQU8sQ0FBQyxZQUFZLEVBQUUsRUFBRSxDQUFDO1NBQ3pCLE9BQU8sQ0FBQyxZQUFZLEVBQUUsRUFBRSxDQUFDO1NBQ3pCLElBQUksRUFBRSxDQUNQO0lBRUQsT0FBTztRQUNOLEdBQUc7UUFDSCxJQUFJO1FBQ0osRUFBRTtRQUNGLEdBQUc7UUFDSCxHQUFHO0tBQ0gsQ0FBQTtBQUNGLENBQUM7QUFwQkQsOENBb0JDO0FBRUQsa0JBQWUsT0FBNEMsQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IElTcGxpdENCUGFyYW1ldGVycyB9IGZyb20gJ0Bub2RlLW5vdmVsL3R4dC1zcGxpdCc7XG5pbXBvcnQgeyBJQ3JlYXRlQ2JQYXJzZUNoYXB0ZXJNYXRjaE9wdGlvbnNTdWIsIElTcGxpdENCUGFyYW1ldGVyc01MYXN0U3ViIH0gZnJvbSAnLi9pbmRleCc7XG5pbXBvcnQgeyBJU3BsaXRDQiB9IGZyb20gJ0Bub2RlLW5vdmVsL3R4dC1zcGxpdC9saWIvaW50ZXJmYWNlJztcbmltcG9ydCBub3ZlbFRleHQgZnJvbSAnbm92ZWwtdGV4dCc7XG5pbXBvcnQgeyBjb25zb2xlIH0gZnJvbSAnQG5vZGUtbm92ZWwvdHh0LXNwbGl0L2xpYi9jb25zb2xlJztcbmltcG9ydCB7IHRyaW1EZXNjIH0gZnJvbSAnLi4vaW5kZXgnO1xuaW1wb3J0IFN0clV0aWwgPSByZXF1aXJlKCdzdHItdXRpbCcpO1xuXG5leHBvcnQgZnVuY3Rpb24gaGFuZGxlTWF0Y2hTdWIwMDEoc3ViOiBJU3BsaXRDQlBhcmFtZXRlcnNNTGFzdFN1YiwgX2RhdGE6IElTcGxpdENCUGFyYW1ldGVycylcbntcblx0bGV0IFtpZG8gPSAnJywgZGVzYyA9ICcnXSA9IHN1YjtcblxuXHRsZXQgaWRuOiBzdHJpbmc7XG5cdGxldCBpZHMgPSBpZG87XG5cblx0bGV0IGlkID0gU3RyVXRpbC50b0hhbGZOdW1iZXIoU3RyVXRpbC56aDJudW0oaWRvKS50b1N0cmluZygpKVxuXHRcdC5yZXBsYWNlKC9eW15cXGTvvJAt77yZXSsvLCAnJylcblx0XHQucmVwbGFjZSgvW15cXGTvvJAt77yZXSskLywgJycpXG5cdFx0LnRyaW0oKVxuXHQ7XG5cblx0cmV0dXJuIHtcblx0XHRpZG8sXG5cdFx0ZGVzYyxcblx0XHRpZCxcblx0XHRpZG4sXG5cdFx0aWRzLFxuXHR9XG59XG5cbmV4cG9ydCBkZWZhdWx0IGV4cG9ydHMgYXMgdHlwZW9mIGltcG9ydCgnLi9oYW5kbGVNYXRjaFN1YicpO1xuIl19