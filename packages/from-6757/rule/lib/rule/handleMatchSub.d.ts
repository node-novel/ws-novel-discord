import { ISplitCBParameters } from '@node-novel/txt-split';
import { ISplitCBParametersMLastSub } from './index';
export declare function handleMatchSub001(sub: ISplitCBParametersMLastSub, _data: ISplitCBParameters): {
    ido: string;
    desc: string;
    id: string;
    idn: string;
    ids: string;
};
declare const _default: typeof import("./handleMatchSub");
export default _default;
