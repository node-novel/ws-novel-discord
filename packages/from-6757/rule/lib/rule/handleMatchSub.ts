import { ISplitCBParameters } from '@node-novel/txt-split';
import { ICreateCbParseChapterMatchOptionsSub, ISplitCBParametersMLastSub } from './index';
import { ISplitCB } from '@node-novel/txt-split/lib/interface';
import novelText from 'novel-text';
import { console } from '@node-novel/txt-split/lib/console';
import { trimDesc } from '../index';
import StrUtil = require('str-util');

export function handleMatchSub001(sub: ISplitCBParametersMLastSub, _data: ISplitCBParameters)
{
	let [ido = '', desc = ''] = sub;

	let idn: string;
	let ids = ido;

	let id = StrUtil.toHalfNumber(StrUtil.zh2num(ido).toString())
		.replace(/^[^\d０-９]+/, '')
		.replace(/[^\d０-９]+$/, '')
		.trim()
	;

	return {
		ido,
		desc,
		id,
		idn,
		ids,
	}
}

export default exports as typeof import('./handleMatchSub');
