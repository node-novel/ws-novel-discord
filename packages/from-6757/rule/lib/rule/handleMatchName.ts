import { ISplitCBParameters } from '@node-novel/txt-split';
import { ICreateCbParseChapterMatchOptions, ICreateCbParseChapterMatchOptionsSub } from './index';
import { ISplitCB } from '@node-novel/txt-split/lib/interface';
import novelText from 'novel-text';
import { console } from '@node-novel/txt-split/lib/console';
import { trimDesc } from '../index';
import StrUtil = require('str-util');
import { handleMatchSubIDString001 } from './handleMatchSubIDString';

export function _handleMatchName(createOptions): ICreateCbParseChapterMatchOptions["handleMatchName"]
{
	return function (argv: ICreateCbParseChapterMatchOptionsSub, _data: ISplitCBParameters)
	{
		argv.desc = trimDesc(argv.desc);

		argv.desc = StrUtil.toFullNumber(argv.desc);

		let name: ISplitCBParameters["name"] = [
			argv.ids,
			argv.desc,
		].filter(v => v !== '').join(createOptions.c);

		name = novelText.trim(name, {
			trim: true,
		});

		return name;
	}
}

export default exports as typeof import('./handleMatchName');
