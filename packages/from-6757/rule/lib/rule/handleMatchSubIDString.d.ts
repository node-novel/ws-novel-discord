import { ISplitCBParameters } from '@node-novel/txt-split';
import { ICreateCbParseChapterMatchOptionsSub } from './index';
/**
 * `第${argv.idn}話`
 */
export declare function handleMatchSubIDString001(argv: ICreateCbParseChapterMatchOptionsSub, _data: ISplitCBParameters): string;
/**
 * `${argv.idn}`
 */
export declare function handleMatchSubIDString000(argv: ICreateCbParseChapterMatchOptionsSub, _data: ISplitCBParameters): string;
/**
 * `第${argv.idn}章`
 */
export declare function handleMatchSubIDString101(argv: ICreateCbParseChapterMatchOptionsSub, _data: ISplitCBParameters): string;
declare const _default: typeof import("./handleMatchSubIDString");
export default _default;
