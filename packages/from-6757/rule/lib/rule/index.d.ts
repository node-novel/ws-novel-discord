/**
 * Created by user on 2019/4/21.
 */
import { ISplitCB, ISplitCBParameters } from '@node-novel/txt-split/lib/interface';
export declare type IBaseCbParseMatch = (argv: ISplitCBParameters) => boolean;
export declare function baseCbParseChapterMatchMain001(fn_list: [IBaseCbParseMatch, ...IBaseCbParseMatch[]], options?: {
    fnAfter?(_data: ISplitCBParameters, bool: boolean): void;
}): ISplitCB;
export declare const baseCbParseChapterMatch001: (_data: ISplitCBParameters) => boolean;
export declare const baseCbParseChapterMatch002: (_data: ISplitCBParameters) => boolean;
export declare const baseCbParseChapterMatch003: (_data: ISplitCBParameters) => boolean;
export declare const baseCbParseChapterMatch004: (_data: ISplitCBParameters) => boolean;
export declare const baseCbParseChapterMatch005: (_data: ISplitCBParameters) => boolean;
export declare type ISplitCBParametersMLastSub = ISplitCBParameters["m_last"]["sub"];
export interface ICreateCbParseChapterMatchOptions {
    logMatch?: boolean;
    handleMatchSub?(sub: ISplitCBParametersMLastSub, _data: ISplitCBParameters): {
        ido: string;
        desc: string;
        id: string;
        idn?: string;
        ids?: string;
    };
    handleMatchSubID?(argv: ICreateCbParseChapterMatchOptionsSub, _data: ISplitCBParameters): ICreateCbParseChapterMatchOptionsSub;
    handleMatchSubIDString?(argv: ICreateCbParseChapterMatchOptionsSub, _data: ISplitCBParameters): string;
    handleMatchName?(argv: ICreateCbParseChapterMatchOptionsSub, _data: ISplitCBParameters): string;
    c?: string;
}
export declare type ICreateCbParseChapterMatchOptionsRuntime = ICreateCbParseChapterMatchOptions & {
    _cache?: {
        [k: string]: unknown;
    };
};
export declare type ICreateCbParseChapterMatchOptionsSub = ReturnType<ICreateCbParseChapterMatchOptions["handleMatchSub"]>;
export declare function createCbParseChapterMatch<T>(createOptionsInput?: ICreateCbParseChapterMatchOptions | ICreateCbParseChapterMatchOptionsRuntime): (_data: ISplitCBParameters) => boolean;
