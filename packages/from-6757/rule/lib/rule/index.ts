/**
 * Created by user on 2019/4/21.
 */
import { ISplitCB, ISplitCBParameters } from '@node-novel/txt-split/lib/interface';
import novelText from 'novel-text';
import { console } from '@node-novel/txt-split/lib/console';
import { trimDesc } from '../index';
import StrUtil = require('str-util');
import { handleMatchSubIDString001, handleMatchSubIDString000 } from './handleMatchSubIDString';
import { handleMatchSubID000, handleMatchSubID001, handleMatchSubID002 } from './handleMatchSubID';
import { _handleMatchName } from './handleMatchName';
import { handleMatchSub001 } from './handleMatchSub';

export type IBaseCbParseMatch = (argv: ISplitCBParameters) => boolean;

export function baseCbParseChapterMatchMain001(fn_list: [IBaseCbParseMatch, ...IBaseCbParseMatch[]], options?: {
	fnAfter?(_data: ISplitCBParameters, bool: boolean): void,
}): ISplitCB
{
	return function (_data: ISplitCBParameters)
	{
		let {
			/**
			 * 於 match 列表中的 index 序列
			 */
			i,
			/**
			 * 檔案序列(儲存檔案時會做為前置詞)
			 */
			id,
			/**
			 * 標題名稱 預設情況下等於 match 到的標題
			 */
			name,
			/**
			 * 本階段的 match 值
			 */
			m,
			/**
			 * 上一次的 match 值
			 *
			 * 但是 實際上 這參數 才是本次 callback 真正的 match 內容
			 */
			m_last,
			/**
			 * 目前已經分割的檔案列表與內容
			 */
			_files,
			/**
			 * 於所有章節中的序列
			 *
			 * @readonly
			 */
			ii,
			/**
			 * 本次 match 的 內文 start index
			 * 可通過修改數值來控制內文範圍
			 *
			 * @example
			 * idx += m_last.match.length; // 內文忽略本次 match 到的標題
			 */
			idx,
		} = _data;

		if (_data.m_last)
		{
			// @ts-ignore
			let self = this;

			let bool = fn_list.some(fn =>
			{
				return fn.call(self, _data);
			});

			options && options.fnAfter && options.fnAfter(_data, bool);
		}

		return {
			/**
			 * 檔案序列(儲存檔案時會做為前置詞)
			 */
			id: _data.id,
			/**
			 * 標題名稱 預設情況下等於 match 到的標題
			 */
			name: _data.name,
			/**
			 * 本次 match 的 內文 start index
			 * 可通過修改數值來控制內文範圍
			 *
			 * @example
			 * idx += m_last.match.length; // 內文忽略本次 match 到的標題
			 */
			idx: _data.idx,
		};
	}
}

export const baseCbParseChapterMatch001 = createCbParseChapterMatch({

	handleMatchSubID: handleMatchSubID001,

	handleMatchSubIDString: handleMatchSubIDString001,

});

export const baseCbParseChapterMatch002 = createCbParseChapterMatch({

	handleMatchSubIDString: handleMatchSubIDString001,

});

export const baseCbParseChapterMatch003 = createCbParseChapterMatch();

export const baseCbParseChapterMatch004 = createCbParseChapterMatch({

	handleMatchSubID: handleMatchSubID002,

	handleMatchSubIDString: handleMatchSubIDString001,

});

export const baseCbParseChapterMatch005 = createCbParseChapterMatch({

	handleMatchSubID: handleMatchSubID000,

});

export type ISplitCBParametersMLastSub = ISplitCBParameters["m_last"]["sub"];

export interface ICreateCbParseChapterMatchOptions
{
	logMatch?: boolean,

	handleMatchSub?(sub: ISplitCBParametersMLastSub, _data: ISplitCBParameters): {
		ido: string,
		desc: string,
		id: string,
		idn?: string,
		ids?: string,
	},

	handleMatchSubID?(argv: ICreateCbParseChapterMatchOptionsSub, _data: ISplitCBParameters): ICreateCbParseChapterMatchOptionsSub,

	handleMatchSubIDString?(argv: ICreateCbParseChapterMatchOptionsSub, _data: ISplitCBParameters): string,

	handleMatchName?(argv: ICreateCbParseChapterMatchOptionsSub, _data: ISplitCBParameters): string,

	c?: string,
}

export type ICreateCbParseChapterMatchOptionsRuntime = ICreateCbParseChapterMatchOptions & {
	_cache?: {
		[k: string]: unknown;
	}
}

export type ICreateCbParseChapterMatchOptionsSub = ReturnType<ICreateCbParseChapterMatchOptions["handleMatchSub"]>;

export function createCbParseChapterMatch<T >(createOptionsInput: ICreateCbParseChapterMatchOptions | ICreateCbParseChapterMatchOptionsRuntime = {})
{
	let createOptions: ICreateCbParseChapterMatchOptionsRuntime = {
		...createOptionsInput,
	};

	createOptions._cache = createOptions._cache || {};

	if (createOptions.c == null)
	{
		createOptions.c = '　';
	}

	if (!createOptions.handleMatchSub)
	{
		createOptions.handleMatchSub = handleMatchSub001;
	}

	if (!createOptions.handleMatchSubID)
	{
		createOptions.handleMatchSubID = handleMatchSubID000;
	}

	if (!createOptions.handleMatchSubIDString)
	{
		createOptions.handleMatchSubIDString = handleMatchSubIDString000
	}

	if (!createOptions.handleMatchName)
	{
		createOptions.handleMatchName = _handleMatchName(createOptions);
	}

	return function (_data: ISplitCBParameters)
	{
		if (_data.m_last)
		{
			let {
				/**
				 * 配對到的內容
				 */
				match,
				/**
				 * 配對出來的陣列
				 */
				sub,
			} = _data.m_last;

			createOptions.logMatch && console.dir({
				sub,
				match,
			});

			if (!sub.some(s => (typeof s === 'string' && s !== '')))
			{
				throw new RangeError(`${sub}`);
			}

			let _cache = createOptions.handleMatchSub(sub, _data);

			_cache = createOptions.handleMatchSubID(_cache, _data);

			_cache.ids = createOptions.handleMatchSubIDString(_cache, _data);

			_data.name = createOptions.handleMatchName(_cache, _data);

			/**
			 * 將定位點加上本次配對到的內容的長度
			 * 此步驟可以省略
			 * 但使用此步驟時可以同時在切割時對於內容作精簡
			 */
			_data.idx += _data.m_last.match.length;

			return true;
		}
	}
}
