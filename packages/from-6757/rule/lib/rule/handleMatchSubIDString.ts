import { ISplitCBParameters } from '@node-novel/txt-split';
import { ICreateCbParseChapterMatchOptionsSub } from './index';

/**
 * `第${argv.idn}話`
 */
export function handleMatchSubIDString001(argv: ICreateCbParseChapterMatchOptionsSub, _data: ISplitCBParameters): string
{
	if (argv.idn)
	{
		return `第${argv.idn}話`;
	}

	return argv.ido;
}

/**
 * `${argv.idn}`
 */
export function handleMatchSubIDString000(argv: ICreateCbParseChapterMatchOptionsSub, _data: ISplitCBParameters): string
{
	if (argv.idn)
	{
		return `${argv.idn}`;
	}

	return argv.ido;
}

/**
 * `第${argv.idn}章`
 */
export function handleMatchSubIDString101(argv: ICreateCbParseChapterMatchOptionsSub, _data: ISplitCBParameters): string
{
	if (argv.idn)
	{
		return `第${argv.idn}章`;
	}

	return argv.ido;
}

export default exports as typeof import('./handleMatchSubIDString');
