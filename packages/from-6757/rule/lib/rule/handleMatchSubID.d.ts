import { ISplitCBParameters } from '@node-novel/txt-split';
import { ICreateCbParseChapterMatchOptionsSub } from './index';
/**
 * StrUtil.toHalfNumber(000.x)
 */
export declare function handleMatchSubID000(argv: ICreateCbParseChapterMatchOptionsSub, _data: ISplitCBParameters): {
    ido: string;
    desc: string;
    id: string;
    idn?: string;
    ids?: string;
};
/**
 * StrUtil.toFullNumber(000.x)
 */
export declare function handleMatchSubID001(argv: ICreateCbParseChapterMatchOptionsSub, _data: ISplitCBParameters): {
    ido: string;
    desc: string;
    id: string;
    idn?: string;
    ids?: string;
};
/**
 * StrUtil.toFullNumber(0.x)
 */
export declare function handleMatchSubID002(argv: ICreateCbParseChapterMatchOptionsSub, _data: ISplitCBParameters): {
    ido: string;
    desc: string;
    id: string;
    idn?: string;
    ids?: string;
};
/**
 * no idn
 */
export declare function handleMatchSubID003(argv: ICreateCbParseChapterMatchOptionsSub, _data: ISplitCBParameters): {
    ido: string;
    desc: string;
    id: string;
    idn?: string;
    ids?: string;
};
declare const _default: typeof import("./handleMatchSubID");
export default _default;
