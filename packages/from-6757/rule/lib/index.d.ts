/**
 * Created by user on 2019/5/1.
 */
export declare const RULE_DEFAULT = "\u7AE0_\u8A71";
export declare const RULE_ROOT: string;
export declare const RULE_PREFIX: string[];
export declare function searchRule(basename: string, options?: {
    rule_root?: string;
    rule_match?: Record<string, string>;
    rule_prefix?: string[];
    returnFullPath?: boolean;
}): {
    rule_name: string;
    rule_file: string;
};
export declare function trimDesc(desc: string): string;
declare const _default: typeof import(".");
export default _default;
