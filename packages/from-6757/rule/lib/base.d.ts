/**
 * Created by user on 2019/4/14.
 */
import { IOptionsRequiredUser } from '@node-novel/txt-split/lib/interface';
export declare const tplBaseOptions: Partial<IOptionsRequiredUser>;
export default tplBaseOptions;
