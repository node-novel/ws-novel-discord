"use strict";
/**
 * Created by user on 2019/4/14.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const console_1 = require("@node-novel/txt-split/lib/console");
exports.tplBaseOptions = {
    beforeStart(options) {
        0 && console_1.console.dir(options);
        [
            'volume',
            'chapter',
        ].forEach(function (key) {
            let data = options[key];
            // @ts-ignore
            if (data && !data.disable) {
                console_1.console.dir(key);
                console_1.console.dir(data.r);
                data.ignoreRe && console_1.console.log(`ignoreRe`, data.ignoreRe);
                console_1.console.gray(`-`.repeat(40));
            }
        });
    },
    saveFileBefore(txt, cn, data_vn, cache) {
        if (txt.replace(/\s+/g, '').length === 0) {
            console_1.console.debug(cache.vn, cn, cache.file);
            return null;
        }
        else {
            //console.log(Buffer.from(txt));
            //console.log(Buffer.from('\n'));
        }
        if (1) {
            txt = (txt + '\n')
                .replace(/\s=+\s*$/g, '')
                .replace(/^\s*\n+/g, '')
                .replace(/^\n+/g, '')
                .replace(/^-+\n+/, '')
                .replace(/\s+$/g, '\n')
                .replace(/^\s*\n+/g, '')
                .replace(/^\n+/g, '');
        }
        return txt;
    },
    readFileAfter(txt) {
        return txt += '\n';
    }
};
exports.default = exports.tplBaseOptions;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmFzZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImJhc2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBOztHQUVHOztBQUtILCtEQUE0RDtBQUUvQyxRQUFBLGNBQWMsR0FBa0M7SUFFNUQsV0FBVyxDQUFDLE9BQU87UUFFbEIsQ0FBQyxJQUFJLGlCQUFPLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBRTFCO1lBQ0MsUUFBUTtZQUNSLFNBQVM7U0FDVCxDQUFDLE9BQU8sQ0FBQyxVQUFVLEdBQUc7WUFFdEIsSUFBSSxJQUFJLEdBQUcsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBRXhCLGFBQWE7WUFDYixJQUFJLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQ3pCO2dCQUNDLGlCQUFPLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUVqQixpQkFBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUE7Z0JBQ25CLElBQUksQ0FBQyxRQUFRLElBQUksaUJBQU8sQ0FBQyxHQUFHLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFFeEQsaUJBQU8sQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO2FBQzdCO1FBQ0YsQ0FBQyxDQUFDLENBQ0Q7SUFDRixDQUFDO0lBRUQsY0FBYyxDQUFDLEdBQVcsRUFDekIsRUFBVSxFQUNWLE9BQTZCLEVBQzdCLEtBQXdHO1FBR3hHLElBQUksR0FBRyxDQUFDLE9BQU8sQ0FBQyxNQUFNLEVBQUUsRUFBRSxDQUFDLENBQUMsTUFBTSxLQUFLLENBQUMsRUFDeEM7WUFDQyxpQkFBTyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsRUFBRSxFQUFFLEVBQUUsRUFBRSxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7WUFFeEMsT0FBTyxJQUFJLENBQUM7U0FDWjthQUVEO1lBQ0MsZ0NBQWdDO1lBQ2hDLGlDQUFpQztTQUNqQztRQUVELElBQUksQ0FBQyxFQUNMO1lBQ0MsR0FBRyxHQUFHLENBQUMsR0FBRyxHQUFHLElBQUksQ0FBQztpQkFDaEIsT0FBTyxDQUFDLFdBQVcsRUFBRSxFQUFFLENBQUM7aUJBQ3hCLE9BQU8sQ0FBQyxVQUFVLEVBQUUsRUFBRSxDQUFDO2lCQUN2QixPQUFPLENBQUMsT0FBTyxFQUFFLEVBQUUsQ0FBQztpQkFFcEIsT0FBTyxDQUFDLFFBQVEsRUFBRSxFQUFFLENBQUM7aUJBRXJCLE9BQU8sQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDO2lCQUV0QixPQUFPLENBQUMsVUFBVSxFQUFFLEVBQUUsQ0FBQztpQkFDdkIsT0FBTyxDQUFDLE9BQU8sRUFBRSxFQUFFLENBQUMsQ0FDckI7U0FDRDtRQUVELE9BQU8sR0FBRyxDQUFDO0lBQ1osQ0FBQztJQUVELGFBQWEsQ0FBQyxHQUFXO1FBRXhCLE9BQU8sR0FBRyxJQUFJLElBQUksQ0FBQztJQUNwQixDQUFDO0NBRUQsQ0FBQztBQUVGLGtCQUFlLHNCQUFjLENBQUEiLCJzb3VyY2VzQ29udGVudCI6WyIvKipcbiAqIENyZWF0ZWQgYnkgdXNlciBvbiAyMDE5LzQvMTQuXG4gKi9cblxuaW1wb3J0IHsgX2hhbmRsZU9wdGlvbnMsIG1ha2VPcHRpb25zIH0gZnJvbSAnQG5vZGUtbm92ZWwvdHh0LXNwbGl0L2xpYi9pbmRleCc7XG5pbXBvcnQgeyBJT3B0aW9ucywgSU9wdGlvbnNSZXF1aXJlZCwgSU9wdGlvbnNSZXF1aXJlZFVzZXIsIElEYXRhVm9sdW1lLCBJRGF0YUNoYXB0ZXIgfSBmcm9tICdAbm9kZS1ub3ZlbC90eHQtc3BsaXQvbGliL2ludGVyZmFjZSc7XG5cbmltcG9ydCB7IGNvbnNvbGUgfSBmcm9tICdAbm9kZS1ub3ZlbC90eHQtc3BsaXQvbGliL2NvbnNvbGUnO1xuXG5leHBvcnQgY29uc3QgdHBsQmFzZU9wdGlvbnM6IFBhcnRpYWw8SU9wdGlvbnNSZXF1aXJlZFVzZXI+ID0ge1xuXG5cdGJlZm9yZVN0YXJ0KG9wdGlvbnMpXG5cdHtcblx0XHQwICYmIGNvbnNvbGUuZGlyKG9wdGlvbnMpO1xuXG5cdFx0W1xuXHRcdFx0J3ZvbHVtZScsXG5cdFx0XHQnY2hhcHRlcicsXG5cdFx0XS5mb3JFYWNoKGZ1bmN0aW9uIChrZXkpXG5cdFx0e1xuXHRcdFx0bGV0IGRhdGEgPSBvcHRpb25zW2tleV07XG5cblx0XHRcdC8vIEB0cy1pZ25vcmVcblx0XHRcdGlmIChkYXRhICYmICFkYXRhLmRpc2FibGUpXG5cdFx0XHR7XG5cdFx0XHRcdGNvbnNvbGUuZGlyKGtleSk7XG5cblx0XHRcdFx0Y29uc29sZS5kaXIoZGF0YS5yKVxuXHRcdFx0XHRkYXRhLmlnbm9yZVJlICYmIGNvbnNvbGUubG9nKGBpZ25vcmVSZWAsIGRhdGEuaWdub3JlUmUpO1xuXG5cdFx0XHRcdGNvbnNvbGUuZ3JheShgLWAucmVwZWF0KDQwKSk7XG5cdFx0XHR9XG5cdFx0fSlcblx0XHQ7XG5cdH0sXG5cblx0c2F2ZUZpbGVCZWZvcmUodHh0OiBzdHJpbmcsXG5cdFx0Y246IHN0cmluZyxcblx0XHRkYXRhX3ZuOiBJRGF0YUNoYXB0ZXI8c3RyaW5nPixcblx0XHRjYWNoZTogeyBmaWxlOiBzdHJpbmc7IGZ1bGxfZmlsZTogc3RyaW5nOyBkYXRhOiBJRGF0YVZvbHVtZTsgb3B0aW9uczogSU9wdGlvbnM7IGNuOiBzdHJpbmc7IHZuOiBzdHJpbmcgfSxcblx0KVxuXHR7XG5cdFx0aWYgKHR4dC5yZXBsYWNlKC9cXHMrL2csICcnKS5sZW5ndGggPT09IDApXG5cdFx0e1xuXHRcdFx0Y29uc29sZS5kZWJ1ZyhjYWNoZS52biwgY24sIGNhY2hlLmZpbGUpO1xuXG5cdFx0XHRyZXR1cm4gbnVsbDtcblx0XHR9XG5cdFx0ZWxzZVxuXHRcdHtcblx0XHRcdC8vY29uc29sZS5sb2coQnVmZmVyLmZyb20odHh0KSk7XG5cdFx0XHQvL2NvbnNvbGUubG9nKEJ1ZmZlci5mcm9tKCdcXG4nKSk7XG5cdFx0fVxuXG5cdFx0aWYgKDEpXG5cdFx0e1xuXHRcdFx0dHh0ID0gKHR4dCArICdcXG4nKVxuXHRcdFx0XHQucmVwbGFjZSgvXFxzPStcXHMqJC9nLCAnJylcblx0XHRcdFx0LnJlcGxhY2UoL15cXHMqXFxuKy9nLCAnJylcblx0XHRcdFx0LnJlcGxhY2UoL15cXG4rL2csICcnKVxuXG5cdFx0XHRcdC5yZXBsYWNlKC9eLStcXG4rLywgJycpXG5cblx0XHRcdFx0LnJlcGxhY2UoL1xccyskL2csICdcXG4nKVxuXG5cdFx0XHRcdC5yZXBsYWNlKC9eXFxzKlxcbisvZywgJycpXG5cdFx0XHRcdC5yZXBsYWNlKC9eXFxuKy9nLCAnJylcblx0XHRcdDtcblx0XHR9XG5cblx0XHRyZXR1cm4gdHh0O1xuXHR9LFxuXG5cdHJlYWRGaWxlQWZ0ZXIodHh0OiBzdHJpbmcpOiBzdHJpbmcgfCB2b2lkXG5cdHtcblx0XHRyZXR1cm4gdHh0ICs9ICdcXG4nO1xuXHR9XG5cbn07XG5cbmV4cG9ydCBkZWZhdWx0IHRwbEJhc2VPcHRpb25zXG4iXX0=