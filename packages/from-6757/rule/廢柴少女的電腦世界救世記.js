"use strict";
/**
 * Created by user on 2019/4/14.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const console_1 = require("@node-novel/txt-split/lib/console");
const base_1 = require("./lib/base");
const rule_1 = require("./lib/rule");
let v_line1 = `☠☠☠♛☠☠☠♛☠☠☠♛☠☠☠♛☠☠☠♛`;
let v_line2 = `♛☠☠☠♛☠☠☠♛☠☠☠♛☠☠☠♛☠☠☠`;
let c_line1 = `❄✡✥✩✧✥✫✪✭☆★✬✰⟡✮✦⟡✯☸✠❇✣`;
let c_line2 = `❄✡✥✩✧✥✫✪✭☆★✬✰⟡✮✦⟡✯☸✠❇✣`;
exports.tplOptions = {
    ...base_1.default,
    /**
     * 這個參數 可刪除或加上 _ 如果沒有用到的話
     */
    volume: {
        /**
         * 故意放一個無效配對 實際使用時請自行更改
         *
         * 當沒有配對到的時候 會自動產生 00000_unknow 資料夾
         */
        r: [
            `${v_line1}\n`,
            '([^\\n]+)\n',
            `${v_line2}\n+`,
        ],
        cb({ 
        /**
         * 於 match 列表中的 index 序列
         */
        i, 
        /**
         * 檔案序列(儲存檔案時會做為前置詞)
         */
        id, 
        /**
         * 標題名稱 預設情況下等於 match 到的標題
         */
        name, 
        /**
         * 本階段的 match 值
         */
        m, 
        /**
         * 上一次的 match 值
         *
         * 但是 實際上 這參數 才是本次 callback 真正的 match 內容
         */
        m_last, 
        /**
         * 目前已經分割的檔案列表與內容
         */
        _files, 
        /**
         * 於所有章節中的序列
         *
         * @readonly
         */
        ii, 
        /**
         * 本次 match 的 內文 start index
         * 可通過修改數值來控制內文範圍
         *
         * @example
         * idx += m_last.match.length; // 內文忽略本次 match 到的標題
         */
        idx, }) {
            /**
             * 依照你給的 regexp 內容來回傳的資料
             */
            if (m_last) {
                let { 
                /**
                 * 配對到的內容
                 */
                match, 
                /**
                 * 配對出來的陣列
                 */
                sub, } = m_last;
                0 && console_1.console.dir({
                    sub,
                    match,
                });
                /**
                 * @todo 這裡可以加上更多語法來格式化標題
                 */
                name = sub[0];
                /**
                 * 將定位點加上本次配對到的內容的長度
                 * 此步驟可以省略
                 * 但使用此步驟時可以同時在切割時對於內容作精簡
                 */
                idx += m_last.match.length;
            }
            return {
                /**
                 * 檔案序列(儲存檔案時會做為前置詞)
                 */
                id,
                /**
                 * 標題名稱 預設情況下等於 match 到的標題
                 */
                name,
                /**
                 * 本次 match 的 內文 start index
                 * 可通過修改數值來控制內文範圍
                 *
                 * @example
                 * idx += m_last.match.length; // 內文忽略本次 match 到的標題
                 */
                idx,
            };
        },
    },
    /**
     * 這個參數是必填選項
     */
    chapter: {
        //r: /^\d+\n(第\d+話) *([^\n]*)$/,
        /**
         * 正常來說不需要弄得這麼複雜的樣式 只需要配對標題就好
         * 但懶惰想要在切 txt 的時候 順便縮減內文的話 就可以寫的複雜一點
         *
         * 當 r 不是 regexp 的時候 在執行時會自動將這個參數的內容轉成 regexp
         */
        r: [
            `${c_line1}\n`,
            '(',
            [
                '序章',
                '(?:幕間|終章)',
                `第[\\d０-９]+(?:\.[\\d０-９]+)?(?:話)`,
            ].join('|'),
            ')',
            '([^\\n]+)',
            '\n',
            `${c_line2}\n`,
        ],
        cb: rule_1.baseCbParseChapterMatchMain001([
            rule_1.baseCbParseChapterMatch003,
        ]),
    },
};
exports.default = exports.tplOptions;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoi5bui5p+05bCR5aWz55qE6Zu76IWm5LiW55WM5pWR5LiW6KiYLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsi5bui5p+05bCR5aWz55qE6Zu76IWm5LiW55WM5pWR5LiW6KiYLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQTs7R0FFRzs7QUFLSCwrREFBNEQ7QUFDNUQscUNBQXdDO0FBQ3hDLHFDQUF3RjtBQUV4RixJQUFJLE9BQU8sR0FBRyxzQkFBc0IsQ0FBQztBQUNyQyxJQUFJLE9BQU8sR0FBRyxzQkFBc0IsQ0FBQztBQUNyQyxJQUFJLE9BQU8sR0FBRyx3QkFBd0IsQ0FBQztBQUN2QyxJQUFJLE9BQU8sR0FBRyx3QkFBd0IsQ0FBQztBQUUxQixRQUFBLFVBQVUsR0FBeUI7SUFFL0MsR0FBRyxjQUFjO0lBRWpCOztPQUVHO0lBQ0gsTUFBTSxFQUFFO1FBQ1A7Ozs7V0FJRztRQUNILENBQUMsRUFBRTtZQUNGLEdBQUcsT0FBTyxJQUFJO1lBQ2QsYUFBYTtZQUNiLEdBQUcsT0FBTyxLQUFLO1NBQ2Y7UUFFRCxFQUFFLENBQUM7UUFDRjs7V0FFRztRQUNILENBQUM7UUFDRDs7V0FFRztRQUNILEVBQUU7UUFDRjs7V0FFRztRQUNILElBQUk7UUFDSjs7V0FFRztRQUNILENBQUM7UUFDRDs7OztXQUlHO1FBQ0gsTUFBTTtRQUNOOztXQUVHO1FBQ0gsTUFBTTtRQUNOOzs7O1dBSUc7UUFDSCxFQUFFO1FBQ0Y7Ozs7OztXQU1HO1FBQ0gsR0FBRyxHQUNIO1lBRUE7O2VBRUc7WUFDSCxJQUFJLE1BQU0sRUFDVjtnQkFDQyxJQUFJO2dCQUNIOzttQkFFRztnQkFDSCxLQUFLO2dCQUNMOzttQkFFRztnQkFDSCxHQUFHLEdBQ0gsR0FBRyxNQUFNLENBQUM7Z0JBRVgsQ0FBQyxJQUFJLGlCQUFPLENBQUMsR0FBRyxDQUFDO29CQUNoQixHQUFHO29CQUNILEtBQUs7aUJBQ0wsQ0FBQyxDQUFDO2dCQUVIOzttQkFFRztnQkFDSCxJQUFJLEdBQUcsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUVkOzs7O21CQUlHO2dCQUNILEdBQUcsSUFBSSxNQUFNLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQzthQUMzQjtZQUVELE9BQU87Z0JBQ047O21CQUVHO2dCQUNILEVBQUU7Z0JBQ0Y7O21CQUVHO2dCQUNILElBQUk7Z0JBQ0o7Ozs7OzttQkFNRztnQkFDSCxHQUFHO2FBQ0gsQ0FBQTtRQUNGLENBQUM7S0FDRDtJQUVEOztPQUVHO0lBQ0gsT0FBTyxFQUFFO1FBQ1IsZ0NBQWdDO1FBRWhDOzs7OztXQUtHO1FBQ0gsQ0FBQyxFQUFFO1lBQ0YsR0FBRyxPQUFPLElBQUk7WUFDZCxHQUFHO1lBQ0g7Z0JBQ0MsSUFBSTtnQkFDSixXQUFXO2dCQUNYLGlDQUFpQzthQUNqQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUM7WUFDWCxHQUFHO1lBQ0gsV0FBVztZQUNYLElBQUk7WUFDSixHQUFHLE9BQU8sSUFBSTtTQUNkO1FBQ0QsRUFBRSxFQUFFLHFDQUE4QixDQUFDO1lBQ2xDLGlDQUEwQjtTQUMxQixDQUFDO0tBQ0Y7Q0FFRCxDQUFDO0FBRUYsa0JBQWUsa0JBQVUsQ0FBQSIsInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogQ3JlYXRlZCBieSB1c2VyIG9uIDIwMTkvNC8xNC5cbiAqL1xuXG5pbXBvcnQgeyBfaGFuZGxlT3B0aW9ucywgbWFrZU9wdGlvbnMgfSBmcm9tICdAbm9kZS1ub3ZlbC90eHQtc3BsaXQvbGliL2luZGV4JztcbmltcG9ydCB7IElPcHRpb25zLCBJT3B0aW9uc1JlcXVpcmVkLCBJT3B0aW9uc1JlcXVpcmVkVXNlciwgSURhdGFWb2x1bWUsIElEYXRhQ2hhcHRlciB9IGZyb20gJ0Bub2RlLW5vdmVsL3R4dC1zcGxpdC9saWIvaW50ZXJmYWNlJztcblxuaW1wb3J0IHsgY29uc29sZSB9IGZyb20gJ0Bub2RlLW5vdmVsL3R4dC1zcGxpdC9saWIvY29uc29sZSc7XG5pbXBvcnQgdHBsQmFzZU9wdGlvbnMgZnJvbSAnLi9saWIvYmFzZSc7XG5pbXBvcnQgeyBiYXNlQ2JQYXJzZUNoYXB0ZXJNYXRjaDAwMywgYmFzZUNiUGFyc2VDaGFwdGVyTWF0Y2hNYWluMDAxIH0gZnJvbSAnLi9saWIvcnVsZSc7XG5cbmxldCB2X2xpbmUxID0gYOKYoOKYoOKYoOKZm+KYoOKYoOKYoOKZm+KYoOKYoOKYoOKZm+KYoOKYoOKYoOKZm+KYoOKYoOKYoOKZm2A7XG5sZXQgdl9saW5lMiA9IGDimZvimKDimKDimKDimZvimKDimKDimKDimZvimKDimKDimKDimZvimKDimKDimKDimZvimKDimKDimKBgO1xubGV0IGNfbGluZTEgPSBg4p2E4pyh4pyl4pyp4pyn4pyl4pyr4pyq4pyt4piG4piF4pys4pyw4p+h4pyu4pym4p+h4pyv4pi44pyg4p2H4pyjYDtcbmxldCBjX2xpbmUyID0gYOKdhOKcoeKcpeKcqeKcp+KcpeKcq+KcquKcreKYhuKYheKcrOKcsOKfoeKcruKcpuKfoeKcr+KYuOKcoOKdh+Kco2A7XG5cbmV4cG9ydCBjb25zdCB0cGxPcHRpb25zOiBJT3B0aW9uc1JlcXVpcmVkVXNlciA9IHtcblxuXHQuLi50cGxCYXNlT3B0aW9ucyxcblxuXHQvKipcblx0ICog6YCZ5YCL5Y+D5pW4IOWPr+WIqumZpOaIluWKoOS4iiBfIOWmguaenOaykuacieeUqOWIsOeahOipsVxuXHQgKi9cblx0dm9sdW1lOiB7XG5cdFx0LyoqXG5cdFx0ICog5pWF5oSP5pS+5LiA5YCL54Sh5pWI6YWN5bCNIOWvpumam+S9v+eUqOaZguiri+iHquihjOabtOaUuVxuXHRcdCAqXG5cdFx0ICog55W25rKS5pyJ6YWN5bCN5Yiw55qE5pmC5YCZIOacg+iHquWLleeUoueUnyAwMDAwMF91bmtub3cg6LOH5paZ5aS+XG5cdFx0ICovXG5cdFx0cjogW1xuXHRcdFx0YCR7dl9saW5lMX1cXG5gLFxuXHRcdFx0JyhbXlxcXFxuXSspXFxuJyxcblx0XHRcdGAke3ZfbGluZTJ9XFxuK2AsXG5cdFx0XSxcblxuXHRcdGNiKHtcblx0XHRcdC8qKlxuXHRcdFx0ICog5pa8IG1hdGNoIOWIl+ihqOS4reeahCBpbmRleCDluo/liJdcblx0XHRcdCAqL1xuXHRcdFx0aSxcblx0XHRcdC8qKlxuXHRcdFx0ICog5qqU5qGI5bqP5YiXKOWEsuWtmOaqlOahiOaZguacg+WBmueCuuWJjee9ruipnilcblx0XHRcdCAqL1xuXHRcdFx0aWQsXG5cdFx0XHQvKipcblx0XHRcdCAqIOaomemhjOWQjeeosSDpoJDoqK3mg4Xms4HkuIvnrYnmlrwgbWF0Y2gg5Yiw55qE5qiZ6aGMXG5cdFx0XHQgKi9cblx0XHRcdG5hbWUsXG5cdFx0XHQvKipcblx0XHRcdCAqIOacrOmajuauteeahCBtYXRjaCDlgLxcblx0XHRcdCAqL1xuXHRcdFx0bSxcblx0XHRcdC8qKlxuXHRcdFx0ICog5LiK5LiA5qyh55qEIG1hdGNoIOWAvFxuXHRcdFx0ICpcblx0XHRcdCAqIOS9huaYryDlr6bpmpvkuIog6YCZ5Y+D5pW4IOaJjeaYr+acrOasoSBjYWxsYmFjayDnnJ/mraPnmoQgbWF0Y2gg5YWn5a65XG5cdFx0XHQgKi9cblx0XHRcdG1fbGFzdCxcblx0XHRcdC8qKlxuXHRcdFx0ICog55uu5YmN5bey57aT5YiG5Ymy55qE5qqU5qGI5YiX6KGo6IiH5YWn5a65XG5cdFx0XHQgKi9cblx0XHRcdF9maWxlcyxcblx0XHRcdC8qKlxuXHRcdFx0ICog5pa85omA5pyJ56ug56+A5Lit55qE5bqP5YiXXG5cdFx0XHQgKlxuXHRcdFx0ICogQHJlYWRvbmx5XG5cdFx0XHQgKi9cblx0XHRcdGlpLFxuXHRcdFx0LyoqXG5cdFx0XHQgKiDmnKzmrKEgbWF0Y2gg55qEIOWFp+aWhyBzdGFydCBpbmRleFxuXHRcdFx0ICog5Y+v6YCa6YGO5L+u5pS55pW45YC85L6G5o6n5Yi25YWn5paH56+E5ZyNXG5cdFx0XHQgKlxuXHRcdFx0ICogQGV4YW1wbGVcblx0XHRcdCAqIGlkeCArPSBtX2xhc3QubWF0Y2gubGVuZ3RoOyAvLyDlhafmloflv73nlaXmnKzmrKEgbWF0Y2gg5Yiw55qE5qiZ6aGMXG5cdFx0XHQgKi9cblx0XHRcdGlkeCxcblx0XHR9KVxuXHRcdHtcblx0XHRcdC8qKlxuXHRcdFx0ICog5L6d54Wn5L2g57Wm55qEIHJlZ2V4cCDlhaflrrnkvoblm57lgrPnmoTos4fmlplcblx0XHRcdCAqL1xuXHRcdFx0aWYgKG1fbGFzdClcblx0XHRcdHtcblx0XHRcdFx0bGV0IHtcblx0XHRcdFx0XHQvKipcblx0XHRcdFx0XHQgKiDphY3lsI3liLDnmoTlhaflrrlcblx0XHRcdFx0XHQgKi9cblx0XHRcdFx0XHRtYXRjaCxcblx0XHRcdFx0XHQvKipcblx0XHRcdFx0XHQgKiDphY3lsI3lh7rkvobnmoTpmaPliJdcblx0XHRcdFx0XHQgKi9cblx0XHRcdFx0XHRzdWIsXG5cdFx0XHRcdH0gPSBtX2xhc3Q7XG5cblx0XHRcdFx0MCAmJiBjb25zb2xlLmRpcih7XG5cdFx0XHRcdFx0c3ViLFxuXHRcdFx0XHRcdG1hdGNoLFxuXHRcdFx0XHR9KTtcblxuXHRcdFx0XHQvKipcblx0XHRcdFx0ICogQHRvZG8g6YCZ6KOh5Y+v5Lul5Yqg5LiK5pu05aSa6Kqe5rOV5L6G5qC85byP5YyW5qiZ6aGMXG5cdFx0XHRcdCAqL1xuXHRcdFx0XHRuYW1lID0gc3ViWzBdO1xuXG5cdFx0XHRcdC8qKlxuXHRcdFx0XHQgKiDlsIflrprkvY3pu57liqDkuIrmnKzmrKHphY3lsI3liLDnmoTlhaflrrnnmoTplbfluqZcblx0XHRcdFx0ICog5q2k5q2l6amf5Y+v5Lul55yB55WlXG5cdFx0XHRcdCAqIOS9huS9v+eUqOatpOatpempn+aZguWPr+S7peWQjOaZguWcqOWIh+WJsuaZguWwjeaWvOWFp+WuueS9nOeyvuewoVxuXHRcdFx0XHQgKi9cblx0XHRcdFx0aWR4ICs9IG1fbGFzdC5tYXRjaC5sZW5ndGg7XG5cdFx0XHR9XG5cblx0XHRcdHJldHVybiB7XG5cdFx0XHRcdC8qKlxuXHRcdFx0XHQgKiDmqpTmoYjluo/liJco5YSy5a2Y5qqU5qGI5pmC5pyD5YGa54K65YmN572u6KmeKVxuXHRcdFx0XHQgKi9cblx0XHRcdFx0aWQsXG5cdFx0XHRcdC8qKlxuXHRcdFx0XHQgKiDmqJnpoYzlkI3nqLEg6aCQ6Kit5oOF5rOB5LiL562J5pa8IG1hdGNoIOWIsOeahOaomemhjFxuXHRcdFx0XHQgKi9cblx0XHRcdFx0bmFtZSxcblx0XHRcdFx0LyoqXG5cdFx0XHRcdCAqIOacrOasoSBtYXRjaCDnmoQg5YWn5paHIHN0YXJ0IGluZGV4XG5cdFx0XHRcdCAqIOWPr+mAmumBjuS/ruaUueaVuOWAvOS+huaOp+WItuWFp+aWh+evhOWcjVxuXHRcdFx0XHQgKlxuXHRcdFx0XHQgKiBAZXhhbXBsZVxuXHRcdFx0XHQgKiBpZHggKz0gbV9sYXN0Lm1hdGNoLmxlbmd0aDsgLy8g5YWn5paH5b+955Wl5pys5qyhIG1hdGNoIOWIsOeahOaomemhjFxuXHRcdFx0XHQgKi9cblx0XHRcdFx0aWR4LFxuXHRcdFx0fVxuXHRcdH0sXG5cdH0sXG5cblx0LyoqXG5cdCAqIOmAmeWAi+WPg+aVuOaYr+W/heWhq+mBuOmghVxuXHQgKi9cblx0Y2hhcHRlcjoge1xuXHRcdC8vcjogL15cXGQrXFxuKOesrFxcZCvoqbEpICooW15cXG5dKikkLyxcblxuXHRcdC8qKlxuXHRcdCAqIOato+W4uOS+huiqquS4jemcgOimgeW8hOW+l+mAmem6vOikh+mbnOeahOaoo+W8jyDlj6rpnIDopoHphY3lsI3mqJnpoYzlsLHlpb1cblx0XHQgKiDkvYbmh7bmg7Dmg7PopoHlnKjliIcgdHh0IOeahOaZguWAmSDpoIbkvr/nuK7muJvlhafmlofnmoToqbEg5bCx5Y+v5Lul5a+r55qE6KSH6Zuc5LiA6bueXG5cdFx0ICpcblx0XHQgKiDnlbYgciDkuI3mmK8gcmVnZXhwIOeahOaZguWAmSDlnKjln7fooYzmmYLmnIPoh6rli5XlsIfpgJnlgIvlj4PmlbjnmoTlhaflrrnovYnmiJAgcmVnZXhwXG5cdFx0ICovXG5cdFx0cjogW1xuXHRcdFx0YCR7Y19saW5lMX1cXG5gLFxuXHRcdFx0JygnLFxuXHRcdFx0W1xuXHRcdFx0XHQn5bqP56ugJyxcblx0XHRcdFx0Jyg/OuW5lemWk3zntYLnq6ApJyxcblx0XHRcdFx0YOesrFtcXFxcZO+8kC3vvJldKyg/OlxcLltcXFxcZO+8kC3vvJldKyk/KD866KmxKWAsXG5cdFx0XHRdLmpvaW4oJ3wnKSxcblx0XHRcdCcpJyxcblx0XHRcdCcoW15cXFxcbl0rKScsXG5cdFx0XHQnXFxuJyxcblx0XHRcdGAke2NfbGluZTJ9XFxuYCxcblx0XHRdLFxuXHRcdGNiOiBiYXNlQ2JQYXJzZUNoYXB0ZXJNYXRjaE1haW4wMDEoW1xuXHRcdFx0YmFzZUNiUGFyc2VDaGFwdGVyTWF0Y2gwMDMsXG5cdFx0XSksXG5cdH0sXG5cbn07XG5cbmV4cG9ydCBkZWZhdWx0IHRwbE9wdGlvbnNcbiJdfQ==