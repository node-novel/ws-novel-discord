"use strict";
/**
 * Created by user on 2019/4/14.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const ___1 = require("./base/\u7AE0_\u8A71");
const rule_1 = require("./lib/rule");
const StrUtil = require("str-util");
let v_line = `▞▦▦▦▚▦▦▦▞▦▦▦▚▦▦▦▞▦▦▦▚▦▦▦▞`;
let v_line2 = v_line;
let c_line = `❤❤ᾪ❤❤ᾪ❤❤ᾪ❤❤ᾪ❤❤ᾪ❤❤ᾪ❤❤ᾪ`;
let c_line2 = c_line;
let _space = ' 　\\t';
const c = '　';
exports.tplOptions = {
    ...___1.default,
    volume: {
        ...___1.default.volume,
        r: [
            `${v_line}\\n`,
            '(',
            [
                `第[一-十]+章`,
                `第[\\d０-９]+章`,
            ].join('|'),
            ')',
            `[${_space}\\-]*`,
            `([^\\n]*)`,
            `\\n${v_line2}\\n`,
        ],
        cb: rule_1.baseCbParseChapterMatchMain001([
            rule_1.createCbParseChapterMatch({
                handleMatchSubIDString(argv, _data) {
                    let ids;
                    if (argv.idn) {
                        ids = `第${argv.idn}章`;
                    }
                    else {
                        ids = argv.ido;
                    }
                    return ids;
                },
                handleMatchSubID(argv, _data) {
                    if (/^\d+(?:\.\d+)?$/.test(argv.id)) {
                        argv.id = String(argv.id);
                        argv.idn = StrUtil.toFullNumber(argv.id.toString());
                    }
                    return argv;
                }
            }),
        ]),
    },
    /**
     * 這個參數是必填選項
     */
    chapter: {
        ...___1.default.chapter,
        r: [
            `${c_line}\\n`,
            '(',
            [
                '(?:幕間|終章)',
                `第[\\d０-９]+(?:\.[\\d０-９]+)?(?:話)`,
                `第[一-十]+(?:話)`,
            ].join('|'),
            ')?',
            `(?:`,
            `([^\\n]+?)`,
            ')',
            `\\n${c_line2}\\n`,
        ],
        cb: rule_1.baseCbParseChapterMatchMain001([
            rule_1.baseCbParseChapterMatch003,
        ]),
    },
};
exports.default = exports.tplOptions;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoi6JGJ6Zqx5qu75LiN5oKy5ZiGLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsi6JGJ6Zqx5qu75LiN5oKy5ZiGLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQTs7R0FFRzs7QUFhSCw2Q0FBd0M7QUFDeEMscUNBS29CO0FBQ3BCLG9DQUFxQztBQUVyQyxJQUFJLE1BQU0sR0FBRywyQkFBMkIsQ0FBQztBQUN6QyxJQUFJLE9BQU8sR0FBRyxNQUFNLENBQUM7QUFDckIsSUFBSSxNQUFNLEdBQUcsdUJBQXVCLENBQUM7QUFDckMsSUFBSSxPQUFPLEdBQUcsTUFBTSxDQUFDO0FBRXJCLElBQUksTUFBTSxHQUFHLE9BQU8sQ0FBQztBQUNyQixNQUFNLENBQUMsR0FBRyxHQUFHLENBQUM7QUFFRCxRQUFBLFVBQVUsR0FBeUI7SUFFL0MsR0FBRyxZQUFjO0lBRWpCLE1BQU0sRUFBRTtRQUVQLEdBQUcsWUFBYyxDQUFDLE1BQU07UUFFeEIsQ0FBQyxFQUFFO1lBQ0YsR0FBRyxNQUFNLEtBQUs7WUFDZCxHQUFHO1lBQ0g7Z0JBQ0MsVUFBVTtnQkFDVixhQUFhO2FBQ2IsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDO1lBQ1gsR0FBRztZQUNILElBQUksTUFBTSxPQUFPO1lBQ2pCLFdBQVc7WUFDWCxNQUFNLE9BQU8sS0FBSztTQUNsQjtRQUVELEVBQUUsRUFBRSxxQ0FBOEIsQ0FBQztZQUVsQyxnQ0FBeUIsQ0FBQztnQkFFekIsc0JBQXNCLENBQUMsSUFBSSxFQUFFLEtBQUs7b0JBRWpDLElBQUksR0FBVyxDQUFDO29CQUVoQixJQUFJLElBQUksQ0FBQyxHQUFHLEVBQ1o7d0JBQ0MsR0FBRyxHQUFHLElBQUksSUFBSSxDQUFDLEdBQUcsR0FBRyxDQUFDO3FCQUN0Qjt5QkFFRDt3QkFDQyxHQUFHLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQztxQkFDZjtvQkFFRCxPQUFPLEdBQUcsQ0FBQTtnQkFDWCxDQUFDO2dCQUVELGdCQUFnQixDQUFDLElBQUksRUFBRSxLQUFLO29CQUUzQixJQUFJLGlCQUFpQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLEVBQ25DO3dCQUNDLElBQUksQ0FBQyxFQUFFLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQzt3QkFFMUIsSUFBSSxDQUFDLEdBQUcsR0FBRyxPQUFPLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQztxQkFDcEQ7b0JBRUQsT0FBTyxJQUFJLENBQUE7Z0JBQ1osQ0FBQzthQUVELENBQUM7U0FFRixDQUFDO0tBRUY7SUFFRDs7T0FFRztJQUNILE9BQU8sRUFBRTtRQUVSLEdBQUcsWUFBYyxDQUFDLE9BQU87UUFFekIsQ0FBQyxFQUFFO1lBQ0YsR0FBRyxNQUFNLEtBQUs7WUFDZCxHQUFHO1lBQ0g7Z0JBQ0MsV0FBVztnQkFDWCxpQ0FBaUM7Z0JBQ2pDLGNBQWM7YUFDZCxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUM7WUFDWCxJQUFJO1lBQ0osS0FBSztZQUNMLFlBQVk7WUFDWixHQUFHO1lBQ0gsTUFBTSxPQUFPLEtBQUs7U0FDbEI7UUFFRCxFQUFFLEVBQUUscUNBQThCLENBQUM7WUFFbEMsaUNBQTBCO1NBRTFCLENBQUM7S0FFRjtDQUVELENBQUM7QUFFRixrQkFBZSxrQkFBVSxDQUFBIiwic291cmNlc0NvbnRlbnQiOlsiLyoqXG4gKiBDcmVhdGVkIGJ5IHVzZXIgb24gMjAxOS80LzE0LlxuICovXG5cbmltcG9ydCB7IF9oYW5kbGVPcHRpb25zLCBtYWtlT3B0aW9ucyB9IGZyb20gJ0Bub2RlLW5vdmVsL3R4dC1zcGxpdC9saWIvaW5kZXgnO1xuaW1wb3J0IHtcblx0SU9wdGlvbnMsXG5cdElPcHRpb25zUmVxdWlyZWQsXG5cdElPcHRpb25zUmVxdWlyZWRVc2VyLFxuXHRJRGF0YVZvbHVtZSxcblx0SURhdGFDaGFwdGVyLFxuXHRJU3BsaXRDQlBhcmFtZXRlcnMsXG59IGZyb20gJ0Bub2RlLW5vdmVsL3R4dC1zcGxpdC9saWIvaW50ZXJmYWNlJztcblxuaW1wb3J0IHsgY29uc29sZSB9IGZyb20gJ0Bub2RlLW5vdmVsL3R4dC1zcGxpdC9saWIvY29uc29sZSc7XG5pbXBvcnQgdHBsQmFzZU9wdGlvbnMgZnJvbSAnLi9iYXNlL+eroF/oqbEnO1xuaW1wb3J0IHtcblx0YmFzZUNiUGFyc2VDaGFwdGVyTWF0Y2gwMDEsXG5cdGJhc2VDYlBhcnNlQ2hhcHRlck1hdGNoMDAzLFxuXHRiYXNlQ2JQYXJzZUNoYXB0ZXJNYXRjaE1haW4wMDEsXG5cdGNyZWF0ZUNiUGFyc2VDaGFwdGVyTWF0Y2gsIElDcmVhdGVDYlBhcnNlQ2hhcHRlck1hdGNoT3B0aW9uc1N1Yixcbn0gZnJvbSAnLi9saWIvcnVsZSc7XG5pbXBvcnQgU3RyVXRpbCA9IHJlcXVpcmUoJ3N0ci11dGlsJyk7XG5cbmxldCB2X2xpbmUgPSBg4pae4pam4pam4pam4paa4pam4pam4pam4pae4pam4pam4pam4paa4pam4pam4pam4pae4pam4pam4pam4paa4pam4pam4pam4paeYDtcbmxldCB2X2xpbmUyID0gdl9saW5lO1xubGV0IGNfbGluZSA9IGDinaTinaThvqrinaTinaThvqrinaTinaThvqrinaTinaThvqrinaTinaThvqrinaTinaThvqrinaTinaThvqpgO1xubGV0IGNfbGluZTIgPSBjX2xpbmU7XG5cbmxldCBfc3BhY2UgPSAnIOOAgFxcXFx0JztcbmNvbnN0IGMgPSAn44CAJztcblxuZXhwb3J0IGNvbnN0IHRwbE9wdGlvbnM6IElPcHRpb25zUmVxdWlyZWRVc2VyID0ge1xuXG5cdC4uLnRwbEJhc2VPcHRpb25zLFxuXG5cdHZvbHVtZToge1xuXG5cdFx0Li4udHBsQmFzZU9wdGlvbnMudm9sdW1lLFxuXG5cdFx0cjogW1xuXHRcdFx0YCR7dl9saW5lfVxcXFxuYCxcblx0XHRcdCcoJyxcblx0XHRcdFtcblx0XHRcdFx0YOesrFvkuIAt5Y2BXSvnq6BgLFxuXHRcdFx0XHRg56ysW1xcXFxk77yQLe+8mV0r56ugYCxcblx0XHRcdF0uam9pbignfCcpLFxuXHRcdFx0JyknLFxuXHRcdFx0YFske19zcGFjZX1cXFxcLV0qYCxcblx0XHRcdGAoW15cXFxcbl0qKWAsXG5cdFx0XHRgXFxcXG4ke3ZfbGluZTJ9XFxcXG5gLFxuXHRcdF0sXG5cblx0XHRjYjogYmFzZUNiUGFyc2VDaGFwdGVyTWF0Y2hNYWluMDAxKFtcblxuXHRcdFx0Y3JlYXRlQ2JQYXJzZUNoYXB0ZXJNYXRjaCh7XG5cblx0XHRcdFx0aGFuZGxlTWF0Y2hTdWJJRFN0cmluZyhhcmd2LCBfZGF0YSlcblx0XHRcdFx0e1xuXHRcdFx0XHRcdGxldCBpZHM6IHN0cmluZztcblxuXHRcdFx0XHRcdGlmIChhcmd2Lmlkbilcblx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHRpZHMgPSBg56ysJHthcmd2Lmlkbn3nq6BgO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0XHRlbHNlXG5cdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0aWRzID0gYXJndi5pZG87XG5cdFx0XHRcdFx0fVxuXG5cdFx0XHRcdFx0cmV0dXJuIGlkc1xuXHRcdFx0XHR9LFxuXG5cdFx0XHRcdGhhbmRsZU1hdGNoU3ViSUQoYXJndiwgX2RhdGEpXG5cdFx0XHRcdHtcblx0XHRcdFx0XHRpZiAoL15cXGQrKD86XFwuXFxkKyk/JC8udGVzdChhcmd2LmlkKSlcblx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHRhcmd2LmlkID0gU3RyaW5nKGFyZ3YuaWQpO1xuXG5cdFx0XHRcdFx0XHRhcmd2LmlkbiA9IFN0clV0aWwudG9GdWxsTnVtYmVyKGFyZ3YuaWQudG9TdHJpbmcoKSk7XG5cdFx0XHRcdFx0fVxuXG5cdFx0XHRcdFx0cmV0dXJuIGFyZ3Zcblx0XHRcdFx0fVxuXG5cdFx0XHR9KSxcblxuXHRcdF0pLFxuXG5cdH0sXG5cblx0LyoqXG5cdCAqIOmAmeWAi+WPg+aVuOaYr+W/heWhq+mBuOmghVxuXHQgKi9cblx0Y2hhcHRlcjoge1xuXG5cdFx0Li4udHBsQmFzZU9wdGlvbnMuY2hhcHRlcixcblxuXHRcdHI6IFtcblx0XHRcdGAke2NfbGluZX1cXFxcbmAsXG5cdFx0XHQnKCcsXG5cdFx0XHRbXG5cdFx0XHRcdCcoPzrluZXplpN857WC56ugKScsXG5cdFx0XHRcdGDnrKxbXFxcXGTvvJAt77yZXSsoPzpcXC5bXFxcXGTvvJAt77yZXSspPyg/OuipsSlgLFxuXHRcdFx0XHRg56ysW+S4gC3ljYFdKyg/OuipsSlgLFxuXHRcdFx0XS5qb2luKCd8JyksXG5cdFx0XHQnKT8nLFxuXHRcdFx0YCg/OmAsXG5cdFx0XHRgKFteXFxcXG5dKz8pYCxcblx0XHRcdCcpJyxcblx0XHRcdGBcXFxcbiR7Y19saW5lMn1cXFxcbmAsXG5cdFx0XSxcblxuXHRcdGNiOiBiYXNlQ2JQYXJzZUNoYXB0ZXJNYXRjaE1haW4wMDEoW1xuXG5cdFx0XHRiYXNlQ2JQYXJzZUNoYXB0ZXJNYXRjaDAwMyxcblxuXHRcdF0pLFxuXG5cdH0sXG5cbn07XG5cbmV4cG9ydCBkZWZhdWx0IHRwbE9wdGlvbnNcbiJdfQ==