/**
 * Created by user on 2019/8/9.
 */

import path = require('path');
import FastGlob = require('@bluelovers/fast-glob/bluebird');
import fs = require('fs-iconv');
import { removeZeroWidth } from 'zero-width/lib';
import { console } from '@node-novel/txt-split/lib/console';
import reportZeroWidthWithSpace from 'zero-width/lib/stat';
import { crlf } from 'crlf-normalize';

const __root_top = path.join(__dirname, '../../..');
const __root_data = path.join(__root_top, 'data');

const __root_src = path.join(__root_data, 'src');

FastGlob
	.async([
	'**/*.txt',
], {
	cwd: __root_src,
	absolute: true,
})
	.map(async (file) => {

		let txt = await fs.loadFile(file, {
			autoDecode: true,
		}).then(s => String(s));

		let txt2 = crlf(removeZeroWidth(txt));

		if (txt != txt2)
		{
			await fs.saveFile(file, txt2);

			console.debug(path.relative(__root_src, file));

			console.dir(reportZeroWidthWithSpace(txt));

			console.dir(reportZeroWidthWithSpace(txt2));
		}
	})
;


//console.dir(reportZeroWidthWithSpace(`但是，姐姐大人‬‭揪‬Ｑ的力度太‭強了，有點痛。‬`));
//console.dir(reportZeroWidthWithSpace(`但是，姐姐大人揪Ｑ的力度太強了，有點痛。`));
