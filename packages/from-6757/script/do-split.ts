/**
 * Created by user on 2019/4/14.
 */

import txtSplit, { autoFile, IOptionsRequiredUser } from '@node-novel/txt-split';
import fsIconv, { trimFilename } from 'fs-iconv';
import fs = require('fs-extra');
import path = require('path');
import FastGlob = require('@bluelovers/fast-glob');
import Bluebird = require('bluebird');
import crossSpawn = require('cross-spawn-extra');
import { searchRule } from '../rule/lib/index';
import { rule_match } from '../setting.conf';

const __root_top = path.join(__dirname, '../../..');

const __root_data = path.join(__root_top, 'data');

const __root_out = path.join(__root_data, 'out/from-6757');
const __root_download = path.join(__root_data, 'src/from-6757');

Bluebird
	.resolve(FastGlob.async<string>([
		'*.txt',
	], {
		cwd: __root_download
	}))
.mapSeries(async function (file)
{
	let basename = path.basename(file, '.txt');
	let { rule_name, rule_file } = searchRule(basename, {
		rule_match,
	});

	console.debug(basename, rule_name);

	let skip: boolean;

	let tplOptions: IOptionsRequiredUser = await import(rule_file)
		.then(m => m.default)
		.catch(e => {
			console.warn(e.message);
			skip = true;
		})
	;

	if (skip)
	{
		return;
	}

	let outDir = path.join(__root_out, basename);
	let fullpath = path.join(__root_download, file);

	await fsIconv
		.loadFile(fullpath, {
//			encoding: 'utf8',
			autoDecode: true,
		})
		.then(data => fsIconv.saveFile(fullpath, data))
	;

	fs.emptyDirSync(path.join(outDir));

	await txtSplit.autoFile(fullpath, {
		...tplOptions,
		outDir,
	});
})
;
