"use strict";
/**
 * Created by user on 2019/4/14.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const txt_split_1 = require("@node-novel/txt-split");
const fs_iconv_1 = require("fs-iconv");
const console_1 = require("@node-novel/txt-split/lib/console");
const index_1 = require("../rule/lib/index");
const setting_conf_1 = require("../setting.conf");
const fs = require("fs-extra");
const path = require("path");
const FastGlob = require("@bluelovers/fast-glob");
const Bluebird = require("bluebird");
const __root_top = path.join(__dirname, '../../..');
const __root_data = path.join(__root_top, 'data');
const folder_id = `from-other`;
const __root_out = path.join(__root_data, 'out', folder_id);
const __root_download = path.join(__root_data, 'src', folder_id);
Bluebird
    .resolve(FastGlob.async([
    '*.txt',
], {
    cwd: __root_download
}))
    .mapSeries(async function (file) {
    let basename = path.basename(file, '.txt');
    let { rule_name = index_1.RULE_DEFAULT, rule_file } = index_1.searchRule(basename, {
        rule_match: setting_conf_1.rule_match,
    });
    console_1.console.debug(basename, rule_name);
    let skip;
    let tplOptions = await Promise.resolve().then(() => require(rule_file)).then(m => m.default)
        .catch(e => {
        console_1.console.warn(e.message);
        skip = true;
    });
    if (skip) {
        return;
    }
    let outDir = path.join(__root_out, basename);
    let fullpath = path.join(__root_download, file);
    await fs_iconv_1.default
        .loadFile(fullpath, {
        //			encoding: 'utf8',
        autoDecode: true,
    })
        .then(data => fs_iconv_1.default.saveFile(fullpath, data));
    fs.emptyDirSync(path.join(outDir));
    await txt_split_1.default.autoFile(fullpath, {
        ...tplOptions,
        outDir,
    });
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZG8tc3BsaXQtMi5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImRvLXNwbGl0LTIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBOztHQUVHOztBQUVILHFEQUF1RTtBQUN2RSx1Q0FBK0I7QUFDL0IsK0RBQTREO0FBQzVELDZDQUE2RDtBQUM3RCxrREFBNkM7QUFDN0MsK0JBQWdDO0FBQ2hDLDZCQUE4QjtBQUM5QixrREFBbUQ7QUFDbkQscUNBQXNDO0FBRXRDLE1BQU0sVUFBVSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLFVBQVUsQ0FBQyxDQUFDO0FBRXBELE1BQU0sV0FBVyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFLE1BQU0sQ0FBQyxDQUFDO0FBRWxELE1BQU0sU0FBUyxHQUFHLFlBQVksQ0FBQztBQUUvQixNQUFNLFVBQVUsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxLQUFLLEVBQUUsU0FBUyxDQUFDLENBQUM7QUFDNUQsTUFBTSxlQUFlLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsS0FBSyxFQUFFLFNBQVMsQ0FBQyxDQUFDO0FBRWpFLFFBQVE7S0FDTixPQUFPLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBUztJQUMvQixPQUFPO0NBQ1AsRUFBRTtJQUNGLEdBQUcsRUFBRSxlQUFlO0NBQ3BCLENBQUMsQ0FBQztLQUNILFNBQVMsQ0FBQyxLQUFLLFdBQVcsSUFBSTtJQUU5QixJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksRUFBRSxNQUFNLENBQUMsQ0FBQztJQUUzQyxJQUFJLEVBQUUsU0FBUyxHQUFHLG9CQUFZLEVBQUUsU0FBUyxFQUFFLEdBQUcsa0JBQVUsQ0FBQyxRQUFRLEVBQUU7UUFDbEUsVUFBVSxFQUFWLHlCQUFVO0tBQ1YsQ0FBQyxDQUFDO0lBRUgsaUJBQU8sQ0FBQyxLQUFLLENBQUMsUUFBUSxFQUFFLFNBQVMsQ0FBQyxDQUFDO0lBRW5DLElBQUksSUFBYSxDQUFDO0lBRWxCLElBQUksVUFBVSxHQUF5QixNQUFNLHFDQUFPLFNBQVMsR0FDM0QsSUFBSSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQztTQUNwQixLQUFLLENBQUMsQ0FBQyxDQUFDLEVBQUU7UUFDVixpQkFBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDeEIsSUFBSSxHQUFHLElBQUksQ0FBQztJQUNiLENBQUMsQ0FBQyxDQUNGO0lBRUQsSUFBSSxJQUFJLEVBQ1I7UUFDQyxPQUFPO0tBQ1A7SUFFRCxJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRSxRQUFRLENBQUMsQ0FBQztJQUM3QyxJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLGVBQWUsRUFBRSxJQUFJLENBQUMsQ0FBQztJQUVoRCxNQUFNLGtCQUFPO1NBQ1gsUUFBUSxDQUFDLFFBQVEsRUFBRTtRQUN0QixzQkFBc0I7UUFDbkIsVUFBVSxFQUFFLElBQUk7S0FDaEIsQ0FBQztTQUNELElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLGtCQUFPLENBQUMsUUFBUSxDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsQ0FBQyxDQUMvQztJQUVELEVBQUUsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO0lBRW5DLE1BQU0sbUJBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUSxFQUFFO1FBQ2pDLEdBQUcsVUFBVTtRQUNiLE1BQU07S0FDTixDQUFDLENBQUM7QUFDSixDQUFDLENBQUMsQ0FDRCIsInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogQ3JlYXRlZCBieSB1c2VyIG9uIDIwMTkvNC8xNC5cbiAqL1xuXG5pbXBvcnQgdHh0U3BsaXQsIHsgSU9wdGlvbnNSZXF1aXJlZFVzZXIgfSBmcm9tICdAbm9kZS1ub3ZlbC90eHQtc3BsaXQnO1xuaW1wb3J0IGZzSWNvbnYgZnJvbSAnZnMtaWNvbnYnO1xuaW1wb3J0IHsgY29uc29sZSB9IGZyb20gJ0Bub2RlLW5vdmVsL3R4dC1zcGxpdC9saWIvY29uc29sZSc7XG5pbXBvcnQgeyBSVUxFX0RFRkFVTFQsIHNlYXJjaFJ1bGUgfSBmcm9tICcuLi9ydWxlL2xpYi9pbmRleCc7XG5pbXBvcnQgeyBydWxlX21hdGNoIH0gZnJvbSAnLi4vc2V0dGluZy5jb25mJztcbmltcG9ydCBmcyA9IHJlcXVpcmUoJ2ZzLWV4dHJhJyk7XG5pbXBvcnQgcGF0aCA9IHJlcXVpcmUoJ3BhdGgnKTtcbmltcG9ydCBGYXN0R2xvYiA9IHJlcXVpcmUoJ0BibHVlbG92ZXJzL2Zhc3QtZ2xvYicpO1xuaW1wb3J0IEJsdWViaXJkID0gcmVxdWlyZSgnYmx1ZWJpcmQnKTtcblxuY29uc3QgX19yb290X3RvcCA9IHBhdGguam9pbihfX2Rpcm5hbWUsICcuLi8uLi8uLicpO1xuXG5jb25zdCBfX3Jvb3RfZGF0YSA9IHBhdGguam9pbihfX3Jvb3RfdG9wLCAnZGF0YScpO1xuXG5jb25zdCBmb2xkZXJfaWQgPSBgZnJvbS1vdGhlcmA7XG5cbmNvbnN0IF9fcm9vdF9vdXQgPSBwYXRoLmpvaW4oX19yb290X2RhdGEsICdvdXQnLCBmb2xkZXJfaWQpO1xuY29uc3QgX19yb290X2Rvd25sb2FkID0gcGF0aC5qb2luKF9fcm9vdF9kYXRhLCAnc3JjJywgZm9sZGVyX2lkKTtcblxuQmx1ZWJpcmRcblx0LnJlc29sdmUoRmFzdEdsb2IuYXN5bmM8c3RyaW5nPihbXG5cdFx0JyoudHh0Jyxcblx0XSwge1xuXHRcdGN3ZDogX19yb290X2Rvd25sb2FkXG5cdH0pKVxuLm1hcFNlcmllcyhhc3luYyBmdW5jdGlvbiAoZmlsZSlcbntcblx0bGV0IGJhc2VuYW1lID0gcGF0aC5iYXNlbmFtZShmaWxlLCAnLnR4dCcpO1xuXG5cdGxldCB7IHJ1bGVfbmFtZSA9IFJVTEVfREVGQVVMVCwgcnVsZV9maWxlIH0gPSBzZWFyY2hSdWxlKGJhc2VuYW1lLCB7XG5cdFx0cnVsZV9tYXRjaCxcblx0fSk7XG5cblx0Y29uc29sZS5kZWJ1ZyhiYXNlbmFtZSwgcnVsZV9uYW1lKTtcblxuXHRsZXQgc2tpcDogYm9vbGVhbjtcblxuXHRsZXQgdHBsT3B0aW9uczogSU9wdGlvbnNSZXF1aXJlZFVzZXIgPSBhd2FpdCBpbXBvcnQocnVsZV9maWxlKVxuXHRcdC50aGVuKG0gPT4gbS5kZWZhdWx0KVxuXHRcdC5jYXRjaChlID0+IHtcblx0XHRcdGNvbnNvbGUud2FybihlLm1lc3NhZ2UpO1xuXHRcdFx0c2tpcCA9IHRydWU7XG5cdFx0fSlcblx0O1xuXG5cdGlmIChza2lwKVxuXHR7XG5cdFx0cmV0dXJuO1xuXHR9XG5cblx0bGV0IG91dERpciA9IHBhdGguam9pbihfX3Jvb3Rfb3V0LCBiYXNlbmFtZSk7XG5cdGxldCBmdWxscGF0aCA9IHBhdGguam9pbihfX3Jvb3RfZG93bmxvYWQsIGZpbGUpO1xuXG5cdGF3YWl0IGZzSWNvbnZcblx0XHQubG9hZEZpbGUoZnVsbHBhdGgsIHtcbi8vXHRcdFx0ZW5jb2Rpbmc6ICd1dGY4Jyxcblx0XHRcdGF1dG9EZWNvZGU6IHRydWUsXG5cdFx0fSlcblx0XHQudGhlbihkYXRhID0+IGZzSWNvbnYuc2F2ZUZpbGUoZnVsbHBhdGgsIGRhdGEpKVxuXHQ7XG5cblx0ZnMuZW1wdHlEaXJTeW5jKHBhdGguam9pbihvdXREaXIpKTtcblxuXHRhd2FpdCB0eHRTcGxpdC5hdXRvRmlsZShmdWxscGF0aCwge1xuXHRcdC4uLnRwbE9wdGlvbnMsXG5cdFx0b3V0RGlyLFxuXHR9KTtcbn0pXG47XG4iXX0=