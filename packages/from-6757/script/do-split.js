"use strict";
/**
 * Created by user on 2019/4/14.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const txt_split_1 = require("@node-novel/txt-split");
const fs_iconv_1 = require("fs-iconv");
const fs = require("fs-extra");
const path = require("path");
const FastGlob = require("@bluelovers/fast-glob");
const Bluebird = require("bluebird");
const index_1 = require("../rule/lib/index");
const setting_conf_1 = require("../setting.conf");
const __root_top = path.join(__dirname, '../../..');
const __root_data = path.join(__root_top, 'data');
const __root_out = path.join(__root_data, 'out/from-6757');
const __root_download = path.join(__root_data, 'src/from-6757');
Bluebird
    .resolve(FastGlob.async([
    '*.txt',
], {
    cwd: __root_download
}))
    .mapSeries(async function (file) {
    let basename = path.basename(file, '.txt');
    let { rule_name, rule_file } = index_1.searchRule(basename, {
        rule_match: setting_conf_1.rule_match,
    });
    console.debug(basename, rule_name);
    let skip;
    let tplOptions = await Promise.resolve().then(() => require(rule_file)).then(m => m.default)
        .catch(e => {
        console.warn(e.message);
        skip = true;
    });
    if (skip) {
        return;
    }
    let outDir = path.join(__root_out, basename);
    let fullpath = path.join(__root_download, file);
    await fs_iconv_1.default
        .loadFile(fullpath, {
        //			encoding: 'utf8',
        autoDecode: true,
    })
        .then(data => fs_iconv_1.default.saveFile(fullpath, data));
    fs.emptyDirSync(path.join(outDir));
    await txt_split_1.default.autoFile(fullpath, {
        ...tplOptions,
        outDir,
    });
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZG8tc3BsaXQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJkby1zcGxpdC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUE7O0dBRUc7O0FBRUgscURBQWlGO0FBQ2pGLHVDQUFpRDtBQUNqRCwrQkFBZ0M7QUFDaEMsNkJBQThCO0FBQzlCLGtEQUFtRDtBQUNuRCxxQ0FBc0M7QUFFdEMsNkNBQStDO0FBQy9DLGtEQUE2QztBQUU3QyxNQUFNLFVBQVUsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxVQUFVLENBQUMsQ0FBQztBQUVwRCxNQUFNLFdBQVcsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRSxNQUFNLENBQUMsQ0FBQztBQUVsRCxNQUFNLFVBQVUsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxlQUFlLENBQUMsQ0FBQztBQUMzRCxNQUFNLGVBQWUsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxlQUFlLENBQUMsQ0FBQztBQUVoRSxRQUFRO0tBQ04sT0FBTyxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQVM7SUFDL0IsT0FBTztDQUNQLEVBQUU7SUFDRixHQUFHLEVBQUUsZUFBZTtDQUNwQixDQUFDLENBQUM7S0FDSCxTQUFTLENBQUMsS0FBSyxXQUFXLElBQUk7SUFFOUIsSUFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLEVBQUUsTUFBTSxDQUFDLENBQUM7SUFDM0MsSUFBSSxFQUFFLFNBQVMsRUFBRSxTQUFTLEVBQUUsR0FBRyxrQkFBVSxDQUFDLFFBQVEsRUFBRTtRQUNuRCxVQUFVLEVBQVYseUJBQVU7S0FDVixDQUFDLENBQUM7SUFFSCxPQUFPLENBQUMsS0FBSyxDQUFDLFFBQVEsRUFBRSxTQUFTLENBQUMsQ0FBQztJQUVuQyxJQUFJLElBQWEsQ0FBQztJQUVsQixJQUFJLFVBQVUsR0FBeUIsTUFBTSxxQ0FBTyxTQUFTLEdBQzNELElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUM7U0FDcEIsS0FBSyxDQUFDLENBQUMsQ0FBQyxFQUFFO1FBQ1YsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDeEIsSUFBSSxHQUFHLElBQUksQ0FBQztJQUNiLENBQUMsQ0FBQyxDQUNGO0lBRUQsSUFBSSxJQUFJLEVBQ1I7UUFDQyxPQUFPO0tBQ1A7SUFFRCxJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRSxRQUFRLENBQUMsQ0FBQztJQUM3QyxJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLGVBQWUsRUFBRSxJQUFJLENBQUMsQ0FBQztJQUVoRCxNQUFNLGtCQUFPO1NBQ1gsUUFBUSxDQUFDLFFBQVEsRUFBRTtRQUN0QixzQkFBc0I7UUFDbkIsVUFBVSxFQUFFLElBQUk7S0FDaEIsQ0FBQztTQUNELElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLGtCQUFPLENBQUMsUUFBUSxDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsQ0FBQyxDQUMvQztJQUVELEVBQUUsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO0lBRW5DLE1BQU0sbUJBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUSxFQUFFO1FBQ2pDLEdBQUcsVUFBVTtRQUNiLE1BQU07S0FDTixDQUFDLENBQUM7QUFDSixDQUFDLENBQUMsQ0FDRCIsInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogQ3JlYXRlZCBieSB1c2VyIG9uIDIwMTkvNC8xNC5cbiAqL1xuXG5pbXBvcnQgdHh0U3BsaXQsIHsgYXV0b0ZpbGUsIElPcHRpb25zUmVxdWlyZWRVc2VyIH0gZnJvbSAnQG5vZGUtbm92ZWwvdHh0LXNwbGl0JztcbmltcG9ydCBmc0ljb252LCB7IHRyaW1GaWxlbmFtZSB9IGZyb20gJ2ZzLWljb252JztcbmltcG9ydCBmcyA9IHJlcXVpcmUoJ2ZzLWV4dHJhJyk7XG5pbXBvcnQgcGF0aCA9IHJlcXVpcmUoJ3BhdGgnKTtcbmltcG9ydCBGYXN0R2xvYiA9IHJlcXVpcmUoJ0BibHVlbG92ZXJzL2Zhc3QtZ2xvYicpO1xuaW1wb3J0IEJsdWViaXJkID0gcmVxdWlyZSgnYmx1ZWJpcmQnKTtcbmltcG9ydCBjcm9zc1NwYXduID0gcmVxdWlyZSgnY3Jvc3Mtc3Bhd24tZXh0cmEnKTtcbmltcG9ydCB7IHNlYXJjaFJ1bGUgfSBmcm9tICcuLi9ydWxlL2xpYi9pbmRleCc7XG5pbXBvcnQgeyBydWxlX21hdGNoIH0gZnJvbSAnLi4vc2V0dGluZy5jb25mJztcblxuY29uc3QgX19yb290X3RvcCA9IHBhdGguam9pbihfX2Rpcm5hbWUsICcuLi8uLi8uLicpO1xuXG5jb25zdCBfX3Jvb3RfZGF0YSA9IHBhdGguam9pbihfX3Jvb3RfdG9wLCAnZGF0YScpO1xuXG5jb25zdCBfX3Jvb3Rfb3V0ID0gcGF0aC5qb2luKF9fcm9vdF9kYXRhLCAnb3V0L2Zyb20tNjc1NycpO1xuY29uc3QgX19yb290X2Rvd25sb2FkID0gcGF0aC5qb2luKF9fcm9vdF9kYXRhLCAnc3JjL2Zyb20tNjc1NycpO1xuXG5CbHVlYmlyZFxuXHQucmVzb2x2ZShGYXN0R2xvYi5hc3luYzxzdHJpbmc+KFtcblx0XHQnKi50eHQnLFxuXHRdLCB7XG5cdFx0Y3dkOiBfX3Jvb3RfZG93bmxvYWRcblx0fSkpXG4ubWFwU2VyaWVzKGFzeW5jIGZ1bmN0aW9uIChmaWxlKVxue1xuXHRsZXQgYmFzZW5hbWUgPSBwYXRoLmJhc2VuYW1lKGZpbGUsICcudHh0Jyk7XG5cdGxldCB7IHJ1bGVfbmFtZSwgcnVsZV9maWxlIH0gPSBzZWFyY2hSdWxlKGJhc2VuYW1lLCB7XG5cdFx0cnVsZV9tYXRjaCxcblx0fSk7XG5cblx0Y29uc29sZS5kZWJ1ZyhiYXNlbmFtZSwgcnVsZV9uYW1lKTtcblxuXHRsZXQgc2tpcDogYm9vbGVhbjtcblxuXHRsZXQgdHBsT3B0aW9uczogSU9wdGlvbnNSZXF1aXJlZFVzZXIgPSBhd2FpdCBpbXBvcnQocnVsZV9maWxlKVxuXHRcdC50aGVuKG0gPT4gbS5kZWZhdWx0KVxuXHRcdC5jYXRjaChlID0+IHtcblx0XHRcdGNvbnNvbGUud2FybihlLm1lc3NhZ2UpO1xuXHRcdFx0c2tpcCA9IHRydWU7XG5cdFx0fSlcblx0O1xuXG5cdGlmIChza2lwKVxuXHR7XG5cdFx0cmV0dXJuO1xuXHR9XG5cblx0bGV0IG91dERpciA9IHBhdGguam9pbihfX3Jvb3Rfb3V0LCBiYXNlbmFtZSk7XG5cdGxldCBmdWxscGF0aCA9IHBhdGguam9pbihfX3Jvb3RfZG93bmxvYWQsIGZpbGUpO1xuXG5cdGF3YWl0IGZzSWNvbnZcblx0XHQubG9hZEZpbGUoZnVsbHBhdGgsIHtcbi8vXHRcdFx0ZW5jb2Rpbmc6ICd1dGY4Jyxcblx0XHRcdGF1dG9EZWNvZGU6IHRydWUsXG5cdFx0fSlcblx0XHQudGhlbihkYXRhID0+IGZzSWNvbnYuc2F2ZUZpbGUoZnVsbHBhdGgsIGRhdGEpKVxuXHQ7XG5cblx0ZnMuZW1wdHlEaXJTeW5jKHBhdGguam9pbihvdXREaXIpKTtcblxuXHRhd2FpdCB0eHRTcGxpdC5hdXRvRmlsZShmdWxscGF0aCwge1xuXHRcdC4uLnRwbE9wdGlvbnMsXG5cdFx0b3V0RGlyLFxuXHR9KTtcbn0pXG47XG4iXX0=