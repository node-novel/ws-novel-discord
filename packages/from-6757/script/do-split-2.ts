/**
 * Created by user on 2019/4/14.
 */

import txtSplit, { IOptionsRequiredUser } from '@node-novel/txt-split';
import fsIconv from 'fs-iconv';
import { console } from '@node-novel/txt-split/lib/console';
import { RULE_DEFAULT, searchRule } from '../rule/lib/index';
import { rule_match } from '../setting.conf';
import fs = require('fs-extra');
import path = require('path');
import FastGlob = require('@bluelovers/fast-glob');
import Bluebird = require('bluebird');

const __root_top = path.join(__dirname, '../../..');

const __root_data = path.join(__root_top, 'data');

const folder_id = `from-other`;

const __root_out = path.join(__root_data, 'out', folder_id);
const __root_download = path.join(__root_data, 'src', folder_id);

Bluebird
	.resolve(FastGlob.async<string>([
		'*.txt',
	], {
		cwd: __root_download
	}))
.mapSeries(async function (file)
{
	let basename = path.basename(file, '.txt');

	let { rule_name = RULE_DEFAULT, rule_file } = searchRule(basename, {
		rule_match,
	});

	console.debug(basename, rule_name);

	let skip: boolean;

	let tplOptions: IOptionsRequiredUser = await import(rule_file)
		.then(m => m.default)
		.catch(e => {
			console.warn(e.message);
			skip = true;
		})
	;

	if (skip)
	{
		return;
	}

	let outDir = path.join(__root_out, basename);
	let fullpath = path.join(__root_download, file);

	await fsIconv
		.loadFile(fullpath, {
//			encoding: 'utf8',
			autoDecode: true,
		})
		.then(data => fsIconv.saveFile(fullpath, data))
	;

	fs.emptyDirSync(path.join(outDir));

	await txtSplit.autoFile(fullpath, {
		...tplOptions,
		outDir,
	});
})
;
