"use strict";
/**
 * Created by user on 2019/4/24.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const cross_spawn_extra_1 = require("cross-spawn-extra");
const path = require("path");
const __root_top = path.join(__dirname, '..');
const __root_data = path.join(__root_top, 'data');
const __root_out = path.join(__root_data, 'out/from-6757');
const __root_download = path.join(__root_data, 'src/from-6757');
(async () => {
    let msg = new Date().toLocaleDateString() + ' ' + new Date().toLocaleTimeString();
    await cross_spawn_extra_1.async('git', [
        'commit',
        '-a',
        '-m',
        msg
    ], {
        stdio: 'inherit',
        cwd: __root_download,
    });
    await cross_spawn_extra_1.async('git', [
        'commit',
        '-m',
        msg
    ], {
        stdio: 'inherit',
        cwd: __root_out,
    });
    await cross_spawn_extra_1.async('yarn', [
        'run',
        'git-prepush',
    ], {
        stdio: 'inherit',
        cwd: __root_top,
    });
    await cross_spawn_extra_1.async('git', [
        'commit',
        '-a',
        '-m',
        msg
    ], {
        stdio: 'inherit',
        cwd: __root_top,
    });
})();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ2l0LWNvbW1pdC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImdpdC1jb21taXQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBOztHQUVHOztBQUVILHlEQUF3RDtBQUd4RCw2QkFBOEI7QUFJOUIsTUFBTSxVQUFVLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLENBQUM7QUFFOUMsTUFBTSxXQUFXLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUUsTUFBTSxDQUFDLENBQUM7QUFFbEQsTUFBTSxVQUFVLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsZUFBZSxDQUFDLENBQUM7QUFDM0QsTUFBTSxlQUFlLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsZUFBZSxDQUFDLENBQUM7QUFFaEUsQ0FBQyxLQUFLLElBQUksRUFBRTtJQUVYLElBQUksR0FBRyxHQUFHLElBQUksSUFBSSxFQUFFLENBQUMsa0JBQWtCLEVBQUUsR0FBRyxHQUFHLEdBQUcsSUFBSSxJQUFJLEVBQUUsQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO0lBRWxGLE1BQU0seUJBQVUsQ0FBQyxLQUFLLEVBQUM7UUFDdEIsUUFBUTtRQUNSLElBQUk7UUFDSixJQUFJO1FBQ0osR0FBRztLQUNILEVBQUU7UUFDRixLQUFLLEVBQUUsU0FBUztRQUNoQixHQUFHLEVBQUUsZUFBZTtLQUNwQixDQUFDLENBQUM7SUFFSCxNQUFNLHlCQUFVLENBQUMsS0FBSyxFQUFDO1FBQ3RCLFFBQVE7UUFDUixJQUFJO1FBQ0osR0FBRztLQUNILEVBQUU7UUFDRixLQUFLLEVBQUUsU0FBUztRQUNoQixHQUFHLEVBQUUsVUFBVTtLQUNmLENBQUMsQ0FBQztJQUVILE1BQU0seUJBQVUsQ0FBQyxNQUFNLEVBQUM7UUFDdkIsS0FBSztRQUNMLGFBQWE7S0FDYixFQUFFO1FBQ0YsS0FBSyxFQUFFLFNBQVM7UUFDaEIsR0FBRyxFQUFFLFVBQVU7S0FDZixDQUFDLENBQUM7SUFFSCxNQUFNLHlCQUFVLENBQUMsS0FBSyxFQUFDO1FBQ3RCLFFBQVE7UUFDUixJQUFJO1FBQ0osSUFBSTtRQUNKLEdBQUc7S0FDSCxFQUFFO1FBQ0YsS0FBSyxFQUFFLFNBQVM7UUFDaEIsR0FBRyxFQUFFLFVBQVU7S0FDZixDQUFDLENBQUM7QUFFSixDQUFDLENBQUMsRUFBRSxDQUFDIiwic291cmNlc0NvbnRlbnQiOlsiLyoqXG4gKiBDcmVhdGVkIGJ5IHVzZXIgb24gMjAxOS80LzI0LlxuICovXG5cbmltcG9ydCB7IGFzeW5jIGFzIGNyb3NzU3Bhd24gfSBmcm9tICdjcm9zcy1zcGF3bi1leHRyYSc7XG5pbXBvcnQgeyBTcGF3bk9wdGlvbnMsIFNwYXduU3luY09wdGlvbnMsIFNwYXduU3luY09wdGlvbnNXaXRoQnVmZmVyRW5jb2RpbmcsIFNwYXduU3luY09wdGlvbnNXaXRoU3RyaW5nRW5jb2RpbmcgfSBmcm9tIFwiY3Jvc3Mtc3Bhd24tZXh0cmEvdHlwZVwiO1xuaW1wb3J0IGZzID0gcmVxdWlyZSgnZnMtZXh0cmEnKTtcbmltcG9ydCBwYXRoID0gcmVxdWlyZSgncGF0aCcpO1xuaW1wb3J0IEZhc3RHbG9iID0gcmVxdWlyZSgnZmFzdC1nbG9iJyk7XG5pbXBvcnQgQmx1ZWJpcmQgPSByZXF1aXJlKCdibHVlYmlyZCcpO1xuXG5jb25zdCBfX3Jvb3RfdG9wID0gcGF0aC5qb2luKF9fZGlybmFtZSwgJy4uJyk7XG5cbmNvbnN0IF9fcm9vdF9kYXRhID0gcGF0aC5qb2luKF9fcm9vdF90b3AsICdkYXRhJyk7XG5cbmNvbnN0IF9fcm9vdF9vdXQgPSBwYXRoLmpvaW4oX19yb290X2RhdGEsICdvdXQvZnJvbS02NzU3Jyk7XG5jb25zdCBfX3Jvb3RfZG93bmxvYWQgPSBwYXRoLmpvaW4oX19yb290X2RhdGEsICdzcmMvZnJvbS02NzU3Jyk7XG5cbihhc3luYyAoKSA9PiB7XG5cblx0bGV0IG1zZyA9IG5ldyBEYXRlKCkudG9Mb2NhbGVEYXRlU3RyaW5nKCkgKyAnICcgKyBuZXcgRGF0ZSgpLnRvTG9jYWxlVGltZVN0cmluZygpO1xuXG5cdGF3YWl0IGNyb3NzU3Bhd24oJ2dpdCcsW1xuXHRcdCdjb21taXQnLFxuXHRcdCctYScsXG5cdFx0Jy1tJyxcblx0XHRtc2dcblx0XSwge1xuXHRcdHN0ZGlvOiAnaW5oZXJpdCcsXG5cdFx0Y3dkOiBfX3Jvb3RfZG93bmxvYWQsXG5cdH0pO1xuXG5cdGF3YWl0IGNyb3NzU3Bhd24oJ2dpdCcsW1xuXHRcdCdjb21taXQnLFxuXHRcdCctbScsXG5cdFx0bXNnXG5cdF0sIHtcblx0XHRzdGRpbzogJ2luaGVyaXQnLFxuXHRcdGN3ZDogX19yb290X291dCxcblx0fSk7XG5cblx0YXdhaXQgY3Jvc3NTcGF3bigneWFybicsW1xuXHRcdCdydW4nLFxuXHRcdCdnaXQtcHJlcHVzaCcsXG5cdF0sIHtcblx0XHRzdGRpbzogJ2luaGVyaXQnLFxuXHRcdGN3ZDogX19yb290X3RvcCxcblx0fSk7XG5cblx0YXdhaXQgY3Jvc3NTcGF3bignZ2l0JyxbXG5cdFx0J2NvbW1pdCcsXG5cdFx0Jy1hJyxcblx0XHQnLW0nLFxuXHRcdG1zZ1xuXHRdLCB7XG5cdFx0c3RkaW86ICdpbmhlcml0Jyxcblx0XHRjd2Q6IF9fcm9vdF90b3AsXG5cdH0pO1xuXG59KSgpO1xuIl19