/**
 * Created by user on 2019/4/24.
 */

import { async as crossSpawn } from 'cross-spawn-extra';
import { SpawnOptions, SpawnSyncOptions, SpawnSyncOptionsWithBufferEncoding, SpawnSyncOptionsWithStringEncoding } from "cross-spawn-extra/type";
import fs = require('fs-extra');
import path = require('path');
import FastGlob = require('fast-glob');
import Bluebird = require('bluebird');

const __root_top = path.join(__dirname, '..');

const __root_data = path.join(__root_top, 'data');

const __root_out = path.join(__root_data, 'out/from-6757');
const __root_download = path.join(__root_data, 'src/from-6757');

(async () => {

	let msg = new Date().toLocaleDateString() + ' ' + new Date().toLocaleTimeString();

	await crossSpawn('git',[
		'commit',
		'-a',
		'-m',
		msg
	], {
		stdio: 'inherit',
		cwd: __root_download,
	});

	await crossSpawn('git',[
		'commit',
		'-m',
		msg
	], {
		stdio: 'inherit',
		cwd: __root_out,
	});

	await crossSpawn('yarn',[
		'run',
		'git-prepush',
	], {
		stdio: 'inherit',
		cwd: __root_top,
	});

	await crossSpawn('git',[
		'commit',
		'-a',
		'-m',
		msg
	], {
		stdio: 'inherit',
		cwd: __root_top,
	});

})();
