# README

> 切割腳本放置於 [這裡](packages/from-6757)

## 安裝

> 請自行安裝 ts-node typescript@next yarn

```
npm i -g ts-node typescript@next yarn
```

> 初次安裝或者每次想更新模組時

```
yarn run do-init
```

## 環境

> txt 檔案放置於 [data](data)

## 使用

> 自動 commit 檔案變化 ([out](data/out) 資料夾除外，此資料夾底下所有更動都需要自行 git add 內，包含已經存在於 git 內但內容有變化的檔案)

```
npm run git-commit
```
